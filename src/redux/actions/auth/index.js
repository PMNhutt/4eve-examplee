// ** UseJWT import to get config
import useJwt from "../../../auth/jwt/useJwt"

const config = useJwt.jwtConfig

// ** Handle User Login
export const handleLogin = data => {
  return dispatch => {
    dispatch({
      type: 'LOGIN',
      data,
      config,
      [config.storageTokenKeyName]: data[config.storageTokenKeyName],
      [config.storageRefreshTokenKeyName]: data[config.storageRefreshTokenKeyName]
    })
    // ** Add to user, accessToken & refreshToken to localStorage
    localStorage.setItem('userData', JSON.stringify(data.user))
    localStorage.setItem(config.storageTokenKeyName, data.token)
    localStorage.setItem(config.storageRefreshTokenKeyName, data.refresh_token)
  }
}

// ** Handle User Logout
export const handleLogout = () => {
  return dispatch => {
    dispatch({ type: 'LOGOUT', [config.storageTokenKeyName]: null, [config.storageRefreshTokenKeyName]: null })

    // ** Remove user, accessToken & refreshToken from localStorage
    localStorage.removeItem('userData')
    localStorage.removeItem(config.storageTokenKeyName)
    localStorage.removeItem(config.storageRefreshTokenKeyName)
  }
}
export const loadUserData = () => {
    const data = JSON.parse(localStorage.getItem('userData'))
    return dispatch => {
        dispatch({ type: 'LOAD_USER_DATA', data: data})
    }
}
