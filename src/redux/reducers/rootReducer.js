// ** Redux Imports
import { combineReducers } from 'redux'

// ** Reducers Imports
import authReducer from './auth'
import navbar from './navbar'
import layout from './layout'
import permissions from "../../pages/users/permission/store/reducer"
import users from "../../pages/users/user/store/reducer"
import profiles from "../../pages/users/profile/store/reducer"
import groups from "../../pages/users/user-group/store/reducer"
import crawler from "../../pages/crawler-account/store/reducer"
import crawlerPage from "../../pages/crawler-page/store/reducer"
import agents from "../../pages/agent/store/reducer"
import telegram from "../../pages/telegram-group/store/reducer"
import company from "../../pages/company/store/reducer"
import telegramToken from "../../pages/telegram-token/store/reducer"
import configNotify from "../../pages/config-notify/store/reducer"
import agentNoDetails from "../../pages/account-warning/store/reducer"
import overview from "../../pages/overview/store/reducer"
import configLog from "../../pages/config-log/store/reducer"
import example from '../../pages/example/store/reducer'

const rootReducer = combineReducers({
  crawlerPage,
  crawler,
  authReducer,
  navbar,
  layout,
  permissions,
  users,
  groups,
  profiles,
  agents,
  telegram,
  company,
  telegramToken,
  configNotify,
  agentNoDetails,
  overview,
  configLog,
  example
})

export default rootReducer
