// ** Navigation sections imports
import settings from "./settings"
import overview from "./dashboards"
import crawler from "./crawler-account"
import agent from "./agent"
import accountWarning from "./account-warning"
import company from "./company"
import telegramToken from "./telegram-token"
import example from "./example"

// ** Merge & Export
export default [...overview, ...company, ...telegramToken, ...crawler, ...agent, ...accountWarning, ...settings, ...example]
