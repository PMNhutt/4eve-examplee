import {List} from 'react-feather'
import React from "react"

export default [
    {
        id: 'agent',
        title: 'Tree Account',
        icon: <List />,
        action: 'view',
        resource: 'agent',
        navLink: '/agent/list'
    }
]
