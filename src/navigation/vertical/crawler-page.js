import {Layout, User} from 'react-feather'
import React from "react"

export default [
    {
        id: 'crawler-page',
        title: 'Thông tin thu thập',
        icon: <Layout />,
        action: 'view',
        resource: 'crawler-page',
        navLink: '/crawler-page/list'
    }
]
