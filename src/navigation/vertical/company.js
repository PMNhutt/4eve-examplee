import {Briefcase} from 'react-feather'
import React from "react"

export default [
    {
        id: 'company',
        title: 'Company',
        icon: <Briefcase />,
        action: 'view',
        resource: 'company',
        navLink: '/company/list'
    }
]
