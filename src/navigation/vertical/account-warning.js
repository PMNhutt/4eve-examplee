import {Info} from "react-feather"
import React from "react"


export default [
    {
        id: 'account-warning',
        title: 'List Account',
        icon: <Info />,
        action: 'view',
        resource: 'account-warning',
        navLink: '/account-warning/list'
    }
]