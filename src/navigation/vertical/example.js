import {Aperture} from 'react-feather'
import React from "react"

export default [
    {
        id: 'example',
        title: 'Example',
        icon: <Aperture />,
        action: 'view',
        resource: 'example',
        navLink: '/example/list'
    }
]