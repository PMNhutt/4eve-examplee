import {Layout, Circle, User, Database, Server, Twitch, Trello} from 'react-feather'
import React from "react"

export default [
    {
        id: 'websites',
        title: 'Setting',
        icon: <Layout />,
        action: 'view',
        resource: 'setting_website',
        children: [
            {
                id: 'user',
                title: 'List user',
                icon: <User />,
                action: 'view',
                resource: 'user_manager',
                navLink: '/user/list'
            },
            {
                id: 'auth',
                title: 'Permission',
                icon: <Circle />,
                navLink: '/user/permission',
                action: 'view',
                resource: 'permission_manager'
            },
            {
                id: 'telegram-token',
                title: 'List token',
                icon: <Database />,
                action: 'view',
                resource: 'telegram_token',
                navLink: '/telegram-token/list'
            },
            {
                id: 'config-notify',
                title: 'List config',
                icon: <Server />,
                action: 'view',
                resource: 'config',
                navLink: '/config-notify/list'
            },
            {
                id: 'config-log',
                title: 'List Log',
                icon: <Trello />,
                action: 'view',
                resource: 'log',
                navLink: '/config-log/list'
            }
        ]
    }
]
