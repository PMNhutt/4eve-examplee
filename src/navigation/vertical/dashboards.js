import {Home} from 'react-feather'

export default [
    {
        id: 'overview',
        title: 'Dashboard',
        icon: <Home />,
        action: 'view',
        resource: 'dashboard',
        navLink: '/dashboard'
    }
]