import {User} from 'react-feather'
import React from "react"

export default [
    {
        id: 'crawler_account',
        title: 'Scan Account',
        icon: <User />,
        action: 'view',
        resource: 'crawler_account',
        navLink: '/crawler-account/list'
    }
]
