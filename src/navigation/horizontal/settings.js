import {
    Layout, Settings, User
} from 'react-feather'
import React from "react"

export default [
    {
        id: 'websites',
        title: 'Cài đặt',
        icon: <Layout />,
        resource: 'setting',
        children: [
            {
                id: 'user',
                title: 'Danh sách thành viên',
                icon: <User />,
                action: 'view',
                resource: 'user',
                navLink: '/user/list'
            },
            {
                id: 'user',
                title: 'Danh sách nhóm người dùng',
                icon: <User />,
                navLink: '/user-group/list',
                action: 'view',
                resource: 'user_manager'
            },
            {
                id: 'auth',
                title: 'Phân quyền',
                icon: <Settings />,
                navLink: '/user/permission',
                action: 'view',
                resource: 'permission_manager'
            }
        ]
    }
]
