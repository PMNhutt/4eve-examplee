import {Layout, User} from 'react-feather'
import React from "react"

export default [
    {
        id: 'action-crawler',
        title: 'Thực hiện thu thập',
        icon: <Layout />,
        action: 'view',
        resource: 'action-crawler',
        navLink: '/action-crawler/list'
    }
]
