import {List} from 'react-feather'
import React from "react"

export default [
    {
        id: 'agent',
        title: 'Danh sách đại lý',
        icon: <List />,
        action: 'view',
        resource: 'agent',
        navLink: '/agent/list'
    }
]
