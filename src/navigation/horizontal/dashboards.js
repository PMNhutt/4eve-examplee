import {Home} from 'react-feather'

export default [
    {
        id: 'dashboards',
        title: 'Tổng quan',
        icon: <Home />,
        action: 'view',
        resource: 'dashboard',
        navLink: '/dashboard'
    }
]