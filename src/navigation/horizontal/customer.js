import {Command, Layout, User, CheckCircle, Circle} from 'react-feather'

export default [
  {
    id: 'customers',
    title: 'Khách hàng',
    icon: <User size={20}/>,
    action: 'view',
    resource: 'customer',
    children: [
      {
        id: 'customer',
        title: 'Quản lý khách hàng',
        icon: <User size={20}/>,
        navLink: '/customer/list',
        action: 'view',
        resource: 'customer_manager'
      },
      {
        id: 'customer',
        title: 'Khách hàng đăng kí trader',
        icon: <User size={20} />,
        navLink: '/customer/trader-register/list',
        action: 'view',
        resource: 'customer_manager'
      }
    ]
  }
]
