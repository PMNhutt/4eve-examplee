// ** Navigation sections imports
import settings from "./settings"
import dashboards from "./dashboards"
import crawler from './crawler-account'
import agent from './agent'
import telegramGroup from "./telegram-group"
import telegramToken from "./telegram-token"
import configNotify from "./config-notify"


// ** Merge & Export
export default [...dashboards, ...telegramToken, ...configNotify, ...telegramGroup, ...crawler, ...agent, ...settings]
