import {Database} from 'react-feather'
import React from "react"

export default [
    {
        id: 'telegram-token',
        title: 'Danh sách token',
        icon: <Database />,
        action: 'view',
        resource: 'telegram_token',
        navLink: '/telegram-token/list'
    }
]
