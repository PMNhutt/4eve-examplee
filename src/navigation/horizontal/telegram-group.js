import {Twitch} from 'react-feather'
import React from "react"

export default [
    {
        id: 'telegram-group',
        title: 'Nhóm telegram',
        icon: <Twitch />,
        action: 'view',
        resource: 'telegram_group',
        navLink: '/telegram-group/list'
    }
]
