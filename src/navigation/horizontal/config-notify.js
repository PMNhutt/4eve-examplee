import {Server} from 'react-feather'
import React from "react"

export default [
    {
        id: 'config-notify',
        title: 'Danh sách config',
        icon: <Server />,
        action: 'view',
        resource: 'config',
        navLink: '/config-notify/list'
    }
]
