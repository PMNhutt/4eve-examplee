import {User} from 'react-feather'
import React from "react"

export default [
    {
        id: 'crawler_account',
        title: 'Danh sách tài khoản',
        icon: <User />,
        action: 'view',
        resource: 'crawler-account',
        navLink: '/crawler-account/list'
    }
]
