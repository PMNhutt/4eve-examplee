export const PRIORITY = [
    {
        id: 1,
        value: 0.41
    },
    {
        id: 2,
        value: 0.51
    },
    {
        id: 3,
        value: 0.64
    },
    {
        id: 4,
        value: 0.80
    },
    {
        id: 5,
        value: 1.0
    }
]