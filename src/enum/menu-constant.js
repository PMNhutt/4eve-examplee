export const  MENU_ENUM = {
  product_desktop: 'product_desktop',
  product_mobile: 'product_mobile',
  gallery: 'gallery',
  wardrobe: 'wardrobe',
  promotion: 'promotion',
  menu_common_desktop: 'menu_common_desktop',
  menu_common_mobile: 'menu_common_mobile'
}

