export const PURCHASE_ORDER_STATUS_ENUM = {
    active: 'active',
    cancelled: 'cancelled',
    completed: 'completed',
    draft: 'draft'
}