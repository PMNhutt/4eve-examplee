export const PROMOTION_CONSTANT = {
    buy_with_shock_deal_gift: 'buy_with_shock_deal_gift',
    buy_with_shock_deal_discount: 'buy_with_shock_deal_discount',
    discount: 'discount',
    voucher: 'voucher',
    combo_promotion: 'combo_promotion',

    sell_items: 'sell_items',
    gift_items: 'gift_items'
}
