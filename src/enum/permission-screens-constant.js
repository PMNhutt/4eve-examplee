export const  PERMISSION_SCREEN_ENUM = {
  proxy_manager: 'proxy_manager',
  account_crawler_manager: 'account_crawler_manager',
  permission_manager: 'permission_manager',
  user_manager: 'user_manager',
  report_crawler: 'report_crawler',
  action_crawler: 'action_crawler',
  telegram_group: 'telegram_group',
  company: 'company',
  crawler_account: 'crawler_account',
  agent: 'agent'
}
