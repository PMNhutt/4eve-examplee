export const ORDER_STATUS_ENUM = {
    pending: 'pending',
    finalized: 'finalized',
    cancelled: 'cancelled',
    canceled: 'canceled',
    completed: 'completed',
    successful: 'successful',
    draft: 'draft',
    waiting_confirmation: 'waiting_confirmation',
    waiting_payment: 'waiting_payment'
}

export const ORDER_FULFILLMENT_STATUS_ENUM = {
    unshipped: 'unshipped',
    shipped: 'shipped'
}

export const ORDER_PACKED_STATUS_ENUM = {
    unpacked: 'unpacked',
    packed: 'packed'
}

export const ORDER_PAYMENT_STATUS_ENUM = {
    all: '',
    unpaid: 'unpaid',
    paid: 'paid'
}

export const ORDER_RETURN_STATUS_ENUM = {
    unreturned: 'unreturned',
    returned: 'returned'
}

export const ORDER_RECEIVED_STATUS_ENUM = {
    unreceived: 'unreceived',
    received: 'received',
    partial: 'partial'
}
