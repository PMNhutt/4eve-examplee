import {
    ORDER_FULFILLMENT_STATUS_ENUM,
    ORDER_PACKED_STATUS_ENUM,
    ORDER_RETURN_STATUS_ENUM,
    ORDER_STATUS_ENUM
} from "../enum/order-status-constant"

export const getOrderStatus = (order) => {
    const status = order?.status
    const return_status = order?.return_status
    const request_return_status = order?.request_return_status
    let statusLabel = ''
    let statusValue = ''
    switch (status) {
        case ORDER_STATUS_ENUM.finalized:
            statusLabel = 'Đang thực hiện'
            statusValue = 'finalized'
            break
        case ORDER_STATUS_ENUM.completed:
            if (request_return_status) {
                if (request_return_status === 'return') {
                    statusLabel = 'Yêu cầu đổi trả'
                    statusValue = 'return'
                } else if (request_return_status === 'refund') {
                    statusLabel = 'Yêu cầu hoàn trả'
                    statusValue = 'refund'
                }
                if (return_status === ORDER_RETURN_STATUS_ENUM.returned) {
                    statusLabel = 'Đổi trả/Hoàn trả'
                    statusValue = 'returned'
                }
            } else {
                if (return_status === ORDER_RETURN_STATUS_ENUM.unreturned) {
                    statusLabel = 'Hoàn thành'
                    statusValue = 'completed'
                } else {
                    statusLabel = 'Đổi trả/Hoàn trả'
                    statusValue = 'returned'
                }
            }
            break
        case ORDER_STATUS_ENUM.cancelled:
            statusLabel = 'Đã hủy'
            statusValue = 'cancelled'
            break
        case ORDER_STATUS_ENUM.waiting_confirmation:
            statusLabel = 'Chưa xác nhận'
            statusValue = 'finalized'
            break
        case ORDER_STATUS_ENUM.waiting_payment:
            statusLabel = 'Đợi thanh toán'
            statusValue = 'finalized'
            break
        case ORDER_STATUS_ENUM.successful:
            statusLabel = 'Hoàn thành'
            statusValue = 'completed'
            break
        case ORDER_STATUS_ENUM.draft:
            statusLabel = 'Đặt hàng'
            statusValue = 'draft'
            break
    }

    return {label: statusLabel, value: statusValue}
}

export const getOrderTrackingStatus = (order) => {
    const status = order?.status
    const return_status = order?.return_status
    const packed_status = order?.packed_status
    const fulfillment_status = order?.fulfillment_status
    const request_return_status = order?.request_return_status
    let activeProgress = -1
    let lastStatus = ''

    switch (status) {
        case ORDER_STATUS_ENUM.draft:
            lastStatus = 'Đặt hàng'
            activeProgress = 0
            break
        case ORDER_STATUS_ENUM.finalized:
            if (fulfillment_status === ORDER_FULFILLMENT_STATUS_ENUM.shipped) {
                lastStatus = 'Đang giao'
                activeProgress = 3
            } else {
                if (packed_status === ORDER_PACKED_STATUS_ENUM.packed) {
                    lastStatus = 'Đã đóng gói'
                    activeProgress = 2
                } else {
                    lastStatus = 'Chưa đóng gói'
                    activeProgress = 1
                }
            }
            break
        case ORDER_STATUS_ENUM.completed:
            activeProgress = 4
            if (request_return_status) {
                if (request_return_status === 'return') {
                    lastStatus = 'Yêu cầu đổi trả'
                } else if (request_return_status === 'refund') {
                    lastStatus = 'Yêu cầu hoàn trả'
                }
            } else {
                if (return_status === ORDER_RETURN_STATUS_ENUM.unreturned) {
                    lastStatus = 'Hoàn thành'
                } else {
                    lastStatus = 'Đổi trả/Hoàn trả'
                }
            }
            break
        case ORDER_STATUS_ENUM.cancelled:
            lastStatus = 'Đã hủy'
            activeProgress = 1
            break
        case ORDER_STATUS_ENUM.waiting_confirmation:
            lastStatus = 'Chưa xác nhận'
            activeProgress = 1
            break
        case ORDER_STATUS_ENUM.waiting_payment:
            lastStatus = 'Đợi thanh toán'
            activeProgress = 1
            break
        case ORDER_STATUS_ENUM.successful:
            activeProgress = 4
            lastStatus = 'Đã nhận hàng'
            break
    }
    return {lastStatus, activeProgress}
}
