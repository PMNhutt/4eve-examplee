export const replaceUrlMedia = (image_url) => {
    if (image_url === undefined || image_url === null) {
        return ''
    }
    let image = image_url
    if (image_url?.includes('feaer.sgp1.digitaloceanspaces.com')) {
        image = image_url?.replace('feaer.sgp1.digitaloceanspaces.com', 'hcm01.vstorage.vngcloud.vn/v1/AUTH_f9b5296a77c5472c8a7ebd72b9ee863c/feaer')
    } else if (!image_url.includes('http')
        && !image_url?.includes('hcm01.vstorage.vngcloud.vn/v1/AUTH_f9b5296a77c5472c8a7ebd72b9ee863c/feaer')) {
        image = `https://hcm01.vstorage.vngcloud.vn/v1/AUTH_f9b5296a77c5472c8a7ebd72b9ee863c/feaer/${image_url}`
    }
    return image
}