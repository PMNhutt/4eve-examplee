import {PURCHASE_ORDER_STATUS_ENUM} from "../enum/purchase-order-status"

export const getPurchaseStatus = (purchaseGroup) => {
    let status = 0
    switch (purchaseGroup?.status) {
        case PURCHASE_ORDER_STATUS_ENUM.active:
            status = 1
            if (purchaseGroup?.purchases?.length > 0) {
                status = 2
                let total_price = 0
                let total_payment = 0
                purchaseGroup?.purchases?.forEach(purchase => {
                    if (purchase?.purchase_receipts?.length > 0 && purchase?.purchase_receipts?.[0]?.line_items?.length > 0) {
                        purchase?.purchase_receipts?.[0]?.line_items?.forEach(line_item => {
                            if (!isNaN(parseInt(line_item?.quantity))) {
                                total_price += (line_item?.price * parseInt(line_item?.quantity))
                            }
                        })
                    }
                    if (purchase?.transactions?.length > 0) {
                        purchase?.transactions?.forEach(transaction => {
                            total_payment += transaction?.amount
                        })
                    }
                })
                if (total_price === total_payment) {
                    status = 3
                }
            }
            break
        case PURCHASE_ORDER_STATUS_ENUM.completed:
            status = 4
            break
        default:
            status = 0
            break
    }
    return status
}