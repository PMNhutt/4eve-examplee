import currencyFormat from "./CurrencyFormat"

export const getTotalPriceProductOrder = (products) => {
    let totalPrice = 0
    if (products && products?.length > 0) {
        for (const product of products) {
            let priceDiscount = 0
            if (product?.discount_items?.length > 0) {
                for (const discount_item of product?.discount_items) {
                    if (discount_item?.amount > 0) {
                        priceDiscount += discount_item?.amount
                    } else {
                        if (discount_item?.value > 0) {
                            priceDiscount += discount_item?.value
                        } else {
                            if (discount_item?.rate > 0) {
                                priceDiscount += Math.round((discount_item?.rate * 100) / product?.price)
                            }
                        }
                    }
                }
            }
            totalPrice += ((product?.price - priceDiscount) * product?.quantity)
        }
    }

    return currencyFormat(totalPrice)
}