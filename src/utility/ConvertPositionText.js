export const ConvertPositionText = (value) => {
    switch (value) {
        case 1:
            return 'center'
        case 2:
            return 'bottomCenter'
        case 3:
            return 'bottomLeft'
        case 4:
            return 'bottomRight'
        default:
            break
    }
}
