
export const promotionStatus = (row) => {
    const status = {}
    if (row?.status_flag === 1) {
        if (row?.status) {
            if (new Date(row?.date_start) > new Date()) {
                status.value = 'upcoming'
                status.label = 'Sắp diễn ra'
            } else {
                if (new Date(row?.date_end) >= new Date()) {
                    status.value = 'happening'
                    status.label = 'Đang diễn ra'
                } else {
                    status.value = 'finished'
                    status.label = 'Đã kết thúc'
                }
            }
        } else {
            status.value = 'inactive'
            status.label = 'Ngưng hoạt động'
        }
    } else {
        status.value = 'delete'
        status.label = 'Đã xóa'
    }
    return status
}