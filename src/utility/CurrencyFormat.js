const currencyFormat = (value) => {
    return new Intl.NumberFormat('vi-VN').format(value)
}
export default currencyFormat