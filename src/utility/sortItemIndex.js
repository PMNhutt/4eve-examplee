export const sortItemIndex = (newIndex, oldIndex, items) => {
    if (newIndex > oldIndex) {
        for (let index = newIndex; index > oldIndex; index--) {
            items[index].position = items[index].position - 1
        }
        items[oldIndex].position = items[newIndex].position + 1
    } else {
        for (let index = newIndex; index < oldIndex; index++) {
            items[index].position = items[index].position + 1
        }
        items[oldIndex].position = items[newIndex].position - 1
    }
    return items
}