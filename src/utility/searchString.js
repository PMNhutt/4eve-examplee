export const searchString = (fullString, searchString) => {
    return fullString?.trim()?.toUpperCase()?.search(searchString?.trim()?.toUpperCase()) > -1
}