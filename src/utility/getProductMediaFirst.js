export const getProductMediaFirst = (product_media) => {
    return product_media?.filter(item => item?.media_type === 'image')?.sort((a, b) => a.position > b.position ? 1 : -1)?.[0]
}