import { lazy } from 'react'

const TelegramGroup = [
    {
        path: '/telegram-group/list',
        component: lazy(() => import('../../pages/telegram-group/list')),
        meta: {
            action: 'view',
            resource: 'telegram_group'
        }
    }
]

export default TelegramGroup
