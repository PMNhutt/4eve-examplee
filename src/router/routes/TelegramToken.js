import { lazy } from 'react'

const TelegramToken = [
    {
        path: '/telegram-token/list',
        component: lazy(() => import('../../pages/telegram-token/list')),
        meta: {
            action: 'view',
            resource: 'telegram_token'
        }
    }
]

export default TelegramToken
