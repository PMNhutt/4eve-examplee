import { lazy } from 'react'

const AccountWarning = [
    {
        path: '/account-warning/list',
        component: lazy(() => import('../../pages/account-warning/list')),
        meta: {
            action: 'view',
            resource: 'agent'
        }
    }
]

export default AccountWarning