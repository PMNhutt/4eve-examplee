import { lazy } from 'react'

const AuthRoutes = [
    {
        path: '/login',
        component: lazy(() => import('../../pages/auth/login/Login')),
        layout: 'BlankLayout',
        meta: {
            authRoute: true
        }
    },
    {
        path: '/forgot-password',
        component: lazy(() => import('../../pages/auth/forgot/ForgotPassword')),
        layout: 'BlankLayout',
        meta: {
            authRoute: true
        }
    },
    {
        path: '/reset-password',
        component: lazy(() => import('../../pages/auth/reset/ResetPassword')),
        layout: 'BlankLayout',
        meta: {
            authRoute: true
        }
    },
    {
        path: '/change-password',
        component: lazy(() => import('../../pages/auth/change-password/ChangePassword')),
        layout: 'BlankLayout',
        meta: {
            authRoute: true
        }
    },
    {
        path: '/profile',
        component: lazy(() => import('../../pages/users/profile/view')),
        meta: {
            action: 'view',
            resource: 'auth'
        }
    },
    {
        path: '/pages/faq',
        component: lazy(() => import('../../pages/faq'))
    },
    {
        path: '/misc/coming-soon',
        component: lazy(() => import('../../pages/misc/ComingSoon')),
        layout: 'BlankLayout',
        meta: {
            publicRoute: true
        }
    },
    {
        path: '/misc/not-authorized',
        component: lazy(() => import('../../pages/misc/NotAuthorized')),
        layout: 'BlankLayout',
        meta: {
            publicRoute: true
        }
    },
    {
        path: '/misc/maintenance',
        component: lazy(() => import('../../pages/misc/Maintenance')),
        layout: 'BlankLayout',
        meta: {
            publicRoute: true
        }
    },
    {
        path: '/misc/error',
        component: lazy(() => import('../../pages/misc/Error')),
        layout: 'BlankLayout',
        meta: {
            publicRoute: true
        }
    }
]

export default AuthRoutes
