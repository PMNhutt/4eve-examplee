import { lazy } from 'react'

const ConfigLog = [
    {
        path: '/config-log/list',
        component: lazy(() => import('../../pages/config-log/list')),
        meta: {
            action: 'view',
            resource: 'config_log'
        }
    }
]

export default ConfigLog
