import { lazy } from 'react'

const ExampleList = [
    { 
        path: '/example/list', 
        component: lazy(() => import('../../pages/example/list')),
        meta: {
            action: 'view', 
            resource: 'example'
        }
    }
]

export default ExampleList