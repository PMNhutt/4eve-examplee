import { lazy } from 'react'

const Company = [
    {
        path: '/company/list',
        component: lazy(() => import('../../pages/company/list')),
        meta: {
            action: 'view',
            resource: 'company'
        }
    }
]

export default Company
