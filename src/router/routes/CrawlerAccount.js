import { lazy } from 'react'

const CrawlerAccount = [
    {
        path: '/crawler-account/list',
        component: lazy(() => import('../../pages/crawler-account/list')),
        meta: {
            action: 'view',
            resource: 'crawler_account'
        }
    }
]

export default CrawlerAccount
