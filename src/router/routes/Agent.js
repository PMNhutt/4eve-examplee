import { lazy } from 'react'

const Agent = [
    {
        path: '/agent/list',
        component: lazy(() => import('../../pages/agent/list')),
        meta: {
            action: 'view',
            resource: 'agent'
        }
    },
    // {
    //     path: '/agent/detail/:id',
    //     component: lazy(() => import('../../pages/agent/detail')),
    //     meta: {
    //         action: 'view',
    //         resource: 'agent'
    //     }
    // },
    {
        path: '/agent/edit/:id',
        component: lazy(() => import('../../pages/agent/edit')),
        meta: {
            action: 'edit',
            resource: 'agent'
        }
    }
]

export default Agent
