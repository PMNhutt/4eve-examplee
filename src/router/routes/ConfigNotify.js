import { lazy } from 'react'

const ConfigNotify = [
    {
        path: '/config-notify/list',
        component: lazy(() => import('../../pages/config-notify/list')),
        meta: {
            action: 'view',
            resource: 'config_notify'
        }
    }
]

export default ConfigNotify
