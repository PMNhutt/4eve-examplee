import { lazy } from 'react'

const Users = [
  {
    path: '/user-group/list',
    component: lazy(() => import('../../pages/users/user-group/list')),
    meta: {
      action: 'view',
      resource: 'user_manager'
    }
  },
  {
    path: '/user-group/detail/:id',
    component: lazy(() => import('../../pages/users/user-group/view')),
    meta: {
      action: 'view',
      resource: 'user_manager',
      navLink: '/user/detail'
    }
  },
  {
    path: '/user/list',
    component: lazy(() => import('../../pages/users/user/list')),
    meta: {
      action: 'view',
      resource: 'user_manager'
    }
  },
  {
    path: '/user/edit/:id',
    component: lazy(() => import('../../pages/users/user/edit')),
    meta: {
      action: 'edit',
      resource: 'user_manager',
      navLink: '/user/edit'
    }
  },
  {
    path: '/user/detail/:id',
    component: lazy(() => import('../../pages/users/user/view')),
    meta: {
      action: 'view',
      resource: 'user_manager',
      navLink: '/user/detail'
    }
  },
  {
    path: '/user/permission',
    component: lazy(() => import('../../pages/users/permission/list')),
    meta: {
      action: 'view',
      resource: 'permission_manager',
      navLink: '/user/permission'
    }
  },
  {
    path: '/permission/create',
    component: lazy(() => import('../../pages/users/permission/create')),
    meta: {
      action: 'create',
      resource: 'permission_manager',
      navLink: '/user/permission/create'
    }
  },
  {
    path: '/permission/edit/:id',
    component: lazy(() => import('../../pages/users/permission/edit')),
    meta: {
      action: 'edit',
      resource: 'permission_manager',
      navLink: '/user/permission/edit'
    }
  }
]

export default Users
