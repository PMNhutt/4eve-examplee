import { lazy } from 'react'

const OverviewRoutes = [
    {
        path: '/dashboard',
        exact: true,
        component: lazy(() => import('../../pages/overview')),
        meta: {
            action: 'view',
            resource: 'dashboard'
        }
    }
]

export default OverviewRoutes