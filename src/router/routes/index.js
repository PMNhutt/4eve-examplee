// ** Routes Imports
import DashboardRoutes from './Dashboards'
import AuthRoutes from './Auth'
import Users from "./Users"
import CrawlerAccount from "./CrawlerAccount"
import CrawlerPage from "./CrawlerPage"
import Agent from "./Agent"
import TelegramGroup from "./TelegramGroup"
import Company from "./Company"
import TelegramToken from "./TelegramToken"
import ConfigNotify from "./ConfigNotify"
import AccountWarning from "./AccountWarning"
import ConfigLog from "./ConfigLog"
import ExampleList from './ExampleList'

// ** Document title
const TemplateTitle = ''

// ** Default Route
const DefaultRoute = '/dashboard'
// ** Merge Routes
const Routes = [
    ...AuthRoutes,
    ...Users,
    ...DashboardRoutes,
    ...CrawlerAccount,
    ...CrawlerPage,
    ...Agent,
    ...TelegramGroup,
    ...Company,
    ...TelegramToken,
    ...ConfigNotify,
    ...AccountWarning,
    ...ConfigLog,
    ...ExampleList
]

export {DefaultRoute, TemplateTitle, Routes}
