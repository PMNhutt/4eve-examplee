import { lazy } from 'react'

const CrawlerPage = [
    {
        path: '/crawler-page/list',
        component: lazy(() => import('../../pages/crawler-page/list')),
        meta: {
            action: 'view',
            resource: 'crawler_page_list'
        }
    }
]

export default CrawlerPage
