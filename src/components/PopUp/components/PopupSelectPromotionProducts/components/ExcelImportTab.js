import {Button} from "reactstrap"
import {Download, UploadCloud} from "react-feather"
import Uppy from '@uppy/core'
import thumbnailGenerator from '@uppy/thumbnail-generator'
import { DragDrop } from '@uppy/react'
import 'uppy/dist/uppy.css'
import '../styles/excel-import-tab.scss'
import React, {useState} from "react"
import XLSX from 'xlsx'
import {Loading} from "../../../../Loading"
import {uploadExcel} from "../../../store/action"
import {toast} from "react-toastify"
import ErrorNotificationToast from "../../../../Toast/ToastFail"
import SuccessNotificationToast from "../../../../Toast/ToastSuccess"

export const ExcelImportTab = (props) => {
    const [isLoading, setIsLoading] = useState(false)

    const uppy = new Uppy({
        restrictions: { maxNumberOfFiles: 1 },
        autoProceed: true
    })

    uppy.use(thumbnailGenerator)

    uppy.on('complete', async result => {
        if (result?.successful?.[0]?.data) {
            if (isLoading) {
                return
            }
            setIsLoading(true)
            const formData = new FormData()
            formData.append(`excel`, result?.successful?.[0]?.data)
            let response = {}
            response = await uploadExcel(formData)
            const data = response?.data?.data
            if (data) {
                toast.success(<SuccessNotificationToast message={'Upload Success, The system will check the account in a few minutes, please wait!'}/>)
                props.getData()
            } else {
                toast.error(<ErrorNotificationToast message={'There was an error while updating the data!, Please contact technical'}/>)
            }
            setIsLoading(false)
            props?.toggle()
        }
    })

    function handleExportExcel() {
        const dataItems = [
            {
                Company: 'VN8899',
                Username: 'VDEJ_68',
                Password: '1111qqqQ',
                SecurityCode: ''
            },
            {
                Company: 'VN868',
                Username: 'me1sub29',
                Password: 'Zzzz9999',
                SecurityCode: ''
            }
        ]

        const wb = XLSX.utils.book_new()
        let fileName = ''
        XLSX.utils.book_append_sheet(wb, XLSX.utils.json_to_sheet(dataItems), 'Sheet')
        fileName = 'account_example.xlsx'
        XLSX.writeFile(wb, fileName, { bookType: 'xlsx', type: 'buffer' })
    }

    return (
        <>
            {isLoading && <Loading />}
            <div className={'excel-import-tab__header d-lg-flex d-block'}>
                <p className={'excel-import-tab__title'}/>
                <Button className={'excel-import-tab__button'} disabled={isLoading} color={'primary'} onClick={() => handleExportExcel()}><Download size={14}/>Download file example</Button>
            </div>
            <div className={'excel-import-tab__body'}>
                <div className={'excel-import-tab__body-content'}>
                    <UploadCloud size={54} color={'grey'}/>
                    <p className={'body-title'}>Choose or Drop file excel</p>
                    <Button className={'excel-import-tab__button'} disabled={isLoading} color={'primary'} onClick={() => {}}>Choose file</Button>
                </div>
                <DragDrop uppy={uppy}/>
            </div>
        </>
    )
}

export default ExcelImportTab
