import React from "react"
import currencyFormat from "../../../../../utility/CurrencyFormat"
import {replaceUrlMedia} from "../../../../../utility/replaceUrlMedia"

// ** Renders Client Columns
const renderClient = row => {
    if (row?.image_base_url) {
        return <img className='mr-1' src={replaceUrlMedia(row?.image_base_url)} width='50' height='50' alt={'image'}/>
    } else {
        return <div style={{width: '50px', height: '50px'}}/>
    }
}

export const columns = [
    {
        name: 'Hình ảnh',
        width: '112px',
        cell: row => renderClient(row)
    },
    {
        name: 'Tên sản phẩm',
        minWidth: '200px',
        selector: 'name',
        cell: row => row?.name
    },
    {
        name: 'Giá',
        minWidth: '100px',
        right: true,
        cell: row => `${currencyFormat(row?.price)} đ`
    }
]
