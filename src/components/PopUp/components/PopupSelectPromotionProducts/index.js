import '../../styles/popup_select_products.scss'
import {Button, Modal, Nav, NavItem, NavLink, TabContent, TabPane} from "reactstrap"
import React, {useState} from "react"
import ExcelImportTab from "./components/ExcelImportTab"
import {X} from "react-feather"

const PopupSelectPromotionProducts = (props) => {
    const [active, setActive] = useState(1)

    function handleChangeTab(tab) {
        setActive(tab)
    }

    return (
        <Modal isOpen={props?.isOpen} className='modal-dialog-centered popup-select-products'
               id={'popupSelectProductContainer'}
               toggle={() => {
                   props?.toggle()
               }}>
            <div className={'popup-select-products__wrapper'}>
                <div className={'popup-select-products__wrapper--header mb-50'}>
                    <h1 className={'mb-0'}>Import file - List of supporting companies</h1>
                    <Button color={'white'} onClick={() => props?.toggle()}><X /></Button>
                </div>
                <p className={'mt-2 body-title'}><b>SV388 - LVS789 - 3IN1BET - HK1101 - ONE789 - VN868</b>
                </p>
                <p className={'body-title'}><b>VN8899 - OK368 - SGD777 - IBET - SBB</b>
                </p>
                <ExcelImportTab productType={props?.productType} toggle={props?.toggle} getData={props.getData}/>
            </div>
        </Modal>
    )
}

export default PopupSelectPromotionProducts