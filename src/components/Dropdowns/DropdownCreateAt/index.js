import React from "react"
import './styles/dropdown_create_at.scss'
import Select from "react-select"
import { selectThemeColors } from '@utils'

const dateOptions = [
    {
        value: 'today', label: 'Hôm nay'
    },
    {
        value: 'yesterday', label: 'Hôm qua'
    },
    {
        value: 'thisWeek', label: 'Tuần này'
    },
    {
        value: 'lastWeek', label: 'Tuần trước'
    },
    {
        value: 'thisMonth', label: 'Tháng này'
    },
    {
        value: 'lastMonth', label: 'Tháng trước'
    }
]

const Index = props => {
    const {handleSelectOption, className} = props

    return (
        <Select
            theme={selectThemeColors}
            isClearable={false}
            className={`react-select select-create-at pl-lg-50 ${className}`}
            classNamePrefix='select'
            options={dateOptions}
            placeholder={'Ngày tạo'}
            onChange={data => {
                handleSelectOption(data)
            }}
        />
    )
}

export default Index
