import React, {Fragment} from "react"
import Avatar from "../../@core/components/avatar"
import {Check} from "react-feather"

const SuccessNotificationToast = (props) => {
    return (
        <Fragment>
            <div className='toastify-header'>
                <div className='title-wrapper'>
                    <Avatar size='sm' color='success' icon={<Check size={12} />} />
                    <h6 className='toast-title'>{props?.message ? props?.message : 'Update successful!'}</h6>
                </div>
            </div>
        </Fragment>
    )
}
export default SuccessNotificationToast
