import React, {Fragment} from "react"
import Avatar from "../../@core/components/avatar"
import {Check, X} from "react-feather"

const ErrorNotificationToast = (props) => {
    return (
        <Fragment>
            <div className='toastify-header'>
                <div className='title-wrapper'>
                    <Avatar size='sm' color='danger' icon={<X size={12} />} />
                    <h6 className='toast-title'>{props.message}</h6>
                </div>
            </div>
        </Fragment>
    )
}
export default ErrorNotificationToast
