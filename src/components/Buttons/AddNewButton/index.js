import {Col, Row} from "reactstrap"
import {Link} from "react-router-dom"
import {PlusCircle} from "react-feather"
import React from "react"

const Index = ({text, link}) => {
    return (
        <Row className='d-flex align-items-center justify-content-end m-0'>
            <Col md={2} className='d-flex align-items-center justify-content-end p-0'>
                <Link className={'btn btn-primary white-space-nowrap'} to={link}><PlusCircle color={'white'} size={18} style={{marginRight: '10px'}}/> {text}</Link>
            </Col>
        </Row>
    )
}
export default Index