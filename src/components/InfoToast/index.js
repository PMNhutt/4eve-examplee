import React, {Fragment} from "react"

export const InfoToast = (props) => {
    return (
        <Fragment>
            <div className='toastify-header pb-0'>
                <h6 className='text-info ml-50 mb-0'>{props?.content}</h6>
            </div>
        </Fragment>
    )
}

export default InfoToast