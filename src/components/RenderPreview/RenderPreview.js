import React from "react"
import icDelete from "../../assets/images/icons/icons/ic_delete.svg"
import iconAdd from "../../assets/images/icons/icons/ic_add.svg"
import {DragDrop} from "@uppy/react"
import Uppy from "@uppy/core"
import thumbnailGenerator from "@uppy/thumbnail-generator"
import './styles/render-preview.scss'
import {replaceUrlMedia} from "../../utility/replaceUrlMedia"

const RenderPreview = props => {
    const {image, setImage, className, hideDelete} = props

    function deleteImage() {
        setImage({preview: null})
    }

    const uppyImage = new Uppy({
        meta: { type: 'avatar' },
        autoProceed: true,
        restrictions: { allowedFileTypes: ['image/*'], maxNumberOfFiles: 1 }
    })

    uppyImage.use(thumbnailGenerator)

    uppyImage.on('thumbnail:generated', (file, preview) => {
        const img = { file: file, preview: preview}
        setImage(img)
    })

    if (image && image !== '') {
        return (
            <div className={`add-image ${className || ''}`}>
                <img style={{objectFit: props.objectFit || "unset"}} className={'w-100 h-auto add-image__image'} src={replaceUrlMedia(image)} alt="Lựa chọn hình ảnh"/>
                {
                    !hideDelete &&
                    <div className={'btn-delete d-flex align-items-center justify-content-center position-absolute position-top-0 position-right-0'}>
                        <img src={icDelete} alt="delete" onClick={(e) => deleteImage()}/>
                    </div>
                }
            </div>
        )
    } else {
        return (
            <div className={`${className || ''} add-image image__empty d-flex align-items-center justify-content-center`}>
                <div className={'text-center'}>
                    <img src={iconAdd} alt="iconAdd"/>
                    <p className={'mb-0'}>Tải ảnh lên</p>
                </div>
                <DragDrop uppy={uppyImage}/>
            </div>
        )
    }

}
export default RenderPreview
