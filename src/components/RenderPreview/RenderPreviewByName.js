import Uppy from "@uppy/core"
import thumbnailGenerator from "@uppy/thumbnail-generator"
import {Link} from "react-router-dom"
import icDelete from "@src/assets/images/icons/icons/ic_delete.svg"
import iconAdd from "@src/assets/images/icons/icons/ic_add.svg"
import {DragDrop} from "@uppy/react"
import React from "react"
import './styles/render-preview.scss'

export const RenderPreviewByName = (props) => {
    const {image, setImage} = props
    const uppyImage = new Uppy({
        meta: { type: 'avatar' },
        autoProceed: true,
        restrictions: { allowedFileTypes: ['image/*'], maxNumberOfFiles: 1 }
    })

    uppyImage.use(thumbnailGenerator)

    uppyImage.on('thumbnail:generated', (file, preview) => {
        const img = { file: file, preview: preview}
        props.setImage(img)
    })

    function deleteImage() {
        const image = {preview: null, file: null}
        setImage(image)
    }

    return (
        <div id={'renderPreviewByName'} className={props?.className || ''}>
            {
                image &&
                (
                    <div className={`add-image mb-2 px-2`}>
                        <Link to={{pathname: image}} target={'_blank'}>
                            {image.replace('https://hcm01.vstorage.vngcloud.vn/v1/AUTH_f9b5296a77c5472c8a7ebd72b9ee863c/feaer', '')}
                        </Link>
                        {
                            !props?.hideDelete &&
                            <div className={'btn-delete d-flex align-items-center justify-content-center position-absolute position-top-0 position-right-0'}>
                                <img src={icDelete} alt="delete" onClick={() => deleteImage()}/>
                            </div>
                        }
                    </div>
                )
            }
            {
                !image &&
                (
                    <div className={'item-content pb-1'}>
                        <div className={`top-banner__section-image add-image image__empty d-flex align-items-center justify-content-center`}>
                            <div className={'text-center'}>
                                <img src={iconAdd} alt="iconAdd"/>
                                <p className={'mb-0'}>Tải ảnh lên</p>
                            </div>
                            <DragDrop uppy={uppyImage}/>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default RenderPreviewByName
