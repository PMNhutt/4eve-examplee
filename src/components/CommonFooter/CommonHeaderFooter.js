// ** React Imports
import './style/common-header-footer.scss'
import {useHistory} from "react-router-dom"
import React, {useEffect} from "react"
import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    DropdownItem,
    DropdownMenu,
    DropdownToggle, UncontrolledButtonDropdown
} from "reactstrap"
import {handleMenuHidden} from "../../redux/actions/layout"
import {useDispatch} from "react-redux"

const CommonHeaderFooter = props => {
    const history = useHistory()
    // ** Props
    const { handleSubmitButton, isLoading, hideButtonBack, buttonSave, textButtonRight, hideButtonSave
        , showButtonUpdate, buttonUpdate, textButtonUpdate, handleSubmitUpdate, showButtonDropdown, dropdownList, textButtonDropdown, handleSubmitButtonDropdown } = props

    const dispatch = useDispatch()

    // ** Handles Menu Hidden
    const setIsHidden = val => dispatch(handleMenuHidden(val))

    useEffect(() => {
        setIsHidden(true)
    }, [dispatch])

    function handleBack() {
        if (props?.goBack && history.action === 'PUSH') {
            history.goBack()
        } else {
            if (props?.header) {
                history.push(props?.breadcrumb?.list_link)
            } else {
                history.push(props?.backUrl)
            }
        }
        setIsHidden(false)
    }

    function handleBackList() {
        history.push(props?.breadcrumb?.list_link)
    }

    return (
        <div className={`custom-header-footer ${props?.header ? 'custom-header-container' : 'custom-footer-container'}`}>
            <div className={`${props?.header ? 'custom-header' : 'custom-footer'}`}>
                {
                    props?.breadcrumb &&
                    <div className={'custom-header__title'}>
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <div className={'link custom-header__title-link'}
                                     onClick={handleBackList}> {props?.breadcrumb?.list_title} </div>
                            </BreadcrumbItem>
                            <BreadcrumbItem active>
                                <span className={'custom-header__title-link'}> {props?.breadcrumb?.detail_title} </span>
                            </BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                }
                <div className={`d-flex justify-content-end container-btn`}>
                    {
                        !hideButtonBack &&
                        <button className={'btn btn-black'} onClick={handleBack}>Go Back</button>
                    }
                    {
                        !hideButtonSave &&
                        <button className={buttonSave ? buttonSave : 'btn btn-black'} onClick={() => {
                            handleSubmitButton()
                        }} disabled={isLoading}>{textButtonRight ? textButtonRight : 'Lưu'}</button>
                    }
                    {
                        showButtonDropdown &&
                        <UncontrolledButtonDropdown>
                            <Button color='primary' className={`mr-0 ${buttonSave ? buttonSave : 'btn btn-black'}`} onClick={() => handleSubmitButtonDropdown(true)}>{textButtonDropdown ? textButtonDropdown : ''}</Button>
                            <DropdownToggle className='dropdown-toggle-split mr-3' color='primary' caret/>
                            <DropdownMenu>
                                {
                                    dropdownList && dropdownList?.length > 0 &&
                                    dropdownList?.map((dropdown, index) => {
                                        return (
                                            <DropdownItem key={index} onClick={() => handleSubmitButtonDropdown(dropdown?.status)}>{dropdown?.title}</DropdownItem>
                                        )
                                    })
                                }
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    }
                    {
                        showButtonUpdate &&
                        <button className={buttonUpdate ? buttonUpdate : 'btn btn-black'} onClick={() => {
                            handleSubmitUpdate()
                        }} disabled={isLoading}>{textButtonUpdate ? textButtonUpdate : 'Update'}</button>
                    }
                    {
                        props?.children
                    }
                </div>
            </div>
        </div>
    )
}

export default CommonHeaderFooter
