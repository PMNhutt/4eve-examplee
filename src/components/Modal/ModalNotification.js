import {Button, Card, Modal} from "reactstrap"
import React from "react"

export const ModalNotification = (props) => {
    return (
        <Modal isOpen={props?.showNotification} centered toggle={() => {}} className={'modal-xs'}>
            <Card className={'mb-0 p-2 text-center'}>
                <p className={'mb-2'}>Bạn có muốn tạo thông báo cho trên website?</p>
                <div className={'d-flex align-items-center justify-content-center'}>
                    <Button outline color={'primary mx-3'} onClick={() => props?.handleNoButton()}>Không</Button>
                    <Button color={'primary mx-3'} onClick={() => props?.handleYesButton()}>Có</Button>
                </div>
            </Card>
        </Modal>
    )
}