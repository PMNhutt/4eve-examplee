import {FormGroup, Input, Label, Modal} from "reactstrap"
import React, {useState} from "react"
import RenderPreview from "../RenderPreview/RenderPreview"
import './styles/modal-category.scss'
import Select from "react-select"

const pannerUrlList = [
    {
        value: '', label: 'Lựa chọn'
    },
    {
        value: 'deal-soc', label: 'Mua kèm deal sốc'
    },
    {
        value: 'combo-khuyen-mai', label: 'Combo khuyến mãi'
    },
    {
        value: 'flash-sale', label: 'Flash sale'
    },
    {
        value: 'voucher', label: 'Mã khuyến mãi'
    },
    {
        value: 'ao', label: 'Áo'
    },
    {
        value: 'quan', label: 'Quần'
    },
    {
        value: 'cua-hang', label: 'Cửa hàng'
    },
    {
        value: 'bo-suu-tap', label: 'Bộ sưu tập'
    },
    {
        value: 'tu-do', label: 'Tủ đồ'
    }
]

export const ModalCategory = (props) => {
    const [title, setTitle] = useState(null)
    const [linkUrl, setLinkUrl] = useState(pannerUrlList[0])
    const [image, setImage] = useState(null)

    function handleChangeTitle(value) {
        setTitle(value)
    }

    function handleSelectPannerUrlList(value) {
        setLinkUrl(value)
    }

    function handleSelectImage(image_url) {
        setImage(image_url)
    }

    return (
        <Modal isOpen={props?.isOpen} className='modal-dialog-centered popup-select-category'
               centered={true}
               toggle={() => props?.toggle()}>
            <div className={'modal-category'}>
                <div>
                    <Label className={'modal-category__label'}>Hình ảnh đại diện</Label>
                    <RenderPreview image={image?.preview} className={'modal-category__image'} setImage={(image_url) => handleSelectImage(image_url)}/>
                </div>
                <FormGroup className={'mt-1'}>
                    <Label className={'modal-category__label'} for='title'>Tiêu đề hình ảnh</Label>
                    <Input type='text' id='title' placeholder='Nhập tiêu đề hình ảnh' autoComplete={'off'} onBlur={(e) => handleChangeTitle(e.target.value)}/>
                </FormGroup>
                <div>
                    <Label className={'modal-category__label'}>Đường dẫn</Label>
                    <Select
                        isClearable={false}
                        className='react-select'
                        classNamePrefix='select'
                        options={pannerUrlList}
                        value={linkUrl}
                        onChange={url => handleSelectPannerUrlList(url)}
                    />
                </div>
                <div className={'d-flex align-items-center justify-content-end w-100'}>
                    <button className={'modal-category__button btn btn-primary mt-2'}
                            onClick={() => {
                                const category = {}
                                category.image_url = image?.preview
                                category.file = image?.file
                                category.name = title
                                category.link_url = linkUrl?.value
                                props?.handleAddCategory(category)
                                setLinkUrl(pannerUrlList[0])
                                setImage(null)
                                setTitle(null)
                            }}
                            disabled={!image?.preview}>LƯU</button>
                </div>
            </div>
        </Modal>
    )
}

export default ModalCategory
