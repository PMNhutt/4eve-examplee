import {Spinner, Button, Card, Modal} from "reactstrap"
import React from "react"
import {Check, X} from "react-feather"
import './styles/modal-check-login.scss'

export const ModalCheckLogin = ({process}) => {
    const isTemp = process === 'verifying' || process === 'verified' || process === 'failed'
    const isVerifying = process === 'verified' || process === 'failed'
    return (
        <Card className={'mb-0 py-3 text-center container-check'}>
            <div className={'container-progress-bar'}>
                <div className={'progress-bar-order'}>
                    <div className={'progress-bar-item'}>
                        <div className={'progress-bar-item-title'}>
                            Saved
                        </div>
                        <div className={'progress-bar-item-line'}/>
                        <div className={'progress-bar-item-ico'}>
                            {
                                process === 'temp' &&
                                <Spinner type='border' color='primary' />
                            }
                            {
                                process !== 'temp' &&
                                <div className={`progress-bar-item-ico-in active`}>
                                    <Check size={20}/>
                                </div>
                            }
                        </div>
                        <div className={`progress-bar-item-line ${isTemp ? 'active' : ''}`}/>
                    </div>
                    <div className={'progress-bar-item'}>
                        <div className={'progress-bar-item-title'}>
                            Verifying
                        </div>
                        <div className={`progress-bar-item-line ${isTemp ? 'active' : ''}`}/>
                        <div className={'progress-bar-item-ico'}>
                            {
                                process === 'verifying' &&
                                <Spinner type='border' color='primary' />
                            }
                            {
                                process !== 'verifying' &&
                                <div className={`progress-bar-item-ico-in ${isVerifying ? 'active' : ''}`}>
                                    {
                                        isVerifying &&
                                        <Check size={20}/>
                                    }
                                    {
                                        isVerifying &&
                                        <X size={20}/>
                                    }
                                </div>
                            }
                        </div>
                        <div className={`progress-bar-item-line ${isVerifying ? 'active' : ''}`}/>
                    </div>
                    {
                        (process === 'temp' || process === 'verifying' || process === 'verified') &&
                        <div className={'progress-bar-item'}>
                            <div className={'progress-bar-item-title'}>
                                Done
                            </div>
                            <div className={`progress-bar-item-line ${isVerifying ? 'active' : ''}`}/>
                            <div className={'progress-bar-item-ico'}>
                                <div className={`progress-bar-item-ico-in ${isVerifying ? 'active' : ''}`}>
                                    {
                                        isVerifying &&
                                        <Check size={20}/>
                                    }
                                    {
                                        isVerifying &&
                                        <X size={20}/>
                                    }
                                </div>
                            </div>
                        </div>
                    }
                    {
                        process === 'failed' &&
                        <div className={'progress-bar-item'}>
                            <div className={'progress-bar-item-title'}>
                                Failure
                            </div>
                            <div className={`progress-bar-item-line ${isVerifying ? 'active' : ''}`}/>
                            <div className={'progress-bar-item-ico'}>
                                {
                                    process === 'failed' &&
                                    <div className={`progress-bar-item-ico-in ${process === 'failed' ? 'active' : ''}`}>
                                        {
                                            process === 'failed' &&
                                            <X size={20}/>
                                        }
                                    </div>
                                }
                            </div>
                        </div>
                    }
                </div>
            </div>
        </Card>
    )
}