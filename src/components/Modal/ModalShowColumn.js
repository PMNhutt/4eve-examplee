import {Button, Card, CardBody, CardHeader, Col, CustomInput, Modal, ModalHeader, Row} from "reactstrap"
import React, {useEffect, useState} from "react"
import {X} from "react-feather"
import classnames from "classnames"
import './styles/modal-show-column.scss'

const ModalShowColumn = (props) => {
    const [columnList, setColumnList] = useState([])
    const [checkAll, setCheckAll] = useState(false)
    useEffect(() => {
        setColumnList(JSON.parse(JSON.stringify(props.columnsTable)))
        changeStatusForAll(JSON.parse(JSON.stringify(props.columnsTable)))
    }, [props.columnsTable])
    function submitButton () {
        props?.submitColumn(columnList)
        props?.toggle()
    }
    function changeStatus(index) {
        columnList[index].checked = !columnList[index]?.checked
        setColumnList([...columnList])
        changeStatusForAll(columnList)
    }
    function handleCancel() {
        setColumnList([...props.columnsTable])
        props?.toggle()
    }
    function changeAllStatus(e) {
        columnList?.forEach(column => {
            column.checked = e.target.checked
        })
        setCheckAll(e.target.checked)
        setColumnList([...columnList])
    }
    function changeStatusForAll(list) {
       const checkColumnAll = list?.find(column => !column?.checked)
        if (checkColumnAll) {
            setCheckAll(false)
        } else {
            setCheckAll(true)
        }
    }
    return (
        <Modal
            isOpen={props?.isOpen}
            centered={true}
            toggle={() => {
                handleCancel()
            }}
            className={'modal-show-column'}
        >
            <ModalHeader toggle={() => {
                handleCancel()
            }}>
               Điều chỉnh cột hiển thị
            </ModalHeader>
            <Card className={'mb-0'}>
                <CardBody>

                    <Row>
                        <Col xs={12} className={'check-all'}>
                            <CustomInput
                                type='checkbox'
                                id={'add'}
                                label={'Tất cả'}
                                checked={checkAll}
                                className={classnames('mr-1')}
                                onChange={(e) => {
                                    changeAllStatus(e)
                                }}
                            />
                        </Col>
                        {columnList?.length > 0 && columnList?.map((columnTable, index) => {
                            return (
                                <Col lg={4} className={'mb-1'}  key={index}>
                                    <CustomInput
                                        type='checkbox'
                                        id={columnTable?.id}
                                        label={columnTable?.name}
                                        checked={columnTable?.checked}
                                        className={classnames('mr-1')}
                                        onChange={() => {
                                            changeStatus(index)
                                        }}
                                    />
                                </Col>
                            )
                        })}
                    </Row>
                </CardBody>
                <div className={'d-flex justify-content-end p-2'}>
                    <Button className={'mr-2'} type={'button'} onClick={() => handleCancel()}>Hủy</Button>
                    <Button color={'primary'} onClick={() => submitButton()}>Xác nhận</Button>
                </div>
            </Card>
        </Modal>
    )
}
export default ModalShowColumn