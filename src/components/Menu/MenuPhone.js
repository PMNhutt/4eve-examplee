import React, {useEffect, useState} from "react"
import {X} from "react-feather"
import Uppy from "@uppy/core"
import thumbnailGenerator from "@uppy/thumbnail-generator"
import {DragDrop} from "@uppy/react"
import './style/style-menu.scss'
import {replaceUrlMedia} from "../../utility/replaceUrlMedia"

const MenuPhone = (props) => {
    const [menu, setMenu] = useState(props?.menu)
    useEffect(() => {
        if (!menu && props?.menu) {
            setMenu(props?.menu)
        }
    })
    const uppyImage = new Uppy({
        meta: { type: 'avatar' },
        autoProceed: true,
        restrictions: { allowedFileTypes: ['image/*'], maxNumberOfFiles: 1 }
    })

    uppyImage.use(thumbnailGenerator)

    uppyImage.on('thumbnail:generated', (file, preview) => {
        props.menu.image_url = preview
        props.menu.file = file
        setMenu({...props.menu})
    })

    function deleteImage() {
        props.menu.image_url = null
        props.menu.file = null
        setMenu({...props.menu})
    }

    return (
        <>
            <div className={`imagesContainer ${props?.className || ''}`}>
                <div
                    className={`dropdownImageContainer`}>
                    <div className={'dropdownImageWrapperPhone'}>
                        {
                            menu?.image_url ? (
                                <div className={`drawerMenuItem`}>
                                    <div className={'drawerMenuItemName'}>{menu?.title || props?.title}</div>
                                    <img src={replaceUrlMedia(menu?.image_url)} alt="" className={'menuItemBackground'}/>
                                </div>
                            ) : (
                                <div className={`uploadFileWrapperEmptyPhone`}>
                                    <div className={`uploadFileIconPhone`}>+</div>
                                    <DragDrop uppy={uppyImage}/>
                                </div>
                            )
                        }
                    </div>
                    {
                        menu?.image_url &&
                        <div className={'dropdownImageDelete'} onClick={() => deleteImage()}>
                            <X size={14} color={'white'}/>
                        </div>
                    }
                </div>
            </div>
        </>
    )
}
export default MenuPhone
