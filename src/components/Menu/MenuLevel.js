import React, {useState} from "react"
import {X} from "react-feather"
import {FormGroup, Input, Label, Modal} from "reactstrap"
import Select from "react-select"
import './style/style-menu.scss'

const MenuLevel = (props) => {
    const [menuDetail, setMenuDetail] = useState(props?.menu_detail)
    const [subDetail, setSubDetail] = useState(null)
    const [subDetailSlug, setSubDetailSlug] = useState(null)
    const [showPopUp, setShowPopUp] = useState(false)
    function handleAddMenuDetail(subDetail, subDetailSlug) {
        menuDetail?.sub_details.push({title: subDetail, slug: subDetailSlug})
        setMenuDetail({...menuDetail})
        props.menu_detail.sub_details = menuDetail?.sub_details
        setSubDetail(null)
        setSubDetailSlug(null)
        setShowPopUp(false)
    }

    return (
        <>
            <div className={'productLevels'}>
                <div className={'productLevelGroup'}>
                    <span className={'productLevelTitle'}>{menuDetail.title}</span>
                    <div className={'productLevelSubMenu'}>
                        {
                            menuDetail?.sub_details?.map((sub, index) => {
                                return (
                                    <div className={'productLevelSubMenuItem d-flex'} key={index}>
                                        <div className={'subMenuItem'}>{sub.title}</div>
                                        <div className={'subMenuItemDelete ml-4'} onClick={() => {
                                            menuDetail?.sub_details.splice(index, 1)
                                            setMenuDetail({...menuDetail})
                                            props.menu_detail.sub_details = menuDetail?.sub_details
                                        }}>
                                            <X size={16}/>
                                        </div>
                                    </div>
                                )
                            })
                        }
                        <div className={'productLevelSubMenuItem d-inline-block'} onClick={() => setShowPopUp(true)}>
                            <div className={'subMenuItem subMenuItemButton'}>+ Thêm</div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal isOpen={showPopUp} toggle={() => {
                setShowPopUp(false)
                setSubDetail(null)
                setSubDetailSlug(null)
            }} centered={true}>
                <div className={'popupInputProductLevel p-2'}>
                    <FormGroup>
                        <Label>Nhập tiêu đề</Label>
                        <Input type={'text'} className={'form-control'} onChange={(e) => { setSubDetail(e.target.value) }}/>
                    </FormGroup>
                    <div className={'mt-1'}>
                        <Label>Chọn đường dẫn</Label>
                        <Select
                            isClearable={false}
                            className='react-select'
                            classNamePrefix='select'
                            options={props?.urls}
                            placeholder={'Chọn đường dẫn'}
                            value={props?.urls?.find(option => option?.value === subDetail)}
                            onChange={ url => { setSubDetailSlug(url?.value) }}
                        />
                    </div>
                    <div className={'d-flex justify-content-end mt-1'}>
                        <button className={'btn btn-primary'} onClick={() => { handleAddMenuDetail(subDetail, subDetailSlug) }}>LƯU</button>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default MenuLevel
