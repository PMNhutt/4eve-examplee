import React, {useState} from "react"
import {FormGroup, Input, Label, Modal} from "reactstrap"
import Select from "react-select"
import './style/style-menu.scss'
import {DragDrop} from "@uppy/react"
import {X} from "react-feather"
import Uppy from "@uppy/core"
import thumbnailGenerator from "@uppy/thumbnail-generator"
import {replaceUrlMedia} from "../../utility/replaceUrlMedia"

const MenuCommon = (props) => {
    const [menuDetail, setMenuDetail] = useState(props?.menu_detail)
    const [image, setImage] = useState(props?.image)
    const [title, setTitle] = useState(props?.title)
    const [file, setFile] = useState(null)
    const [slug, setSlug] = useState(props?.slug)
    const [showPopUp, setShowPopUp] = useState(false)

    const uppyImage = new Uppy({
        meta: { type: 'avatar' },
        autoProceed: true,
        restrictions: { allowedFileTypes: ['image/*'], maxNumberOfFiles: 1 }
    })

    uppyImage.use(thumbnailGenerator)

    uppyImage.on('thumbnail:generated', (file, preview) => {
        setImage(preview)
        setFile(file)
    })

    function handleAddMenuDetail(image, file, title, slug) {
        menuDetail?.sub_details.push({image_url: image, file: file, title: title, slug: slug})
        setMenuDetail({...menuDetail})
        props.menu_detail.sub_details = menuDetail?.sub_details
        setTitle(null)
        setFile(null)
        setSlug(null)
        setImage(null)
        setShowPopUp(false)
    }

    return (
        <>
            <div className={`${props?.menu_type === 'product_list' ? 'productList' : 'productGrid'}`}>
                {
                    menuDetail.sub_details?.map((item, index) => {
                        return (
                            <div className={'productItem'} key={index}>
                                <div className={'productItemImageContainer'}>
                                    {
                                        item?.image_url ? <img src={replaceUrlMedia(item?.image_url)} alt=""/> : <div className={`uploadFileWrapper`}>
                                            <input type="file"/>
                                            <div className={`uploadFileIcon`}>
                                                +
                                            </div>
                                        </div>
                                    }
                                </div>
                                <div className={'productItemName'}>{item?.title}</div>
                                <div className={'subMenuItemDelete ml-4'} onClick={() => {
                                    menuDetail?.sub_details.splice(index, 1)
                                    setMenuDetail({...menuDetail})
                                    props.menu_detail.sub_details = menuDetail?.sub_details
                                }}>
                                    <X size={16}/>
                                </div>
                            </div>
                        )
                    })
                }
                <div className={'productItem'} onClick={ () => { setShowPopUp(true) }}>
                    <div className={'productItemImageContainer'}>
                        <div className={`uploadFileWrapper`}>
                            <input type="file"/>
                            <div className={`uploadFileIcon`}>
                                +
                            </div>
                        </div>
                    </div>
                    <div className={'productItemName'}>+ Thêm</div>
                </div>
            </div>
            <Modal isOpen={showPopUp} toggle={() => {
                setTitle(null)
                setFile(null)
                setSlug(null)
                setImage(null)
                setShowPopUp(false)
            }} centered={true}>
                <div className={'popupInputProductLevel p-2'}>
                    <div
                        className={`dropdownImageContainer multiImage`}>
                        <div className={'dropdownImageWrapper'}>
                            {
                                image ? <img src={replaceUrlMedia(image)} alt=""/> : <div className={`uploadFileWrapperEmpty`}>
                                    <div className={`uploadFileIcon`}>+</div>
                                    <DragDrop uppy={uppyImage}/>
                                </div>
                            }
                        </div>
                        {
                            image &&
                            <div className={'dropdownImageDelete'} onClick={() => {
                                setImage(null)
                            }}>
                                <X size={14} color={'white'}/>
                            </div>
                        }
                    </div>
                    <div className={'mt-1'}>
                        <Label>{props.label}</Label>
                        <Select
                            isClearable={false}
                            className='react-select'
                            classNamePrefix='select'
                            options={props?.urls || []}
                            value={props?.urls?.find(option => option?.slug === slug)}
                            getOptionValue={option => option?.slug}
                            getOptionLabel={option => option?.name}
                            placeholder={props.label}
                            onChange={ url => {
                                setSlug(url?.slug)
                                setTitle(url?.name)
                            }}
                        />
                    </div>
                    <div className={'d-flex justify-content-end mt-1'}>
                        <button className={'btn btn-primary'} disabled={ !image || slug === '' } onClick={() => {
                            handleAddMenuDetail(image, file, title, slug)
                        }}>LƯU</button>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default MenuCommon
