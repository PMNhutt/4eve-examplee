import React, {useState} from "react"
import {FormGroup, Input, Label, Modal} from "reactstrap"
import Select from "react-select"
import './style/style-menu.scss'
import {X} from "react-feather"


const MenuTextMobile = (props) => {
    const [menuDetail, setMenuDetail] = useState(props?.menu_detail)
    const [subMenuDetail, setSubMenuDetail] = useState({})
    const [showMenuPopUp, setShowMenuPopUp] = useState(false)

    function handleAddMenuDetail(subMenuDetail) {
        if (subMenuDetail.position === undefined) {
            subMenuDetail.position = menuDetail?.sub_details?.length + 1
        }
        menuDetail?.sub_details.push(subMenuDetail)
        setSubMenuDetail({})
        setShowMenuPopUp(false)
    }

    return (
        <>
            <div className={`${props?.menu_type === 'product_list' ? 'productList' : 'productGrid'}`}>
                {
                    menuDetail.sub_details?.sort((a, b) => a.position > b.position ? 1 : -1).map((item, index) => {
                        return (
                            <div key={index}>
                                <div className={'productItem'}>
                                    <div className={'productItemName'}>{`${item?.title}`} {item?.slug && <span>({item?.slug})</span>}</div>
                                    <div className={'subMenuItemDelete ml-4'} onClick={() => {
                                        menuDetail?.sub_details.splice(index, 1)
                                        setMenuDetail({...menuDetail})
                                        props.menu_detail.sub_details = menuDetail?.sub_details
                                    }}>
                                        <X size={16}/>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
            <div className={`${props?.menu_type === 'product_list' ? 'productList' : 'productGrid'}`}>
                <div className={'productItem'} onClick={ () => { setShowMenuPopUp(true) }}>
                    <div className={'productItemName'}>+ Thêm Menu</div>
                </div>
            </div>
            <Modal isOpen={showMenuPopUp} toggle={() => {
                setSubMenuDetail({})
                setShowMenuPopUp(false)
            }} centered={true}>
                <div className={'popupInputProductLevel p-2'}>
                    <h2>Tạo mới menu chính</h2>
                    <FormGroup>
                        <Label>Nhập tiêu đề</Label>
                        <Input type={'text'} className={'form-control'} onChange={(e) => {
                            subMenuDetail.title = e.target.value
                            setSubMenuDetail(subMenuDetail)
                        }}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Nhập slug</Label>
                        <Input type={'text'} className={'form-control'} onChange={(e) => {
                            subMenuDetail.slug = e.target.value
                            setSubMenuDetail(subMenuDetail)
                        }}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Màu background</Label>
                        <Input type={'text'} className={'form-control'} onChange={(e) => {
                            subMenuDetail.background_color = e.target.value
                            setSubMenuDetail(subMenuDetail)
                        }}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Vị Trí</Label>
                        <Input type={'number'} className={'form-control'} onChange={(e) => {
                            subMenuDetail.position = +e.target.value
                            setSubMenuDetail(subMenuDetail)
                        }}/>
                    </FormGroup>
                    {
                        props?.urls?.length > 0 &&
                        <div className={'mt-1'}>
                            <Label>{props.label}</Label>
                            <Select
                                isClearable={false}
                                className='react-select'
                                classNamePrefix='select'
                                options={props?.urls || []}
                                value={props?.urls?.find(option => option?.slug === subMenuDetail?.slug)}
                                getOptionValue={option => option?.slug}
                                getOptionLabel={option => option?.name}
                                placeholder={props.label}
                                onChange={ url => {
                                    subMenuDetail.slug = `${props?.slug_value}${url?.name}`
                                    subMenuDetail.title = url?.name
                                    subMenuDetail.category_id = url?.id
                                    setSubMenuDetail(subMenuDetail)
                                }}
                            />
                        </div>
                    }
                    <div className={'d-flex justify-content-end mt-1'}>
                        <button className={'btn btn-primary mr-2'} onClick={() => {
                            setSubMenuDetail({})
                            setShowMenuPopUp(false)
                        }}>HỦY</button>
                        <button className={'btn btn-primary'} disabled={ subMenuDetail === {} } onClick={() => { handleAddMenuDetail(subMenuDetail) }}>LƯU</button>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default MenuTextMobile
