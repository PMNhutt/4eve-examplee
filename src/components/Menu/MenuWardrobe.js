import React, {Fragment, useState} from "react"
import {Check, X} from "react-feather"
import './style/style-menu.scss'
import {FormGroup, Input, Label, Modal} from "reactstrap"
import Select from "react-select"
import {replaceUrlMedia} from "../../utility/replaceUrlMedia"

const MenuWardrobe = (props) => {
    const [wardrobe, setWardrobe] = useState(props?.wardrobe)
    const [showPopUp, setShowPopUp] = useState(false)

    return (
        <>
            <div className={`imagesContainer ${props?.className || ''}`}>
                <div className={'dropdownImageWrapper dropdownImageWrapperWardrobe'}>
                    {
                        props?.position !== 3 &&
                        <>
                            {
                                wardrobe?.vertical_url ? <img src={replaceUrlMedia(wardrobe.vertical_url)} alt=""/> : <div className={`uploadFileWrapper`} onClick={() => setShowPopUp(true)}>
                                    <div className={`uploadFileIcon`}>+</div>
                                </div>
                            }
                            {
                                wardrobe?.vertical_url &&
                                <div className={'dropdownImageDelete'} onClick={() => {
                                    setWardrobe(null)
                                }}>
                                    <X size={14} color={'white'}/>
                                </div>
                            }
                        </>
                    }
                    {
                        props?.position === 3 &&
                        <>
                            {
                                wardrobe?.horizontal_url ? <img src={replaceUrlMedia(wardrobe.horizontal_url)} alt=""/> : <div className={`uploadFileWrapper`} onClick={() => setShowPopUp(true)}>
                                    <div className={`uploadFileIcon`}>+</div>
                                </div>
                            }
                            {
                                wardrobe?.horizontal_url &&
                                <div className={'dropdownImageDelete'} onClick={() => {
                                    setWardrobe(null)
                                }}>
                                    <X size={14} color={'white'}/>
                                </div>
                            }
                        </>
                    }
                </div>
            </div>

            <Modal isOpen={showPopUp} toggle={() => setShowPopUp(false)} centered={true}>
                <div className={'popupInputImage p-2'}>
                    <div className={'mt-1'}>
                        <Label>Chọn đường dẫn</Label>
                        <Select
                            isClearable={false}
                            className='react-select'
                            classNamePrefix='select'
                            noOptionsMessage={() => 'Hiện tại chưa có tủ đồ'}
                            options={props?.wardrobes || []}
                            value={wardrobe?.id ? wardrobe : ''}
                            getOptionValue={option => option?.id}
                            getOptionLabel={option => option?.name}
                            placeholder={'Lựa chọn tủ đồ'}
                            onChange={wardrobe => { setWardrobe(wardrobe) }}
                        />
                    </div>
                    <div className={`mt-2 dropdownImageContainer multiImage`}>
                        <div className={'dropdownImageWrapper'}>
                            {
                                wardrobe?.vertical_url ? <img src={replaceUrlMedia(wardrobe?.vertical_url)} alt=""/> : <div className={`uploadFileWrapperEmpty`}>
                                    <div className={`uploadFileIcon`}>+</div>
                                </div>
                            }
                        </div>
                    </div>
                    <div className={'d-flex justify-content-end mt-1'}>
                        <button className={'btn btn-primary'} disabled={ !wardrobe } onClick={() => {
                            props.handleWardrobeSelected(wardrobe)
                            setShowPopUp(false)
                        }}>LƯU</button>
                    </div>
                </div>
            </Modal>
        </>
    )
}
export default MenuWardrobe
