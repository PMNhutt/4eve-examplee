import React, {useState} from "react"
import {X} from "react-feather"
import './style/style-menu.scss'
import {Label, Modal} from "reactstrap"
import Select from "react-select"
import {replaceUrlMedia} from "../../utility/replaceUrlMedia"

const MenuGallery = (props) => {
    const [gallery, setGallery] = useState(props?.gallery)
    const [showPopUp, setShowPopUp] = useState(false)

    return (
        <>
            <div className={`imagesContainer ${props?.className || ''}`}>
                <div className={'dropdownImageWrapper'}>
                    {
                        gallery?.image_url ? <img src={replaceUrlMedia(gallery.image_url)} alt=""/> : <div className={`uploadFileWrapper`} onClick={() => setShowPopUp(true)}>
                            <div className={`uploadFileIcon`}>+</div>
                        </div>
                    }
                    {
                        gallery?.image_url &&
                        <div className={'dropdownImageDelete'} onClick={() => {
                            setGallery(null)
                        }}>
                            <X size={14} color={'white'}/>
                        </div>
                    }
                </div>
            </div>

            <Modal isOpen={showPopUp} toggle={() => setShowPopUp(false)} centered={true}>
                <div className={'popupInputImage p-2'}>
                    <div className={'mt-1'}>
                        <Label>Chọn đường dẫn</Label>
                        <Select
                            isClearable={false}
                            className='react-select'
                            classNamePrefix='select'
                            noOptionsMessage={() => 'Hiện tại bộ sưu tập nào'}
                            options={props?.galleries || []}
                            value={gallery?.id ? gallery : ''}
                            getOptionValue={option => option?.id}
                            getOptionLabel={option => option?.email}
                            placeholder={'Lựa chọn bộ sưu tập'}
                            onChange={wardrobe => { setGallery(wardrobe) }}
                        />
                    </div>
                    <div className={`mt-2 dropdownImageContainer multiImage`}>
                        <div className={'dropdownImageWrapper'}>
                            {
                                gallery?.image_url ? <img src={replaceUrlMedia(gallery?.image_url)} alt=""/> : <div className={`uploadFileWrapperEmpty`}>
                                    <div className={`uploadFileIcon`}>+</div>
                                </div>
                            }
                        </div>
                    </div>
                    <div className={'d-flex justify-content-end mt-1'}>
                        <button className={'btn btn-primary'} disabled={ !gallery } onClick={() => {
                            props.handleGallerySelected(gallery)
                            setShowPopUp(false)
                        }}>LƯU</button>
                    </div>
                </div>
            </Modal>
        </>
    )
}
export default MenuGallery
