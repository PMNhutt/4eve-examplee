import React, {useState} from "react"
import {FormGroup, Input, Label, Modal} from "reactstrap"
import Select from "react-select"
import './style/style-menu.scss'
import {X} from "react-feather"


const MenuTextDesktop = (props) => {
    const [menuDetail, setMenuDetail] = useState(props?.menu_detail)
    const [childMenuDetails, setChildMenuDetails] = useState([])
    const [isUpdate, setIsUpdate] = useState(false)
    const [childMenuDetail, setChildMenuDetail] = useState({})
    const [subMenuDetail, setSubMenuDetail] = useState({})
    const [showMenuPopUp, setShowMenuPopUp] = useState(false)
    const [showSubMenuPopUp, setShowSubMenuPopUp] = useState(false)

    function handleAddMenuDetail(subMenuDetail) {
        if (subMenuDetail.position === undefined) {
            subMenuDetail.position = menuDetail?.sub_details?.length + 1
        }
        if (childMenuDetails && childMenuDetails.length > 0) {
            subMenuDetail.child_details = childMenuDetails
        } else {
            subMenuDetail.child_details = []
        }
        if (isUpdate) {

        } else {
            menuDetail?.sub_details.push(subMenuDetail)
        }
        setSubMenuDetail({})
        setChildMenuDetail({})
        setChildMenuDetails([])
        setShowMenuPopUp(false)
        setIsUpdate(false)
    }

    function handleAddSubMenuDetail(childMenuDetail) {
        childMenuDetails.push(childMenuDetail)
        setChildMenuDetail({})
        setShowSubMenuPopUp(false)
        setShowMenuPopUp(true)
    }

    return (
        <>
            <div className={`${props?.menu_type === 'product_list' ? 'productList' : 'productGrid'}`}>
                {
                    menuDetail.sub_details?.sort((a, b) => a.position > b.position ? 1 : -1).map((item, index) => {
                        return (
                            <div key={index}>
                                <div className={'productItem'}>
                                    <div className={'productItemName'} onClick={() => {
                                        setSubMenuDetail(item)
                                        setShowMenuPopUp(true)
                                        setIsUpdate(true)
                                    }}>{`${item?.title}`} {item?.slug && <span>({item?.slug})</span>}</div>
                                    <div className={'subMenuItemDelete ml-3'} onClick={() => {
                                        menuDetail?.sub_details.splice(index, 1)
                                        setMenuDetail({...menuDetail})
                                        props.menu_detail.sub_details = menuDetail?.sub_details
                                    }}>
                                        <X size={16}/>
                                    </div>
                                </div>
                                {/*<div className={'mt-50'}>{`Có ${item?.child_details ? item?.child_details.length : 0} menu con`}</div>*/}
                                {
                                    item?.child_details.length > 0 &&
                                    <div className={'px-50 pt-50'}>
                                        {
                                            item?.child_details?.sort((a, b) => a.position > b.position ? 1 : -1)?.map((childDetail, i) => {
                                                return (
                                                    <div key={i} className={'d-flex justify-content-between'}>
                                                        <p className={'m-0 childDetailTitle pb-50'}>{childDetail?.title} {childDetail?.slug && <span>({childDetail?.slug})</span>}</p>
                                                        <div className={'subMenuItemDelete ml-3'} onClick={() => {
                                                            menuDetail.sub_details?.[index]?.child_details.splice(i, 1)
                                                            setMenuDetail({...menuDetail})
                                                            props.menu_detail.sub_details = menuDetail?.sub_details
                                                        }}>
                                                            <X size={16}/>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                }
                            </div>
                        )
                    })
                }
            </div>
            <div className={`${props?.menu_type === 'product_list' ? 'productList' : 'productGrid'}`}>
                <div className={'productItem'} onClick={ () => { setShowMenuPopUp(true) }}>
                    <div className={'productItemName'}>+ Thêm Menu</div>
                </div>
            </div>
            <Modal isOpen={showMenuPopUp} toggle={() => {
                setSubMenuDetail({})
                setChildMenuDetails([])
                setShowMenuPopUp(false)
            }} centered={true}>
                <div className={'popupInputProductLevel p-2'}>
                    <h2>Tạo mới menu chính</h2>
                    <FormGroup>
                        <Label>Nhập tiêu đề</Label>
                        <Input type={'text'} className={'form-control'} defaultValue={subMenuDetail?.title} onChange={(e) => {
                            subMenuDetail.title = e.target.value
                            setSubMenuDetail(subMenuDetail)
                        }}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Nhập slug</Label>
                        <Input type={'text'} className={'form-control'} defaultValue={subMenuDetail?.slug} onChange={(e) => {
                            subMenuDetail.slug = e.target.value
                            setSubMenuDetail(subMenuDetail)
                        }}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Màu background</Label>
                        <Input type={'text'} className={'form-control'} defaultValue={subMenuDetail?.background_color} onChange={(e) => {
                            subMenuDetail.background_color = e.target.value
                            setSubMenuDetail(subMenuDetail)
                        }}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Vị Trí</Label>
                        <Input type={'number'} className={'form-control'} defaultValue={subMenuDetail?.position} onChange={(e) => {
                            subMenuDetail.position = +e.target.value
                            setSubMenuDetail(subMenuDetail)
                        }}/>
                    </FormGroup>
                    <div className={'productItemName mt-2'}>{`Đã tạo ${childMenuDetails.length} menu phụ`}</div>

                    <div className={`${props?.menu_type === 'product_list' ? 'productList' : 'productGrid'} mt-2`}>
                        <div className={'productItem'} onClick={ () => { setShowSubMenuPopUp(true) }}>
                            <div className={'productItemName'}>+ Thêm Submenu</div>
                        </div>
                    </div>
                    <div className={'d-flex justify-content-end mt-1'}>
                        <button className={'btn btn-primary mr-2'} disabled={ childMenuDetail === {} } onClick={() => {
                            setSubMenuDetail({})
                            setChildMenuDetails([])
                            setShowMenuPopUp(false)
                        }}>HỦY</button>
                        <button className={'btn btn-primary'} disabled={ subMenuDetail === {} } onClick={() => { handleAddMenuDetail(subMenuDetail) }}>LƯU</button>
                    </div>
                </div>
            </Modal>

            <Modal isOpen={showSubMenuPopUp} toggle={() => {
                setChildMenuDetails([])
                setShowSubMenuPopUp(false)
            }} centered={true}>
                <div className={'popupInputProductLevel p-2'}>
                    <h2>Tạo mới menu phụ</h2>
                    <FormGroup>
                        <Label>Nhập tiêu đề</Label>
                        <Input type={'text'} className={'form-control'} onChange={(e) => {
                            childMenuDetail.title = e.target.value
                            setChildMenuDetail(childMenuDetail)
                        }}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Nhập slug</Label>
                        <Input type={'text'} className={'form-control'} onChange={(e) => {
                            childMenuDetail.slug = e.target.value
                            setChildMenuDetail(childMenuDetail)
                        }}/>
                    </FormGroup>
                    {
                        props?.urls?.length > 0 &&
                        <div className={'mt-1'}>
                            <Label>{props.label}</Label>
                            <Select
                                isClearable={false}
                                className='react-select'
                                classNamePrefix='select'
                                options={props?.urls || []}
                                value={props?.urls?.find(option => option?.slug === childMenuDetail?.slug)}
                                getOptionValue={option => option?.slug}
                                getOptionLabel={option => option?.name}
                                placeholder={props.label}
                                onChange={ url => {
                                    childMenuDetail.slug = `${props?.slug_value}${url?.name}`
                                    childMenuDetail.title = url?.name
                                    childMenuDetail.position = childMenuDetails.length + 1
                                    setChildMenuDetail(childMenuDetail)
                                }}
                            />
                        </div>
                    }
                    <div className={'d-flex justify-content-end mt-1'}>
                        <button className={'btn btn-primary mr-2'} disabled={ childMenuDetail === {} } onClick={() => {
                            setChildMenuDetails([])
                            setShowSubMenuPopUp(false)
                        }}>HỦY</button>
                        <button className={'btn btn-primary'} disabled={ childMenuDetail === {} } onClick={() => { handleAddSubMenuDetail(childMenuDetail) }}>LƯU</button>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default MenuTextDesktop
