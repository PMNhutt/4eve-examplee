import React, {Fragment, useState} from "react"
import {Check, X} from "react-feather"
import Uppy from "@uppy/core"
import thumbnailGenerator from "@uppy/thumbnail-generator"
import {DragDrop} from "@uppy/react"
import './style/style-menu.scss'
import {FormGroup, Input, Label, Modal} from "reactstrap"
import Select from "react-select"
import {replaceUrlMedia} from "../../utility/replaceUrlMedia"

const MenuImage = (props) => {
    const [image, setImage] = useState(props?.image)
    const [imageThumb, setImageThumb] = useState(props?.image_thumb_url)
    const [title, setTitle] = useState(props?.title)
    const [sub_title, setSubTitle] = useState(props?.sub_title)
    const [file, setFile] = useState(null)
    const [slug, setSlug] = useState(props?.slug)
    const [showPopUp, setShowPopUp] = useState(false)

    const uppyImage = new Uppy({
        meta: { type: 'avatar' },
        autoProceed: true,
        restrictions: { allowedFileTypes: ['image/*'], maxNumberOfFiles: 1 }
    })

    uppyImage.use(thumbnailGenerator)

    uppyImage.on('thumbnail:generated', (file, preview) => {
        setImage(preview)
        setImageThumb(preview)
        setFile(file)
    })

    return (
        <>
            <div className={`imagesContainer ${props?.className || ''}`}>
                <div
                    className={`dropdownImageContainer ${props?.multi_image ? 'multiImage' : ''}`}>
                    <div className={'dropdownImageWrapper'}>
                        {
                            image ? (
                                <>
                                    <div className={'imageContainer'}>
                                        {props?.hasTitle && <div className={'imageInsetShadow'}/>}
                                        <img src={replaceUrlMedia(image)} alt=""/>
                                    </div>
                                    {
                                        props?.hasTitle &&
                                        <div className={'dropdownImageContent'} dangerouslySetInnerHTML={{__html: (title + (sub_title ? `<br/> ${sub_title}` : ''))?.replace('\n', '<br/>')}}/>
                                    }
                                </>
                            ) : (
                                <div className={`uploadFileWrapper`} onClick={() => setShowPopUp(true)}>
                                    <div className={`uploadFileIcon`}>+</div>
                                </div>
                            )
                        }
                    </div>
                    {
                        image &&
                        <div className={'dropdownImageDelete'} onClick={() => {
                            setImage(null)
                            setImageThumb(null)
                            props.handleMenuImage(null, null, null, null, null, null)
                        }}>
                            <X size={14} color={'white'}/>
                        </div>
                    }
                </div>
            </div>

            <Modal isOpen={showPopUp} toggle={() => setShowPopUp(false)} centered={true}>
                <div className={'popupInputImage p-2'}>
                    <div
                        className={`dropdownImageContainer multiImage`}>
                        <div className={'dropdownImageWrapper'}>
                            {
                                image ? (
                                    <img src={replaceUrlMedia(image)} alt=""/>
                                ) : (
                                    <div className={`uploadFileWrapperEmpty`}>
                                        <div className={`uploadFileIcon`}>+</div>
                                        <DragDrop uppy={uppyImage}/>
                                    </div>
                                )
                            }
                        </div>
                        {
                            image &&
                            <div className={'dropdownImageDelete'} onClick={() => {
                                setImage(null)
                                setImageThumb(null)
                            }}>
                                <X size={14} color={'white'}/>
                            </div>
                        }
                    </div>
                    {
                        props?.hideTilte &&
                        <FormGroup>
                            <Label>Nhập tiêu đề</Label>
                            <Input type={'text'} className={'form-control'} onChange={(e) => { setTitle(e.target.value) }}/>
                        </FormGroup>
                    }

                    {
                        props?.showSubTitle &&
                        <FormGroup>
                            <Label>Nhập mô tả</Label>
                            <Input type={'text'} className={'form-control'} onChange={(e) => { setSubTitle(e.target.value) }}/>
                        </FormGroup>
                    }
                    {
                        !props?.hideUrl &&
                        <div className={'mt-1'}>
                            <Label>Chọn đường dẫn</Label>
                            <Select
                                isClearable={false}
                                className='react-select'
                                classNamePrefix='select'
                                options={props?.urls}
                                placeholder={'Chọn đường dẫn'}
                                value={props?.urls?.find(option => option?.value === slug)}
                                onChange={url => {
                                    setSlug(url?.value)
                                }}
                            />
                        </div>
                    }
                    <div className={'d-flex justify-content-end mt-1'}>
                        <button className={'btn btn-primary'} disabled={ !image || slug === '' } onClick={() => {
                            props.handleMenuImage(image, imageThumb, file, title, sub_title, slug)
                            setShowPopUp(false)
                        }}>LƯU</button>
                    </div>
                </div>
            </Modal>
        </>
    )
}
export default MenuImage
