import imgShoppingCard from '@src/assets/images/icons/icons/ic_shopping_card_black.svg'
import './style/collection-product-card.scss'
import {replaceUrlMedia} from "../../utility/replaceUrlMedia"
import {getProductMediaFirst} from "../../utility/getProductMediaFirst"

export const CollectionProductCard = (props) => {
    const image = getProductMediaFirst(props?.product?.product?.product_media)

    return (
        <div className={props?.className || ''} id={'galleryProductCard'}>
            <div className={'product-image'}>
                {
                    <div className={'image-wrapper'}>
                        {
                            image?.thumb_url && <img src={replaceUrlMedia(image?.thumb_url)} alt="productImg"/>
                        }
                    </div>
                }
            </div>
            <div className={'progress-order'}>
                <div className={'progress-bar'}>
                    <div className={'progress-active'}/>
                </div>
                <p className={'progress-description'}>Số lượng đã đặt <b>0/{props?.product?.quantity || 0}</b></p>
            </div>
            <div className={'product-content'}>
                <div>
                    <div className='d-flex flex-wrap'>
                        {
                            !props?.hideTag && <p className={'product-tag'}>Dark Bear</p>
                        }
                        <p className={'product-name'}>{props?.product?.product?.name_display}</p>
                    </div>
                    <p className={'product-price'}>{props?.product?.product?.price_display}</p>
                </div>
                {
                    !props?.hideShopCardIcon &&
                    <div className={`btn-order`}>
                        <img src={imgShoppingCard} alt="shopCardIcon"/>
                    </div>
                }
            </div>
        </div>
    )
}

export default CollectionProductCard