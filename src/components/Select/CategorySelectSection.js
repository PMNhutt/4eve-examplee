import icDelete from "@src/assets/images/icons/icons/ic_delete.svg"
import {Plus} from "react-feather"
import {Button} from "reactstrap"
import React from "react"
import './styles/category-select-section.scss'
import {replaceUrlMedia} from "../../utility/replaceUrlMedia"

export const CategorySelectSection = (props) => {
    return (
        <div className={'popup__content'}>
            <p className={'m-0 popup__content-title mb-50'}>Danh mục nổi bật</p>
            <div className={'popup__content-products'}>
                {
                    props?.linkProducts?.map((item, index) => {
                        return (
                            <div className={`popup__content-product`} key={index}>
                                <div className={'btn-delete d-flex align-items-center justify-content-center position-absolute position-top-0 position-right-0'}>
                                    <img src={icDelete} alt="delete" onClick={() => {
                                        props?.handleRemoveItem(index)
                                    }}/>
                                </div>
                                <img className={'popup__content-product--image'} src={replaceUrlMedia(item?.image_url)} alt="product image"/>
                                <p className={'popup__content-product--name'}>{item?.name}</p>
                            </div>
                        )
                    })
                }
                <div className={'d-flex align-items-end'}>
                    <Button className={'button-add'} color={'primary'} onClick={() => props?.handleAddCategory()}><Plus size={20}/></Button>
                </div>
            </div>
        </div>
    )
}

export default CategorySelectSection
