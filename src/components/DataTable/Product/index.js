import './styles/data_table_promotion.scss'
import React, {forwardRef, useState} from "react"
import ReactPaginate from "react-paginate"
import {ChevronDown} from "react-feather"
import DataTable from "react-data-table-component"
import {NoDataComponent} from "../../NoDataComponent"

// ** Bootstrap Checkbox Component
const BootstrapCheckbox = forwardRef(({ onClick, ...rest }, ref) => (
    <div className='custom-control custom-checkbox promotion-checkbox'>
        <input type='checkbox' className='custom-control-input' ref={ref} {...rest} />
        <label className='custom-control-label' onClick={onClick} />
    </div>
))

const DataTableProduct = (props) => {

    const rowsPerPage = props?.rowsPerPage || 6
    const [currentPage, setCurrentPage] = useState(1)

    const dataToRender = () => {
        const filters = {}

        const isFiltered = Object.keys(filters).some(function (k) {
            return filters[k].length > 0
        })
        if (props.products?.length > 0) {
            if (props.products?.length > rowsPerPage) {
                const productList = []
                const count = currentPage * rowsPerPage
                for (let index = (currentPage - 1) * rowsPerPage; index < count; index++) {
                    const product = props?.products[index]
                    if (product?.id) {
                        productList.push(product)
                    }
                }
                return productList
            } else {
                return props?.products
            }

        } else if (props.products?.length === 0 && isFiltered) {
            return []
        }
    }

    const handlePagination = page => {
        props?.handlePageChange(page.selected + 1)
        setCurrentPage(page.selected + 1)
    }

    const count = Number(Math.ceil(props?.totalPages / rowsPerPage))

    const ProductPagination = () => {
        return (
            <ReactPaginate
                previousLabel={''}
                nextLabel={''}
                pageCount={count || 1}
                activeClassName='active'
                forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                onPageChange={page => handlePagination(page)}
                pageClassName={'page-item'}
                nextLinkClassName={'page-link'}
                nextClassName={'page-item next'}
                previousClassName={'page-item prev'}
                previousLinkClassName={'page-link'}
                breakClassName={'page-item'}
                breakLinkClassName={'page-link'}
                pageLinkClassName={'page-link'}
                containerClassName={'pagination react-paginate justify-content-end mt-2 mb-0 pr-1'}
            />
        )
    }

    return (
        <>
            <DataTable
                noHeader
                responsive
                selectableRows={!props?.disabledCheckbox}
                selectableRowsComponent={BootstrapCheckbox}
                selectableRowSelected={row => row?.checked}
                onSelectedRowsChange={state => {
                    props?.setSelectedProduct(state?.selectedRows, currentPage, dataToRender())
                }}
                persistTableHead
                expandableRows={props?.isExpand}
                expandOnRowClicked={props?.isExpand}
                expandableRowExpanded={row => row?.checked || row?.checked === undefined}
                expandableRowsComponent={props?.expandableRowsComponent}
                columns={props?.columns}
                sortIcon={<ChevronDown />}
                className={`custom-dataTable ${props?.classNameDataTable || ''}`}
                data={dataToRender()}
                noDataComponent={<NoDataComponent message={'Hiện tại chưa có sản phẩm nào!'}/>}
            />
            {
                (props?.totalPages > rowsPerPage && !props?.hidePagination) && <ProductPagination />
            }
        </>
    )
}

export default DataTableProduct
