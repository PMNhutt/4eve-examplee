import './styles/data_table_promotion.scss'
import React, {forwardRef, useState} from "react"
import ReactPaginate from "react-paginate"
import {ChevronDown} from "react-feather"
import DataTable from "react-data-table-component"
import {NoDataComponent} from "../../NoDataComponent"
import Select from "react-select"

// ** Bootstrap Checkbox Component
const BootstrapCheckbox = forwardRef(({ onClick, ...rest }, ref) => (
    <div className='custom-control custom-checkbox promotion-checkbox'>
        <input type='checkbox' className='custom-control-input' ref={ref} {...rest} />
        <label className='custom-control-label' onClick={onClick} />
    </div>
))

const selectStyles = {
    menu: (provided) => ({
        ...provided,
        zIndex: 999999
    })
}

const perPageOptions = [
    {label: 5, value: 5},
    {label: 10, value: 10},
    {label: 20, value: 20},
    {label: 50, value: 50},
    {label: 100, value: 100}
]

const DataTablePromotionProduct = (props) => {

    const [currentPage, setCurrentPage] = useState(1)
    const [currentPerPage, setCurrentPerPage] = useState(5)

    const dataToRender = () => {
        const filters = {}

        const isFiltered = Object.keys(filters).some(function (k) {
            return filters[k].length > 0
        })
        if (props.products?.length > 0) {
            return props?.products
        } else if (props.products?.length === 0 && isFiltered) {
            return []
        } else return []
    }

    const handlePagination = page => {
        props?.handlePageChange(page.selected + 1, currentPerPage)
        setCurrentPage(page.selected + 1)
    }

    const count = Number(Math.ceil(props?.totalPages / currentPerPage))

    const ProductPagination = () => {
        return (
            <ReactPaginate
                previousLabel={''}
                nextLabel={''}
                pageCount={count || 1}
                activeClassName='active'
                forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                onPageChange={page => handlePagination(page)}
                pageClassName={'page-item'}
                nextLinkClassName={'page-link'}
                nextClassName={'page-item next'}
                previousClassName={'page-item prev'}
                previousLinkClassName={'page-link'}
                breakClassName={'page-item'}
                breakLinkClassName={'page-link'}
                pageLinkClassName={'page-link'}
                containerClassName={'pagination react-paginate justify-content-lg-end justify-content-start mt-2 mb-0 pr-1 overflow-auto'}
            />
        )
    }

    return (
        <>
            <div className={'header-table'}>
                <p className={'mb-0 mb-50'}>Lựa chọn số lượng sản phẩm hiển thị:</p>
                <Select
                    styles={selectStyles}
                    isSearchable={false}
                    name='rowsPerPage'
                    options={perPageOptions}
                    className='select-option section-select width-20-per section-phone'
                    classNamePrefix='select'
                    placeholder={'Lựa chọn'}
                    isDisabled={props?.disabled || props?.edit}
                    onChange={(perPage) => {
                        props?.handlePageChange(1, perPage?.value)
                        setCurrentPerPage(perPage?.value)
                        setCurrentPage(1)
                    }}
                    value={perPageOptions?.find(item => item?.value === currentPerPage)}
                />
            </div>
            <DataTable
                noHeader
                responsive
                selectableRows={!props?.disabledCheckbox}
                selectableRowsComponent={BootstrapCheckbox}
                selectableRowSelected={row => row?.checked}
                onSelectedRowsChange={state => {
                    props?.setSelectedProduct(state?.selectedRows, currentPage, dataToRender())
                }}
                persistTableHead
                expandableRows={props?.isExpand}
                expandOnRowClicked={props?.isExpand}
                expandableRowExpanded={row => row?.checked || row?.checked === undefined}
                expandableRowsComponent={props?.expandableRowsComponent}
                columns={props?.columns}
                sortIcon={<ChevronDown />}
                className={`custom-dataTable ${props?.classNameDataTable || ''} data-table__scroll`}
                data={dataToRender()}
                noDataComponent={<NoDataComponent message={'Hiện tại chưa có sản phẩm nào!'}/>}
            />
            {
                (props?.totalPages > currentPerPage && !props?.hidePagination) && <ProductPagination />
            }
        </>
    )
}

export default DataTablePromotionProduct
