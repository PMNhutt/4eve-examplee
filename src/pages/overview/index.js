import {Card, CardBody, CardHeader, Col, Row} from "reactstrap"
import {getUserData} from "../../auth/utils"
import Timeline from "../../@core/components/timeline"
import {convertDateTime} from "../../utility/ConvertDate"
import React, {useEffect} from "react"
import StatsWithAreaChart from "../../@core/components/widgets/stats/StatsWithAreaChart"
import {handleMenuHidden} from "../../redux/actions/layout"
import {useDispatch, useSelector} from "react-redux"
import {countAgentNoDetails, countAgents, countCrawlerAccounts, countDomains} from "./store/action"
import {AlertCircle, Package, Users} from "react-feather"

// ** styles
import './styles/styles.scss'

const Overview = () => {
    const user = getUserData()
    const dispatch = useDispatch()
    const store = useSelector(state => state?.overview)

    useEffect(() => {

        dispatch(countAgentNoDetails())
        dispatch(countAgents())
        dispatch(countDomains())
        dispatch(countCrawlerAccounts())
        dispatch(handleMenuHidden(false))
    }, [])

    const userActionsData = [
        {
            title: `${user?.name} has logged into the system`,
            meta: convertDateTime(new Date())
        }
    ]

    return (
        <Row id={'overview'}>
            <Col xl={3} sm={12}>
                <StatsWithAreaChart
                    icon={<Users size={21} />}
                    color='primary'
                    stats={store?.total_crawler_accounts?.toString()}
                    statTitle='Accounts'
                    type='area'
                    series={[]}/>
            </Col>
            <Col xl={3} sm={12}>
                <StatsWithAreaChart
                    icon={<Package size={21} />}
                    color='warning'
                    stats={store?.total_agents?.toString()}
                    statTitle='Agents'
                    type='area'
                    series={[]}/>
            </Col>
            <Col xl={3} sm={12}>
                <StatsWithAreaChart
                    icon={<AlertCircle size={21} />}
                    color='primary'
                    stats={store?.total_agents_no_details?.toString()}
                    statTitle='Warning Accounts'
                    type='area'
                    series={[]}/>
            </Col>
            <Col xl={3} sm={12}>
                <StatsWithAreaChart
                    icon={<Package size={21} />}
                    color='warning'
                    stats={store?.total_domains?.toString()}
                    statTitle='Domain'
                    type='area'
                    series={[]}/>
            </Col>
            <Col md={6} sm={12}>
                <Card>
                    <CardHeader>RECENT ACTIVITY</CardHeader>
                    <CardBody>
                        <Timeline data={userActionsData}/>
                    </CardBody>
                </Card>
            </Col>
        </Row>
    )
}

export default Overview
