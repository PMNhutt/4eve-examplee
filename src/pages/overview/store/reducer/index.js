import {
  COUNT_AGENTS_BEGIN,
  COUNT_AGENTS_FAIL,
  COUNT_AGENTS_NO_DETAILS_BEGIN,
  COUNT_AGENTS_NO_DETAILS_FAIL,
  COUNT_AGENTS_NO_DETAILS_SUCCESS,
  COUNT_AGENTS_SUCCESS,
  COUNT_CRAWLER_ACCOUNTS_BEGIN,
  COUNT_CRAWLER_ACCOUNTS_FAIL,
  COUNT_CRAWLER_ACCOUNTS_SUCCESS,
  COUNT_DOMAINS_BEGIN,
  COUNT_DOMAINS_FAIL,
  COUNT_DOMAINS_SUCCESS

} from '../action'

// ** Initial State
const initialState = {
  loading: false,
  total_crawler_accounts: 0,
  total_domains: 0,
  total_agents: 0,
  total_agents_no_details: 0,
  errorMessage: null
}

const overview = (state = initialState, action) => {
  switch (action.type) {
    case COUNT_CRAWLER_ACCOUNTS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case COUNT_CRAWLER_ACCOUNTS_SUCCESS:
      return {
        ...state,
        total_crawler_accounts: action.total,
        loading: false
      }
    case COUNT_CRAWLER_ACCOUNTS_FAIL:
      return {
        ...state,
        errorMessage: action.errorMessage,
        loading:  false
      }
    case COUNT_DOMAINS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case COUNT_DOMAINS_SUCCESS:
      return {
        ...state,
        total_domains: action.total,
        loading: false
      }
    case COUNT_DOMAINS_FAIL:
      return {
        ...state,
        errorMessage: action.errorMessage,
        loading:  false
      }
    case COUNT_AGENTS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case COUNT_AGENTS_SUCCESS:
      return {
        ...state,
        total_agents: action.total,
        loading: false
      }
    case COUNT_AGENTS_FAIL:
      return {
        ...state,
        errorMessage: action.errorMessage,
        loading:  false
      }
    case COUNT_AGENTS_NO_DETAILS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case COUNT_AGENTS_NO_DETAILS_SUCCESS:
      return {
        ...state,
        total_agents_no_details: action.total,
        loading: false
      }
    case COUNT_AGENTS_NO_DETAILS_FAIL:
      return {
        ...state,
        errorMessage: action.errorMessage,
        loading:  false
      }
    default:
      return { ...state }
  }
}
export default overview
