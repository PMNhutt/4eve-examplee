import instances from "../../../../@core/plugin/axios"

export const COUNT_DOMAINS_BEGIN = 'COUNT_DOMAINS_BEGIN'
export const COUNT_DOMAINS_SUCCESS = 'COUNT_DOMAINS_SUCCESS'
export const COUNT_DOMAINS_FAIL = 'COUNT_DOMAINS_FAIL'

export const COUNT_CRAWLER_ACCOUNTS_BEGIN = 'COUNT_CRAWLER_ACCOUNTS_BEGIN'
export const COUNT_CRAWLER_ACCOUNTS_SUCCESS = 'COUNT_CRAWLER_ACCOUNTS_SUCCESS'
export const COUNT_CRAWLER_ACCOUNTS_FAIL = 'COUNT_CRAWLER_ACCOUNTS_FAIL'

export const COUNT_AGENTS_BEGIN = 'COUNT_AGENTS_BEGIN'
export const COUNT_AGENTS_SUCCESS = 'COUNT_AGENTS_SUCCESS'
export const COUNT_AGENTS_FAIL = 'COUNT_AGENTS_FAIL'

export const COUNT_AGENTS_NO_DETAILS_BEGIN = 'COUNT_AGENTS_NO_DETAILS_BEGIN'
export const COUNT_AGENTS_NO_DETAILS_SUCCESS = 'COUNT_AGENTS_NO_DETAILS_SUCCESS'
export const COUNT_AGENTS_NO_DETAILS_FAIL = 'COUNT_AGENTS_NO_DETAILS_FAIL'

export const countDomains = () => {
    return async dispatch => {
        dispatch({
            type: COUNT_DOMAINS_BEGIN
        })
        await instances.get('/admin/domain/all').then(response => {
            dispatch({
                type: COUNT_DOMAINS_SUCCESS,
                total: response.data?.data?.domains?.length
            })
        }).catch(error => {
            dispatch({
                type: COUNT_DOMAINS_FAIL,
                errorMessage: error
            })
        })
    }
}

export const countCrawlerAccounts = () => {
    const config = {params: {page: 1, perPage: 1, status: true}}
    return async dispatch => {
        dispatch({
            type: COUNT_CRAWLER_ACCOUNTS_BEGIN
        })
        await instances.get(`/admin/crawler-account/paging`, config).then(response => {
            dispatch({
                type: COUNT_CRAWLER_ACCOUNTS_SUCCESS,
                total: response.data?.data?.totalRecords
            })
        }).catch(error => {
            dispatch({
                type: COUNT_CRAWLER_ACCOUNTS_FAIL,
                errorMessage: error
            })
        })
    }
}

export const countAgents = () => {
    const config = {params: {page: 1, perPage: 1, status: true}}

    return async dispatch => {
        dispatch({
            type: COUNT_AGENTS_BEGIN
        })
        await instances.get(`/admin/agent/paging`, config)
            .then(response => {
                dispatch({
                    type: COUNT_AGENTS_SUCCESS,
                    total: response?.data?.data?.totalRecords
                })
            })
            .catch(error => {
                dispatch({
                    type: COUNT_AGENTS_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const countAgentNoDetails = () => {
    const config = {params: {page: 1, perPage: 1, status: true}}

    return async dispatch => {
        dispatch({
            type: COUNT_AGENTS_NO_DETAILS_BEGIN
        })
        await instances.get(`/admin/agent-no-detail/paging`, config)
            .then(response => {
                dispatch({
                    type: COUNT_AGENTS_NO_DETAILS_SUCCESS,
                    total: response?.data?.data?.totalRecords
                })
            })
            .catch(error => {
                dispatch({
                    type: COUNT_AGENTS_NO_DETAILS_FAIL,
                    errorMessage: error
                })
            })
    }
}
