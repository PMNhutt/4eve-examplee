import { GET_COMPANY_BEGIN, GET_COMPANY_FAIL, GET_COMPANY_SUCCESS,
GET_COMPANY_BY_ID_BEGIN, GET_COMPANY_BY_ID_SUCCESS, GET_COMPANY_BY_ID_FAIL} from '../action'

const initialState = {
    loading: false,
    expampleCompanies: [],
    totalRecords: 0,
    errorMessage: null
    // exampleCompany: null
}

const example = (state = initialState, action) => {
    switch (action.type) {
        case GET_COMPANY_BEGIN:
            return {
                ...state,
                loading: true
            }
        case GET_COMPANY_SUCCESS:
            return {
                ...state,
                loading: false,
                expampleCompanies: action.expampleCompanies,
                totalRecords: action.totalRecords
            }
        case GET_COMPANY_FAIL:
            return {
                loading: false,
                errorMessage: action.errorMessage
            }
        default:
            return { ...state }
    }
}

export default example