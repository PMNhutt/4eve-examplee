import React, { Fragment, useState, useEffect } from 'react'

import { columns } from './column'
import Sidebar from './Sidebar'

// ** Store & Actions
import { useDispatch, useSelector } from 'react-redux'
import { getCompanies } from '../store/action'

// ** Third Party Components
import DataTable from 'react-data-table-component'
import ReactPaginate from 'react-paginate'
import { Card, Button, CardTitle, Input, Label, Row, Col } from 'reactstrap'
import { ChevronDown } from 'react-feather'
import { Loading } from '../../../components/Loading'
import { NoDataComponent } from '../../../components/NoDataComponent'
import { useHistory, useLocation } from 'react-router-dom'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/user.scss'

const Table = () => {

    // ** States
    const [currentPage, setCurrentPage] = useState(searchParams?.get('page') ? searchParams?.get('page') : 1)
    const rowsPerPage = 7
    const history = useHistory()
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)
    const [sidebarOpen, setSidebarOpen] = useState(false)
    const [companyID, setCompanyID] = useState()

    // ** Store Vars
    const dispatch = useDispatch()
    const store = useSelector(state => state.example)

    // ** Get data on mount
    useEffect(() => {
        const params = { page: currentPage, perPage: rowsPerPage }
        dispatch(getCompanies(params))
        // checkPermissionByScreen(PERMISSION_SCREEN_ENUM.company).then(res => {
        //     setCheckPermission(res)
        // })
    }, [])

    // ** Toggle Sidebar open
    const toggleSidebar = () => {
        if (sidebarOpen) {
            setCompanyID(null)
        }
        setSidebarOpen(!sidebarOpen)
    }

    const handleShowPopupUpdate = id => {
        setSidebarOpen(true)
        setCompanyID(id)
    }

    // ** Toggle modal new company

    const handleCloseModal = () => {
        setIsOpenModal(false)
    }

    // ** Table data
    const dataToRender = () => {
        if (store.expampleCompanies?.length > 0) {
            return store.expampleCompanies
        } else {
            return []
        }
    }

    // ** Custom Pagination
    const CustomPagination = () => {
        const count = Number(Math.ceil(store.totalRecords / rowsPerPage))
        if (count > 1) {
            return (
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    pageCount={count || 1}
                    activeClassName='active'
                    forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                    onPageChange={handlePagination}
                    pageClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next'}
                    previousClassName={'page-item prev'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
                />
            )
        } else return <></>
    }

    const handlePagination = page => {
        const params = { page: page.selected + 1, perPage: rowsPerPage }
        dispatch(getCompanies(params))
        checkParams(params)
        setCurrentPage(page.selected + 1)
    }

    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.page) {
            params.page = filterParams.page
        }
        const urlSearchParams = new URLSearchParams(params)
        history.replace({ pathname: location.pathname, search: urlSearchParams.toString() })
    }

    return (
        <Fragment>
            <Card id={'userContainer'}>
                <Row className='mx-0 mt-1'>
                    {
                        <div className={'ml-auto mb-1 mr-1'}>
                            <Button.Ripple color='primary' onClick={toggleSidebar}>Create new</Button.Ripple>
                        </div>
                    }
                </Row>
                <DataTable
                    noHeader
                    responsive
                    persistTableHead
                    paginationServer
                    columns={columns(handleShowPopupUpdate)}
                    sortIcon={<ChevronDown />}
                    className='react-dataTable px-0'
                    data={dataToRender()}
                    noDataComponent={<NoDataComponent message={'Currently, no company created yet'} />}
                />
                <div>
                    {
                        CustomPagination()
                    }
                </div>
            </Card>
            {
                sidebarOpen &&
                <Sidebar id={companyID} open={sidebarOpen} toggleSidebar={toggleSidebar} />
            }
        </Fragment>
    )
}

export default Table