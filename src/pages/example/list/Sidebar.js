import { useEffect, useState } from 'react'

import Sidebar from '@components/sidebar'

// ** Third Party Components
import { Button, FormGroup, Form, Input, Label } from 'reactstrap'
import { useForm } from 'react-hook-form'
import classnames from 'classnames'

// ** Store & Actions
import { getCompanyById } from '../store/action'
import { store } from '@store/storeConfig/store'

const SidebarExample = ({ id, open, toggleSidebar }) => {

    const { register, errors } = useForm()
    // ** States
    const company = store.getState().example?.expampleCompanies

    console.log(company)

    // ** handle submit form 
    const handleSubmit = () => {

    }


    return (
        <Sidebar
            size='lg'
            open={open}
            title={`${id ? 'Edit' : 'Create new'} company`}
            headerClassName='mb-1'
            contentClassName='pt-0 user__side-bar'
            toggleSidebar={toggleSidebar}
        >
            <Form onSubmit={handleSubmit} autoComplete={'off'}>
                <FormGroup>
                    <Label className={'label'} for='url'>Company Name <span className='text-danger'>*</span></Label>
                    <Input
                        name='name'
                        id='url'
                        placeholder='Enter company name'
                        type={'text'}
                        innerRef={register({ required: true })}
                        defaultValue={company?.name || ''}
                        className={classnames({ 'is-invalid': errors['name'] })}
                    />
                </FormGroup>
                <div className={'row mt-2'}>
                    <Button type='submit' className={'ml-1 mr-1'} color='primary'>Save</Button>
                    <Button type='reset' color='secondary' outline onClick={toggleSidebar}>Cancel</Button>
                </div>
            </Form>
        </Sidebar>
    )
}

export default SidebarExample