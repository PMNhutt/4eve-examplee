import { convertDate } from '../../../utility/ConvertDate'
import { Badge, Button, UncontrolledTooltip } from 'reactstrap'
import { Edit3 } from 'react-feather'
import '../styles/user.scss'

export const columns = ((handleShowPopupUpdate) => [
    {
        name: 'NO',
        width: '50px',
        sortable: true,
        cell: row => row?.id
    },
    {
        name: 'Company Name',
        sortable: true,
        width: '150px',
        cell: row => row?.name
    },
    {
        name: 'Package Name',
        sortable: true,
        width: '150px',
        cell: row => row?.package_name
    },
    {
        name: 'Domain',
        sortable: true,
        width: '250px',
        cell: row => row?.domain
    },
    {
        name: 'Login Domain',
        sortable: true,
        width: '230px',
        cell: row => row?.login_domain
    },
    {
        name: 'Status',
        sortable: true,
        width: '120px',
        cell: row => <Badge className='text-capitalize' color={row?.status ? 'light-success' : 'light-warning'} pill>
            {row.status ? 'Active' : 'Inactive'}
        </Badge>
    },
    {
        name: 'Created At',
        sortable: true,
        cell: row => convertDate(row?.created_at)
    },
    {
        name: 'Actions',
        center: 'true',
        cell: row => (
            <>
                <Button.Ripple
                    id={`edit${row?.id}`}
                    className='btn-icon'
                    color='flat-primary'
                    size='sm'
                    onClick={() => handleShowPopupUpdate(row?.id)}>
                    <Edit3 size={16} />
                </Button.Ripple>
                <UncontrolledTooltip placement='top' target={`edit${row?.id}`}>
                    Edit
                </UncontrolledTooltip>
            </>
        )

    }
])