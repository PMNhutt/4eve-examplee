import Table from './Table'
import '@styles/react/apps/app-users.scss'

const ExampleList = () => {
  return (
    <div id="exampleList" className="app-user-list">
        <Table />
    </div>
  )
}

export default ExampleList
