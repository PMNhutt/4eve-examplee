import { isUserLoggedIn } from '@utils'
import { useSkin } from '@hooks/useSkin'
import { ChevronLeft } from 'react-feather'
import {Link, Redirect, useHistory} from 'react-router-dom'
import {Row, Col, CardTitle, CardText, Form, FormGroup, Label, Input, Button} from 'reactstrap'
import {Fragment, useState} from "react"
import classnames from "classnames"
import {useForm} from "react-hook-form"
import {isObjEmpty} from "../../../utility/Utils"
import useJwt from "../../../auth/jwt/useJwt"
import {Slide, toast} from "react-toastify"

// ** Configs
import themeConfig from '@configs/themeConfig'

// ** styles
import '@styles/base/pages/page-auth.scss'

const ToastContent = ({ message }) => (
    <Fragment>
      <div className={'toastify-header'}>
        <div className='title-wrapper'>
          <h6 className='toast-title font-weight-bold'>Thông báo</h6>
        </div>
      </div>
      <div className='toastify-body'>
        <span>{message}</span>
      </div>
    </Fragment>
)

const ForgotPassword = () => {
  const [skin, setSkin] = useSkin()
  const [email, setEmail] = useState(null)
  const { register, errors, handleSubmit } = useForm()
  const history = useHistory()

  const illustration = skin === 'dark' ? 'forgot-password-v2-dark.svg' : 'forgot-password-v2.svg',
    source = require(`@src/assets/images/pages/${illustration}`).default

  const onSubmit = () => {
    if (isObjEmpty(errors)) {
      useJwt
          .forgotPassword(email)
          .then(res => {
            if (res.data && res.data.status) {
              history.push('/reset-password')
              toast.success(<ToastContent is_success={true} message={'Vui lòng kiểm tra email để nhận mã xác thực OTP'}/>,
                  { transition: Slide, hideProgressBar: true, autoClose: 2000 })

            } else {
              toast.error(<ToastContent message={'Tài khoản chưa tồn tại, vui lòng xem lại!'}/>,
                  { transition: Slide, hideProgressBar: true, autoClose: 2000 })
            }
          })
          .catch(err => {
            toast.error(<ToastContent message={'Hệ thống xảy ra lỗi, vui lòng thử lại sau!'}/>,
                { transition: Slide, hideProgressBar: true, autoClose: 2000 })
          })
    }
  }

  if (!isUserLoggedIn()) {
    return (
      <div className='auth-wrapper auth-v2'>
        <Row className='auth-inner m-0'>
          <Link className='brand-logo' to='/' onClick={e => e.preventDefault()}>
            <img src={themeConfig.app.appLogoImage} alt=""/>
          </Link>
          <Col className='d-none d-lg-flex align-items-center p-5' lg='8' sm='12'>
            <div className='w-100 d-lg-flex align-items-center justify-content-center px-5'>
              <img className='img-fluid' src={source} alt='Login V2' />
            </div>
          </Col>
          <Col className='d-flex align-items-center auth-bg px-2 p-lg-5' lg='4' sm='12'>
            <Col className='px-xl-2 mx-auto' sm='8' md='6' lg='12'>
              <CardTitle tag='h2' className='font-weight-bold mb-1'>
                Quên mật khẩu? 🔒
              </CardTitle>
              <CardText className='mb-2'>
                Hãy nhập email của bạn và chúng tôi sẽ gửi hướng dẫn để đặt lại mật khẩu của bạn
              </CardText>
              <Form className='auth-forgot-password-form mt-2' onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
                <FormGroup>
                  <Label className='form-label' for='login-email'>
                    Email
                  </Label>
                  <Input type='email'
                         id='login-email'
                         placeholder='abc@feaer.vn'
                         name='login-email'
                         onChange={(e) => setEmail(e.target.value)}
                         className={classnames({ 'is-invalid': errors['login-email'] })}
                         innerRef={register({ required: true, validate: value => value !== '' })}
                         autoFocus />
                </FormGroup>
                <Button.Ripple type='submit' color='primary' block>
                  Gửi thông tin
                </Button.Ripple>
              </Form>
              <p className='text-center mt-2'>
                <Link to='/login'>
                  <ChevronLeft className='mr-25' size={14} />
                  <span className='align-middle'>Quay về</span>
                </Link>
              </p>
            </Col>
          </Col>
        </Row>
      </div>
    )
  } else {
    return <Redirect to='/' />
  }
}

export default ForgotPassword
