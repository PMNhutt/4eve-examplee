import {Link, useHistory} from 'react-router-dom'
import { ChevronLeft } from 'react-feather'
import InputPassword from '@components/input-password-toggle'
import {Card, CardBody, CardTitle, CardText, Form, FormGroup, Label, Button, Input} from 'reactstrap'
import {Fragment, useState} from "react"
import OtpInput from 'react-otp-input'
import jwt from "../../../auth/jwt/useJwt"
import classnames from "classnames"
import {useForm} from "react-hook-form"
// ** styles
import '@styles/base/pages/page-auth.scss'
// ** Configs
import themeConfig from '@configs/themeConfig'
import {Slide, toast} from "react-toastify"

const ResetPassword = () => {
  const [password, setPassword] = useState(null)
  const [newPassword, setNewPassword] = useState(null)
  const [otpCode, setOtpCode] = useState(null)
  const [error, setError] = useState(null)
  const { register, errors } = useForm()

  const history = useHistory()

  function handleChange(otp) {
    setOtpCode(otp)
  }
  const ToastContent = ({ message }) => (
      <Fragment>
        <div className={'toastify-header'}>
          <div className='title-wrapper'>
            <h6 className='toast-title font-weight-bold'>Thông báo</h6>
          </div>
        </div>
        <div className='toastify-body'>
          <span>{message}</span>
        </div>
      </Fragment>
  )
  function handleSubmit() {
    if (otpCode) {
      if ((newPassword && password) && newPassword === password) {
        jwt.setPassword(otpCode, newPassword)
            .then(res => {
              if (res && res?.data?.status && res.data.status === true) {
                toast.success(<ToastContent is_success={true} message={'Đổi mật khẩu thành công, vui lòng đăng nhập lại'}/>,
                    { transition: Slide, hideProgressBar: true, autoClose: 2000 })
                history.push('/login')
              } else {
                setError(res.data.message ? res.data.message : 'Hệ thống đang xãy ra sự cố, vui lòng thực hiện lại')
              }
            })
            .catch(error => {
              setError('Hệ thống đang xãy ra sự cố, vui lòng thực hiện lại')
            })
      } else {
        setError('Mật khẩu của bạn không trùng khớp, vui lòng kiểm tra lại')
      }
    } else {
      setError('Mã xác thực không được để trống. Vui lòng nhập đầy đủ thông tin!')
    }
  }

  return (
    <div className='auth-wrapper auth-v1 px-2'>
      <div className='auth-inner py-2'>
        <Card className='mb-0'>
          <CardBody>
            <Link className='brand-logo' to='/' onClick={e => e.preventDefault()}>
              <img src={themeConfig.app.appLogoImage} alt=""/>
            </Link>
            <CardTitle tag='h4' className='mb-1'>
              Nhập thông tin đặt lại mật khẩu 🔒
            </CardTitle>
            <CardText className='mb-2'>Mã xác thực được gửi về email của bạn!<br/>Mật khẩu mới của bạn phải khác với mật khẩu trước đó!</CardText>
            <div className='auth-reset-password-form mt-2'>
              <FormGroup>
                <Label className='form-label'>
                  Mã xác thực OTP
                </Label>
                <OtpInput value={otpCode} onChange={handleChange} numInputs={6} inputStyle={'otpInput'}/>
                <Input className={'opacity-0 p-0 h-0'} name={'optCode'} type={'text'} defaultValue={otpCode ? otpCode : ''}/>  {/* input hide to fix autofill */}
              </FormGroup>
              <FormGroup>
                <Label className='form-label' for='new-password'>
                  Mật khẩu mới
                </Label>
                <Input className={'opacity-0 p-0 h-0'} name={'newPassword'} type={'password'}/> {/* input hide to fix autofill */}
                <InputPassword className={`input-group-merge ${classnames({ 'is-invalid': errors['new-password'] })}`}
                               id='new-password'
                               name={'newPassword'}
                               innerRef={register({ required: true, validate: value => value !== '' })}
                               autoFocus onChange={e => setPassword(e.target.value)}/>
              </FormGroup>
              <FormGroup>
                <Label className='form-label' for='confirm-password'>
                  Nhập lại mật khẩu
                </Label>
                <InputPassword className={`input-group-merge ${classnames({ 'is-invalid': errors['confirm-password'] })}`}
                               id='confirm-password'
                               name={'confirmPassword'}
                               innerRef={register({ required: true, validate: value => value !== '' })}
                               onChange={e => setNewPassword(e.target.value)}/>
              </FormGroup>
              <div className={'text-danger mb-1'}>{error}</div>
              <Button.Ripple color='primary' block onClick={() => handleSubmit()}>
                Cập nhật
              </Button.Ripple>
            </div>
            <p className='text-center mt-2'>
              <Link to='/forgot-password'>
                <ChevronLeft className='mr-25' size={14} />
                <span className='align-middle'>Gửi lại mã OTP</span>
              </Link>
            </p>
          </CardBody>
        </Card>
      </div>
    </div>
  )
}

export default ResetPassword
