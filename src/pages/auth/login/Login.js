import React, { useState, useContext, Fragment } from 'react'
import classnames from 'classnames'
import { useDispatch } from 'react-redux'
import { useForm } from 'react-hook-form'
import { toast, Slide } from 'react-toastify'
import { Link, useHistory } from 'react-router-dom'
import {
    CardTitle,
    CardText,
    Form,
    Input,
    FormGroup,
    Label,
    CustomInput,
    Button,
    Card,
    CardBody
} from 'reactstrap'
import '@styles/base/pages/page-auth.scss'
import {useSkin} from "../../../utility/hooks/useSkin"
import {AbilityContext} from "../../../utility/context/Can"
import {isObjEmpty} from "../../../utility/Utils"
import {handleLogin} from "../../../redux/actions/auth"
import {getHomeRouteForLoggedInUser} from "../../../auth/utils"
import InputPasswordToggle from "../../../@core/components/input-password-toggle"
// ** Configs
import themeConfig from '@configs/themeConfig'
import useJwt from "../../../auth/jwt/useJwt"
const config = useJwt.jwtConfig

const ToastContent = ({ message }) => (
    <Fragment>
        <div className='toastify-header'>
            <div className='title-wrapper'>
                <h6 className='toast-title font-weight-bold'>Notification</h6>
            </div>
        </div>
        <div className='toastify-body'>
            <span>{message}</span>
        </div>
    </Fragment>
)

const Login = () => {
    const [skin] = useSkin()
    const ability = useContext(AbilityContext)
    const dispatch = useDispatch()
    const history = useHistory()
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const { register, errors, handleSubmit } = useForm()

    const onSubmit = () => {
        if (isObjEmpty(errors)) {
            localStorage.removeItem('userData')
            localStorage.removeItem(config.storageTokenKeyName)
            localStorage.removeItem(config.storageRefreshTokenKeyName)
            useJwt
                .login({ username, password })
                .then(res => {
                    let message = res?.data?.message
                    if (!message || message !== 'success') {
                        message = 'The account or password is incorrect, please check again!'
                    }
                    if (res.data.data && res.data.status && res.data.data.user) {
                        const response = res.data.data
                        const user = response.user
                        user.ability = response.ability
                        dispatch(handleLogin(response))
                        ability.update(response.ability)
                        history.push(getHomeRouteForLoggedInUser(response?.user))
                    } else {
                        toast.error(<ToastContent message={ message }/>,
                            { transition: Slide, hideProgressBar: true, autoClose: 2000 })
                    }
                })
                .catch(() => {
                    toast.error(<ToastContent message={'There is an error in the system, please try again later!'}/>,
                        { transition: Slide, hideProgressBar: true, autoClose: 2000 })
                })
        }
    }

    return (
        <div className='auth-wrapper auth-v1 px-2'>
            <div className='auth-inner py-2'>
                <Card className='mb-0'>
                    <CardBody>
                        <Link className='brand-logo' to='/' onClick={e => e.preventDefault()}>
                            <img src={themeConfig.app.appLogoImage} width={150} alt=""/>
                        </Link>
                        <CardTitle tag='h6' className='text-center'>
                            Welcome to the reporting statistics system!
                        </CardTitle>
                        <Form className='auth-login-form mt-2' onSubmit={handleSubmit(onSubmit)}>
                            <FormGroup>
                                <Label className='form-label' for='login-email'>
                                    Username
                                </Label>
                                <Input
                                    autoFocus
                                    type='email'
                                    value={username}
                                    id='login-email'
                                    name='login-email'
                                    placeholder='abc@gmail.vn'
                                    onChange={e => setUsername(e.target.value)}
                                    className={classnames({ 'is-invalid': errors['login-email'] })}
                                    innerRef={register({ required: true, validate: value => value !== '' })}
                                />
                            </FormGroup>
                            <FormGroup>
                                <div className='d-flex justify-content-between'>
                                    <Label className='form-label' for='login-password'>
                                        Password
                                    </Label>
                                    {/*<Link to='/forgot-password'>*/}
                                    {/*    <small>Quên mật khẩu?</small>*/}
                                    {/*</Link>*/}
                                </div>
                                <InputPasswordToggle
                                    value={password}
                                    id='login-password'
                                    name='login-password'
                                    className={`input-group-merge ${classnames({ 'is-invalid': errors['login-password'] })}`}
                                    onChange={e => setPassword(e.target.value)}
                                    innerRef={register({ required: true, validate: value => value !== '' })}
                                />
                            </FormGroup>
                            {/*<FormGroup>*/}
                            {/*    <CustomInput type='checkbox' className='custom-control-Primary' id='remember-me' label='Nhớ mật khẩu' />*/}
                            {/*</FormGroup>*/}
                            <Button.Ripple type='submit' color='primary' block>
                                Log in
                            </Button.Ripple>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        </div>
    )
}

export default Login
