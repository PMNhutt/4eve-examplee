import {Link, useHistory} from 'react-router-dom'
import InputPassword from '@components/input-password-toggle'
import {Card, CardBody, CardTitle, CardText, Form, FormGroup, Label, Button, Input} from 'reactstrap'
import React, {Fragment, useState} from "react"
import jwt from "../../../auth/jwt/useJwt"
import classnames from "classnames"
import {useForm} from "react-hook-form"
// ** styles
import '@styles/base/pages/page-auth.scss'
// ** Configs
import themeConfig from '@configs/themeConfig'
import {getUserData} from "../../../auth/utils"
import {handleLogout} from "../../../redux/actions/auth"
import {useDispatch} from "react-redux"
import {Slide, toast} from "react-toastify"

const ChangePassword = () => {
    const user = getUserData()
    const dispatch = useDispatch()
    const [oldPassword, setOldPassword] = useState(null)
    const [confirmPassword, setConfirmPassword] = useState(null)
    const [newPassword, setNewPassword] = useState(null)
    const [error, setError] = useState(null)
    const {register, errors} = useForm()
    const history = useHistory()

    const ToastContent = ({ message }) => (
        <Fragment>
            <div className={'toastify-header'}>
                <div className='title-wrapper'>
                    <h6 className='toast-title font-weight-bold'>Thông báo</h6>
                </div>
            </div>
            <div className='toastify-body'>
                <span>{message}</span>
            </div>
        </Fragment>
    )
    function handleSubmit() {
        if (newPassword && confirmPassword && oldPassword) {
            console.log(newPassword, confirmPassword, oldPassword)
            if (!(newPassword === oldPassword)) {
                if (confirmPassword === newPassword) {
                    jwt.changePassword(user.id, oldPassword, newPassword)
                        .then(res => {
                            console.log(res)
                            if (res && res.data.status && res.data.status === true) {
                                console.log('changed')
                                toast.success(<ToastContent is_success={true} message={'Thay đổi mật khẩu thành công, vui lòng đăng nhập lại'}/>,
                                    { transition: Slide, hideProgressBar: true, autoClose: 2000 })
                                dispatch(handleLogout())
                                history.push('/login')
                            } else {
                                setError(res.data.message ? res.data.message : 'Sai thông tin, vui lòng nhập lại')
                            }
                        })
                        .catch(error => {
                            setError('Sai thông tin, vui lòng nhập lại')
                        })
                } else {
                    setError('Mật khẩu xác nhận không trùng khớp, vui lòng kiểm tra lại')
                }
            } else {
                setError('Mật khẩu mới không được trùng với mật khẩu hiện tại, vui lòng kiểm tra lại')
            }

        } else {
            setError('Vui lòng nhập đầy đủ thông tin')
        }
    }
    return (
        <div className='auth-wrapper auth-v1 px-2'>
            <div className='auth-inner py-2'>
                <Card className='mb-0'>
                    <CardBody>
                        <Link className='brand-logo' to='/' onClick={e => e.preventDefault()}>
                            <img src={themeConfig.app.appLogoImage} alt=""/>
                        </Link>
                        <CardTitle tag='h4' className='mb-1'>
                            Vui lòng đổi mật khẩu trong lần đăng nhập đầu tiên🔒
                        </CardTitle>
                        <Form autoComplete={'off'}>
                            <div className='auth-reset-password-form mt-2'>
                                <FormGroup>
                                    <Label className='form-label' for='old-password'>
                                        Mật khẩu hiện tại
                                    </Label>
                                    <Input className={'opacity-0 p-0 h-0'} name={'oldPassword'}
                                           type={'password'}/> {/* input hide to fix autofill */}
                                    <InputPassword
                                        className={`input-group-merge ${classnames({'is-invalid': errors['old-password']})}`}
                                        id='old-password'
                                        name={'oldPassword'}
                                        innerRef={register({required: true, validate: value => value !== ''})}
                                        autoFocus onChange={e => setOldPassword(e.target.value)}/>
                                </FormGroup>
                                <FormGroup>
                                    <Label className='form-label' for='new-password'>
                                        Mật khẩu mới
                                    </Label>
                                    <Input className={'opacity-0 p-0 h-0'} name={'newPassword'}
                                           type={'password'}/> {/* input hide to fix autofill */}
                                    <InputPassword
                                        className={`input-group-merge ${classnames({'is-invalid': errors['new-password']})}`}
                                        id='new-password'
                                        name={'newPassword'}
                                        innerRef={register({required: true, validate: value => value !== ''})}
                                        autoFocus onChange={e => setNewPassword(e.target.value)}/>
                                </FormGroup>
                                <FormGroup>
                                    <Label className='form-label' for='confirm-password'>
                                        Nhập lại mật khẩu
                                    </Label>
                                    <InputPassword
                                        className={`input-group-merge ${classnames({'is-invalid': errors['confirm-password']})}`}
                                        id='confirm-password'
                                        name={'confirmPassword'}
                                        innerRef={register({required: true, validate: value => value !== ''})}
                                        onChange={e => setConfirmPassword(e.target.value)}/>
                                </FormGroup>
                                <div className={'text-danger mb-1'}>{error}</div>
                                <Button.Ripple color='primary' block onClick={() => handleSubmit()}>
                                    Cập nhật
                                </Button.Ripple>
                                <div className='misc-inner p-2 p-sm-3'>
                                    <div className='w-100 text-center'>
                                        <Button tag={Link} to='/login' color='primary' className='btn-sm-block mb-1' onClick={() => dispatch(handleLogout())}>
                                            Quay lại đăng nhập
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        </div>
    )
}

export default ChangePassword
