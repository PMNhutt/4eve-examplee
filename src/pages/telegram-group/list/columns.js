// ** Store & Actions
import {deleteTelegramGroup} from '../store/action'
import { store } from '@store/storeConfig/store'

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'
import {Edit3, Tool, MoreVertical, Trash2} from 'react-feather'
import React from 'react'
import {convertDate} from "../../../utility/ConvertDate"


export const columns = ((handleShowPopupUpdate, handleButtonClick) => [
  {
    name: 'ChatId',
    minWidth: '300px',
    sortable: true,
    cell: row => row?.chat_id
  },
  {
    name: 'GroupName',
    minWidth: '150px',
    sortable: true,
    cell: row => row?.telegram_group_name
  },
  {
    name: 'BotName',
    minWidth: '150px',
    cell: row => row?.telegram_token?.name
  },
  {
    name: 'Tree Account Notify',
    center: true,
    cell: row => (
        <>
        <span onClick={() => handleButtonClick(row.agent_details)} className='btn-sm btn-primary'>{row?.agent_details?.length} <Tool/></span>
        </>
    )
  },
  {
    name: 'CreatedDate',
    minWidth: '150px',
    center: 'true',
    sortable: true,
    cell: row => convertDate(row?.created_at)
  },
  {
    name: 'Actions',
    width: '100px',
    cell: row => (
        <UncontrolledDropdown>
          <DropdownToggle tag='div' className='btn btn-sm'>
            <MoreVertical size={14} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem
                onClick={() => handleShowPopupUpdate(row?.id)}
                className='w-100'>
              <Edit3 size={14} className='mr-50' />
              <span className='align-middle'>Edit</span>
            </DropdownItem>
            <DropdownItem
                onClick={() => store.dispatch(deleteTelegramGroup(row?.id))}
                className='w-100'>
              <Trash2 size={14} className='mr-50' />
              <span className='align-middle'>Delete</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
])
