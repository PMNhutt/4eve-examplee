// ** React Import
import React, {useEffect, useState} from 'react'

// ** Custom Components
import Sidebar from '@components/sidebar'
import "flatpickr/dist/themes/material_blue.css"
// ** Utils
import {isObjEmpty} from '@utils'

// ** Third Party Components
import classnames from 'classnames'
import {useForm} from 'react-hook-form'
import {Button, FormGroup, Form, Input, Label} from 'reactstrap'

// ** Store & Actions
import {createTelegramGroup, getTelegramGroupById, getTelegramGroups, getTelegramTokens, updateTelegramGroup} from '../store/action'
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../components/Toast/ToastFail"
import Select from "react-select"
import { store } from '@store/storeConfig/store'

const SidebarCrawlerAccount = ({id, open, toggleSidebar}) => {
    const telegramTokens = store.getState().telegram?.telegramTokens
    const telegramGroup = store.getState().telegram?.telegramGroup
    console.log(store.telegram)
    const params = store.getState().telegram?.params
    const [tokenId, setTokenId] = useState(null)
    // ** Vars
    const {register, errors, handleSubmit} = useForm()

    useEffect(() => {
        if (id) {
            store?.dispatch(getTelegramGroupById(id))
        }
        store?.dispatch(getTelegramTokens())
    }, [])

    useEffect(() => {
        if (telegramGroup?.id && (tokenId === undefined || tokenId === null)) {
            setTokenId(telegramGroup?.token_id)
        }
    }, [telegramGroup])

    // ** Function to handle form submit
    const onSubmit = async (values) => {
        if (!tokenId) {
            toast.error(<ErrorNotificationToast message={'Please select a token for the telegram group!'}/>)
        } else  {
            if (isObjEmpty(errors)) {
                const data = {}
                data.chat_id = values.chat_id
                data.telegram_group_name = values.telegram_group_name
                data.token_id = tokenId
                if (telegramGroup?.id) {
                    updateTelegramGroup(telegramGroup?.id, data).then(res => {
                        if (res?.data?.status) {
                            store.dispatch(getTelegramGroups(params))
                            toggleSidebar()
                            toast.success(<SuccessNotificationToast/>)
                        }
                    }).catch(error => {
                        toast.error(<ErrorNotificationToast message={error.response?.data?.message}/>)
                    })
                } else {
                    createTelegramGroup(data).then(res => {
                        if (res?.data?.status) {
                            store.dispatch(getTelegramGroups(params))
                            toggleSidebar()
                            toast.success(<SuccessNotificationToast message={'Create new successfully!'}/>)
                        }
                    }).catch(error => {
                        toast.error(<ErrorNotificationToast message={error.response?.data?.message}/>)
                    })
                }
            }
        }
    }

    function handleChangeToken(value) {
        setTokenId(value?.id)
    }

    return (
        <Sidebar
            size='lg'
            open={open}
            title={`${id ? 'Edit' : 'Create new'} telegram group`}
            headerClassName='mb-1'
            contentClassName='pt-0 user__side-bar'
            toggleSidebar={toggleSidebar}
        >
            <Form onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
                <FormGroup>
                    <Label className={'label'} for='url'>Chat id <span className='text-danger'>*</span></Label>
                    <Input
                        name='chat_id'
                        id='url'
                        placeholder='Enter chat id'
                        type={'number'}
                        innerRef={register({required: true})}
                        defaultValue={telegramGroup?.chat_id || ''}
                        className={classnames({'is-invalid': errors['chat_id']})}
                    />
                </FormGroup>
                <FormGroup>
                    <Label className={'label'} for='name'>Telegram group name <span className='text-danger'>*</span></Label>
                    <div className={'d-flex align-items-center'}>
                        <Input className={'w-0 p-0 border-0'} />
                        <Input
                            name='telegram_group_name'
                            id='name'
                            placeholder='Enter telegram group name'
                            defaultValue={telegramGroup?.telegram_group_name || ''}
                            innerRef={register({required: true})}
                            className={classnames({'is-invalid': errors['telegram_group_name']})}
                        />
                    </div>
                </FormGroup>
                <FormGroup>
                    <Label className={'label'}>Token <span className='text-danger'>*</span></Label>
                    <Select
                        isClearable={false}
                        className='react-select'
                        classNamePrefix='select'
                        options={telegramTokens}
                        placeholder={'Select token'}
                        getOptionValue={option => option?.id}
                        getOptionLabel={option => option?.name}
                        value={telegramTokens?.find(item => item?.id === telegramGroup?.token_id)}
                        onChange={handleChangeToken}
                    />
                </FormGroup>
                <div className={'row mt-2'}>
                    <Button type='submit' className={'ml-1 mr-1'} color='primary'>Save</Button>
                    <Button type='reset' color='secondary' outline onClick={toggleSidebar}>Cancel</Button>
                </div>
            </Form>
        </Sidebar>
    )
}

export default SidebarCrawlerAccount
