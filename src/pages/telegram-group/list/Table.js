// ** React Imports
import React, {Fragment, useState, useEffect} from 'react'

// ** Invoice List Sidebar
import Sidebar from './Sidebar'

// ** Columns
import {columns} from './columns'

// ** Store & Actions
import { getTelegramGroups } from '../store/action'
import {useDispatch, useSelector} from 'react-redux'

// ** Third Party Components
import {ChevronDown} from 'react-feather'
import DataTable from 'react-data-table-component'
import {Button, Card} from 'reactstrap'
import {NoDataComponent} from "../../../components/NoDataComponent"
import {useHistory, useLocation} from 'react-router-dom'
import {Loading} from "../../../components/Loading"
import ReactPaginate from "react-paginate"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/user.scss'
import {checkPermissionByScreen} from "../../users/profile/store/action"
import {PERMISSION_SCREEN_ENUM} from "../../../enum/permission-screens-constant"
import ModalAgentDetails from "../components/modal-tree-account"


const TelegramGroupList = () => {
    // ** Store Vars
    const dispatch = useDispatch()
    const store = useSelector(state => state.telegram)
    const history = useHistory()
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)
    const [checkPermission, setCheckPermission] = useState(null)
    const [agentDetails, setAgentDetails] = useState([])
    const [isOpenModal, setIsOpenModal] = useState(false)

    const rowPerPage = 10

    // ** States
    const [currentPage, setCurrentPage] = useState(searchParams?.get('page') ? searchParams?.get('page') : 1)
    const [sidebarOpen, setSidebarOpen] = useState(false)
    const [telegramGroupIdUpdate, setTelegramGroupIdUpdate] = useState(null)

    // ** Function to toggle sidebar
    const toggleSidebar = () => {
        if (sidebarOpen) {
            setTelegramGroupIdUpdate(null)
        }
        setSidebarOpen(!sidebarOpen)
    }

    // ** Get data on mount
    useEffect(() => {
        const params = {page: currentPage, perPage: rowPerPage}
        dispatch(getTelegramGroups(params))
        checkPermissionByScreen(PERMISSION_SCREEN_ENUM.telegram_group).then(res => {
            setCheckPermission(res)
        })
    }, [])

    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.page) {
            params.page = filterParams.page
        }
        const urlSearchParams = new URLSearchParams(params)
        history.replace({pathname: location.pathname, search: urlSearchParams.toString()})
    }

    // ** Table data to render
    const dataToRender = () => {
        if (store.telegramGroups?.length > 0) {
            return store.telegramGroups
        } else {
            return []
        }
    }

    // ** Custom Pagination
    const CustomPagination = () => {
        const count = Number(Math.ceil(store.totalRecords / rowPerPage))
        if (count > 1) {
            return (
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    pageCount={count || 1}
                    activeClassName='active'
                    forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                    onPageChange={handlePagination}
                    pageClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next'}
                    previousClassName={'page-item prev'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
                />
            )
        } else return <></>
    }

    const handlePagination = page => {
        const params = {page: page.selected + 1, perPage: rowPerPage}
        dispatch(getTelegramGroups(params))
        checkParams(params)
        setCurrentPage(page.selected + 1)
    }

    const handleShowPopupUpdate = id => {
        setSidebarOpen(true)
        setTelegramGroupIdUpdate(id)
    }

    const handleButtonClick = agentDetails => {
        setAgentDetails(agentDetails)
        setIsOpenModal(true)
    }

    const handleCloseModal = () => {
        setAgentDetails([])
        setIsOpenModal(false)
    }
    return (
        <Fragment>
            {
                store.loading && <Loading/>
            }
            <Card id={'userContainer'}>
                {
                    checkPermission?.create &&
                    <div className={'ml-auto mb-1'}>
                        <Button.Ripple color='primary' onClick={toggleSidebar}>Create new</Button.Ripple>
                    </div>
                }

                <DataTable
                    noHeader
                    responsive
                    persistTableHead
                    paginationServer
                    columns={columns(handleShowPopupUpdate, handleButtonClick)}
                    sortIcon={<ChevronDown/>}
                    className='react-dataTable px-0'
                    data={dataToRender()}
                    noDataComponent={<NoDataComponent message={'Currently, no telegram groups yet'}/>}
                />
                <div>
                    {
                        CustomPagination()
                    }
                </div>
            </Card>
            {
                sidebarOpen &&
                <Sidebar id={telegramGroupIdUpdate} open={sidebarOpen} toggleSidebar={toggleSidebar}/>
            }
            {
                isOpenModal &&
                <ModalAgentDetails isOpen={isOpenModal}
                                   newAgentDetails={agentDetails}
                                   dispatch={dispatch}
                                   toggle={handleCloseModal}/>
            }
        </Fragment>
    )
}

export default TelegramGroupList
