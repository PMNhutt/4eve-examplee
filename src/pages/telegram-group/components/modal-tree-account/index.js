import {
    Button,
    Modal,
    ModalBody,
    ModalHeader
} from "reactstrap"
import React, {useEffect, useState} from "react"
import {Trash2} from "react-feather"
// styles
import './style/style.scss'
import {Loading} from "../../../../components/Loading"
import {deleteAgentDetail, getTelegramGroups} from "../../store/action"
import {toast} from "react-toastify"
import ErrorNotificationToast from "../../../../components/Toast/ToastFail"
import SuccessNotificationToast from "../../../../components/Toast/ToastSuccess"
import { store } from '@store/storeConfig/store'

const ModalAgentDetails = ({isOpen, toggle, dispatch, newAgentDetails}) => {
    const [agentDetails, setAgentDetails] = useState(newAgentDetails)
    const [isLoading, setIsLoading] = useState(false)
    const [modalOpen, setModalOpen] = useState(false)
    const [item, setItem] = useState({})

    useEffect(() => {
    }, [agentDetails])

    const handleDeleteAgentDetail = async (item) => {
        if (isLoading) {
            return
        }
        const response = await deleteAgentDetail(item.id)
        setIsLoading(false)
        if (response && response.status) {
            toast.success(<SuccessNotificationToast message={'Remove notify agent from telegram group successful!'}/>)
            const newSelectedAccounts = agentDetails?.filter(agent => agent?.id !== item?.id)
            if (newSelectedAccounts?.length === 0) {
                handleCancel()
            } else {
                setAgentDetails([...newSelectedAccounts])
            }
            dispatch(getTelegramGroups(store.getState().telegram.params))
            setItem({})
        } else {
            toast.error(<ErrorNotificationToast message={'System has problem, please contact IT support team!'}/>)
        }
    }

    const handleCancel = () => {
        toggle()
    }

    return (
        <Modal isOpen={isOpen} toggle={handleCancel} centered={true} className={'modal-config'}>
            {
                isLoading && <Loading/>
            }
            <ModalHeader toggle={handleCancel}>
                <span className='align-middle'>Tree Account Notify</span>
            </ModalHeader>
            <ModalBody>
                <h5>List agents</h5>
                <div className={'config-agent-border'}>
                    {
                        agentDetails?.length > 0 &&
                        agentDetails?.map((agent, index) => {
                            return (
                                <div key={index} className={'d-flex justify-content-between align-items-center selected-agent-item'}>
                                    <span>{agent?.domain}</span>
                                    <span>{agent?.crawler_account}</span>
                                    <span>{agent?.account_id}</span>
                                    <span>{agent?.config_notify?.name}</span>
                                    <Button color={'primary'} className={'btn-delete-agent'} onClick={() => {
                                        setModalOpen(true)
                                        setItem(agent)
                                    }}><Trash2 size={15}/></Button>
                                </div>
                            )
                        })
                    }
                </div>
                <Modal isOpen={modalOpen} toggle={() => setModalOpen(false)} centered={true}>
                    <div className={'modal-confirm p-3'}>
                        <p className={'modal-confirm__title'}>Do you want to delete?</p>
                        <div>
                            <Button className={'btn btn-primary'} color={'primary'} onClick={() => {
                                setModalOpen(false)
                                setItem({})
                            }}>No</Button>
                            <Button className={'btn btn-primary ml-2'} color={'primary'} onClick={() => {
                                handleDeleteAgentDetail(item).then()
                                setIsLoading(true)
                                setModalOpen(false)
                            }}>Yes</Button>
                        </div>
                    </div>
                </Modal>
                <div className={'d-flex align-items-center justify-content-end mb-1'}>
                    <Button color={'primary'} outline={true} className={'mt-2 w-fit-content'} onClick={handleCancel}>Back</Button>
                </div>
            </ModalBody>
        </Modal>
    )
}

export default ModalAgentDetails
