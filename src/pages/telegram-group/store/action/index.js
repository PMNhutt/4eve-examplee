import instances from "../../../../@core/plugin/axios"

export const GET_TELEGRAM_GROUPS_BEGIN = 'GET_TELEGRAM_GROUPS_BEGIN'
export const GET_TELEGRAM_GROUPS_SUCCESS = 'GET_TELEGRAM_GROUPS_SUCCESS'
export const GET_TELEGRAM_GROUPS_FAIL = 'GET_TELEGRAM_GROUPS_FAIL'

export const GET_TELEGRAM_TOKENS_BEGIN = 'GET_TELEGRAM_TOKENS_BEGIN'
export const GET_TELEGRAM_TOKENS_SUCCESS = 'GET_TELEGRAM_TOKENS_SUCCESS'
export const GET_TELEGRAM_TOKENS_FAIL = 'GET_TELEGRAM_TOKENS_FAIL'

export const GET_COMPANIES_BEGIN = 'GET_COMPANIES_BEGIN'
export const GET_COMPANIES_SUCCESS = 'GET_COMPANIES_SUCCESS'
export const GET_COMPANIES_FAIL = 'GET_COMPANIES_FAIL'

export const GET_TELEGRAM_GROUP_BY_ID_BEGIN = 'GET_TELEGRAM_GROUP_BY_ID_BEGIN'
export const GET_TELEGRAM_GROUP_BY_ID_SUCCESS = 'GET_TELEGRAM_GROUP_BY_ID_SUCCESS'
export const GET_TELEGRAM_GROUP_BY_ID_FAIL = 'GET_TELEGRAM_GROUP_BY_ID_FAIL'

export const DELETE_TELEGRAM_GROUP_BY_ID_BEGIN = 'DELETE_TELEGRAM_GROUP_BY_ID_BEGIN'
export const DELETE_TELEGRAM_GROUP_BY_ID_SUCCESS = 'DELETE_TELEGRAM_GROUP_BY_ID_SUCCESS'
export const DELETE_TELEGRAM_GROUP_BY_ID_FAIL = 'DELETE_TELEGRAM_GROUP_BY_ID_FAIL'

export const getTelegramGroups = (params) => {
    const config = {params: params}
    return async dispatch => {
        dispatch({
            type: GET_TELEGRAM_GROUPS_BEGIN
        })
        await instances.get('/admin/telegram-group', config).then(response => {
            dispatch({
                type: GET_TELEGRAM_GROUPS_SUCCESS,
                telegramGroups: response.data?.data?.data,
                totalRecords: response.data?.data?.totalRecords,
                params
            })
        }).catch(error => {
            dispatch({
                type: GET_TELEGRAM_GROUPS_FAIL,
                errorMessage: error,
                params
            })
        })
    }
}
export const getCompanies = () => {
    return async dispatch => {
        dispatch({
            type: GET_COMPANIES_BEGIN
        })
        await instances.get('admin/company/all').then(response => {
            dispatch({
                type: GET_COMPANIES_SUCCESS,
                companies: response.data?.data?.companies,
                totalRecords: response.data?.data?.totalRecords
            })
        }).catch(error => {
            dispatch({
                type: GET_COMPANIES_FAIL,
                errorMessage: error
            })
        })
    }
}

export const getTelegramGroupById = (id) => {
    return async dispatch => {
        dispatch({
            type: GET_TELEGRAM_GROUP_BY_ID_BEGIN
        })
        await instances.get(`/admin/telegram-group/${id}`).then(response => {
            dispatch({
                type: GET_TELEGRAM_GROUP_BY_ID_SUCCESS,
                telegramGroup: response.data?.data
            })
        }).catch(error => {
            dispatch({
                type: GET_TELEGRAM_GROUP_BY_ID_FAIL,
                errorMessage: error
            })
        })
    }
}

export const createTelegramGroup = async (data) => {
    return await instances.post('/admin/telegram-group', data)
}

export const updateTelegramGroup = async (id, data) => {
    return await instances.put(`/admin/telegram-group/${id}`, data)
}

export const deleteTelegramGroup = (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DELETE_TELEGRAM_GROUP_BY_ID_BEGIN
        })
        await instances.delete(`/admin/telegram-group/${id}`)
            .then(() => {
                dispatch({
                    type: DELETE_TELEGRAM_GROUP_BY_ID_SUCCESS
                })
            })
            .then(() => {
                dispatch(getTelegramGroups(getState().telegram.params))
            })
            .catch(error => {
                dispatch({
                    type: DELETE_TELEGRAM_GROUP_BY_ID_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const deleteAgentDetail = (id) => {
    return instances.delete(`/admin/agent/detail/${id}`)
}

export const getTelegramTokens = () => {
    return async dispatch => {
        dispatch({
            type: GET_TELEGRAM_TOKENS_BEGIN
        })
        await instances.get('/admin/telegram/all').then(response => {
            dispatch({
                type: GET_TELEGRAM_TOKENS_SUCCESS,
                data: response.data?.data
            })
        }).catch(error => {
            dispatch({
                type: GET_TELEGRAM_TOKENS_FAIL,
                errorMessage: error
            })
        })
    }
}
