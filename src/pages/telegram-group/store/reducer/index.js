import {
  DELETE_TELEGRAM_GROUP_BY_ID_BEGIN,
  DELETE_TELEGRAM_GROUP_BY_ID_FAIL,
  DELETE_TELEGRAM_GROUP_BY_ID_SUCCESS,
  GET_TELEGRAM_GROUP_BY_ID_BEGIN,
  GET_TELEGRAM_GROUP_BY_ID_FAIL,
  GET_TELEGRAM_GROUP_BY_ID_SUCCESS,
  GET_TELEGRAM_GROUPS_BEGIN,
  GET_TELEGRAM_GROUPS_FAIL,
  GET_TELEGRAM_GROUPS_SUCCESS,
  GET_TELEGRAM_TOKENS_BEGIN,
  GET_TELEGRAM_TOKENS_FAIL,
  GET_TELEGRAM_TOKENS_SUCCESS,
  GET_COMPANIES_BEGIN,
  GET_COMPANIES_SUCCESS,
  GET_COMPANIES_FAIL
} from "../action"

// ** Initial State
const initialState = {
  loading: false,
  telegramGroups: [],
  totalRecords: 0,
  totalRecordCompanies: 0,
  telegramGroup: null,
  errorMessage: null,
  companies: [],
  telegramTokens: [],
  params: {}
}

const telegram = (state = initialState, action) => {
  switch (action.type) {
    case GET_TELEGRAM_GROUPS_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_TELEGRAM_GROUPS_SUCCESS:
      return {
        ...state,
        telegramGroups: action.telegramGroups,
        totalRecords: action.totalRecords,
        params: action.params,
        loading: false
      }
    case GET_TELEGRAM_GROUPS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false,
        params: action.params
      }
    case GET_TELEGRAM_GROUP_BY_ID_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_TELEGRAM_GROUP_BY_ID_SUCCESS:
      return {
        ...state,
        telegramGroup: action.telegramGroup,
        loading: false
      }
    case GET_TELEGRAM_GROUP_BY_ID_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case DELETE_TELEGRAM_GROUP_BY_ID_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case DELETE_TELEGRAM_GROUP_BY_ID_SUCCESS:
      return {
        ...state,
        loading: false
      }
    case DELETE_TELEGRAM_GROUP_BY_ID_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_COMPANIES_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_COMPANIES_SUCCESS:
      return {
        ...state,
        companies: action.companies,
        totalRecordCompanies: action.totalRecordCompanies,
        loading: false
      }
    case GET_COMPANIES_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_TELEGRAM_TOKENS_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_TELEGRAM_TOKENS_SUCCESS:
      return {
        ...state,
        telegramTokens: action.data,
        loading: false
      }
    case GET_TELEGRAM_TOKENS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    default:
      return { ...state }
  }
}
export default telegram
