import instances from "../../../../../@core/plugin/axios"
import {getUserData} from "../../../../../auth/utils"
export const GET_PROFILE = 'GET_PROFILE'
// ** Get all Data
export const getMyProfile = () => {
  return async dispatch => {
    await instances.get('/user/profile').then(response => {
      dispatch({
        type: GET_PROFILE,
        data: response.data
      })
    })
  }
}

export const checkPermissionByScreen = async (permissionScreen) => {
  const userRole = getUserData()?.role
  let permissionCheck = {view: false, create: false, delete: false, edit: false, import: false, export: false}
  if (userRole?.name === 'ADMIN') {
    permissionCheck = {view: true, create: true, delete: true, edit: true, import: true, export: true}
  } else {
    if (userRole?.id) {
      await instances.get(`/user/role/${userRole?.id}`).then(res => {
        if (res?.data?.data) {
          const permissions = res?.data?.data?.permissions
          if (permissions?.length > 0) {
            permissions?.forEach(permission => {
              if (permission?.name?.includes(permissionScreen)) {
                switch (permission?.name?.split('_')?.[0]) {
                  case 'view':
                    permissionCheck.view = true
                    break
                  case 'create':
                    permissionCheck.create = true
                    break
                  case 'edit':
                    permissionCheck.edit = true
                    break
                  case 'delete':
                    permissionCheck.delete = true
                    break
                  case 'import':
                    permissionCheck.import = true
                    break
                  case 'export':
                    permissionCheck.export = true
                    break
                  default:
                    break
                }
              }
            })
          }
        }
      })
    }
  }
  return permissionCheck
}
