import {Button, Col, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap"
import classnames from "classnames"
import InputPassword from '@components/input-password-toggle'
import {useForm} from "react-hook-form"
import React, {Fragment, useState} from "react"
import jwt from "../../../../../../../auth/jwt/useJwt"
import {Slide, toast} from "react-toastify"

const ToastContent = ({message}) => (
    <Fragment>
        <div className='toastify-header'>
            <div className='title-wrapper'>
                <h6 className='toast-title font-weight-bold'>Thông báo</h6>
            </div>
        </div>
        <div className='toastify-body'>
            <span>{message}</span>
        </div>
    </Fragment>
)


const ChangePasswordModal = (props) => {
    const {register, errors} = useForm()
    const [oldPassword, setOldPassword] = useState(null)
    const [newPassword, setNewPassword] = useState(null)
    const [confirmPassword, setConfirmPassword] = useState(null)
    const [error, setError] = useState('')

    const handleSubmit = () => {
        if (props?.user?.id) {
            if (newPassword && oldPassword && confirmPassword) {
                if (oldPassword !== newPassword) {
                    if (confirmPassword === newPassword) {
                        jwt.changePassword(props?.user?.id, oldPassword, newPassword)
                            .then(res => {
                                console.log(res)
                                if (res?.data && res?.data?.status) {
                                    toast.success(<ToastContent message={'Mật khẩu của bạn đã được thay đổi'}/>,
                                        {transition: Slide, hideProgressBar: true, autoClose: 2000})
                                    props?.toggle()
                                } else {
                                    setError('Mật khẩu của bạn không chính xác, vui lòng kiểm tra lại')
                                }
                            })
                            .catch(error => {
                                setError('Hệ thống xảy ra lỗi, vui lòng thử lại sau!')
                            })
                    } else {
                        setError('Mật khẩu của bạn không trùng khớp, vui lòng kiểm tra lại')
                    }
                } else {
                    setError('Mật khẩu mới không được trùng với mật khẩu hiện tại')
                }
            } else {
                setError('Vui lòng nhập đầy đủ thông tin!')
            }
        }
    }

    return (
        <Modal isOpen={props?.isOpen} toggle={props?.toggle} centered={true} id={'changePasswordModal'}>
            <ModalHeader toggle={props?.toggle} tag={'div'}>
                Đổi mật khẩu
            </ModalHeader>
            <ModalBody>
                <Form autoComplete={'off'}>
                    <Row>
                        <Col sm={12} md={12} className={'px-1'}>
                            <FormGroup>
                                <Label className='form-label' for='old-password'>
                                    Mật khẩu cũ <span className='text-danger'>*</span>
                                </Label>
                                <div className={'d-flex'}>
                                    <Input type={'password'} name={'old-password'} className={`w-0 opacity-0 cursor-default p-0`}
                                           placeholder={''}/>
                                    <InputPassword
                                        name={'old-password'}
                                        className={`w-100 input-group-merge ${classnames({'is-invalid': errors['old-password']})}`}
                                        id='old-password'
                                        autoComplete={'off'}
                                        placeholder={''}
                                        innerRef={register({required: true, validate: value => value !== ''})}
                                        autoFocus
                                        onChange={e => {
                                            setOldPassword(e.target.value)
                                        }}/>
                                </div>
                            </FormGroup>
                        </Col>
                        <Col sm={12} md={6} className={'px-1'}>
                            <FormGroup>
                                <Label className='form-label' for='new-password'>
                                    Mật khẩu mới <span className='text-danger'>*</span>
                                </Label>
                                <div className={'d-flex'}>
                                    <Input type={'password'} name={'new-password'} className={`w-0 opacity-0 cursor-default p-0`}
                                           placeholder={''}/>
                                    <InputPassword
                                        name={'new-password'}
                                        className={`input-group-merge ${classnames({'is-invalid': errors['new-password']})}`}
                                        id='new-password'
                                        autoComplete={'off'}
                                        innerRef={register({required: true, validate: value => value !== ''})}
                                        autoFocus onChange={e => {
                                        setNewPassword(e.target.value)
                                    }}/>
                                </div>
                            </FormGroup>
                        </Col>
                        <Col sm={12} md={6} className={'px-1'}>
                            <FormGroup>
                                <Label className='form-label' for='confirm-password'>
                                    Nhập lại mật khẩu mới <span className='text-danger'>*</span>
                                </Label>
                                <div className={'d-flex'}>
                                    <Input type={'password'} name={'confirm-password'} className={`w-0 opacity-0 cursor-default p-0`}
                                           placeholder={''}/>
                                    <InputPassword
                                        className={`input-group-merge ${classnames({'is-invalid': errors['confirm-password']})}`}
                                        name={'confirm-password'}
                                        id='confirm-password'
                                        autoComplete={'off'}
                                        innerRef={register({required: true, validate: value => value !== ''})}
                                        autoFocus onChange={e => {
                                        setConfirmPassword(e.target.value)
                                    }}/>
                                </div>
                            </FormGroup>
                        </Col>
                    </Row>
                    <div className={'text-danger'}>{error}</div>
                </Form>
            </ModalBody>
            <ModalFooter>
                <Button color='primary' onClick={() => handleSubmit()}>Lưu</Button>
            </ModalFooter>
        </Modal>
    )
}

export default ChangePasswordModal