import React, {useEffect, useState} from 'react'
import {Form, Card, Button, Label, Input, FormGroup, Row, Col} from 'reactstrap'
import {replaceUrlMedia} from "../../../../../utility/replaceUrlMedia"
import classnames from "classnames"
import {useForm} from "react-hook-form"
import Flatpickr from "react-flatpickr"
import Select from "react-select"
import {useDispatch, useSelector} from "react-redux"
import {updateUser} from "../../../user/store/action"
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../../../components/Toast/ToastSuccess"
import {Image, X} from "react-feather"
import {DragDrop} from "@uppy/react"
import Uppy from "@uppy/core"
import thumbnailGenerator from "@uppy/thumbnail-generator"
import { isObjEmpty } from '@utils'
import ChangePasswordModal from "./components/ChangePasswordModal"

// ** styles
import "flatpickr/dist/themes/material_blue.css"
import './styles/styles.scss'

// ** images
import avatar from '@src/assets/images/imgs/im_user.png'

const registerOptions = {
  province: { required: "Vui lòng nhập thành phố" },
  district: { required: "Vui lòng nhập quận huyện" },
  ward: { required: "Vui lòng nhập phường xã" },
  role: { required: "Vui lòng chọn nhóm người dùng" }
}

const genderOptions = [
  { value: '', label: 'Lựa chọn' },
  { value: 'female', label: 'Nữ' },
  { value: 'male', label: 'Nam' },
  { value: 'other', label: 'Khác' }
]

const ProfileHeader = ({ data }) => {
  const [editData, setEditData] = useState(data)
  const [isOpenModal, setIsOpen] = useState(false)
  const store = useSelector(state => state.users)

  const { register, errors } = useForm()
  const dispatch = useDispatch()

  const onSubmit = async () => {
    if (isObjEmpty(errors)) {
        if (editData?.avatar_file) {
            const formData = new FormData()
            formData.append(`image`, editData.avatar_file)
            // const image = await uploadImage(formData)
            // if (image?.data?.data) {
            //     editData.avatar = image?.data?.data?.url
            //     editData.avatar_file = null
            // }
        }
        dispatch(
            updateUser({
                name: editData?.name,
                phone: editData?.phone,
                address: editData?.address,
                dob: editData?.dob?.toString(),
                gender: editData?.gender,
                avatar: editData?.avatar
            }, editData?.id)
        )
        toast.success(<SuccessNotificationToast message={'Cập nhật thông tin cá nhân thành công!'}/>)
    }
  }

  const uppy = new Uppy({
    meta: { type: 'avatar' },
    autoProceed: true,
    restrictions: { allowedFileTypes: ['image/*'] }
  })
  uppy.use(thumbnailGenerator)
  uppy.on('thumbnail:generated', (file, preview) => {
    data.avatar = preview
    data.avatar_file = file.data
    setEditData({...data})
  })

  const renderPreview = () => {
    return (
        <div className={'add-image__item-container profile-avatar mb-2'}>
          <img className={'add-image__item-image'} src={data?.avatar ? replaceUrlMedia(data?.avatar) : avatar} alt="Lựa chọn hình ảnh"/>
          <div className={'d-flex align-items-center justify-content-center box-shadow-1 profile-upload-image'}>
            <Image />
          </div>
          <DragDrop uppy={uppy}/>
        </div>
    )
  }

  return (
      <>
        <Card className='profile-header mb-2'>
          <div className='position-relative p-2'>
            <h2 className={'font-medium-3 mb-1'}>Thông tin cá nhân</h2>
            <Row className='mx-0 profile-img-container justify-content-between'>
              <Col md={3} className='col-12 profile-info-section p-1 d-flex align-items-center justify-content-center flex-column'>
                {renderPreview()}
                <p className={'font-medium-2 mt-25 mb-0'}>{data?.name}</p>
                <p className={'font-medium-1 mt-25 mb-0'}>{data?.role?.name}</p>
              </Col>
              <Col md={8} className='col-12 profile-title profile-info-section p-1'>
                  <Form autoComplete={'off'}>
                      <Row>
                          <Col md='6' sm='12'>
                              <FormGroup>
                                  <Label for='full-name'> Họ và tên <span className='text-danger'>*</span></Label>
                                  <Input
                                      name='full-name'
                                      id='full-name'
                                      placeholder='Nhập họ và tên'
                                      innerRef={register({ required: true })}
                                      defaultValue={data?.name ? data?.name : ''}
                                      onChange={e => {
                                          data.name = e.target.value
                                          setEditData({...data})
                                      }}
                                      className={classnames({ 'is-invalid': errors['full-name'] })}
                                  />
                              </FormGroup>
                          </Col>
                          <Col md='6' sm='12'>
                              <FormGroup>
                                  <Label className={'label'} for='phone-number'>
                                      Số điện thoại <span className='text-danger'>*</span>
                                  </Label>
                                  <Input
                                      name='phone-number'
                                      id='phone-number'
                                      type='number'
                                      placeholder='Nhập số điện thoại'
                                      innerRef={register({ required: true })}
                                      defaultValue={data?.phone ? data?.phone : ''}
                                      className={classnames({ 'is-invalid': errors['phone-number'] })}
                                  />
                              </FormGroup>
                          </Col>
                          <Col md='6' sm='12'>
                              <FormGroup>
                                  <Label className={'label'} for='email'>
                                      Email <span className='text-danger'>*</span>
                                  </Label>
                                  <Input
                                      type='email'
                                      name='email'
                                      id='email'
                                      disabled={true}
                                      defaultValue={data?.email ? data?.email : ''}
                                      className={'cursor-not-allowed'}
                                  />
                              </FormGroup>
                          </Col>
                          <Col md='6' sm='12'>
                              <FormGroup>
                                  <Label for='gender'>Giới tính</Label>
                                  <Select
                                      isClearable={false}
                                      className='react-select'
                                      classNamePrefix='select'
                                      name='gender'
                                      options={genderOptions}
                                      value={genderOptions?.find(option => option?.value === (data?.gender ? data?.gender : ''))}
                                      placeholder={'Chọn Giới Tính'}
                                      onChange={gender => {
                                          data.gender = gender?.value
                                          setEditData({...data})
                                      }}
                                  />
                              </FormGroup>
                          </Col>
                          <Col md='6' sm='12'>
                              <FormGroup>
                                  <Label className={'label'} for='dob'>Ngày sinh</Label>
                                  <Flatpickr id={'dob'} className='form-control form-control__date'
                                             value={data?.dob}
                                             onChange={(_, date) => {
                                                 data.dob = date
                                                 setEditData({...data})
                                             }}/>
                              </FormGroup>
                          </Col>
                          <Col sm='12' md={6}>
                              <FormGroup>
                                  <Label className={'label d-flex align-iretems-center justify-content-between'} for='email'>
                                      <span>Mật khẩu <span className={'text-danger'}>*</span></span><span className={'cursor-pointer text-primary text-underline'} onClick={() => setIsOpen(true)}>Đổi mật khẩu</span>
                                  </Label>
                                  <Input
                                      type='password'
                                      name='password'
                                      id='password'
                                      placeholder='********'
                                      disabled={true}
                                      className={'cursor-not-allowed'}
                                  />
                              </FormGroup>
                          </Col>
                          <Col sm='12' md={12}>
                              <FormGroup>
                                  <Label className={'label'} for='email'>
                                      Địa chỉ <span className='text-danger'>*</span>
                                  </Label>
                                  <Input
                                      type='text'
                                      name='address'
                                      id='address'
                                      placeholder='Nhập địa chỉ'
                                      innerRef={register({ required: true })}
                                      defaultValue={data?.address ? data?.address : ''}
                                      onChange={e => {
                                          data.address = e.target.value
                                          setEditData({...data})
                                      }}
                                      className={classnames({ 'is-invalid': errors['address'] })}
                                  />
                              </FormGroup>
                          </Col>
                          {/*<Col md='4' sm='12'>*/}
                          {/*    <FormGroup>*/}
                          {/*        <Label for='role'>Tỉnh/Thành <span className='text-danger'>*</span></Label>*/}
                          {/*        <Select*/}
                          {/*            isClearable={false}*/}
                          {/*            className='react-select'*/}
                          {/*            classNamePrefix='select'*/}
                          {/*            name='province'*/}
                          {/*            options={store?.provinces}*/}
                          {/*            value={data?.province ? data?.province : ''}*/}
                          {/*            getOptionValue={option => option?.id}*/}
                          {/*            getOptionLabel={option => option?.name}*/}
                          {/*            placeholder={'Chọn Thành Phố'}*/}
                          {/*            rules={registerOptions.province}*/}
                          {/*            onChange={province => {*/}
                          {/*                changeProvince(province)*/}
                          {/*                data.province = province*/}
                          {/*                setEditData({...data})*/}
                          {/*                dispatch(getDistricts(province?.id))*/}
                          {/*            }}*/}
                          {/*        />*/}
                          {/*    </FormGroup>*/}
                          {/*</Col>*/}
                          {/*<Col md='4' sm='12'>*/}
                          {/*    <FormGroup>*/}
                          {/*        <Label for='role'>Quận/Huyện <span className='text-danger'>*</span></Label>*/}
                          {/*        <Select*/}
                          {/*            isClearable={false}*/}
                          {/*            className='react-select'*/}
                          {/*            classNamePrefix='select'*/}
                          {/*            name='district'*/}
                          {/*            options={store?.districts}*/}
                          {/*            value={data?.district ? data?.district : ''}*/}
                          {/*            getOptionValue={option => option?.id}*/}
                          {/*            getOptionLabel={option => option?.name}*/}
                          {/*            placeholder={'Chọn quận huyện'}*/}
                          {/*            onChange={district => {*/}
                          {/*                changeDistrict(district)*/}
                          {/*                data.district = district*/}
                          {/*                setEditData({...data})*/}
                          {/*                dispatch(getWards(district?.id))*/}
                          {/*            }}*/}
                          {/*        />*/}
                          {/*    </FormGroup>*/}
                          {/*</Col>*/}
                          {/*<Col md='4' sm='12'>*/}
                          {/*    <FormGroup>*/}
                          {/*        <Label for='role'>Phường/Xã <span className='text-danger'>*</span></Label>*/}
                          {/*        <Select*/}
                          {/*            isClearable={false}*/}
                          {/*            className='react-select'*/}
                          {/*            classNamePrefix='select'*/}
                          {/*            name='ward'*/}
                          {/*            options={store?.wards}*/}
                          {/*            value={data?.ward ? data?.ward : ''}*/}
                          {/*            getOptionValue={option => option?.id}*/}
                          {/*            getOptionLabel={option => option?.name}*/}
                          {/*            placeholder={'Chọn phường xã'}*/}
                          {/*            onChange={ward => {*/}
                          {/*                changeWard(ward)*/}
                          {/*                data.ward = ward*/}
                          {/*                setEditData({...data})*/}
                          {/*            }}*/}
                          {/*        />*/}
                          {/*    </FormGroup>*/}
                          {/*</Col>*/}
                      </Row>
                  </Form>

              </Col>
            </Row>
          </div>
          <div className={'d-flex justify-content-end m-2'}>
            <Button color={'primary'} onClick={() => onSubmit()}>Lưu</Button>
          </div>
        </Card>
          <ChangePasswordModal isOpen={isOpenModal} toggle={() => setIsOpen(false)} user={data}/>
      </>
  )
}

export default ProfileHeader
