import { Fragment, useState, useEffect } from 'react'
import { Row, Col, Button } from 'reactstrap'
import '@styles/react/pages/page-profile.scss'
import * as PropTypes from "prop-types"
import ProfileHeader from "../components/ProfileHeader/ProfileHeader"
import {getMyProfile} from "../store/action"
import {useDispatch, useSelector} from "react-redux"

function ProfilePoll(props) {
  return null
}

ProfilePoll.propTypes = {data: PropTypes.any}
const Profile = () => {
  const [block, setBlock] = useState(false)
  const store = useSelector(state => state.profiles)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getMyProfile())
  }, [dispatch])

  return (
    <Fragment>
      {store.user !== null ? (
        <div id='user-profile'>
          <Row>
            <Col sm='12'>
              <ProfileHeader data={store?.user?.user} />
            </Col>
          </Row>
          <section id='profile-info'>
            <Row>
              <Col lg={{ size: 3, order: 1 }} sm={{ size: 12 }} xs={{ order: 2 }}>
              </Col>
              <Col lg={{ size: 6, order: 2 }} sm={{ size: 12 }} xs={{ order: 1 }}>
              </Col>
              <Col lg={{ size: 3, order: 3 }} sm={{ size: 12 }} xs={{ order: 3 }}>
              </Col>
            </Row>
          </section>
        </div>
      ) : null}
    </Fragment>
  )
}

export default Profile
