// ** React Imports
import { Link } from 'react-router-dom'

// ** Store & Actions
import {changeStatusPermission} from '../store/action'
import { store } from '@store/storeConfig/store'

// ** Third Party Components
import {MoreVertical, FileText, Trash2, Activity} from 'react-feather'
import {convertDate} from '../../../../utility/ConvertDate'
import React from "react"
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap"

export const columns = [
  {
    name: 'PermissionGroupName',
    width: '40%',
    selector: 'name',
    sortable: true,
    cell: row => (
      <div className='d-flex justify-content-left align-items-center'>
        <Link
            to={`/permission/edit/${row.id}`}
            className='user-name text-truncate mb-0'>
          <span className='font-weight-bold'>{row?.name}</span>
        </Link>
      </div>
    )
  },
  {
    name: 'Status',
    minWidth: '120px',
    selector: 'createdAt',
    sortable: true,
    center: true,
    cell: row => row?.status ? "Active" : "Inactive"
  },
  {
    name: 'CreatedDate',
    minWidth: '320px',
    selector: 'createdAt',
    sortable: true,
    center: true,
    cell: row => convertDate(row?.createdAt)
  },
  {
    name: 'Actions',
    minWidth: '300px',
    cell: row => (
        <UncontrolledDropdown>
          <DropdownToggle tag='div' className='btn btn-sm'>
            <MoreVertical size={14} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem
                tag={Link}
                to={`/permission/edit/${row.id}`}
                className='w-100'>
              <FileText size={14} className='mr-50' />
              <span className='align-middle'>Edit</span>
            </DropdownItem>
            {
              row?.status &&
              <DropdownItem className='w-100' onClick={() => store.dispatch(changeStatusPermission(row?.id, false))}>
                <Trash2 size={14} className='mr-50' />
                <span className='align-middle'>Inactive</span>
              </DropdownItem>
            }
            {
              !row?.status &&
              <DropdownItem className='w-100' onClick={() => store.dispatch(changeStatusPermission(row?.id, true))}>
                <Activity size={14} className='mr-50' />
                <span className='align-middle'>Active</span>
              </DropdownItem>
            }
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
]
