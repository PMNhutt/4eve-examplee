// ** React Imports
import React, { Fragment, useState, useEffect } from 'react'

// ** Columns
import { columns } from './columns'

// ** Store & Actions
import {getRoles} from '../store/action'
import { useDispatch, useSelector } from 'react-redux'

// ** Third Party Components
import {ChevronDown} from 'react-feather'
import DataTable from 'react-data-table-component'
import { Card } from 'reactstrap'

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import AddNewButton from "../../../../components/Buttons/AddNewButton"
import {NoDataComponent} from "../../../../components/NoDataComponent"

// ** Table Header
const CustomHeader = ({}) => {
  return (
    <div className='invoice-list-table-header ml-50 mt-2 mb-75'>
        <div
          className='d-flex align-items-center justify-content-end mr-2'
        >
            <AddNewButton text={'Create permission'} link={'/permission/create'}/>
        </div>
    </div>
  )
}

const UsersList = () => {
  // ** Store Vars
  const dispatch = useDispatch()
  const store = useSelector(state => state.permissions)
  const [isLoaded, setLoaded] = useState(false)
  // ** States
  const [searchTerm, setSearchTerm] = useState('')
  const [currentRole, setCurrentRole] = useState({ value: '', label: 'Select user role name' })

  // ** Get data on mount
  useEffect(() => {
      if (!isLoaded) {
          setLoaded(true)
          dispatch(
              getRoles()
          )
      }
  }, [dispatch, store?.roles?.length])

  // ** Function in get data on search query change
  const handleFilter = val => {
    setSearchTerm(val)
    dispatch(
        getRoles()
    )
  }

  // ** Table data to render
  const dataToRender = () => {
    const filters = {
      role: currentRole.value,
      q: searchTerm
    }

    const isFiltered = Object.keys(filters).some(function (k) {
      return filters[k].length > 0
    })
    if (store?.roles?.length > 0) {
      return store?.roles
    } else if (store?.roles?.length === 0 && isFiltered) {
      return []
    }
  }
  return (
    <Fragment>
      <Card id={'permissionContainer'}>
          <CustomHeader
              searchTerm={searchTerm}
              handleFilter={handleFilter}
          />
        <DataTable
          noHeader
          responsive
          persistTableHead
          columns={columns}
          sortIcon={<ChevronDown />}
          className='react-dataTable mb-2'
          data={dataToRender()}
          noDataComponent={<NoDataComponent message={'Currently, no permission groups yet!'}/>}
        />
      </Card>
    </Fragment>
  )
}

export default UsersList
