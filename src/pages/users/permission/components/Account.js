// ** React Imports
import React, {useState} from 'react'
import '../styles/account.scss'
// ** Third Party Components
import {Lock} from 'react-feather'
import {Row, Col, Button, Form, Input, Label, FormGroup} from 'reactstrap'
import DataTable from "react-data-table-component"
import {columns} from './columns'
import {NoDataComponent} from "../../../../components/NoDataComponent"
import PermissionScreenTable from "./PermissionScreenTable"

const UserAccountTab = (props) => {
  // ** States
  let viewPermission = useState(false)
  let addPermission = useState(false)
  let editPermission = useState(false)
  let deletePermission = useState(false)
  let importPermission = useState(false)
  let exportPermission = useState(false)
  const [name, setName] = useState(props?.name ? props?.name : '')
  const [permissions, setPermissions] = useState(props?.permission_uses && props?.permission_uses.length > 0 ? props?.permission_uses : [])

  const getPermissionIds = () => {
    setPermissions([...permissions])
  }
  const selectPermission = (permission, e, groupId) => {
    switch (permission.split('_')[0]) {
      case 'view':
        viewPermission = e.target.checked
          if (viewPermission) {
            const index = permissions?.findIndex(p => p === permission)
            if (index === -1) {
              permissions.push(permission)
            }
          } else {
            document.getElementById(`viewAll${groupId}`).checked = false
            permissions.splice(permissions.indexOf(permission), 1)
          }
        break
      case 'create':
        addPermission = e.target.checked
        if (addPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          document.getElementById(`createAll${groupId}`).checked = false
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'edit':
        editPermission = e.target.checked
        if (editPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          document.getElementById(`editAll${groupId}`).checked = false
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'delete':
        deletePermission = e.target.checked
        if (deletePermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          document.getElementById(`deleteAll${groupId}`).checked = false
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'import':
        importPermission = e.target.checked
        if (importPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          document.getElementById(`importAll${groupId}`).checked = false
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'export':
        exportPermission = e.target.checked
        if (exportPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          document.getElementById(`exportAll${groupId}`).checked = false
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
    }
    getPermissionIds()
  }

  const selectAllPermissionByGroup = (permission, e) => {
    switch (permission.split('_')[0]) {
      case 'view':
        viewPermission = e
        if (viewPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'create':
        addPermission = e
        if (addPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'edit':
        editPermission = e
        if (editPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'delete':
        deletePermission = e
        if (deletePermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'import':
        importPermission = e
        if (importPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
      case 'export':
        exportPermission = e
        if (exportPermission) {
          const index = permissions?.findIndex(p => p === permission)
          if (index === -1) {
            permissions.push(permission)
          }
        } else {
          permissions.splice(permissions.indexOf(permission), 1)
        }
        break
    }
    getPermissionIds()
  }
  const allPermissionByGroup = (e, action, groupId) => {
    if (props?.permissionGroups?.length > 0) {
      props?.permissionGroups?.forEach(group => {
        if (group?.id === groupId) {
          if (group?.permission_screens?.length > 0) {
            group?.permission_screens?.forEach(screen => {
              switch (action) {
                case 'viewAll':
                  selectAllPermissionByGroup(`view_${screen?.name}`, e.target.checked === true)
                  break
                case 'createAll':
                  selectAllPermissionByGroup(`create_${screen?.name}`, e.target.checked === true)
                  break
                case 'editAll':
                  selectAllPermissionByGroup(`edit_${screen?.name}`, e.target.checked === true)
                  break
                case 'deleteAll':
                  selectAllPermissionByGroup(`delete_${screen?.name}`, e.target.checked === true)
                  break
                case 'importAll':
                  selectAllPermissionByGroup(`import_${screen?.name}`, e.target.checked === true)
                  break
                case 'exportAll':
                  selectAllPermissionByGroup(`export_${screen?.name}`, e.target.checked === true)
                  break
              }
            })
          }
        }
      })
    }
  }

  function checkCheckedItem(screen) {
    if (permissions && permissions.length > 0) {
      let check = false
      for (const permission of permissions) {
        if (permission === screen) {
          check = true
        }
      }
      return check
    } else {
      return false
    }
  }

  function checkCheckedAll(type, groupId) {
    const permission_groups = props?.permissionGroups
    let max = 0
    let count = 0
    if (permission_groups?.length > 0 && permissions?.length > 0) {
      permission_groups?.forEach(group => {
        if (groupId === group?.id) {
          max += group?.permission_screens?.length
          if (group?.permission_screens?.length > 0) {
            group?.permission_screens?.forEach(screen => {
              permissions?.forEach(permission => {
                if (`${type}_${screen?.name}` === permission) {
                  count++
                }
              })
            })
          }
        }
      })
      return count === max
    }
  }

  return (
    <Row>
      <Col sm='12'>
        <Form onSubmit={e => e.preventDefault()}>
          <Row>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label for='username' className={'font-medium-1'}>User group name</Label>
                <Input type='text' id='username' placeholder='Enter user group name' autoComplete='off' defaultValue={name} onChange={e => {
                  setName(e.target.value)
                }}/>
              </FormGroup>
            </Col>
            <Col sm='12'>
              <div className='permissions border mt-1'>
                <h6 className='py-1 mx-1 mb-0 font-medium-2'>
                  <Lock size={18} className='mr-25' />
                  <span className='align-middle'>Permission</span>
                </h6>
                <DataTable columns={columns((action, groupId) => checkCheckedAll(action, groupId), (e, action, groupId) => allPermissionByGroup(e, action, groupId))}
                           className={'permission-dataTable table-bm'}
                           noDataComponent={<NoDataComponent message={'No permission screens yet!'}/>}
                           expandableRows
                           expandOnRowClicked
                           noHeader
                           expandableRowsComponent={<PermissionScreenTable selectPermission={(permissionScreen, e, groupId) => selectPermission(permissionScreen, e, groupId)}
                                                                           checkCheckedItem={(screen) => checkCheckedItem(screen)}/>}
                           data={props?.permissionGroups}
                />
              </div>
            </Col>
            <Col className='d-flex flex-sm-row flex-column mt-2' sm='12'>
              <Button className='mb-1 mb-sm-0 mr-0 mr-sm-1' type='submit' color='primary'
                             disabled={ permissions.length === 0}
                             onClick={() => {
                               props.handleSubmitButton(name, permissions)
                             }}>Save
              </Button>
              <Button color='secondary' outline onClick={e => history.go(-1)}>
                Go back
              </Button>
            </Col>
          </Row>
        </Form>
      </Col>
    </Row>
  )
}
export default UserAccountTab
