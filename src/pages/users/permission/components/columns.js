import React from "react"
import {CustomInput} from "reactstrap"

export const columns = ((checkCheckedAll, allPermissionByGroup) => [
    {
        name: 'List screen',
        minWidth: '250px',
        cell: row => row?.display_name
    },
    {
        name: 'View',
        center: true,
        minWidth: '200px',
        cell: row => <CustomInput type='checkbox' id={`viewAll${row?.id}`} defaultChecked={checkCheckedAll('view', row?.id)} label='' onClick={e => allPermissionByGroup(e, 'viewAll', row?.id)}/>
    },
    {
        name: 'Add',
        center: true,
        minWidth: '200px',
        cell: row => <CustomInput type='checkbox' id={`createAll${row?.id}`} defaultChecked={checkCheckedAll('create', row?.id)} label='' onClick={e => allPermissionByGroup(e, 'createAll', row?.id)}/>
    },
    {
        name: 'Edit',
        center: true,
        minWidth: '200px',
        cell: row => <CustomInput type='checkbox' id={`editAll${row?.id}`} defaultChecked={checkCheckedAll('edit', row?.id)} label='' onClick={e => allPermissionByGroup(e, 'editAll', row?.id)}/>
    },
    {
        name: 'Delete',
        center: true,
        minWidth: '200px',
        cell: row => <CustomInput type='checkbox' id={`deleteAll${row?.id}`} defaultChecked={checkCheckedAll('delete', row?.id)} label='' onClick={e => allPermissionByGroup(e, 'deleteAll', row?.id)}/>
    },
    {
        name: 'Import File',
        center: true,
        minWidth: '200px',
        cell: row => <CustomInput type='checkbox' id={`importAll${row?.id}`} defaultChecked={checkCheckedAll('import', row?.id)} label='' onClick={e => allPermissionByGroup(e, 'importAll', row?.id)}/>
    },
    {
        name: 'Export File',
        center: true,
        minWidth: '200px',
        cell: row => <CustomInput type='checkbox' id={`exportAll${row?.id}`} defaultChecked={checkCheckedAll('export', row?.id)} label='' onClick={e => allPermissionByGroup(e, 'exportAll', row?.id)}/>
    }
])
