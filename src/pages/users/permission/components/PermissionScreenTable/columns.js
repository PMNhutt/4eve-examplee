import React from "react"
import {CustomInput} from "reactstrap"

export const columns = ((checkCheckedItem, selectPermission) => [
    {
        name: 'List Screen',
        minWidth: '250px',
        cell: row => row?.display_name
    },
    {
        name: 'View',
        center: true,
        minWidth: '200px',
        cell: row => (
            <CustomInput type='checkbox' defaultChecked={checkCheckedItem(`view_${row?.name}`)}
                         id={`view-${row?.name}`} value={`view_${row?.name}`} label=''
                         onClick={e => selectPermission(`view_${row?.name}`, e, row?.group_id)}/>
        )
    },
    {
        name: 'Add',
        center: true,
        minWidth: '200px',
        cell: row => (
            <CustomInput type='checkbox' defaultChecked={checkCheckedItem(`create_${row?.name}`)}
                         id={`create-${row?.name}`} value={`create_${row?.name}`} label=''
                         onClick={e => selectPermission(`create_${row?.name}`, e, row?.group_id)}/>
        )
    },
    {
        name: 'Edit',
        center: true,
        minWidth: '200px',
        cell: row => (
            <CustomInput type='checkbox' defaultChecked={checkCheckedItem(`edit_${row?.name}`)}
                         id={`edit-${row?.name}`} value={`edit_${row?.name}`} label=''
                         onClick={e => selectPermission(`edit_${row?.name}`, e, row?.group_id)}/>
        )
    },
    {
        name: 'Delete',
        center: true,
        minWidth: '200px',
        cell: row => (
            <CustomInput type='checkbox' defaultChecked={checkCheckedItem(`delete_${row?.name}`)}
                         id={`delete-${row?.name}`} value={`delete_${row?.name}`} label=''
                         onClick={e => selectPermission(`delete_${row?.name}`, e, row?.group_id)}/>
        )
    },
    {
        name: 'Import File',
        center: true,
        minWidth: '200px',
        cell: row => (
            <CustomInput type='checkbox' defaultChecked={checkCheckedItem(`import_${row?.name}`)}
                         id={`import-${row?.name}`} value={`import_${row?.name}`} label=''
                         onClick={e => selectPermission(`import_${row?.name}`, e, row?.group_id)}/>
        )
    },
    {
        name: 'Export File',
        center: true,
        minWidth: '200px',
        cell: row => (
            <CustomInput type='checkbox' defaultChecked={checkCheckedItem(`export_${row?.name}`)}
                         id={`export-${row?.name}`} value={`export_${row?.name}`} label=''
                         onClick={e => selectPermission(`export_${row?.name}`, e, row?.group_id)}/>
        )
    }
])
