import DataTable from "react-data-table-component"
import {columns} from './columns'
import React from "react"

const PermissionScreenTable = (props) => {
    return (
        <DataTable columns={columns((screen) => props?.checkCheckedItem(screen), (permission, e, groupId) => props?.selectPermission(permission, e, groupId))}
                   noHeader={true}
                   noTableHead={true}
                   expandableRows
                   className={'permission-screen-dataTable'}
                   data={props?.data?.permission_screens}
        />
    )
}

export default PermissionScreenTable