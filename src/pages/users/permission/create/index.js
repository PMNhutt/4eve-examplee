// ** React Imports
import React, { useEffect } from 'react'

// ** User Edit Components
import AccountTab from '../components/Account'

// ** Store & Actions
import {createRole, getPermissionGroups, getPermissions} from '../store/action'
import { useSelector, useDispatch } from 'react-redux'

// ** Third Party Components
import { Card, CardBody, Row, Col} from 'reactstrap'

// ** Styles
import '@styles/react/apps/app-users.scss'
import '../styles/account.scss'
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../../components/Toast/ToastFail"

const CreatePermission = () => {
  const dispatch = useDispatch()
  const store = useSelector(state => state.permissions)
  // ** Function to get user on mount
  useEffect(() => {
      dispatch(getPermissionGroups())
      dispatch(getPermissions())
  }, [dispatch])

  const createRolePermission = async(name, permissions) => {
      if (name) {
          const res = await createRole({name: name, grand: 'API', permissions: permissions})
          if (res?.status) {
              store.permissions = []
              store.permission_groups = []
              toast.success(<SuccessNotificationToast />)
              history?.go(-1)
          } else {
              toast.error(<ErrorNotificationToast />)
          }
      } else {
          toast.error(<ErrorNotificationToast message={'Please enter name'} />)
      }
  }

  return (
      <Row className='app-user-edit'>
          <Col sm='12'>
              <Card>
                  <CardBody className='pt-2'>
                      <AccountTab permissionGroups={store?.permission_groups}
                                  permissions={store?.permissions}
                                  handleSubmitButton={(name, permissions) => createRolePermission(name, permissions)}/>
                  </CardBody>
              </Card>
          </Col>
      </Row>
  )
}
export default CreatePermission
