// ** React Imports
import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'

// ** User Edit Components
import AccountTab from '../components/Account'

// ** Store & Actions
import {
    getPermissionGroups,
    getPermissions,
    getRole,
    updateRole
} from '../store/action'
import { useSelector, useDispatch } from 'react-redux'

// ** Third Party Components
import { Card, CardBody, Row, Col} from 'reactstrap'

// ** Styles
import '@styles/react/apps/app-users.scss'
import '../styles/account.scss'
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../../components/Toast/ToastFail"

const EditPermission = () => {
  const dispatch = useDispatch()
  const store = useSelector(state => state.permissions)
  const role_id = useParams().id
  const [reload, setReload] = useState(false)

    // ** Function to get user on mount
  useEffect(() => {
      if (!reload) {
          setReload(true)
          dispatch(getRole(role_id))
          dispatch(getPermissionGroups())
          dispatch(getPermissions())
      }
  }, [dispatch, store?.screens?.length, store?.permissions?.length])

  const updateRolePermission = async(name, permissions) => {
      if (name) {
          const res = await updateRole({name: name, permissions: permissions}, role_id)
          if (res?.status) {
              store.role = null
              store.permissions = []
              store.screens = []
              toast.success(<SuccessNotificationToast />)
              history?.go(-1)
          } else {
              toast.error(<ErrorNotificationToast />)
          }
      } else {
          toast.error(<ErrorNotificationToast message={'Vui lòng nhập tên'} />)
      }
  }

    function getPermissionUses(role) {
        const permissions = []
        role?.permissions.map(item => { permissions.push(item.name) })
        return permissions
    }

    return store?.role && store?.role?.id && store?.role?.id === role_id ? (<Row className='app-user-edit'>
              <Col sm='12'>
                  <Card>
                      <CardBody className='pt-2'>
                          <AccountTab permissionGroups={store?.permission_groups}
                                      permissions={store?.permissions}
                                      name={store.role?.name}
                                      permission_uses={getPermissionUses(store?.role)}
                                      handleSubmitButton={(name, permissions) => updateRolePermission(name, permissions)}/>
                      </CardBody>
                  </Card>
              </Col>
        </Row>) : (<div/>)
}
export default EditPermission
