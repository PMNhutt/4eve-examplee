// ** React Imports
import { Link } from 'react-router-dom'

// ** Custom Components
import Avatar from '@components/avatar'

// ** Store & Actions

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge} from 'reactstrap'
import {MoreVertical, Archive, Settings} from 'react-feather'
import React from 'react'
import {convertDate} from "../../../../utility/ConvertDate"

// ** Renders Client Columns
const renderClient = row => {
  const stateNum = Math.floor(Math.random() * 6),
    states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
    color = states[stateNum]
  const name_display = row?.name ? row?.name.split(' ')[0] : ''
  if (row?.avatar) {
    return <Avatar className='mr-1' img={row?.avatar} width='32' height='32' />
  } else {
    return <Avatar color={color || 'primary'} className='mr-1' content={name_display} initials />
  }
}

const statusObj = {
  ACTIVE: 'light-success',
  INACTIVE: 'light-warning',
  DELETED: 'light-danger'
}

const webActiveObj = {
  false: 'light-warning',
  true: 'light-success',
  null: 'light-warning'
}

function paymentWebsite(row) {
  row.web_active = !row.web_active
}

export const columns = ((openEditSideBar) => [
  {
    name: 'Tên nhóm',
    minWidth: '297px',
    selector: 'name',
    sortable: true,
    cell: row => row?.display_name
  },
  {
    name: 'Người quản lý',
    minWidth: '297px',
    selector: 'manager.name',
    sortable: true,
    cell: row => row?.manager?.name
  },
  {
    name: 'Ngày tạo',
    minWidth: '150px',
    selector: 'create_at',
    sortable: true,
    center: 'true',
    cell: row => convertDate(row?.createdAt)
  },
  {
    name: 'Actions',
    minWidth: '100px',
    cell: row => (
      <UncontrolledDropdown>
        <DropdownToggle tag='div' className='btn btn-sm'>
          <MoreVertical size={14} className='cursor-pointer' />
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem
            className='w-100'
            onClick={() => openEditSideBar(row?.id)}
          >
            <Archive size={14} className='mr-50' />
            <span className='align-middle'>Chỉnh sửa</span>
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    )
  }
])
