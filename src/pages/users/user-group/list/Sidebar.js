// ** React Import
import React, {useEffect, useState} from 'react'

// ** Custom Components
import Sidebar from '@components/sidebar'
import "flatpickr/dist/themes/material_blue.css"
// ** Utils
import { isObjEmpty } from '@utils'

// ** Third Party Components
import classnames from 'classnames'
import { useForm } from 'react-hook-form'
import {Button, FormGroup, Label, FormText, Form, Input, InputGroupText, InputGroup} from 'reactstrap'

// ** Store & Actions
import {addUser, getUserAll, updateUser} from '../store/action'
import {useDispatch, useSelector} from 'react-redux'
import Select from "react-select"
import {getRoles} from "../../permission/store/action"
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../../components/Toast/ToastFail"
import {ChevronDown, ChevronLeft, ChevronRight, Search} from "react-feather"
import '../styles/user.scss'

const registerOptions = {
  province: { required: "Vui lòng nhập thành phố" },
  district: { required: "Vui lòng nhập quận huyện" },
  ward: { required: "Vui lòng nhập phường xã" },
  role: { required: "Vui lòng chọn nhóm người dùng" }
}

const SidebarNewUsers = ({open, toggleSidebar, userGroup}) => {
  // ** States
  const [dataFilter, setDataFilter] = useState([])
  const [isShow, setIsShow] = useState(false)
  const [page, setPage] = useState(1)
  const perPage = 2
  const [totalRecords, setTotalRecords] = useState(0)
  const [users, setUsers] = useState([])
  const storeRole = useSelector(state => state.permissions)
  const storeUser = useSelector(state => state.users)
  const [search, setSearch] = useState('')
  const [user, setUser] = useState('')
  const [userObj, setUserObj] = useState(null)

  // ** Store Vars
  const dispatch = useDispatch()


  useEffect(() => {
    if (userGroup) {
      setUserObj(userGroup?.manager)
      setUser(userGroup?.manager?.name)
    } else {
      setUserObj(null)
      setUser('')
    }
  }, [userGroup])
  useEffect(() => {
    dispatch(getRoles())
  }, [dispatch])

  async function getUsers(params) {
    const resp = await getUserAll(params)
    setTotalRecords(resp?.data?.data?.totalRecords)
    setUsers(resp?.data?.data?.users)
  }
  // ** Vars
  const { register, errors, handleSubmit } = useForm()
  const handleNextPagination = () => {
    if (page < (totalRecords / perPage)) {
      setPage(page + 1)
      const params = {page: page + 1, perPage: perPage, q: search}
      getUsers(params)
    }
  }
  const handlePrevPagination = () => {
    if (page > 1) {
      setPage(page - 1)
      const params = {page: page - 1, perPage: perPage, q: search}
      getUsers(params)
    }
  }
  const searchUser = (e) => {
    const params = {page: 1, perPage: perPage, q: e.target.value}
    getUsers(params)
    setUser(e?.target?.value)
    setUserObj(null)
  }
  const getData = (item) => {
    setUserObj(item)
    setUser(item?.name)
    setIsShow(!isShow)
  }
  // ** Function to handle form submit
  const onSubmit = async values => {
    if (isObjEmpty(errors)) {
      if (userGroup) {
        dispatch(
            await updateUser({
              display_name: values['display-name'],
              user_manager: userObj?.id ? userObj?.id : null
            }, userGroup?.id)
        )
      } else {
        dispatch(
            await addUser({
              display_name: values['display-name'],
              user_manager: userObj?.id ? userObj?.id : null
            })
        )
      }

      if (storeUser && storeUser?.errorMessage) {
        toast.error(<ErrorNotificationToast message={storeUser?.errorMessage}/>)
      } else {
        toggleSidebar()
        toast.success(<SuccessNotificationToast/>)
      }
    }
  }
  async function openModalSearch(status) {
    setIsShow(status)
    if (status) {
      const params = {q: search, page: page, perPage: perPage}
      await getUsers(params)
    }
  }
  return (
    <Sidebar
      size='lg'
      open={open}
      title={userGroup ? 'Chỉnh sửa nhóm người dùng' : 'Tạo mới nhóm người dùng'}
      headerClassName='mb-1'
      contentClassName='pt-0 user__side-bar'
      toggleSidebar={toggleSidebar}
    >
      <Form onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
        <FormGroup>
          <Label className={'label'} for='display-name'>
            Tên hiển thị <span className='text-danger'>*</span>
          </Label>
          <Input
            name='display-name'
            id='display-name'
            placeholder='Nhập tên nhóm người dùng'
            defaultValue={userGroup ? userGroup?.display_name : ''}
            innerRef={register({ required: true })}
            className={classnames({ 'is-invalid': errors['display-name'] })}
          />
        </FormGroup>
        <FormGroup>
          <Label className={'label'} for='province'>
            Trưởng nhóm
          </Label>
          <div>
            <Input placeholder={'Tìm kiếm sản phẩm theo tên sản phẩm'}
                   value={user}
                   onClick={() => openModalSearch(!isShow)} onChange={(e) => searchUser(e)}/>
            {
              isShow &&
              <div className={'filter-body--suggest'}>
                <div className={'popover'}>
                  <div className={'arrow'}/>
                  <div className={'popover-body'}>
                    <div className={'dropdown-filter'}>
                      <div className={'suggest-result'}>
                        {
                          users?.length === 0 &&
                          <div className={'py-5 text-center'}>
                            Không tìm thấy user
                          </div>
                        }
                        {
                          users?.length > 0 && users?.map((item, index) => {
                            return (
                                <div className={'suggest-select'} key={index} onClick={() => getData(item)}>
                                  <div className={'suggest-item'}>
                                    <div className={'d-flex justify-content-between w-100'}>
                                      <div className={'suggest-info'}>
                                        <p className={'name'}>{item?.name}</p>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                            )
                          })
                        }
                      </div>
                      <div className={'dropdown-paginate'}>
                        <div className={`btn-prev btn-page btn btn-default ${page > 1 ? '' : 'disabled'}`} onClick={() => handlePrevPagination()}>
                          <ChevronLeft/>
                        </div>
                        <div className={`btn-next btn-page btn btn-default ${page < totalRecords / perPage ? '' : 'disabled'}`} onClick={() => handleNextPagination()}>
                          <ChevronRight/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            }
          </div>
        </FormGroup>
        <Button type='submit' className='mr-1' color='primary'>
          {userGroup ? 'Cập nhật' : 'Lưu'}
        </Button>
        <Button type='reset' color='secondary' outline onClick={toggleSidebar}>
          Hủy
        </Button>
      </Form>
    </Sidebar>
  )
}

export default SidebarNewUsers
