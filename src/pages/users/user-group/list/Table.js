// ** React Imports
import React, { Fragment, useState, useEffect } from 'react'

// ** Invoice List Sidebar
import Sidebar from './Sidebar'

// ** Columns
import { columns } from './columns'

// ** Store & Actions
import {getUserGroupById, getUserGroups} from '../store/action'
import { useDispatch, useSelector } from 'react-redux'

// ** Third Party Components
import ReactPaginate from 'react-paginate'
import {ChevronDown} from 'react-feather'
import DataTable from 'react-data-table-component'
import {
    Card,
    Row,
    Col,
    Button
} from 'reactstrap'
import {NoDataComponent} from "../../../../components/NoDataComponent"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/user.scss'
import {useHistory, useLocation} from 'react-router-dom'

// ** Table Header
const CustomHeader = ({ toggleSidebar, searchTerm, removeValue}) => {
  return (
    <div className='invoice-list-table-header w-100 mt-2 mb-75'>
      <Row className={'w-100 justify-content-end mx-0'}>
        <Col xl='2'
          className='d-flex align-items-sm-center justify-content-lg-end justify-content-start flex-lg-nowrap flex-wrap flex-sm-row flex-column px-0 mt-lg-0 mt-1'>
            <Button.Ripple color='primary' onClick={toggleSidebar}>Tạo mới</Button.Ripple>
        </Col>
      </Row>
    </div>
  )
}

const UsersList = () => {
  // ** Store Vars
  const dispatch = useDispatch()
  const storeGroup = useSelector(state => state.groups)
    const history = useHistory()
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)

  // ** States
  const [searchTerm, setSearchTerm] = useState(searchParams?.get('search') ? searchParams?.get('search') : '')
  const [currentPage, setCurrentPage] = useState(searchParams?.get('page') ? searchParams?.get('page') : 1)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [sidebarOpen, setSidebarOpen] = useState(false)
    const [userGroup, setUserGroup] = useState(null)

  // ** Function to toggle sidebar
  const toggleSidebar = () => {
      setUserGroup(null)
      setSidebarOpen(!sidebarOpen)
  }

  // ** Get data on mount
  useEffect(() => {
    dispatch(getUserGroups({
        page: currentPage,
        perPage: rowsPerPage}
        ))
  }, [dispatch])

    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.category) {
            params.category = filterParams.category
        }
        if (filterParams.search) {
            params.search = filterParams.search
        }
        if (filterParams.page) {
            params.page = filterParams.page
        }
        params.perPage = filterParams.perPage

        const urlSearchParams = new URLSearchParams(params)
        history.replace({pathname: location.pathname, search: urlSearchParams.toString()})
        localStorage.removeItem('urlUser')
        localStorage.setItem('urlUser', `/user/list?${urlSearchParams}`)
    }
  // ** Function in get data on page change
  const handlePagination = page => {
      checkParams({page: page.selected + 1, perPage: rowsPerPage})
    dispatch(
        getUserGroups({
        page: page.selected + 1,
        perPage: rowsPerPage
      })
    )
    setCurrentPage(page.selected + 1)
  }

  // ** Function in get data on rows per page
  const handlePerPage = e => {
    const value = parseInt(e.currentTarget.value)
    dispatch(
        getUserGroups({
        page: currentPage,
        perPage: value
      })
    )
    setRowsPerPage(value)
  }
  const openEditSideBar = (id) => {
      getUserGroupId(id)
      setSidebarOpen(true)
  }
  // ** Custom Pagination
  const CustomPagination = () => {
    const count = Number(Math.ceil(storeGroup.totalGroups / rowsPerPage))

    return (
      <ReactPaginate
        previousLabel={''}
        nextLabel={''}
        pageCount={count || 1}
        activeClassName='active'
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        onPageChange={page => handlePagination(page)}
        pageClassName={'page-item'}
        nextLinkClassName={'page-link'}
        nextClassName={'page-item next'}
        previousClassName={'page-item prev'}
        previousLinkClassName={'page-link'}
        pageLinkClassName={'page-link'}
        containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
      />
    )
  }

  // ** Table data to render
  const dataToRender = () => {
    const filters = {
      q: searchTerm
    }

    const isFiltered = Object.keys(filters).some(function (k) {
      return filters[k].length > 0
    })

    if (storeGroup?.groups?.length > 0) {
      return storeGroup.groups
    } else if (storeGroup?.groups?.length === 0 && isFiltered) {
      return []
    }
  }
  const removeValue = () => {
      setSearchTerm('')
  }
  const getUserGroupId = (id) => {
      getUserGroupById(id).then(resp => {
          setUserGroup(resp?.data?.data?.user_group)
      })
  }
  return (
    <Fragment>
      <Card id={'userContainer'}>
          <div className={'user'}>
              <CustomHeader
                  toggleSidebar={toggleSidebar}
                  handlePerPage={handlePerPage}
                  searchTerm={searchTerm}
                  removeValue={removeValue}
              />
          </div>
          <DataTable
              noHeader
              responsive
              persistTableHead
              paginationServer
              columns={columns(openEditSideBar)}
              sortIcon={<ChevronDown />}
              className='react-dataTable px-0'
              data={dataToRender()}
              noDataComponent={<NoDataComponent message={'Chưa có nhóm nào'}/>}
          />
          <div>
              {
                  CustomPagination()
              }
          </div>
      </Card>

      <Sidebar open={sidebarOpen} toggleSidebar={toggleSidebar} userGroup={userGroup} />
    </Fragment>
  )
}

export default UsersList
