// ** React Imports
import { useState, useEffect } from 'react'
import {useParams, Link, useHistory} from 'react-router-dom'

// ** User Edit Components
import UserAccountTab from './Account'

// ** Store & Actions
import {getUser, getUserV2} from '../store/action'
import { useSelector, useDispatch } from 'react-redux'

// ** Third Party Components
import { User} from 'react-feather'
import { Card, CardBody, Row, Col, Nav, NavItem, NavLink, TabContent, TabPane, Alert } from 'reactstrap'

// ** Styles
import '@styles/react/apps/app-users.scss'

const UserEdit = () => {
  // ** States & Vars
  const [activeTab, setActiveTab] = useState('1'),
    store = useSelector(state => state.users),
    storeRole = useSelector(state => state.permissions),
    { id } = useParams()
  const dispatch = useDispatch()
  const [user, setUser] = useState({})
  const history = useHistory()

  // ** Function to toggle tabs
  const toggle = tab => setActiveTab(tab)

  // ** Function to get user on mount
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    if (!isLoading) {
      if (id) {
        setLoading(true)
        getUserV2(id).then(response => {
          if (response.data.data) {
            setUser(response.data.data)
          }
        })
      }
    }
  }, [dispatch])

  return user && user?.id ? (
    <Row className='app-user-edit app-user-list'>
      <Col sm='12'>
        <Card>
          <CardBody className='pt-2'>
            <Nav pills>
              <NavItem>
                <NavLink active={activeTab === '1'} onClick={() => toggle('1')}>
                  <User size={14} />
                  <span className='align-middle d-none d-sm-block'>User information</span>
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
              <TabPane tabId='1'>
                <UserAccountTab selectedUser={user} store={store} storeRole={storeRole} id={id} />
              </TabPane>
            </TabContent>
          </CardBody>
        </Card>
      </Col>
    </Row>
  ) : (
    <Alert color='danger'>
      <h4 className='alert-heading'>User not found</h4>
      <div className='alert-body'>
        User with id: {id} doesn't exist. Check list of all Users: <Link to='/apps/user/list'>Users List</Link>
      </div>
    </Alert>
  )
}
export default UserEdit
