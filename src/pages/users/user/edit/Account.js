// ** React Imports
import React, { useState, useEffect } from 'react'

import { isObjEmpty } from '@utils'
// ** Custom Components
import "flatpickr/dist/themes/material_blue.css"
// ** Third Party Components
import { Row, Col, Button, Form, Input, Label, FormGroup } from 'reactstrap'
import {getDistricts, getProvinces, getWards, updateUser} from "../store/action"
import Select from "react-select"
import {useDispatch} from "react-redux"
import {getRoles} from '../../permission/store/action'
import Flatpickr from 'react-flatpickr'
import {toast} from 'react-toastify'
import ErrorNotificationToast from '../../../../components/Toast/ToastFail'
import {useForm} from 'react-hook-form'
import classnames from 'classnames'
import SuccessNotificationToast from "../../../../components/Toast/ToastSuccess"
import {Link, useHistory} from 'react-router-dom'
import {selectThemeColors} from "../../../../utility/Utils"

const registerOptions = {
  province: {required: "Please enter province"},
  district: {required: "Please enter district"},
  ward: {required: "Please enter ward"},
  role: {required: "Please select user role"}
}

const genderOptions = [
  { value: null, label: 'Option', number: 0 },
  { value: 'male', label: 'Male', number: 1 },
  { value: 'female', label: 'Female', number: 2 },
  { value: 'other', label: 'Other', number: 3 }
]

const statusOptions = [
  { value: 'ACTIVE', label: 'Active', number: 1 },
  { value: 'INACTIVE', label: 'Inactive', number: 2 },
  { value: 'DELETED', label: 'Deleted', number: 3 }
]

const UserAccountTab = ({ selectedUser, store, storeRole, id }) => {
  // ** States
  const [provinceId, setProvinceId] = useState(selectedUser?.province?.id)
  const [districtId, setDistrictId] = useState(selectedUser?.district?.id)
  const [wardId, setWardId] = useState(selectedUser?.ward?.id)
  const [roleId, setRoleId] = useState(selectedUser?.role?.id)
  const [dob, setDob] = useState(selectedUser?.dob)
  const [gender, setGender] = useState(selectedUser?.gender)
  const [status, setStatus] = useState(selectedUser?.status)
  const [webActive, setWebActive] = useState(selectedUser?.web_active)
  const history = useHistory()
  const dispatch = useDispatch()

  // ** Update user image on mount or change
  useEffect(() => {
    dispatch(getProvinces())
    if (selectedUser?.province) {
      dispatch(getDistricts(selectedUser?.province?.id))
    }
    if (selectedUser?.district) {
      dispatch(getWards(selectedUser?.district?.id))
    }
    dispatch(getRoles())
  }, [dispatch])

  const changeRole = (role) => {
    selectedUser.role = role
  }
  const changeProvince = (province) => {
    selectedUser.province = province
    selectedUser.district = ''
    setDistrictId('')
    selectedUser.ward = ''
    setWardId('')
  }
  const changeDistrict = (district) => {
    selectedUser.district = district
    selectedUser.ward = ''
    setWardId('')
  }
  const changeWard = (ward) => {
    selectedUser.ward = ward
  }

  const { register, errors, handleSubmit } = useForm()

  const onSubmit = values => {
    if (isObjEmpty(errors)) {
      if (roleId === '' || roleId === undefined || roleId === null) {
        toast.error(<ErrorNotificationToast message={'Please select user role!'}/>)
      } else if (provinceId === '' || provinceId === undefined || provinceId === null) {
        toast.error(<ErrorNotificationToast message={'Please select Province!'}/>)
      } else if (districtId === '' || districtId === undefined || districtId === null) {
        toast.error(<ErrorNotificationToast message={'Please select District!'}/>)
      } else if (wardId === '' || wardId === undefined || wardId === null) {
        toast.error(<ErrorNotificationToast message={'Please select Ward!'}/>)
      } else {
        dispatch(
            updateUser({
              name: values['full-name'],
              phone: values['phone-number'],
              roleId: !roleId ? null : roleId,
              provinceId: !provinceId ? null : provinceId,
              districtId: !districtId ? null : districtId,
              wardId: !wardId ? null : wardId,
              email: values?.email,
              address: values?.address,
              dob: dob,
              gender: gender,
              status: status,
              web_active: webActive
            }, id)
        )
        toast.success(<SuccessNotificationToast message={'Edit user successfully!'}/>)
        history.push(`${localStorage.getItem('urlUser') ? localStorage.getItem('urlUser') : '/user/list'}`)
      }
    }
  }

  return (
    <Row>
      <Col sm='12'>
        <Form onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
          <Row>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label for='full-name'>Full name <span className='text-danger'>*</span></Label>
                <Input
                    name='full-name'
                    id='full-name'
                    placeholder='Enter user name'
                    innerRef={register({ required: true })}
                    defaultValue={selectedUser && selectedUser.name}
                    className={classnames({ 'is-invalid': errors['full-name'] })}
                />
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label className={'label'} for='phone-number'>
                  Phone number <span className='text-danger'>*</span>
                </Label>
                <Input
                    name='phone-number'
                    id='phone-number'
                    type='number'
                    placeholder='Enter phone number'
                    innerRef={register({ required: true })}
                    defaultValue={selectedUser && selectedUser?.phone}
                    className={classnames({ 'is-invalid': errors['phone-number'] })}
                />
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label className={'label'} for='email'>
                  Email <span className='text-danger'>*</span>
                </Label>
                <Input
                    type='email'
                    name='email'
                    id='email'
                    placeholder='abc@example.com'
                    defaultValue={selectedUser && selectedUser?.email}
                    innerRef={register({ required: true })}
                    className={classnames({ 'is-invalid': errors['email'] })}
                />
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label for='gender'>Gender</Label>
                <Select
                    theme={selectThemeColors}
                    isClearable={false}
                    classNamePrefix='select'
                    options={genderOptions}
                    placeholder={'Select gender'}
                    value={genderOptions?.find(data => data?.number === gender)}
                    onChange={data => { setGender(data.number) }}
                />
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label className={'label'} for='dob'>Day of birth</Label>
                <Flatpickr id={'dob'} className='form-control form-control__date'
                           value={selectedUser?.dob}
                           onChange={(_, date) => setDob(date)}/>
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label className={'label'} for='user-role'>Role user</Label>
                <Select
                    isClearable={false}
                    className='react-select'
                    classNamePrefix='select'
                    name='role'
                    options={storeRole.roles}
                    value={selectedUser?.role ? selectedUser?.role : ''}
                    getOptionValue={option => option?.id}
                    getOptionLabel={option => option?.name}
                    placeholder={'Select role user'}
                    onChange={
                      role => {
                        changeRole(role)
                        setRoleId(role?.id)
                      }
                    }
                />
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label for='role'>Province <span className='text-danger'>*</span></Label>
                <Select
                    isClearable={false}
                    className='react-select'
                    classNamePrefix='select'
                    name='province'
                    options={store?.provinces}
                    value={selectedUser?.province ? selectedUser?.province : ''}
                    getOptionValue={option => option?.id}
                    getOptionLabel={option => option?.name}
                    placeholder={'Select province'}
                    rules={registerOptions.province}
                    onChange={province => {
                      changeProvince(province)
                      setProvinceId(province?.id)
                      dispatch(getDistricts(province?.id))
                    }}
                />
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label for='role'>District <span className='text-danger'>*</span></Label>
                <Select
                    isClearable={false}
                    className='react-select'
                    classNamePrefix='select'
                    name='district'
                    options={store?.districts}
                    value={selectedUser?.district ? selectedUser?.district : ''}
                    getOptionValue={option => option?.id}
                    getOptionLabel={option => option?.name}
                    placeholder={'Select district'}
                    onChange={district => {
                      changeDistrict(district)
                      setDistrictId(district?.id)
                      dispatch(getWards(district?.id))
                    }}
                />
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label for='role'>Ward <span className='text-danger'>*</span></Label>
                <Select
                    isClearable={false}
                    className='react-select'
                    classNamePrefix='select'
                    name='ward'
                    options={store?.wards}
                    value={selectedUser?.ward ? selectedUser?.ward : ''}
                    getOptionValue={option => option?.id}
                    getOptionLabel={option => option?.name}
                    placeholder={'Select ward'}
                    onChange={ward => {
                      changeWard(ward)
                      setWardId(ward?.id)
                    }}
                />
              </FormGroup>
            </Col>
            <Col sm='12'>
              <FormGroup>
                <Label className={'label'} for='email'>
                  Address <span className='text-danger'>*</span>
                </Label>
                <Input
                    type='text'
                    name='address'
                    id='address'
                    placeholder='Enter address'
                    innerRef={register({ required: true })}
                    defaultValue={selectedUser && selectedUser?.address}
                    className={classnames({ 'is-invalid': errors['address'] })}
                />
              </FormGroup>
            </Col>
            <Col md='4' sm='12'>
              <FormGroup>
                <Label for='gender'>Status</Label>
                <Select
                    theme={selectThemeColors}
                    isClearable={false}
                    classNamePrefix='select'
                    options={statusOptions}
                    value={statusOptions?.find(data => data?.value === status)}
                    onChange={data => { setStatus(data.value) }}
                />
              </FormGroup>
            </Col>
            <Col className='d-flex flex-sm-row flex-column mt-2' sm='12'>
              {
                selectedUser?.status !== 'DELETED' &&
                <Button.Ripple className='mb-1 mb-sm-0 mr-0 mr-sm-1' type='submit' color='primary'>
                  Save
                </Button.Ripple>
              }
              <Button.Ripple color='secondary' tag={Link} to={`${localStorage.getItem('urlUser') ? localStorage.getItem('urlUser') : '/user/list'}`} outline >
                Go back
              </Button.Ripple>
            </Col>
          </Row>
        </Form>
      </Col>
    </Row>
  )
}
export default UserAccountTab
