// ** React Import
import React, {useEffect, useState} from 'react'

// ** Custom Components
import Sidebar from '@components/sidebar'
import "flatpickr/dist/themes/material_blue.css"
// ** Utils
import {isObjEmpty} from '@utils'

// ** Third Party Components
import classnames from 'classnames'
import {useForm} from 'react-hook-form'
import {Button, FormGroup, Label, FormText, Form, Input} from 'reactstrap'

// ** Store & Actions
import {addUser, createGroupUser, getDistricts, getProvinces, getUsers, getWards} from '../store/action'
import {useDispatch, useSelector} from 'react-redux'
import Select from "react-select"
import {getRoles} from "../../permission/store/action"
import Flatpickr from "react-flatpickr"
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../../components/Toast/ToastFail"
import {getUserData} from "../../../../auth/utils"
import {getGroupAll, getUserAll, getUserGroups} from "../../user-group/store/action"
import {ChevronLeft, ChevronRight} from "react-feather"

const registerOptions = {
    province: {required: "Please enter province"},
    district: {required: "Please enter district"},
    ward: {required: "Please enter ward"},
    role: {required: "Please select user role"}
}

const SidebarNewUsers = ({open, toggleSidebar}) => {
    // ** States
    const userData = getUserData()
    const [roleId, setRoleId] = useState(userData.role.id)
    const [gender, setGender] = useState('')
    const [provinceId, setProvinceId] = useState('')
    const [districtId, setDistrictId] = useState('')
    const [wardId, setWardId] = useState('')
    const [dob, setDob] = useState(new Date())
    const [group, setGroup] = useState('')
    const [selectedGroup, setSelectedGroup] = useState(null)
    // ** Group list paging
    const [groups, setGroups] = useState([])
    const [page, setPage] = useState(1)
    const perPage = 5
    const [isShow, setIsShow] = useState(false)
    const [keyword, setKeyword] = useState('')
    const [totalRecords, setTotalRecords] = useState(null)

    // ** Store Vars
    const storeGroup = useSelector(state => state.groups)
    const storeRole = useSelector(state => state.permissions)
    const storeUser = useSelector(state => state.users)
    const dispatch = useDispatch()

    useEffect(() => {
        const params = {keyword: keyword, page: page, perPage: perPage}
        dispatch(getUserGroups(params))
        dispatch(getProvinces())
        dispatch(getRoles())
    }, [dispatch])

    // ** Vars
    const {register, errors, handleSubmit} = useForm()

    // ** Function to handle form submit
    const onSubmit = async (values) => {
        if (isObjEmpty(errors)) {
            if (roleId === '' || roleId === undefined || roleId === null) {
                toast.error(<ErrorNotificationToast message={'Please select user role!'}/>)
            } else if (provinceId === '' || provinceId === undefined || provinceId === null) {
                toast.error(<ErrorNotificationToast message={'Please select Province!'}/>)
            } else if (districtId === '' || districtId === undefined || districtId === null) {
                toast.error(<ErrorNotificationToast message={'Please select District!'}/>)
            } else if (wardId === '' || wardId === undefined || wardId === null) {
                toast.error(<ErrorNotificationToast message={'Please select Ward!'}/>)
            } else {
                dispatch(
                    await createGroupUser({
                        name: values['full-name'],
                        phone: values['phone-number'],
                        province_id: !provinceId ? null : provinceId,
                        district_id: !districtId ? null : districtId,
                        ward_id: !wardId ? null : wardId,
                        email: values['email'],
                        address: values.address,
                        dob: dob?.toString(),
                        gender: parseInt(gender),
                        role_id: roleId,
                        status: 'active'
                    })
                )
                if (storeUser && storeUser?.errorMessage) {
                    toast.error(<ErrorNotificationToast message={storeUser?.errorMessage}/>)
                } else {
                    toggleSidebar()
                    setRoleId('')
                    setGender('')
                    setProvinceId('')
                    setDistrictId('')
                    setWardId('')
                    setSelectedGroup(null)
                    setDob(new Date())
                    toast.success(<SuccessNotificationToast/>)
                }
            }
        }
    }

    async function openModalSearch(status) {
        setIsShow(status)
        if (status) {
            const params = {keyword: keyword, page: page, perPage: perPage}
            await dispatch(getUserGroups(params))
            setTotalRecords(storeGroup?.totalGroups)
        }
    }

    const searchGroup = async (e) => {
        const params = {page: 1, perPage: perPage, keyword: e.target.value}
        await dispatch(getUserGroups(params))
        setGroup(e?.target?.value)
    }
    const handleNextPagination = async () => {
        if (page < (totalRecords / perPage)) {
            setPage(page + 1)
            const params = {page: page + 1, perPage: perPage, keyword: keyword}
            await dispatch(getUserGroups(params))
        }
    }
    const handlePrevPagination = async () => {
        if (page > 1) {
            setPage(page - 1)
            const params = {page: page - 1, perPage: perPage, keyword: keyword}
            await dispatch(getUserGroups(params))
        }
    }
    const handleSelectedGroup = (selectedGroup) => {
        setSelectedGroup(selectedGroup)
        setIsShow(false)
        setGroup(selectedGroup.display_name)
    }
    return (
        <Sidebar
            size='lg'
            open={open}
            title='Create new user'
            headerClassName='mb-1'
            contentClassName='pt-0 user__side-bar'
            toggleSidebar={toggleSidebar}
        >
            <Form onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
                <div className={'row'}>
                    <div className={'col-6'}><FormGroup>
                        <Label className={'label'} for='full-name'>
                            Full name <span className='text-danger'>*</span>
                        </Label>
                        <Input
                            name='full-name'
                            id='full-name'
                            placeholder='Enter user name'
                            innerRef={register({required: true})}
                            className={classnames({'is-invalid': errors['full-name']})}
                        />
                    </FormGroup>
                        <FormGroup>
                            <Label className={'label'} for='phone-number'>
                                Phone number <span className='text-danger'>*</span>
                            </Label>
                            <Input
                                name='phone-number'
                                id='phone-number'
                                type='number'
                                placeholder='Enter phone number'
                                innerRef={register({required: true})}
                                className={classnames({'is-invalid': errors['phone-number']})}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className={'label'} for='email'>
                                Email <span className='text-danger'>*</span>
                            </Label>
                            <Input
                                type='email'
                                name='email'
                                id='email'
                                placeholder='abc@example.com'
                                innerRef={register({required: true})}
                                className={classnames({'is-invalid': errors['email']})}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className={'label'} for='user-role'>Gender</Label>
                            <Input type='select' id='gender' name='gender' value={gender}
                                   onChange={e => setGender(e.target.value)}>
                                <option value={0}>Option</option>
                                <option value={1}>Male</option>
                                <option value={2}>Female</option>
                                <option value={3}>Other</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label className={'label'} for='dob'>Day of birth</Label>
                            <Flatpickr id={'dob'} className='form-control form-control__date'
                                       value={dob}
                                       onChange={(date) => {
                                           setDob(date)
                                       }}/>
                        </FormGroup></div>
                    <div className={'col-6'}><FormGroup>
                        <Label className={'label'} for='email'>
                            Address <span className='text-danger'>*</span>
                        </Label>
                        <Input
                            type='text'
                            name='address'
                            id='address'
                            placeholder='Enter address'
                            innerRef={register({required: true})}
                            className={classnames({'is-invalid': errors['address']})}
                        />
                    </FormGroup>
                        <FormGroup>
                            <Label className={'label'} for='province'>
                                Province <span className='text-danger'>*</span>
                            </Label>
                            <Select
                                isClearable={false}
                                className='react-select'
                                classNamePrefix='select'
                                name='province'
                                options={storeUser?.provinces}
                                getOptionValue={option => option?.id}
                                getOptionLabel={option => option?.name}
                                placeholder={'Select province'}
                                rules={registerOptions.province}
                                onChange={province => {
                                    setProvinceId(province?.id)
                                    dispatch(getDistricts(province?.id))
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className={'label'} for='district'>
                                District <span className='text-danger'>*</span>
                            </Label>
                            <Select
                                isClearable={false}
                                className='react-select'
                                classNamePrefix='select'
                                name='district'
                                options={storeUser?.districts}
                                getOptionValue={option => option?.id}
                                getOptionLabel={option => option?.name}
                                placeholder={'Select district'}
                                onChange={district => {
                                    setDistrictId(district?.id)
                                    dispatch(getWards(district?.id))
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className={'label'} for='ward'>
                                Ward <span className='text-danger'>*</span>
                            </Label>
                            <Select
                                isClearable={false}
                                className='react-select'
                                classNamePrefix='select'
                                name='ward'
                                options={storeUser?.wards}
                                getOptionValue={option => option?.id}
                                getOptionLabel={option => option?.name}
                                placeholder={'Select ward'}
                                onChange={ward => setWardId(ward?.id)}
                            />
                        </FormGroup></div>
                </div>
                {
                    userData.role.name === "ADMIN" &&
                    <FormGroup>
                        <Label className={'label'} for='user-role'>Role user</Label>
                        <Select
                            isClearable={false}
                            className='react-select'
                            classNamePrefix='select'
                            name='role'
                            options={storeRole.roles}
                            getOptionValue={option => option?.id}
                            getOptionLabel={option => option?.name}
                            placeholder={'Select role user'}
                            onChange={role => setRoleId(role?.id)}
                        />
                    </FormGroup>
                }

                <div className={'row mt-2'}>
                    <Button type='submit' className={'ml-1 mr-1'} color='primary'>
                        Save
                    </Button>
                    <Button type='reset' color='secondary' outline onClick={toggleSidebar}>
                        Cancel
                    </Button>
                </div>

            </Form>
        </Sidebar>
    )
}

export default SidebarNewUsers
