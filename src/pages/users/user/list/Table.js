// ** React Imports
import React, { Fragment, useState, useEffect } from 'react'

// ** Invoice List Sidebar
import Sidebar from './Sidebar'

// ** Columns
import { columns } from './columns'

// ** Store & Actions
import { getUsers } from '../store/action'
import { useDispatch, useSelector } from 'react-redux'

// ** Third Party Components
import ReactPaginate from 'react-paginate'
import {ChevronDown, Search} from 'react-feather'
import DataTable from 'react-data-table-component'
import {
    Card,
    Input,
    Row,
    Col,
    Button,
    InputGroup,
    InputGroupAddon,
    InputGroupText
} from 'reactstrap'
import {NoDataComponent} from "../../../../components/NoDataComponent"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/user.scss'
import {useHistory, useLocation} from 'react-router-dom'
import {checkPermissionByScreen} from "../../profile/store/action"
import {PERMISSION_SCREEN_ENUM} from "../../../../enum/permission-screens-constant"

// ** Table Header
const CustomHeader = ({ toggleSidebar, handleFilter, searchTerm, removeValue, checkPermission}) => {
  return (
    <div className='invoice-list-table-header w-100 mt-2 mb-75'>
      <Row className={'w-100 justify-content-between mx-0'}>
          <Col lg={5} sm={12} className='d-flex align-items-center p-0'>
              <InputGroup className='ml-sm-0 input-group-merge product-search'>
                  <Input placeholder='Search by username and email & phone number' value={searchTerm} onChange={e => handleFilter(e.target.value)}/>
                  <InputGroupAddon addonType='append'>
                      <InputGroupText>
                          <Search size={14} />
                      </InputGroupText>
                  </InputGroupAddon>
              </InputGroup>
              <Button.Ripple color='primary' className={'ml-2 w-25'} onClick={ () => {
                  removeValue()
                  handleFilter('')
              }}>Refresh</Button.Ripple>
          </Col>
        <Col xl='2'
          className='d-flex align-items-sm-center justify-content-lg-end justify-content-start flex-lg-nowrap flex-wrap flex-sm-row flex-column px-0 mt-lg-0 mt-1'>
            {
                checkPermission?.create &&  <Button.Ripple color='primary' onClick={toggleSidebar}>Create new</Button.Ripple>
            }

        </Col>
      </Row>
    </div>
  )
}

const UsersList = () => {
  // ** Store Vars
  const dispatch = useDispatch()
  const store = useSelector(state => state.users)
    const history = useHistory()
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)

  // ** States
  const [searchTerm, setSearchTerm] = useState(searchParams?.get('search') ? searchParams?.get('search') : '')
  const [currentPage, setCurrentPage] = useState(searchParams?.get('page') ? searchParams?.get('page') : 1)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [sidebarOpen, setSidebarOpen] = useState(false)
    const [checkPermission, setCheckPermission] = useState(null)
  // ** Function to toggle sidebar
  const toggleSidebar = () => setSidebarOpen(!sidebarOpen)

  // ** Get data on mount
  useEffect(() => {
      localStorage.removeItem('urlUser')
      localStorage.setItem('urlUser', `/user/list?${searchTerm ? `search=${searchTerm}&` : ''}&page=${currentPage}&perPage=10`)
    dispatch(
        getUsers({
        page: currentPage,
        perPage: rowsPerPage,
        q: searchTerm
      })
    )
      checkPermissionByScreen(PERMISSION_SCREEN_ENUM.user_manager).then(res => {
          setCheckPermission(res)
      })
  }, [dispatch, store?.users?.length])

    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.category) {
            params.category = filterParams.category
        }
        if (filterParams.search) {
            params.search = filterParams.search
        }
        if (filterParams.page) {
            params.page = filterParams.page
        }
        params.perPage = filterParams.perPage

        const urlSearchParams = new URLSearchParams(params)
        history.replace({pathname: location.pathname, search: urlSearchParams.toString()})
        localStorage.removeItem('urlUser')
        localStorage.setItem('urlUser', `/user/list?${urlSearchParams}`)
    }
  // ** Function in get data on page change
  const handlePagination = page => {
      checkParams({page: page.selected + 1, perPage: rowsPerPage, search: searchTerm})
    dispatch(
        getUsers({
        page: page.selected + 1,
        perPage: rowsPerPage,
        q: searchTerm
      })
    )
    setCurrentPage(page.selected + 1)
  }

  // ** Function in get data on rows per page
  const handlePerPage = e => {
    const value = parseInt(e.currentTarget.value)
    dispatch(
        getUsers({
        page: currentPage,
        perPage: value,
        q: searchTerm
      })
    )
    setRowsPerPage(value)
  }
  // ** Function in get data on search query change
    const handleFilter = val => {
        checkParams({page: 1, perPage: rowsPerPage, search: val})
        setSearchTerm(val)
        setCurrentPage(1)
        dispatch(
            getUsers({
            page: currentPage,
            perPage: rowsPerPage,
            q: val
            })
        )
    }

  // ** Custom Pagination
  const CustomPagination = () => {
    const count = Number(Math.ceil(store.total / rowsPerPage))

    return (
      <ReactPaginate
        previousLabel={''}
        nextLabel={''}
        pageCount={count || 1}
        activeClassName='active'
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        onPageChange={page => handlePagination(page)}
        pageClassName={'page-item'}
        nextLinkClassName={'page-link'}
        nextClassName={'page-item next'}
        previousClassName={'page-item prev'}
        previousLinkClassName={'page-link'}
        pageLinkClassName={'page-link'}
        containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
      />
    )
  }
  // ** Table data to render
  const dataToRender = () => {
    const filters = {
      q: searchTerm
    }

    const isFiltered = Object.keys(filters).some(function (k) {
      return filters[k].length > 0
    })

    if (store?.users?.length > 0) {
      return store.users
    } else if (store?.users?.length === 0 && isFiltered) {
      return []
    }
  }
  const removeValue = () => {
      setSearchTerm('')
  }

  return (
    <Fragment>
      <Card id={'userContainer'}>
          <div className={'user'}>
              <CustomHeader
                  toggleSidebar={toggleSidebar}
                  handlePerPage={handlePerPage}
                  handleFilter={handleFilter}
                  searchTerm={searchTerm}
                  removeValue={removeValue}
                  checkPermission={checkPermission}
              />
          </div>
          <DataTable
              noHeader
              responsive
              persistTableHead
              paginationServer
              columns={columns}
              sortIcon={<ChevronDown />}
              className='react-dataTable px-0'
              data={dataToRender()}
              noDataComponent={<NoDataComponent message={'No users yet'}/>}
          />
          <div>
              {
                  CustomPagination()
              }
          </div>
      </Card>

      <Sidebar open={sidebarOpen} toggleSidebar={toggleSidebar} />
    </Fragment>
  )
}

export default UsersList
