// ** React Imports
import { Link } from 'react-router-dom'

// ** Custom Components
import Avatar from '@components/avatar'

// ** Store & Actions
import { getUser, deleteUser } from '../store/action'
import { store } from '@store/storeConfig/store'

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge} from 'reactstrap'
import {MoreVertical, Archive, Settings} from 'react-feather'
import React from 'react'
import {convertDate} from "../../../../utility/ConvertDate"

// ** Renders Client Columns
const renderClient = row => {
  const stateNum = Math.floor(Math.random() * 6),
    states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
    color = states[stateNum]
  const name_display = row?.name ? row?.name.split(' ')[0] : ''
  if (row?.avatar) {
    return <Avatar className='mr-1' img={row?.avatar} width='32' height='32' />
  } else {
    return <Avatar color={color || 'primary'} className='mr-1' content={name_display} initials />
  }
}

const statusObj = {
  ACTIVE: 'light-success',
  INACTIVE: 'light-warning',
  DELETED: 'light-danger'
}

const webActiveObj = {
  false: 'light-warning',
  true: 'light-success',
  null: 'light-warning'
}

function paymentWebsite(row) {
  row.web_active = !row.web_active
}

export const columns = [
  {
    name: 'UserInformation',
    minWidth: '297px',
    selector: 'name',
    sortable: true,
    cell: row => (
        <div className='d-flex justify-content-left align-items-center'>
          {renderClient(row)}
          <div className='d-flex flex-column'>
            <Link
                to={`/user/edit/${row.id}`}
                className='user-name text-truncate mb-0'>
              <span className='font-weight-bold'>{row.name}</span>
            </Link>
            <small className='text-truncate text-muted mb-0'>{`phone: ${row.phone}`}</small>
            <small className='text-truncate text-muted mb-0'>{row.email ? `email: ${row.email}` : ''}</small>
          </div>
        </div>
    )
  },
  {
    name: 'Role',
    minWidth: '200px',
    selector: 'name',
    sortable: true,
    cell: row => row?.role?.name
  },
  {
    name: 'Group',
    minWidth: '200px',
    selector: 'name',
    sortable: true,
    cell: row => row?.group?.display_name
  },
  {
    name: 'Status',
    minWidth: '100px',
    selector: 'name',
    sortable: true,
    cell: row => (
        <Badge className='text-capitalize' color={statusObj[row.status]} pill>
          {row.status === 'ACTIVE' ? 'Active' : row.status === 'INACTIVE' ? 'Inactive' : 'Deleted'}
        </Badge>
    )
  },
  {
    name: 'CreatedDate',
    minWidth: '150px',
    selector: 'create_at',
    sortable: true,
    center: 'true',
    cell: row => convertDate(row?.createdAt)
  },
  {
    name: 'Actions',
    minWidth: '100px',
    cell: row => (
      <UncontrolledDropdown>
        <DropdownToggle tag='div' className='btn btn-sm'>
          <MoreVertical size={14} className='cursor-pointer' />
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem
            tag={Link}
            to={`/user/edit/${row?.id}`}
            className='w-100'
          >
            <Archive size={14} className='mr-50' />
            <span className='align-middle'>Edit</span>
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    )
  }
]
