import {GET_DISTRICTS, GET_PROVINCES, GET_USERS, GET_WARDS, GET_ERROR, GET_USER, GET_GROUPS} from '../action'

// ** Initial State
const initialState = {
  allData: [],
  data: [],
  users:[],
  total: 1,
  params: {},
  selectedUser: null,
  errorMessage: null,
  provinces: [],
  districts: [],
  wards: [],
  groups: []
}

const users = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.users,
        total: action.totalPages,
        params: action.params
      }
    case GET_GROUPS:
      return {
        ...state,
        groups: action.groups,
        total: action.totalPages,
        params: action.params
      }
    case GET_USER:
      return {
        ...state,
        selectedUser: action.selectedUser
      }
    case GET_PROVINCES:
      return {
        ...state,
        provinces: action.provinces
      }
    case GET_DISTRICTS:
      return {
        ...state,
        districts: action.districts
      }
    case GET_WARDS:
      return {
        ...state,
        wards: action.wards
      }
    case GET_ERROR:
      return {
        errorMessage: action.errorMessage
      }
    default:
      return { ...state }
  }
}
export default users
