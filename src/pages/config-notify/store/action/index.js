import instances from "../../../../@core/plugin/axios"

export const GET_CONFIG_NOTIFIES_BEGIN = 'GET_CONFIG_NOTIFIES_BEGIN'
export const GET_CONFIG_NOTIFIES_SUCCESS = 'GET_CONFIG_NOTIFIES_SUCCESS'
export const GET_CONFIG_NOTIFIES_FAIL = 'GET_CONFIG_NOTIFIES_FAIL'

export const GET_CONFIG_NOTIFY_BY_ID_BEGIN = 'GET_CONFIG_NOTIFY_BY_ID_BEGIN'
export const GET_CONFIG_NOTIFY_BY_ID_SUCCESS = 'GET_CONFIG_NOTIFY_BY_ID_SUCCESS'
export const GET_CONFIG_NOTIFY_BY_ID_FAIL = 'GET_CONFIG_NOTIFY_BY_ID_FAIL'

export const UPDATE_CONFIG_NOTIFY_BEGIN = 'UPDATE_CONFIG_NOTIFY_BEGIN'
export const UPDATE_CONFIG_NOTIFY_SUCCESS = 'UPDATE_CONFIG_NOTIFY_SUCCESS'
export const UPDATE_CONFIG_NOTIFY_FAIL = 'UPDATE_CONFIG_NOTIFY_FAIL'

export const CREATE_CONFIG_NOTIFY_BEGIN = 'CREATE_CONFIG_NOTIFY_BEGIN'
export const CREATE_CONFIG_NOTIFY_SUCCESS = 'CREATE_CONFIG_NOTIFY_SUCCESS'
export const CREATE_CONFIG_NOTIFY_FAIL = 'CREATE_CONFIG_NOTIFY_FAIL'

export const DELETE_CONFIG_NOTIFY_BEGIN = 'DELETE_CONFIG_NOTIFY_BEGIN'
export const DELETE_CONFIG_NOTIFY_SUCCESS = 'DELETE_CONFIG_NOTIFY_SUCCESS'
export const DELETE_CONFIG_NOTIFY_FAIL = 'DELETE_CONFIG_NOTIFY_FAIL'

export const getConfigNotifies = (params) => {
    const config = {params: params}
    return async dispatch => {
        dispatch({
            type: GET_CONFIG_NOTIFIES_BEGIN
        })
        await instances.get('/admin/config-notify/paging', config).then(response => {
            dispatch({
                type: GET_CONFIG_NOTIFIES_SUCCESS,
                data: response.data?.data?.configNotifies,
                totalRecords: response.data?.data?.totalRecords,
                params
            })
        }).catch(error => {
            dispatch({
                type: GET_CONFIG_NOTIFIES_FAIL,
                errorMessage: error
            })
        })
    }
}

export const getConfigNotify = (id) => {
    return async dispatch => {
        dispatch({
            type: GET_CONFIG_NOTIFY_BY_ID_BEGIN
        })
        await instances.get(`/admin/config-notify/${id}`).then(response => {
            dispatch({
                type: GET_CONFIG_NOTIFY_BY_ID_SUCCESS,
                data: response.data?.data
            })
        }).catch(error => {
            dispatch({
                type: GET_CONFIG_NOTIFY_BY_ID_FAIL,
                errorMessage: error
            })
        })
    }
}

export const createConfigNotify = (data) => {
    return async (dispatch, getState) => {
        dispatch({
            type: CREATE_CONFIG_NOTIFY_BEGIN
        })
        await instances.post(`/admin/config-notify/create`, data)
            .then(() => {
                dispatch({
                    type: CREATE_CONFIG_NOTIFY_SUCCESS
                })
            })
            .then(() => {
                dispatch(getConfigNotifies(getState().telegramToken.params))
            })
            .catch(error => {
                dispatch({
                    type: CREATE_CONFIG_NOTIFY_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const updateConfigNotify = (id, data) => {
    return async (dispatch, getState) => {
        dispatch({
            type: UPDATE_CONFIG_NOTIFY_BEGIN
        })
        await instances.put(`/admin/config-notify/${id}`, data)
            .then(() => {
                dispatch({
                    type: UPDATE_CONFIG_NOTIFY_SUCCESS
                })
            })
            .then(() => {
                dispatch(getConfigNotifies(getState().telegramToken.params))
            })
            .catch(error => {
                dispatch({
                    type: UPDATE_CONFIG_NOTIFY_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const deleteConfigNotify = (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DELETE_CONFIG_NOTIFY_BEGIN
        })
        await instances.delete(`/admin/config-notify/${id}`)
            .then(() => {
                dispatch({
                    type: DELETE_CONFIG_NOTIFY_SUCCESS
                })
            })
            .then(() => {
                dispatch(getConfigNotifies(getState().telegramToken.params))
            })
            .catch(error => {
                dispatch({
                    type: DELETE_CONFIG_NOTIFY_FAIL,
                    errorMessage: error
                })
            })
    }
}
