import {
  UPDATE_CONFIG_NOTIFY_SUCCESS,
  UPDATE_CONFIG_NOTIFY_FAIL,
  UPDATE_CONFIG_NOTIFY_BEGIN,
  GET_CONFIG_NOTIFIES_BEGIN,
  GET_CONFIG_NOTIFIES_SUCCESS,
  GET_CONFIG_NOTIFIES_FAIL,
  CREATE_CONFIG_NOTIFY_FAIL,
  CREATE_CONFIG_NOTIFY_SUCCESS,
  CREATE_CONFIG_NOTIFY_BEGIN,
  GET_CONFIG_NOTIFY_BY_ID_BEGIN,
  GET_CONFIG_NOTIFY_BY_ID_SUCCESS,
  GET_CONFIG_NOTIFY_BY_ID_FAIL,
  DELETE_CONFIG_NOTIFY_FAIL,
  DELETE_CONFIG_NOTIFY_SUCCESS,
  DELETE_CONFIG_NOTIFY_BEGIN
} from "../action"

// ** Initial State
const initialState = {
  loading: false,
  totalRecords: 0,
  errorMessage: null,
  configNotifies: [],
  configNotify: [],
  params: {}
}

const configNotify = (state = initialState, action) => {
  switch (action.type) {
    case GET_CONFIG_NOTIFIES_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_CONFIG_NOTIFIES_SUCCESS:
      return {
        ...state,
        configNotifies: action.data,
        totalRecords: action.totalRecords,
        params: action.params,
        loading: false
      }
    case GET_CONFIG_NOTIFIES_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case UPDATE_CONFIG_NOTIFY_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case UPDATE_CONFIG_NOTIFY_SUCCESS:
      return {
        ...state,
        configNotify: null,
        loading: false
      }
    case UPDATE_CONFIG_NOTIFY_FAIL:
      return {
        configNotify: null,
        errorMessage: action.errorMessage,
        loading: false
      }
    case CREATE_CONFIG_NOTIFY_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case CREATE_CONFIG_NOTIFY_SUCCESS:
      return {
        ...state,
        loading: false
      }
    case CREATE_CONFIG_NOTIFY_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case DELETE_CONFIG_NOTIFY_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case DELETE_CONFIG_NOTIFY_SUCCESS:
      return {
        ...state,
        loading: false
      }
    case DELETE_CONFIG_NOTIFY_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_CONFIG_NOTIFY_BY_ID_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_CONFIG_NOTIFY_BY_ID_SUCCESS:
      return {
        ...state,
        configNotify: action.data,
        loading: false
      }
    case GET_CONFIG_NOTIFY_BY_ID_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    default:
      return { ...state }
  }
}
export default configNotify
