// ** User List Component
import Table from './Table'

// ** Styles
import '@styles/react/apps/app-users.scss'
import React from "react"

const ConfigNotifyList = () => {
  return (
    <div id='userCustomer' className='app-user-list'>
      <Table />
    </div>
  )
}

export default ConfigNotifyList
