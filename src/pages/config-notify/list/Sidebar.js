// ** React Import
import React, {useEffect, useState} from 'react'

// ** Custom Components
import Sidebar from '@components/sidebar'
import "flatpickr/dist/themes/material_blue.css"
// ** Utils
import {isObjEmpty} from '@utils'

// ** Third Party Components
import classnames from 'classnames'
import {useForm} from 'react-hook-form'
import {Button, FormGroup, Form, Input, Label, CustomInput} from 'reactstrap'

// ** Store & Actions
import {
    createConfigNotify,
    getConfigNotify,
    updateConfigNotify
} from '../store/action'
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../components/Toast/ToastFail"
import { store } from '@store/storeConfig/store'

const SidebarConfigNotify = ({id, open, toggleSidebar}) => {
    const configNotifyStore = store.getState()?.configNotify
    // ** Vars
    const {register, errors, handleSubmit} = useForm()
    const [isSOS, setIsSOS] = useState(true)

    useEffect(() => {
        if (id) {
            store.dispatch(getConfigNotify(id))
        }
    }, [])
    useEffect(() => {
        if (id) {
            setIsSOS(configNotifyStore?.configNotify?.is_sos)
        }
    }, [configNotifyStore?.configNotify])

    // ** Function to handle form submit
    const onSubmit = async (values) => {
        if (isObjEmpty(errors)) {
            const data = {}
            data.name = values.name
            data.outstanding = values.outstanding
            data.win_loss = values.win_loss
            data.is_sos = isSOS
            if (configNotifyStore?.configNotify?.id) {
                await store.dispatch(updateConfigNotify(configNotifyStore?.configNotify?.id, data))
                if (!configNotifyStore?.errorMessage) {
                    toggleSidebar()
                    toast.success(<SuccessNotificationToast/>)
                } else {
                    toast.error(<ErrorNotificationToast message={configNotifyStore?.errorMessage}/>)
                }
            } else {
                await store.dispatch(createConfigNotify(data))
                if (!configNotifyStore?.errorMessage) {
                    toggleSidebar()
                    toast.success(<SuccessNotificationToast message={'Create new successfully!'}/>)
                } else {
                    toast.error(<ErrorNotificationToast message={configNotifyStore?.errorMessage}/>)
                }
            }
        }
    }

    function changeIsSOS (e) {
        setIsSOS(e?.target?.checked)
    }

    return (
        <Sidebar
            size='lg'
            open={open}
            title={`${id ? 'Edit' : 'Create new'} config`}
            headerClassName='mb-1'
            contentClassName='pt-0 user__side-bar'
            toggleSidebar={toggleSidebar}
        >
            <Form onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
                <FormGroup>
                    <Label className={'label'} for='name'>Name <span className='text-danger'>*</span></Label>
                    <Input
                        name='name'
                        id='name'
                        placeholder='Enter name'
                        innerRef={register({required: true})}
                        defaultValue={configNotifyStore?.configNotify?.name}
                        className={classnames({'is-invalid': errors['name']})}
                    />
                </FormGroup>
                <FormGroup>
                    <Label className={'label'} for='outstanding'>Outstanding <span className='text-danger'>*</span></Label>
                    <Input
                        name='outstanding'
                        id='outstanding'
                        placeholder='Enter outstanding'
                        type={'number'}
                        innerRef={register({required: true})}
                        defaultValue={configNotifyStore?.configNotify?.outstanding}
                        className={classnames({'is-invalid': errors['outstanding']})}
                    />
                </FormGroup>
                <FormGroup>
                    <Label className={'label'} for='token_id'>Win/Loss <span className='text-danger'>*</span></Label>
                    <Input
                        name='win_loss'
                        id='win_loss'
                        placeholder='Enter win/loss'
                        type={'number'}
                        defaultValue={configNotifyStore?.configNotify?.win_loss}
                        innerRef={register({required: true})}
                        className={classnames({'is-invalid': errors['win_loss']})}
                    />
                </FormGroup>
                <FormGroup>
                    <CustomInput
                        type='checkbox'
                        id={'add'}
                        label={'SOS'}
                        checked={isSOS}
                        className={classnames('mr-1')}
                        onChange={(e) => {
                            changeIsSOS(e)
                        }}
                    />
                </FormGroup>
                <div className={'row mt-2'}>
                    <Button type='submit' className={'ml-1 mr-1'} color='primary'>Save</Button>
                    <Button type='reset' color='secondary' outline onClick={toggleSidebar}>Cancel</Button>
                </div>
            </Form>
        </Sidebar>
    )
}

export default SidebarConfigNotify
