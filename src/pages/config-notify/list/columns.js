// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge} from 'reactstrap'
import {Edit3, MoreVertical, Trash2} from 'react-feather'
import React from 'react'
import {convertDate} from "../../../utility/ConvertDate"

import { store } from '@store/storeConfig/store'

import {deleteConfigNotify} from '../store/action'

export const columns = (handleShowPopupUpdate => [
  {
    name: 'Id',
    width: '100px',
    sortable: true,
    selector: 'id',
    cell: row => row?.id
  },
  {
    name: 'Name',
    minWidth: '150px',
    cell: row => row?.name
  },
  {
    name: 'Outstanding',
    minWidth: '150px',
    center: 'true',
    cell: row => row?.outstanding
  },
  {
    name: 'Win/Loss',
    minWidth: '150px',
    center: 'true',
    cell: row => row?.win_loss
  },
  {
    name: 'Warning 🆘',
    minWidth: '150px',
    center: 'true',
    cell: row => (
        <Badge className='text-capitalize' color={row?.is_sos ? 'light-danger' : 'light-success'} pill>
          {row.is_sos ? 'Warning' : 'Notify'}
        </Badge>
    )
  },
  {
    name: 'CreateDate',
    minWidth: '150px',
    selector: 'created_at',
    center: 'true',
    sortable: true,
    cell: row => convertDate(row?.created_at)
  },
  {
    name: 'Actions',
    width: '100px',
    cell: row => (
        <UncontrolledDropdown>
          <DropdownToggle tag='div' className='btn btn-sm'>
            <MoreVertical size={14} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem
                onClick={() => handleShowPopupUpdate(row?.id)}
                className='w-100'>
              <Edit3 size={14} className='mr-50' />
              <span className='align-middle'>Edit</span>
            </DropdownItem>
            <DropdownItem
                onClick={() => store.dispatch(deleteConfigNotify(row?.id))}
                className='w-100'>
              <Trash2 size={14} className='mr-50' />
              <span className='align-middle'>Delete</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
])
