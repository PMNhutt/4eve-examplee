// ** React Imports
import React, {Fragment, useState, useEffect} from 'react'

// ** Invoice List Sidebar
import Sidebar from './Sidebar'

// ** Columns
import {columns} from './columns'

// ** Store & Actions
import {
    fetchCrawlerAccount,
    updateStatusCrawlerAccount
} from '../store/action'
import {useDispatch, useSelector} from 'react-redux'

// ** Third Party Components
import {ChevronDown, RotateCcw, Search} from 'react-feather'
import DataTable from 'react-data-table-component'
import {
    Card,
    Input,
    Row,
    Col,
    Button,
    InputGroup,
    InputGroupAddon,
    InputGroupText, FormGroup
} from 'reactstrap'
import {NoDataComponent} from "../../../components/NoDataComponent"
import {useHistory, useLocation} from 'react-router-dom'
import {Loading} from "../../../components/Loading"
import ReactPaginate from "react-paginate"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/crawler-account.scss'
import {checkPermissionByScreen} from "../../users/profile/store/action"
import {PERMISSION_SCREEN_ENUM} from "../../../enum/permission-screens-constant"
import Select from "react-select"
import Countdown from "react-countdown"
import PopupSelectPromotionProducts from "../../../components/PopUp/components/PopupSelectPromotionProducts"


// ** Table Header
const CustomHeader = ({toggleSidebar, handleFilter, searchTerm, checkPermission, storeCrawler, handleChangeDomain, domain, count, renderCountdown, setIsOpen}) => {
    return (
        <div className='invoice-list-table-header w-100 mt-2 mb-75'>
            <Row className={'w-100 justify-content-between mx-0'}>
                <Col lg={9} sm={12} className='d-flex align-items-center p-0'>
                    <Row className={'w-100'}>
                        <Col lg={3}>
                            <InputGroup className='ml-sm-0 input-group-merge product-search'>
                                <Input placeholder='Search by account name' value={searchTerm}
                                       onChange={e => handleFilter(e.target.value)}/>
                                <InputGroupAddon addonType='append'>
                                    <InputGroupText>
                                        <Search size={14}/>
                                    </InputGroupText>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                        <Col lg={3}>
                            <Select
                                isClearable={false}
                                className='react-select'
                                classNamePrefix='select'
                                name='domain'
                                options={[{id: 0, name: 'Select all'}, ...storeCrawler.domains]}
                                getOptionValue={option => option.id}
                                getOptionLabel={option => option.name}
                                value={storeCrawler.domains.find(item => item.id === parseInt(domain)) || {id: 0, name: 'Select all'}}
                                placeholder={'Select domain'}
                                onChange={handleChangeDomain}
                            />
                        </Col>
                        <Col lg={5}>
                            <div className={'d-flex align-items-center'}>
                                {
                                    <Countdown className={'container-countdown'} renderer={renderCountdown} date={Date.now() + count}/>
                                }
                                Or
                                <Button color='primary' className={'px-2 ml-1'} onClick={() => window.location.reload(false)}><RotateCcw size={14}/></Button>
                            </div>

                        </Col>
                    </Row>
                </Col>
                <Col xl='2'
                     className='d-flex align-items-sm-center justify-content-lg-end justify-content-start flex-lg-nowrap flex-wrap flex-sm-row flex-column px-0 mt-lg-0 mt-1'>
                    {
                        checkPermission?.create &&
                        <Button.Ripple color='primary' className={'mr-1'} onClick={() => setIsOpen(true)}>Import</Button.Ripple>
                    }
                    {
                        checkPermission?.create &&
                        <Button.Ripple color='primary' onClick={toggleSidebar}>Create</Button.Ripple>
                    }
                </Col>
            </Row>
        </div>
    )
}

const CrawlerAccountList = () => {
    // ** Store Vars
    const dispatch = useDispatch()
    const storeCrawler = useSelector(state => state.crawler)
    const history = useHistory()
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)
    const rowPerPage = 10
    const [checkPermission, setCheckPermission] = useState(null)
    const [dataDetail, setDataDetail] = useState(null)

    // ** States
    const [searchTerm, setSearchTerm] = useState(searchParams?.get('search') ? searchParams?.get('search') : '')
    const [domain, setDomain] = useState(searchParams?.get('domain') ? searchParams?.get('domain') : '')
    const [currentPage, setCurrentPage] = useState(searchParams?.get('page') ? searchParams?.get('page') : 1)
    const [sidebarOpen, setSidebarOpen] = useState(false)
    const time = new Date().getTime()
    const count = 300000
    const [isOpen, setIsOpen] = useState(false)

    // ** Function to toggle sidebar
    const toggleSidebar = () => {
        setDataDetail(null)
        setSidebarOpen(!sidebarOpen)
    }
    // ** Get data on mount
    useEffect(() => {
        const params = {domain_id: domain, search: searchTerm, page: currentPage, perPage: rowPerPage}
        handleGetCrawlerAccounts(params)
        checkPermissionByScreen(PERMISSION_SCREEN_ENUM.crawler_account).then(res => {
            setCheckPermission(res)
        })
    }, [])
    function refresh() {
        if (new Date().getTime() - time >= count) {
            window.location.reload(1)
        } else {
           setTimeout(refresh, count)
        }

    }
    setTimeout(refresh, count)


    const handleGetCrawlerAccounts = (params) => {
        dispatch(fetchCrawlerAccount(params))
    }
    const getData = () => {
        const params = {domain_id: domain, search: searchTerm, page: 1, perPage: rowPerPage}
        handleGetCrawlerAccounts(params)
    }
    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.search) {
            params.search = filterParams.search
        }
        if (filterParams.page) {
            params.page = filterParams.page
        }
        if (filterParams?.domain_id) {
            params.domain = filterParams?.domain_id
        }
        const urlSearchParams = new URLSearchParams(params)
        history.replace({pathname: location.pathname, search: urlSearchParams.toString()})
    }

    // ** Function in get data on search query change
    const handleFilter = val => {
        const params = {domain_id: domain, search: val, page: 1, perPage: rowPerPage}
        checkParams(params)
        handleGetCrawlerAccounts(params)
        setSearchTerm(val)
        setCurrentPage(1)
    }
    const handleChangeDomain = value => {
        const params = {search: searchTerm, domain_id: value?.id, page: 1, perPage: rowPerPage}
        checkParams(params)
        handleGetCrawlerAccounts(params)
        setDomain(value?.id)
        setCurrentPage(1)
    }

    // ** Table data to render
    const dataToRender = () => {
        if (storeCrawler.crawler_accounts?.length > 0) {
            return storeCrawler.crawler_accounts
        } else {
            return []
        }
    }

    // ** Custom Pagination
    const CustomPagination = () => {
        const count = Number(Math.ceil(storeCrawler.totalRecords / rowPerPage))
        if (count > 1) {
            return (
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    pageCount={count || 1}
                    activeClassName='active'
                    forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                    onPageChange={handlePagination}
                    pageClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next'}
                    previousClassName={'page-item prev'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
                />
            )
        } else return <></>
    }

    const handlePagination = page => {
        const params = {page: page.selected + 1, perPage: rowPerPage, search: searchTerm, domain_id: domain}
        handleGetCrawlerAccounts(params)
        checkParams(params)
        setCurrentPage(page.selected + 1)
    }

    const handleGetDataDetail = (data) => {
        setDataDetail(data)
        setSidebarOpen(true)
    }
    function renderCountdown ({minutes, seconds, completed }) {
        if (completed) {
            return (
                <div className={'mr-1'}>
                    Update time
                    <p className={'text-danger mb-0'}> 00:00</p>
                </div>
            )
        } else {
            return (
                <div className={'mr-1'}>
                    Update time
                    <p className={'text-danger mb-0'}> {minutes < 10 ? `0${minutes}` : minutes}:{seconds < 10 ? `0${seconds}` : seconds}</p>
                </div>
            )
        }
    }

    return (
        <Fragment>
            {
                storeCrawler.loading && <Loading/>
            }
            <Card id={'userContainer'}>
                <div className={'user'}>
                    <CustomHeader
                        toggleSidebar={toggleSidebar}
                        handleFilter={handleFilter}
                        searchTerm={searchTerm}
                        checkPermission={checkPermission}
                        storeCrawler={storeCrawler}
                        handleChangeDomain={handleChangeDomain}
                        domain={domain}
                        count={count}
                        renderCountdown={renderCountdown}
                        setIsOpen={setIsOpen}
                    />
                </div>
                <DataTable
                    noHeader
                    responsive
                    persistTableHead
                    paginationServer
                    columns={columns(storeCrawler?.domains, handleGetDataDetail)}
                    sortIcon={<ChevronDown/>}
                    className='react-dataTable px-0'
                    data={dataToRender()}
                    noDataComponent={<NoDataComponent message={'Currently, there is no crawler account'}/>}
                />
                <div>
                    {
                        CustomPagination()
                    }
                </div>
            </Card>

            <Sidebar open={sidebarOpen} storeCrawler={storeCrawler} toggleSidebar={toggleSidebar} dataDetail={dataDetail}/>
            <PopupSelectPromotionProducts isOpen={isOpen} toggle={() => setIsOpen(false)} getData={() => getData()}
            />
        </Fragment>
    )
}

export default CrawlerAccountList
