// ** React Import
import React, {useEffect, useState} from 'react'

// ** Custom Components
import Sidebar from '@components/sidebar'
import "flatpickr/dist/themes/material_blue.css"
// ** Utils
import {isObjEmpty} from '@utils'

// ** Third Party Components
import classnames from 'classnames'
import {useForm} from 'react-hook-form'
import {Button, FormGroup, Form, Input, Label, CustomInput} from 'reactstrap'

// ** Store & Actions
import {
    createCrawlerAccount, deleteAccount, deleteCrawlerAccount,
    fetchCrawlerAccount,
    fetchDomains,
    updateCrawlerAccount, updateCrawlerAccountStatus,
    verifyLoginAccount
} from '../store/action'
import Select from "react-select"
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../components/Toast/ToastFail"
import { store } from '@store/storeConfig/store'
import {useSelector} from "react-redux"
import {Eye, EyeOff} from "react-feather"
import {decrypt} from "../../../utility/Crypto"
import {ModalCheckLogin} from "../../../components/Modal/ModalCheckLogin"
import {Loading} from "../../../components/Loading"

const crawlerTimes = [
    {
        label: '5 minutes',
        value: 300
    },
    {
        label: '15 minutes',
        value: 900
    },
    {
        label: '30 minutes',
        value: 1800
    },
    {
        label: '1 hour',
        value: 3600
    },
    {
        label: '2 hours',
        value: 7200
    },
    {
        label: '6 hours',
        value: 21600
    },
    {
        label: '12 hours',
        value: 43200
    }
]

const levels = [
    {
        label: 'Level 1 agents',
        value: 1
    },
    {
        label: 'Level 2 agents',
        value: 2
    },
    {
        label: 'Level 3 agents',
        value: 3
    },
    {
        label: 'Level 4 agents',
        value: 4
    },
    {
        label: 'Level 5 agents',
        value: 5
    }
]

const SidebarCrawlerAccount = ({open, toggleSidebar, storeCrawler, dataDetail}) => {
    const storeTelegram = useSelector(state => state.telegram)
    const [showPassword, setShowPassword] = useState(false)
    const [showSecureCode, setShowSecureCode] = useState(false)
    const [defaultUser, setDefaultUser] = useState('')
    const [defaultPass, setDefaultPass] = useState('')
    const [defaultCode, setDefaultCode] = useState('')
    // ** States
    const [account, setAccount] = useState({})
    // ** Vars
    const {register, errors, handleSubmit} = useForm()
    const [process, setProcess] = useState('temp')
    const [status, setStatus] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        store.dispatch(fetchDomains())
    }, [])
    useEffect(() => {
        if (dataDetail) {
            handleUpdateAccount(dataDetail)
        } else {
            setDefaultUser('')
            setDefaultPass('')
            setDefaultCode('')
            delete account.id
            account.time_crawler = crawlerTimes[2]?.value
            account.status = false
            account.process = 'temp'
            setProcess(account.process)
            setErrorMessage('')
            setAccount({...account})
        }
    }, [dataDetail])

    const handleUpdateAccount = (dataDetail) => {
        account.id = +dataDetail.id
        account.time_crawler = dataDetail?.time_crawler
        account.status = dataDetail?.status
        account.domain_id = dataDetail?.domain_id
        account.process = dataDetail?.process
        account.error_count = dataDetail.error_count
        const passwords = dataDetail?.password?.split("@")
        if (passwords) {
            setDefaultPass(decrypt(passwords[0], passwords[1]))
            account.password = decrypt(passwords[0], passwords[1])
        }
        const secureCodes = dataDetail?.secure_code?.split("@")
        if (secureCodes) {
            setDefaultCode(decrypt(secureCodes[0], secureCodes[1]))
            account.secure_code = decrypt(secureCodes[0], secureCodes[1])
        }
        account.username = dataDetail?.username
        setDefaultUser(account.username)
        setProcess(dataDetail?.process ? dataDetail?.process : 'temp')
        setStatus(dataDetail.status)
        setErrorMessage(dataDetail?.error_message)
        setAccount({...account})
    }

    // ** Function to handle form submi

    const handleChangeTimeCrawler = (timeObj) => {
        account.time_crawler = timeObj?.value
        setAccount({...account})
    }

    const onSubmit = async (values) => {
        if (isObjEmpty(errors)) {
            if (!account?.domain_id || !account.time_crawler) {
                toast.error(<ErrorNotificationToast message={'Please complete all information!'}/>)
            } else {
                account.username = values.username
                account.password = values.password || undefined
                account.secure_code = values.secure_code || undefined
                account.status = false
                account.process = 'verifying'
                if (account.id === undefined || account.id === null) {
                    createCrawlerAccount(account).then(res => {
                        if (res?.data?.status) {
                            store.dispatch(fetchCrawlerAccount(store.getState().crawler.params))
                            handleUpdateAccount(res?.data?.data)
                            handleLogin()
                            toast.success(<SuccessNotificationToast/>)
                        }
                    }).catch(error => {
                        toast.error(<ErrorNotificationToast message={error.response?.data?.message}/>)
                    })
                } else {
                    updateCrawlerAccount(account?.id, account).then(res => {
                        if (res?.data?.status) {
                            store.dispatch(fetchCrawlerAccount(store.getState().crawler.params))
                            handleUpdateAccount(res?.data?.data)
                            handleLogin()
                            toast.success(<SuccessNotificationToast/>)
                        }
                    }).catch(error => {
                        toast.error(<ErrorNotificationToast message={error.response?.data?.message}/>)
                    })
                }

            }
        }
    }

    const handleChangeDomain = (domainObj) => {
        account.domain_id = domainObj?.id
        setAccount({...account})
    }

    function handleDelete() {
        deleteCrawlerAccount(account?.id).then(res => {
            if (res?.data) {
                store.dispatch(fetchCrawlerAccount(store.getState().crawler.params))
                toggleSidebar()
                toast.success(<SuccessNotificationToast/>)
            }
        }).catch(error => {
            toast.error(<ErrorNotificationToast message={error.response?.data?.message}/>)
        })
    }

    async function handleActive() {
        const data = {}
        data.status = !account.status
        await updateCrawlerAccountStatus(account?.id, data)
        account.status = data.status
        setAccount({...account})
        setStatus(data.status)
        store.dispatch(fetchCrawlerAccount(store.getState().crawler.params))
    }

    async function handleLogin() {
        setLoading(true)
        if (loading) {
            return
        }
        const domain = storeCrawler.domains.find(item => item?.id === account?.domain_id)
        const body = {}
        body.packageName = domain.package_name
        body.domain = domain.domain
        body.username = account.username
        body.password = account.password
        body.securityCode = account.secure_code
        setProcess('verifying')
        const data = {}
        data.process = 'verifying'
        data.error_count = 0
        data.status = false
        const response = await updateCrawlerAccountStatus(account?.id, data)
        if (response.data.status) {
            const res = await verifyLoginAccount(domain.login_domain, body)
            setLoading(false)
            if ((res.data.authAccessToken !== undefined && res.data.authAccessToken !== null)
                || (res.data.authCookie !== undefined && res.data.authCookie !== null)) {
                data.process = 'verified'
                data.status = false
                data.error_message = ''
                setErrorMessage(data?.error_message)
                setProcess('verified')
                await updateCrawlerAccountStatus(account?.id, data)
                store.dispatch(fetchCrawlerAccount(store.getState().crawler.params))
            } else {
                data.process = 'failed'
                data.status = false
                data.error_message = 'Username and password are incorrect'
                setErrorMessage(data?.error_message)
                setProcess('failed')
                await updateCrawlerAccountStatus(account?.id, data)
                store.dispatch(fetchCrawlerAccount(store.getState().crawler.params))
            }
        }
    }
    return (
        <Sidebar
            size='lg'
            open={open}
            title={`${dataDetail ? 'Update' : 'Create new'} account`}
            headerClassName='mb-1'
            contentClassName='pt-0 user__side-bar'
            toggleSidebar={toggleSidebar}
        >
            {
                loading && <Loading/>
            }
            <Form onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
                <FormGroup>
                    <Label className={'label'}>Select domain <span className='text-danger'>*</span></Label>
                    <Select
                        isClearable={false}
                        className='react-select'
                        classNamePrefix='select'
                        name='company'
                        options={storeCrawler.domains}
                        getOptionValue={option => option?.id}
                        getOptionLabel={option => option?.name}
                        defaultValue={storeCrawler.domains.find(item => item?.id === account?.domain_id)}
                        placeholder={'Select company'}
                        onChange={handleChangeDomain}
                    />
                </FormGroup>
                <FormGroup>
                    <Label className={'label'} for='username'>Account <span className='text-danger'>*</span></Label>
                    <Input
                        name='username'
                        id='username'
                        placeholder='account'
                        defaultValue={defaultUser}
                        innerRef={register({required: true})}
                        className={classnames({'is-invalid': errors['username']})}
                    />
                </FormGroup>
                <FormGroup>
                    <Label className={'label'} for='password'>Password <span className='text-danger'>*</span></Label>
                    <div className={'d-flex align-items-center position-relative'}>
                        <Input type='password' className={'w-0 p-0 border-0'} />
                        <Input
                            name='password'
                            id='password'
                            type={showPassword ? 'text' : 'password'}
                            placeholder='Nhập password'
                            defaultValue={defaultPass}
                            innerRef={register({required: !dataDetail })}
                            className={classnames({'is-invalid': errors['password']})}
                        />
                        <div className={'position-absolute icon-password'} onClick={() => setShowPassword(!showPassword)}>
                            {
                                showPassword &&
                                <EyeOff size={12}/>
                            }
                            {
                                !showPassword &&
                                <Eye size={12}/>
                            }
                        </div>
                    </div>
                </FormGroup>
                <FormGroup>
                    <Label className={'label'} for='secure_code'>
                        Security code
                    </Label>
                    <div className={'d-flex align-items-center position-relative'}>
                        <Input type='password' className={'w-0 p-0 border-0'} />
                        <Input
                            name='secure_code'
                            id='secure_code'
                            type={showSecureCode ? 'text' : 'password'}
                            defaultValue={defaultCode}
                            innerRef={register({required: false })}
                            placeholder='Nhập secure code'
                            className={classnames({'is-invalid': errors['secure_code']})}
                        />
                        <div className={'position-absolute icon-password'} onClick={() => setShowSecureCode(!showSecureCode)}>
                            {
                                showSecureCode &&
                                    <EyeOff size={12}/>
                            }
                            {
                                !showSecureCode &&
                                <Eye size={12}/>
                            }

                        </div>
                    </div>

                </FormGroup>
                <FormGroup>
                    <Label className={'label'}>Time crawler <span className='text-danger'>*</span></Label>
                    <Select
                        isDisabled={true}
                        isClearable={false}
                        className='react-select'
                        classNamePrefix='select'
                        name='timeCrawler'
                        options={crawlerTimes}
                        defaultValue={crawlerTimes?.find(item => item?.value === account?.time_crawler) || crawlerTimes[2]}
                        placeholder={'time crawler'}
                        onChange={handleChangeTimeCrawler}
                    />
                </FormGroup>
                {
                    errorMessage &&
                    <FormGroup>
                        <Label className={'label'}>Message <span className='text-danger'>*</span></Label>
                        <Label
                            isClearable={false}
                            className='react-select'
                            classNamePrefix='select'
                            isDisabled={dataDetail}
                        >
                            {errorMessage}
                        </Label>
                    </FormGroup>
                }
                <ModalCheckLogin process={process}/>

                <div className={'row mt-2'}>
                    <Button type='submit' className={'ml-1'} color='primary'>Save</Button>
                    {
                        (process === 'verifying' || process === 'failed' || process === 'verified') &&
                        <Button onClick={handleLogin} className={'ml-1'} color='warning'>Verify login</Button>
                    }
                    {
                        process === 'verified' && status === true &&
                        <Button onClick={handleActive} className={'ml-1'} color='warning'>Locked</Button>
                    }
                    {
                        process === 'verified' && status === false &&
                        <Button onClick={handleActive} className={'ml-1'} color='success'>Unlock</Button>
                    }
                    {
                        (process === 'failed') &&
                        <Button onClick={handleDelete} className={'ml-1'} color='danger'>Delete</Button>
                    }
                    <Button type='reset' color='secondary' className={'ml-1'} outline onClick={toggleSidebar}>Cancel</Button>
                </div>
            </Form>
        </Sidebar>
    )
}

export default SidebarCrawlerAccount
