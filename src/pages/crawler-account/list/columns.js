// ** Store & Actions
import {actionCrawlerWithAccount, deleteAccount, updateStatusCrawlerAccount} from '../store/action'
import { store } from '@store/storeConfig/store'

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge} from 'reactstrap'
import {MoreVertical, Lock, Unlock, Play, Archive, Trash} from 'react-feather'
import React from 'react'
import {convertTimeDateNotYear} from "../../../utility/ConvertDate"
import {Link} from "react-router-dom"

function handleCheckStatus (row) {
  if (row.crawler_status) {
    return 'Scan successful'
  } else if (row.process === 'verified') {
    if (row.error_count === 0 && row.last_time_crawler !== null) {
      return 'Scan in progress'
    } else if (row.error_count > 0 && row.last_time_crawler !== null) {
      return `Scan failed`
    } else {
      return `Waiting for scan`
    }
  } else {
    if (row.error_count > 0 && row.last_time_crawler !== null) {
      return `Scan failed`
    } else {
      return 'Not run'
    }
  }
}

function handleColor (message) {
  switch (message) {
    case 'Scan successful' :
      return 'light-success'
    case 'Scan in progress' :
      return 'light-warning'
    case 'Scan failed' :
      return 'light-danger'
    case 'Not run' :
      return 'light-secondary'
    case 'Waiting for scan':
      return 'light-info'
  }
}
export const columns = ((domains, handleGetDataDetail) => [
  {
    name: 'Account',
    minWidth: '150px',
    sortable: true,
    cell: row => <Link to={`/agent/list?page=1&status=true&perPage=10&account=${row?.username}`} target={'_blank'}>
      {row?.username}
    </Link>
  },
  {
    name: 'Company',
    minWidth: '150px',
    sortable: true,
    cell: row => row?.domain?.name
  },
  {
    name: 'ScanTime',
    width: '150px',
    center: true,
    cell: row => {
      if (row?.time_crawler >= 3600) {
        return `${row?.time_crawler / 3600} hours`
      } else {
        if (row?.time_crawler < 60) {
          return `${row?.time_crawler} seconds`
        } else {
          return `${row?.time_crawler / 60} minutes`
        }
      }
    }
  },
  {
    name: 'LastScan',
    width: '150px',
    center: true,
    cell: row => {
      return convertTimeDateNotYear(new Date(row.last_time_crawler))
    }
  },
  {
    name: 'NextScan',
    width: '150px',
    center: true,
    cell: row => {
      return convertTimeDateNotYear(new Date(row.next_time_crawler))
    }
  },
  {
    name: 'ScanStatus',
    minWidth: '150px',
    center: 'true',
    sortable: true,
    cell: row => (
        <Badge className='text-capitalize' color={handleColor(handleCheckStatus(row))} pill>
          {handleCheckStatus(row)}
        </Badge>
    )
  },
  {
    name: 'Status',
    minWidth: '150px',
    center: 'true',
    sortable: true,
    cell: row => (
        <Badge className='text-capitalize' color={row?.status ? 'light-success' : 'light-warning'} pill>
          {row.status ? 'Active' : 'Inactive'}
        </Badge>
    )
  },
  {
    name: 'Actions',
    width: '100px',
    cell: row => (
        <UncontrolledDropdown>
          <DropdownToggle tag='div' className='btn btn-sm'>
            <MoreVertical size={14} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem
                onClick={() => {
                  handleGetDataDetail(row)
                }}
                className='w-100'>
               <Archive size={14} className='mr-50' />
              <span className='align-middle'>{'Edit'}</span>
            </DropdownItem>
            {/*<DropdownItem*/}
            {/*    onClick={() => {*/}
            {/*      store.dispatch(actionCrawlerWithAccount(row?.id))*/}
            {/*    }}*/}
            {/*    className='w-100'>*/}
            {/*  <Play size={14} className='mr-50' />*/}
            {/*  <span className='align-middle'>{'Check scan data'}</span>*/}
            {/*</DropdownItem>*/}
            {/*{*/}
            {/*  row.process === 'verified' &&*/}
            {/*  */}
            {/*}*/}
            <DropdownItem
                onClick={() => {
                  row.status = !row?.status
                  store.dispatch(updateStatusCrawlerAccount(row?.id, row))
                }}
                className='w-100'>
              {
                row?.status ? <Lock size={14} className='mr-50' /> : <Unlock size={14} className='mr-50' />
              }
              <span className='align-middle'>{row?.status ? 'Lock' : 'Unlock'}</span>
            </DropdownItem>
            <DropdownItem
                onClick={() => {
                  store.dispatch(deleteAccount(row?.id))
                }}
                className='w-100'>
              <Trash size={14} className='mr-50' />
              <span className='align-middle'>Delete</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
])
