import instances from "../../../../@core/plugin/axios"
import axios from "axios"

export const GET_CRAWLER_ACCOUNTS_ERROR = 'GET_CRAWLER_ACCOUNTS_ERROR'
export const GET_CRAWLER_ACCOUNTS_SUCCESS = 'GET_CRAWLER_ACCOUNTS_SUCCESS'
export const GET_CRAWLER_ACCOUNTS_BEGIN = 'GET_CRAWLER_ACCOUNTS_BEGIN'

export const CALL_API_BEGIN = 'CALL_API_BEGIN'
export const CALL_API_FAIL = 'CALL_API_FAIL'

export const GET_DOMAINS_BEGIN = 'GET_DOMAINS_BEGIN'
export const GET_DOMAINS_SUCCESS = 'GET_DOMAINS_SUCCESS'
export const GET_DOMAINS_ERROR = 'GET_DOMAINS_ERROR'

export const UPDATE_STATUS_CRAWLER_ACCOUNT_BEGIN = 'UPDATE_STATUS_CRAWLER_ACCOUNT_BEGIN'
export const UPDATE_STATUS_CRAWLER_ACCOUNT_SUCCESS = 'UPDATE_STATUS_CRAWLER_ACCOUNT_SUCCESS'
export const UPDATE_STATUS_CRAWLER_ACCOUNT_FAIL = 'UPDATE_STATUS_CRAWLER_ACCOUNT_FAIL'

export const ACTION_CRAWLER_ACCOUNT_BEGIN = 'ACTION_CRAWLER_ACCOUNT_BEGIN'
export const ACTION_CRAWLER_ACCOUNT_SUCCESS = 'ACTION_CRAWLER_ACCOUNT_SUCCESS'
export const ACTION_CRAWLER_ACCOUNT_FAIL = 'ACTION_CRAWLER_ACCOUNT_FAIL'

export const DELETE_CRAWLER_ACCOUNT_BEGIN = 'DELETE_CRAWLER_ACCOUNT_BEGIN'
export const DELETE_CRAWLER_ACCOUNT_SUCCESS = 'DELETE_CRAWLER_ACCOUNT_SUCCESS'
export const DELETE_CRAWLER_ACCOUNT_FAIL = 'DELETE_CRAWLER_ACCOUNT_FAIL'

const crawlerAccountURL = '/admin/crawler-account'

export const fetchDomains = () => {
    return async dispatch => {
        dispatch({
            type: GET_DOMAINS_BEGIN
        })
        await instances.get('/admin/domain/all').then(response => {
            dispatch({
                type: GET_DOMAINS_SUCCESS,
                domains: response.data?.data?.domains
            })
        }).catch(error => {
            dispatch({
                type: GET_DOMAINS_ERROR,
                errorMessage: error
            })
        })
    }
}

export const createCrawlerAccount = (accountData) => {
    return instances.post(`${crawlerAccountURL}/create`, accountData)
}
export const updateCrawlerAccount = (id, accountData) => {
    return instances.put(`${crawlerAccountURL}/${id}`, accountData)
}

export const updateCrawlerAccountStatus = (id, accountData) => {
    return instances.post(`${crawlerAccountURL}/status/${id}`, accountData)
}

export const verifyLoginAccount = (domain, body) => {
    return axios.post(`${domain}/api/auth/page`, body)
}

export const deleteCrawlerAccount = (id) => {
    return instances.delete(`${crawlerAccountURL}/${id}`)
}

export const fetchCrawlerAccount = (params) => {
    const config = {params: params}
    return async dispatch => {
        dispatch({
            type: GET_CRAWLER_ACCOUNTS_BEGIN
        })
        await instances.get(`${crawlerAccountURL}/paging`, config).then(response => {
            dispatch({
                type: GET_CRAWLER_ACCOUNTS_SUCCESS,
                crawler_accounts: response.data?.data?.accounts,
                totalRecords: response.data?.data?.totalRecords,
                params
            })
        }).catch(error => {
            dispatch({
                type: GET_CRAWLER_ACCOUNTS_ERROR,
                errorMessage: error
            })
        })
    }
}

export const updateStatusCrawlerAccount = (id, data) => {
    delete data.password
    delete data.secure_code
    return async (dispatch, getState) => {
        dispatch({
            type: UPDATE_STATUS_CRAWLER_ACCOUNT_BEGIN
        })
        await instances.put(`${crawlerAccountURL}/${id}`, data)
            .then(() => {
                dispatch({
                    type: UPDATE_STATUS_CRAWLER_ACCOUNT_SUCCESS
                })
            })
            .then(response => {
                dispatch(fetchCrawlerAccount(getState().crawler.params))
                return response
            })
            .catch(error => {
                dispatch({
                    type: UPDATE_STATUS_CRAWLER_ACCOUNT_FAIL,
                    errorMessage: error
                })
            })
    }
}


export const actionCrawlerWithAccount = (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: ACTION_CRAWLER_ACCOUNT_BEGIN
        })
        await instances.post(`${crawlerAccountURL}/action/${id}`)
            .then(() => {
                dispatch({
                    type: ACTION_CRAWLER_ACCOUNT_SUCCESS
                })
            })
            .then(response => {
                dispatch(fetchCrawlerAccount(getState().crawler.params))
                return response
            })
            .catch(error => {
                dispatch({
                    type: ACTION_CRAWLER_ACCOUNT_FAIL,
                    errorMessage: error
                })
            })
    }
}
export const deleteAccount = (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DELETE_CRAWLER_ACCOUNT_BEGIN
        })
        await instances.delete(`${crawlerAccountURL}/${id}`)
            .then(() => {
                dispatch({
                    type: DELETE_CRAWLER_ACCOUNT_SUCCESS
                })
            })
            .then(response => {
                dispatch(fetchCrawlerAccount(getState().crawler.params))
                return response
            })
            .catch(error => {
                dispatch({
                    type: DELETE_CRAWLER_ACCOUNT_FAIL,
                    errorMessage: error
                })
            })
    }
}
