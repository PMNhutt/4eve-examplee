import {
  GET_CRAWLER_ACCOUNTS_ERROR,
  GET_CRAWLER_ACCOUNTS_SUCCESS,
  CALL_API_BEGIN,
  GET_DOMAINS_SUCCESS,
  GET_DOMAINS_ERROR,
  CALL_API_FAIL,
  GET_CRAWLER_ACCOUNTS_BEGIN,
  GET_DOMAINS_BEGIN,
  UPDATE_STATUS_CRAWLER_ACCOUNT_FAIL,
  UPDATE_STATUS_CRAWLER_ACCOUNT_BEGIN,
  UPDATE_STATUS_CRAWLER_ACCOUNT_SUCCESS,
  DELETE_CRAWLER_ACCOUNT_BEGIN,
  DELETE_CRAWLER_ACCOUNT_SUCCESS,
  DELETE_CRAWLER_ACCOUNT_FAIL
} from '../action'

// ** Initial State
const initialState = {
  loading: false,
  crawler_accounts: [],
  errorMessage: null,
  domains: [],
  totalRecords: 0
}

const crawler = (state = initialState, action) => {
  switch (action.type) {
    case CALL_API_BEGIN:
      return {
        ...state,
        loading: true
      }
    case CALL_API_FAIL:
      return {
        ...state,
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_CRAWLER_ACCOUNTS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_CRAWLER_ACCOUNTS_SUCCESS:
      return {
        ...state,
        crawler_accounts: action.crawler_accounts,
        totalRecords: action.totalRecords,
        loading: false,
        params: action.params
      }
    case GET_CRAWLER_ACCOUNTS_ERROR:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_DOMAINS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_DOMAINS_SUCCESS:
      return {
        ...state,
        domains: action.domains,
        loading: false
      }
    case GET_DOMAINS_ERROR:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case UPDATE_STATUS_CRAWLER_ACCOUNT_BEGIN:
      return {
        ...state,
        loading: true
      }
    case UPDATE_STATUS_CRAWLER_ACCOUNT_SUCCESS:
      return {
        ...state,
        loading: false
      }
    case UPDATE_STATUS_CRAWLER_ACCOUNT_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case DELETE_CRAWLER_ACCOUNT_BEGIN:
      return {
        ...state,
        loading: true
      }
    case DELETE_CRAWLER_ACCOUNT_SUCCESS:
      return {
        ...state,
        loading: false
      }
    case DELETE_CRAWLER_ACCOUNT_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    default:
      return { ...state }
  }
}
export default crawler
