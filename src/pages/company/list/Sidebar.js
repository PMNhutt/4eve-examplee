// ** React Import
import React from 'react'

// ** Custom Components
import Sidebar from '@components/sidebar'
import "flatpickr/dist/themes/material_blue.css"
// ** Utils
import {isObjEmpty} from '@utils'

// ** Third Party Components
import classnames from 'classnames'
import {useForm} from 'react-hook-form'
import {Button, FormGroup, Form, Input, Label} from 'reactstrap'

// ** Store & Actions
import {createCompany, getCompanies} from '../store/action'
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../components/Toast/ToastFail"
import { store } from '@store/storeConfig/store'

const SidebarCrawlerAccount = ({open, toggleSidebar}) => {
    // ** Vars
    const {register, errors, handleSubmit} = useForm()

    // ** Function to handle form submit
    const onSubmit = async (values) => {

        if (isObjEmpty(errors)) {
            const data = {}
            data.name = values.name
            createCompany(data).then(res => {
                if (res?.data?.status) {
                    store.dispatch(getCompanies(store.getState().company.params))
                    toggleSidebar()
                    toast.success(<SuccessNotificationToast/>)
                }
            }).catch(error => {
                toast.error(<ErrorNotificationToast message={error.response?.data?.message}/>)
            })
        }
    }

    return (
        <Sidebar
            size='lg'
            open={open}
            title='Tạo mới công ty'
            headerClassName='mb-1'
            contentClassName='pt-0 user__side-bar'
            toggleSidebar={toggleSidebar}
        >
            <Form onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
                <FormGroup>
                    <Label className={'label'} for='name'>Tên company <span className='text-danger'>*</span></Label>
                    <div className={'d-flex align-items-center'}>
                        <Input className={'w-0 p-0 border-0'} />
                        <Input
                            name='name'
                            id='name'
                            placeholder='Nhập tên'
                            innerRef={register({required: true})}
                            className={classnames({'is-invalid': errors['name']})}
                        />
                    </div>
                </FormGroup>
                <div className={'row mt-2'}>
                    <Button type='submit' className={'ml-1 mr-1'} color='primary'>Lưu</Button>
                    <Button type='reset' color='secondary' outline onClick={toggleSidebar}>Hủy</Button>
                </div>
            </Form>
        </Sidebar>
    )
}

export default SidebarCrawlerAccount
