// ** User List Component
import Table from './Table'

// ** Styles
import '@styles/react/apps/app-users.scss'
import React from "react"

const Company = () => {
  return (
    <div id='company' className='app-user-list'>
      <Table />
    </div>
  )
}

export default Company
