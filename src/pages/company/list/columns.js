// ** Store & Actions
import {deleteTelegramGroup} from '../store/action'
import { store } from '@store/storeConfig/store'

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge} from 'reactstrap'
import {MoreVertical, Trash2} from 'react-feather'
import React from 'react'
import {convertDate} from "../../../utility/ConvertDate"

export const columns = [
  {
    name: 'No.',
    width: '50px',
    sortable: true,
    cell: row => row?.id
  },
  {
    name: 'Company Name',
    sortable: true,
    cell: row => row?.name
  },
  {
    name: 'Domain',
    sortable: true,
    cell: row => row?.domain
  },
  {
    name: 'Login By',
    sortable: true,
    cell: row => row?.login_domain
  },
  {
    name: 'Status',
    sortable: true,
    width: '200px',
    cell: row =>  <Badge className='text-capitalize' color={row?.status ? 'light-success' : 'light-warning'} pill>
      {row.status ? 'Active' : 'Inactive'}
    </Badge>
  },
  {
    name: 'Created At',
    minWidth: '150px',
    center: 'true',
    sortable: true,
    cell: row => convertDate(row?.created_at)
  }
]
