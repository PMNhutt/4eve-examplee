import instances from "../../../../@core/plugin/axios"

export const GET_COMPANY_BEGIN = 'GET_COMPANY_BEGIN'
export const GET_COMPANY_SUCCESS = 'GET_COMPANY_SUCCESS'
export const GET_COMPANY_FAIL = 'GET_COMPANY_FAIL'

export const GET_COMPANY_BY_ID_BEGIN = 'GET_COMPANY_BY_ID_BEGIN'
export const GET_COMPANY_BY_ID_SUCCESS = 'GET_COMPANY_BY_ID_SUCCESS'
export const GET_COMPANY_BY_ID_FAIL = 'GET_COMPANY_BY_ID_FAIL'

export const DELETE_COMPANY_BY_ID_BEGIN = 'DELETE_COMPANY_BY_ID_BEGIN'
export const DELETE_COMPANY_BY_ID_SUCCESS = 'DELETE_COMPANY_BY_ID_SUCCESS'
export const DELETE_COMPANY_BY_ID_FAIL = 'DELETE_COMPANY_BY_ID_FAIL'

export const getCompanies = (params) => {
    const config = {params: params}
    return async dispatch => {
        dispatch({
            type: GET_COMPANY_BEGIN
        })
        await instances.get('/admin/domain/paging', config).then(response => {
            dispatch({
                type: GET_COMPANY_SUCCESS,
                companies: response.data?.data?.domains,
                totalRecords: response.data?.data?.totalRecords
            })
        }).catch(error => {
            dispatch({
                type: GET_COMPANY_FAIL,
                errorMessage: error
            })
        })
    }
}

export const createCompany = async (data) => {
    return await instances.post('/admin/company/create', data)
}

export const deleteTelegramGroup = async (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DELETE_COMPANY_BY_ID_BEGIN
        })
        await instances.delete(`/admin/telegram-group/${id}`)
            .then(() => {
                dispatch({
                    type: DELETE_COMPANY_BY_ID_SUCCESS
                })
            })
            .then(() => {
                dispatch(getCompanies(getState().company.params))
            })
            .catch(error => {
                dispatch({
                    type: DELETE_COMPANY_BY_ID_FAIL,
                    errorMessage: error
                })
            })
    }
}
