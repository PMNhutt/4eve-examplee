import {
  DELETE_COMPANY_BY_ID_BEGIN, DELETE_COMPANY_BY_ID_FAIL,
  DELETE_COMPANY_BY_ID_SUCCESS,
  GET_COMPANY_BY_ID_BEGIN,
  GET_COMPANY_BY_ID_FAIL, GET_COMPANY_BY_ID_SUCCESS,
  GET_COMPANY_BEGIN,
  GET_COMPANY_FAIL,
  GET_COMPANY_SUCCESS
} from "../action"

// ** Initial State
const initialState = {
  loading: false,
  companies: [],
  totalRecords: 0,
  company: null,
  errorMessage: null
}

const company = (state = initialState, action) => {
  switch (action.type) {
    case GET_COMPANY_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_COMPANY_SUCCESS:
      return {
        ...state,
        companies: action.companies,
        totalRecords: action.totalRecords,
        loading: false
      }
    case GET_COMPANY_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_COMPANY_BY_ID_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_COMPANY_BY_ID_SUCCESS:
      return {
        ...state,
        company: action.company,
        loading: false
      }
    case GET_COMPANY_BY_ID_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case DELETE_COMPANY_BY_ID_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case DELETE_COMPANY_BY_ID_SUCCESS:
      return {
        ...state,
        loading: false
      }
    case DELETE_COMPANY_BY_ID_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    default:
      return { ...state }
  }
}
export default company
