import {
  GET_DATA_BEGIN,
  GET_DATA_FAILURE,
  GET_DATA_SUCCESS,
  GET_DOMAINS_BEGIN,
  GET_DOMAINS_FAIL,
  GET_DOMAINS_SUCCESS,
  GET_CRAWLER_ACCOUNTS_BEGIN,
  GET_CRAWLER_ACCOUNTS_FAIL,
  GET_CRAWLER_ACCOUNTS_SUCCESS,
  UPDATE_AGENT_STATUS_BEGIN,
  UPDATE_AGENT_STATUS_FAIL,
  UPDATE_AGENT_STATUS_SUCCESS,
  GET_CONFIG_NOTIFIES_BEGIN,
  GET_CONFIG_NOTIFIES_FAIL,
  GET_CONFIG_NOTIFIES_SUCCESS,
  GET_TELEGRAM_GROUPS_BEGIN,
  GET_TELEGRAM_GROUPS_FAIL,
  GET_TELEGRAM_GROUPS_SUCCESS
} from "../action"

// ** Initial State
const initialState = {
  loading: false,
  params: {},
  agents: [],
  domains: [],
  crawlerAccounts: [],
  configNotifies: [],
  telegramGroups: []
}

const agentNoDetails = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_DATA_SUCCESS:
      return {
        ...state,
        agents: action.data,
        totalRecords: action?.totalRecords,
        params: action.params,
        loading: false
      }
    case GET_DATA_FAILURE:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_DOMAINS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_DOMAINS_SUCCESS:
      return {
        ...state,
        domains: action.domains,
        loading: false
      }
    case GET_DOMAINS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_CRAWLER_ACCOUNTS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_CRAWLER_ACCOUNTS_SUCCESS:
      return {
        ...state,
        crawlerAccounts: action.data,
        loading: false
      }
    case GET_CRAWLER_ACCOUNTS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case UPDATE_AGENT_STATUS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case UPDATE_AGENT_STATUS_SUCCESS:
      return {
        ...state,
        agent: null,
        loading: false
      }
    case UPDATE_AGENT_STATUS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_CONFIG_NOTIFIES_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_CONFIG_NOTIFIES_SUCCESS:
      return {
        ...state,
        configNotifies: action.data,
        loading: false
      }
    case GET_CONFIG_NOTIFIES_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_TELEGRAM_GROUPS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_TELEGRAM_GROUPS_SUCCESS:
      return {
        ...state,
        telegramGroups: action.data,
        loading: false
      }
    case GET_TELEGRAM_GROUPS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    default:
      return { ...state }
  }
}
export default agentNoDetails
