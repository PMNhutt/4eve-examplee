import instances from "../../../../@core/plugin/axios"

export const GET_DATA_BEGIN = 'GET_DATA_BEGIN'
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS'
export const GET_DATA_FAILURE = 'GET_DATA_FAILURE'

export const GET_DOMAINS_BEGIN = 'GET_DOMAINS_BEGIN'
export const GET_DOMAINS_SUCCESS = 'GET_DOMAINS_SUCCESS'
export const GET_DOMAINS_FAIL = 'GET_DOMAINS_FAIL'

export const GET_CRAWLER_ACCOUNTS_BEGIN = 'GET_CRAWLER_ACCOUNTS_BEGIN'
export const GET_CRAWLER_ACCOUNTS_FAIL = 'GET_CRAWLER_ACCOUNTS_FAIL'
export const GET_CRAWLER_ACCOUNTS_SUCCESS = 'GET_CRAWLER_ACCOUNTS_SUCCESS'

export const UPDATE_AGENT_STATUS_BEGIN = 'UPDATE_AGENT_STATUS_BEGIN'
export const UPDATE_AGENT_STATUS_SUCCESS = 'UPDATE_AGENT_STATUS_SUCCESS'
export const UPDATE_AGENT_STATUS_FAIL = 'UPDATE_AGENT_STATUS_FAIL'

export const UPDATE_AGENT_STATUS_BY_AGENTS_BEGIN = 'UPDATE_AGENT_STATUS_BY_AGENTS_BEGIN'
export const UPDATE_AGENT_STATUS_BY_AGENTS_SUCCESS = 'UPDATE_AGENT_STATUS_BY_AGENTS_SUCCESS'
export const UPDATE_AGENT_STATUS_BY_AGENTS_FAIL = 'UPDATE_AGENT_STATUS_BY_AGENTS_FAIL'

export const GET_CONFIG_NOTIFIES_BEGIN = 'GET_CONFIG_NOTIFIES_BEGIN'
export const GET_CONFIG_NOTIFIES_FAIL = 'GET_CONFIG_NOTIFIES_FAIL'
export const GET_CONFIG_NOTIFIES_SUCCESS = 'GET_CONFIG_NOTIFIES_SUCCESS'

export const GET_TELEGRAM_GROUPS_BEGIN = 'GET_TELEGRAM_GROUPS_BEGIN'
export const GET_TELEGRAM_GROUPS_FAIL = 'GET_TELEGRAM_GROUPS_FAIL'
export const GET_TELEGRAM_GROUPS_SUCCESS = 'GET_TELEGRAM_GROUPS_SUCCESS'

export const getAgentNoDetails = (params) => {
    const config = {params: {...params, status: true}}

    return async dispatch => {
        dispatch({
            type: GET_DATA_BEGIN
        })
        await instances.get(`/admin/agent-no-detail/paging`, config)
            .then(response => {
                dispatch({
                    type: GET_DATA_SUCCESS,
                    data: response?.data?.data?.data,
                    totalRecords: response?.data?.data?.totalRecords,
                    params
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_DATA_FAILURE,
                    errorMessage: error
                })
            })
    }
}
export const updateStatusAgent = (id, data, isUpdateStatus) => {
    return (dispatch, getState) => {
        dispatch({
            type: UPDATE_AGENT_STATUS_BEGIN
        })
        instances.put(`/admin/agent/update-status/${id}`, data)
            .then(() => {
                dispatch({
                    type: UPDATE_AGENT_STATUS_SUCCESS
                })
            })
            .then(() => {
                dispatch(getAgentNoDetails(getState().agentNoDetails.params))
            })
            .catch(error => {
                dispatch({
                    type: UPDATE_AGENT_STATUS_FAIL,
                    errorMessage: error
                })
            })
    }
}
export const updateStatusAgents = (agents) => {
    const body = {agents}
    return (dispatch, getState) => {
        dispatch({
            type: UPDATE_AGENT_STATUS_BY_AGENTS_BEGIN
        })
        instances.put(`/admin/agent/update-status-agents`, body)
            .then(() => {
                dispatch({
                    type: UPDATE_AGENT_STATUS_BY_AGENTS_SUCCESS
                })
            })
            .then(() => {
                dispatch(getAgentNoDetails(getState().agentNoDetails.params))
            })
            .catch(error => {
                dispatch({
                    type: UPDATE_AGENT_STATUS_BY_AGENTS_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const getDomains = () => {
    return async dispatch => {
        dispatch({
            type: GET_DOMAINS_BEGIN
        })
        await instances.get('/admin/domain/all').then(response => {
            dispatch({
                type: GET_DOMAINS_SUCCESS,
                domains: [{id: '', name: 'Select all companies'}, ...response.data?.data?.domains]
            })
        }).catch(error => {
            dispatch({
                type: GET_DOMAINS_FAIL,
                errorMessage: error
            })
        })
    }
}

export const getCrawlerAccounts = (params) => {
    const config = {params: params}

    return async dispatch => {
        dispatch({
            type: GET_CRAWLER_ACCOUNTS_BEGIN
        })
        await instances.get(`/admin/crawler-account/all`, config)
            .then(response => {
                dispatch({
                    type: GET_CRAWLER_ACCOUNTS_SUCCESS,
                    data: [{id: 0, username: 'Select all crawler accounts'}, ...response?.data?.data?.accounts]
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_CRAWLER_ACCOUNTS_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const getConfigNotifies = () => {
    return async dispatch => {
        dispatch({
            type: GET_CONFIG_NOTIFIES_BEGIN
        })
        await instances.get(`/admin/config-notify/all`)
            .then(response => {
                dispatch({
                    type: GET_CONFIG_NOTIFIES_SUCCESS,
                    data: response?.data?.data?.configNotifies
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_CONFIG_NOTIFIES_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const getTelegramGroups = () => {
    return async dispatch => {
        dispatch({
            type: GET_TELEGRAM_GROUPS_BEGIN
        })
        await instances.get(`/admin/telegram-group/all`)
            .then(response => {
                dispatch({
                    type: GET_TELEGRAM_GROUPS_SUCCESS,
                    data: response?.data?.data
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_TELEGRAM_GROUPS_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const updateAgentDetailsByAgentIds = async (agentIds, agentDetails) => {
    const body = {agentIds, agentDetails}
    return await instances.put(`/admin/agent/update-config-agents`, body)
}
