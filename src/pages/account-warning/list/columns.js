// ** Store & Actions
import {updateStatusAgent} from '../store/action'
import { store } from '@store/storeConfig/store'

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge} from 'reactstrap'
import {Edit3, FileText, MoreVertical, PlayCircle, StopCircle, Trash2} from 'react-feather'
import React from 'react'
import {convertDate} from "../../../utility/ConvertDate"
import {Link} from "react-router-dom"

export const columns = ((domains, checkPermission) => [
  {
    name: 'AccountId',
    width: '200px',
    cell: row => {
      if (checkPermission) {
        return <Link to={`/agent/edit/${row.id}`} target="_blank" className={'text-link'}>{row?.account_id}</Link>
      } else return row?.account_id
    }
  },
  {
    name: 'AccountName',
    minWidth: '150px',
    sortable: true,
    cell: row => row?.display_name
  },
  {
    name: 'Company',
    minWidth: '200px',
    center: true,
    cell: row => {
      const domain = domains?.find(domain => domain?.id === row?.crawler_account?.domain_id)
      if (domain?.id) {
        return domain?.name
      } else {
        return ''
      }
    }
  },
  {
    name: 'WarningGroup',
    minWidth: '100px',
    cell: row => (
        <Badge className='text-capitalize' color={row.agent_details?.length === 0 ? 'light-danger' : 'light-success'} pill>
          {row.agent_details?.length === 0 ? 'Not added yet' : `There are ${row.agent_details?.length} groups`}
        </Badge>
    )
  },
  {
    name: 'CreatedDate',
    width: '120px',
    selector: 'created_at',
    sortable: true,
    cell: row => convertDate(row?.created_at)
  },
  {
    name: 'Actions',
    width: '120px',
    cell: row => (
        <UncontrolledDropdown>
          <DropdownToggle tag='div' className='btn btn-sm'>
            <MoreVertical size={14} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu right>
            {
              <DropdownItem
                  tag={Link}
                  to={`/agent/detail/${row?.id}`}
                  target="_blank"
                  className='w-100'>
                <FileText size={14} className='mr-50' />
                <span className='align-middle'>See detail</span>
              </DropdownItem>
            }
            {
              <>
                <DropdownItem
                    tag={Link}
                    to={`/agent/edit/${row?.id}`}
                    target="_blank"
                    className='w-100'>
                  <FileText size={14} className='mr-50' />
                  <span className='align-middle'>Edit</span>
                </DropdownItem>
              </>
            }
            <DropdownItem
                onClick={() => {
                  store.dispatch(updateStatusAgent(row?.account_id, {is_warning: !row?.is_warning}, true))
                }}
                className='w-100'>
              {
                row?.status ? <StopCircle size={14} className='mr-50' /> : <PlayCircle size={14} className='mr-50' />
              }
              <span className='align-middle'>{row?.is_warning ? 'Stop warning' : 'Warning'}</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
])
