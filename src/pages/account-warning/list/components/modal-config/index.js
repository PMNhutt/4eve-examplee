import {
    Button,
    Col, DropdownItem, DropdownMenu,
    DropdownToggle,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalHeader,
    Row,
    UncontrolledDropdown
} from "reactstrap"
import React, {useEffect, useState} from "react"
import {Archive, MoreVertical, Trash2, X} from "react-feather"
import Select from "react-select"
import {TIMERS} from "../../../../../enum/time-constant"
import {Loading} from "../../../../../components/Loading"
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../../../components/Toast/ToastSuccess"
import {updateAgentDetailsByAgentIds} from "../../../store/action"
import ErrorNotificationToast from "../../../../../components/Toast/ToastFail"

// styles
import './style/style.scss'

const ModalConfig = ({isOpen, toggle, selectedAgents, configNotifies, telegramGroups, handleReloadAgents}) => {
    const [agentDetails, setAgentDetails] = useState([])
    const [agents, setAgents] = useState([])
    const [isShowTime, setIsShowTime] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        setAgents(selectedAgents)
    }, [selectedAgents && agents?.length === 0])

    useEffect(() => {
        if (agentDetails?.length > 0) {
            getIsShowTimer()
        }
    }, [agentDetails])

    const handleDeleteAgent = (item) => {
        const newSelectedAccounts = agents?.filter(agent => agent?.id !== item?.id)
        if (newSelectedAccounts?.length === 0) {
            handleCancel()
        } else {
            setAgents([...newSelectedAccounts])
        }
    }

    const handleAddAgentDetail = () => {
        agentDetails.push({
            time_notify: []
        })
        isShowTime?.push(false)
        setIsShowTime([...isShowTime])
        setAgentDetails([...agentDetails])
    }

    const handleChangeTelegramGroup = (telegramGroup, index) => {
        if (agentDetails[index]) {
            agentDetails[index].telegram_group_id = telegramGroup?.id
            setAgentDetails([...agentDetails])
        }
    }

    const getIsShowTimer = () => {
        const array = []
        agentDetails?.forEach((item) => {
            if (item?.time_notify?.length > 0) {
                array?.push(true)
            } else {
                array?.push(false)
            }

        })
        setIsShowTime([...array])
    }

    const handleChangeConfigNotify = (configNotify, index) => {
        if (agentDetails[index]) {
            agentDetails[index].config_notify_id = configNotify?.id
            setAgentDetails([...agentDetails])
        }
    }

    const handleDeleteAgentDetail = (index) => {
        agentDetails.splice(index, 1)
        isShowTime?.splice(index, 1)
        setIsShowTime([...isShowTime])
        setAgentDetails([...agentDetails])
    }

    const handleShowTime = (index) => {
        isShowTime[index] = true
        if (!agentDetails[index].time_notify) {
            agentDetails[index] = {...agentDetails[index], time_notify: []}
            setAgentDetails([...agentDetails])
        }
        setIsShowTime([...isShowTime])
    }

    const handleHideTime = (index) => {
        isShowTime[index] = false
        setIsShowTime([...isShowTime])
        agentDetails[index].time_notify = []
        setAgentDetails([...agentDetails])
    }

    const handleCheckActive = (index, value) => {
        const findData = agentDetails[index]?.time_notify?.findIndex(item => item === value)
        return findData >= 0
    }

    const handleCancel = () => {
        setAgentDetails([])
        setIsShowTime([])
        setAgents([])
        toggle()
    }

    const handleSubmitButton = async () => {
        if (isLoading) return
        setIsLoading(true)
        if (agents?.length > 0) {
            const agentIds = []
            for (const agent of agents) {
                agentIds.push(agent?.account_id)
            }

            const agentDetailsData = []
            let canSubmit = true

            if (agentDetails?.length > 0) {
                for (const agentDetail of agentDetails) {
                    if (agentDetail?.telegram_group_id && agentDetail?.config_notify_id) {
                        agentDetailsData.push(agentDetail)
                    } else {
                        toast.error(<ErrorNotificationToast message={'Please select telegram group and config notify!'}/>)
                        canSubmit = false
                    }
                }
            }
            if (canSubmit) {
                updateAgentDetailsByAgentIds(agentIds, agentDetailsData)
                    .then(res => {
                        setIsLoading(false)
                        if (res?.status === 200) {
                            handleCancel()
                            handleReloadAgents()
                            toast.success(<SuccessNotificationToast message={'Update successful!'}/>)
                        }
                    })
                    .catch(err => {
                        setIsLoading(false)
                        toast.error(<ErrorNotificationToast message={err.response?.data?.message}/>)
                    })
            } else {
                setIsLoading(false)
            }
        }
    }
    const handleAddTime = (index, value) => {
        const findData = agentDetails[index]?.time_notify?.findIndex(item => item === value)
        if (findData >= 0) {
            agentDetails[index].time_notify.splice(findData, 1)
            setAgentDetails([...agentDetails])
        } else {
            agentDetails[index]?.time_notify?.push(value)
            setAgentDetails([...agentDetails])
        }
    }

    return (
        <Modal isOpen={isOpen} toggle={handleCancel} centered={true} className={'modal-config'}>
            {
                isLoading && <Loading/>
            }
            <ModalHeader toggle={handleCancel}>
                <span className='align-middle'>Config Account</span>
            </ModalHeader>
            <ModalBody>
                <h5>List selected agents</h5>
                <div className={'config-agent-border'}>
                    {
                        agents?.length > 0 &&
                        agents?.map((agent, index) => {
                            return (
                                <div key={index} className={'d-flex justify-content-between align-items-center selected-agent-item'}>
                                    <span>{agent?.account_id}</span>
                                    <Button color={'primary'} className={'btn-delete-agent'} onClick={() => handleDeleteAgent(agent)}><Trash2 size={15}/></Button>
                                </div>
                            )
                        })
                    }
                </div>
                <div className={'d-flex justify-content-between mt-1'}>
                    <h5>List warning groups</h5>
                    <p><b>Number of telegram groups: </b>{agentDetails?.length}</p>
                </div>

                {
                    agentDetails?.length > 0 &&
                    <div className={'config-agent-border mx-0 p-50'}>
                        {
                            agentDetails?.map((agent_detail, index) => {
                                const config_notify = configNotifies?.find(item => item?.id === agent_detail?.config_notify_id)
                                return (
                                    <div key={index}>
                                        <Row className={index === 0 ? 'mx-0 align-items-end' : 'mx-0 pt-50 border-top align-items-end'}>
                                            <Col lg={3} sm={12}>
                                                <FormGroup>
                                                    <Label className={'sub-label'}>Telegram group</Label>
                                                    <Select className='react-select'
                                                            classNamePrefix='select'
                                                            options={telegramGroups}
                                                            placeholder={'Select telegram group'}
                                                            getOptionValue={option => option?.id}
                                                            getOptionLabel={option => option?.telegram_group_name}
                                                            value={telegramGroups?.find(item => item?.id === agent_detail?.telegram_group_id)}
                                                            onChange={(item) => handleChangeTelegramGroup(item, index)}
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col lg={3} sm={12}>
                                                <FormGroup>
                                                    <Label className={'sub-label'}>Config</Label>
                                                    <Select className='react-select'
                                                            classNamePrefix='select'
                                                            options={configNotifies}
                                                            placeholder={'Select config'}
                                                            getOptionValue={option => option?.id}
                                                            getOptionLabel={option => option?.name}
                                                            value={configNotifies?.find(item => item?.id === agent_detail?.config_notify_id)}
                                                            onChange={(item) => handleChangeConfigNotify(item, index)}
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col lg={2} sm={12}>
                                                <FormGroup>
                                                    <Label className={'sub-label'}>Outstanding</Label>
                                                    <p className={'form-control form-disabled'}>{config_notify?.outstanding || 0}</p>
                                                </FormGroup>
                                            </Col>
                                            <Col lg={2} sm={12}>
                                                <FormGroup>
                                                    <Label className={'sub-label'}>Win/Loss</Label>
                                                    <p className={'form-control form-disabled'}>{config_notify?.win_loss || 0}</p>
                                                </FormGroup>
                                            </Col>
                                            <Col lg={2} sm={12} className={'pb-2 d-flex align-items-center'}>
                                                {
                                                    !isShowTime[index] &&
                                                    <UncontrolledDropdown>
                                                        <DropdownToggle tag='div' className='btn btn-sm p-0 pr-1'>
                                                            <Button color={'primary'} className={'p-25'}>
                                                                <MoreVertical size={18} />
                                                            </Button>
                                                        </DropdownToggle>
                                                        <DropdownMenu right>
                                                            <DropdownItem
                                                                className='w-100'
                                                                onClick={() => handleShowTime(index)}
                                                            >
                                                                <Archive size={14} className='mr-50' />
                                                                <span className='align-middle'>{'Select a warning time frame'}</span>
                                                            </DropdownItem>
                                                        </DropdownMenu>
                                                    </UncontrolledDropdown>
                                                }
                                                <Button color={'primary'} className={'p-25'} onClick={() => handleDeleteAgentDetail(index)}><Trash2 size={18}/></Button>
                                            </Col>
                                        </Row>
                                        {
                                            isShowTime[index] &&
                                            <div className={'config-container-time'}>
                                                <div className={'timer'}>
                                                    <Label className={'timer-label'}>Select time frame</Label>
                                                    <X size={12} className={'ml-1 cursor-pointer'} onClick={() => handleHideTime(index)}/>
                                                </div>
                                                <Row>
                                                    {
                                                        TIMERS?.map((timer, time_index) => {
                                                            return (
                                                                <Col key={`${index}Item${time_index}`} lg={1} sm={2}  className={''}>
                                                                    <div className={`config-container-time__item ${handleCheckActive(index, timer?.value) ? 'active' : ''}`} onClick={() => handleAddTime(index, timer?.value)}>
                                                                        {timer?.label}
                                                                    </div>

                                                                </Col>
                                                            )
                                                        })
                                                    }
                                                </Row>
                                            </div>
                                        }
                                    </div>
                                )
                            })
                        }
                    </div>
                }
                <Button color={'primary'} className={'mt-2 w-fit-content'} onClick={handleAddAgentDetail}>Add warning group</Button>
                <div className={'d-flex align-items-center justify-content-end mb-1'}>
                    <Button color={'primary'} className={'mt-2 w-fit-content mr-1'} onClick={handleSubmitButton} disabled={isLoading}>Save</Button>
                    <Button color={'primary'} outline={true} className={'mt-2 w-fit-content'} onClick={handleCancel}>Cancel</Button>
                </div>
            </ModalBody>
        </Modal>
    )
}

export default ModalConfig
