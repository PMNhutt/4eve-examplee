// ** React Imports
import React, {Fragment, useState, useEffect, forwardRef} from 'react'

// ** Columns
import {columns} from './columns'

// ** Store & Actions
import {
    getAgentNoDetails,
    getCrawlerAccounts,
    getDomains,
    getConfigNotifies,
    getTelegramGroups,
    updateAgentDetailsByAgentIds, updateStatusAgents
} from '../store/action'
import {useDispatch, useSelector} from 'react-redux'

// ** Third Party Components
import {ChevronDown, X} from 'react-feather'
import DataTable from 'react-data-table-component'
import {Button, Card, Col, Input, Row} from 'reactstrap'
import {NoDataComponent} from "../../../components/NoDataComponent"
import {useHistory, useLocation} from 'react-router-dom'
import {Loading} from "../../../components/Loading"
import ReactPaginate from "react-paginate"
import {checkPermissionByScreen} from "../../users/profile/store/action"
import {PERMISSION_SCREEN_ENUM} from "../../../enum/permission-screens-constant"
import {handleMenuHidden} from "../../../redux/actions/layout"
import Select from "react-select"
import ModalConfig from "./components/modal-config"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/user.scss'

// ** Bootstrap Checkbox Component
const BootstrapCheckbox = forwardRef(({ onClick, ...rest }, ref) => {
    return (
        <div className='custom-control custom-checkbox promotion-checkbox' style={{zIndex:0}}>
            <input type='checkbox' className='custom-control-input' ref={ref} {...rest} />
            <label className='custom-control-label' onClick={onClick} />
        </div>
    )
})

const AccountWarningHeader = ({searchTemp, setSearchTemp, domains, crawlerAccounts, currentDomain, currentCrawlerAccount, handleSelectDomain, handleSelectCrawlerAccount, handleConfigSubmit, disabledConfigButton, handleStopWarningSubmit}) => {
    return (
        <Row className={'mb-1'}>
            <Col sm={12} lg={3}>
                <Select
                    isSearchable={false}
                    name='rowsPerPage'
                    options={domains}
                    noOptionsMessage={() => 'No companies yet'}
                    className='select-option section-select w-100'
                    classNamePrefix='select'
                    placeholder={'Select company'}
                    onChange={handleSelectDomain}
                    getOptionValue={option => option?.id}
                    getOptionLabel={option => option?.name}
                    value={domains?.find(domain => domain?.id === +currentDomain)}
                />
            </Col>
            <Col sm={12} lg={3}>
                <Select
                    isSearchable={false}
                    name='rowsPerPage'
                    options={crawlerAccounts}
                    noOptionsMessage={() => 'No crawler account yet'}
                    className='select-option section-select w-100'
                    classNamePrefix='select'
                    placeholder={'Select crawler account'}
                    getOptionValue={option => option?.id}
                    getOptionLabel={option => option?.username}
                    onChange={handleSelectCrawlerAccount}
                    value={crawlerAccounts?.find(crawlerAccount => crawlerAccount?.id === +currentCrawlerAccount)}
                />
            </Col>
            <Col sm={12} lg={3}>
                <Input
                    name='idAgents'
                    id='name'
                    placeholder='Enter account id'
                    className={'form-control'}
                    value={searchTemp}
                    onChange={setSearchTemp}
                />
            </Col>
            <Col sm={6} lg={3} className={'d-flex align-items-center justify-content-end'}>
                <Button color={'primary'} disabled={disabledConfigButton} onClick={handleConfigSubmit}>Config</Button>
                <Button color={'primary'} className={'ml-1'} disabled={disabledConfigButton} onClick={handleStopWarningSubmit}>Stop warning</Button>
            </Col>
        </Row>
    )
}

const AccountWarning = () => {
    // ** Store Vars
    const dispatch = useDispatch()
    const store = useSelector(state => state.agentNoDetails)
    const history = useHistory()
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)
    const rowsPerPage = 10

    const [checkPermission, setCheckPermission] = useState(null)
    const [selectedAgents, setSelectedAgents] = useState([])
    const [isOpenModal, setIsOpenModal] = useState(false)

    // ** States
    const [currentPage, setCurrentPage] = useState(searchParams?.get('page') ? searchParams?.get('page') : 1)
    const [searchTemp, setSearchTemp] = useState(searchParams?.get('search') ? searchParams?.get('search') : '')
    const [currentDomain, setCurrentDomain] = useState(searchParams?.get('domain_id') ? searchParams?.get('domain_id') : 0)
    const [currentCrawlerAccount, setCurrentCrawlerAccount] = useState(searchParams?.get('crawler_account_id') ? searchParams?.get('crawler_account_id') : 0)

    // ** Get data on mount
    useEffect(() => {
        const params = {
            search: searchTemp,
            page: currentPage,
            perPage: rowsPerPage,
            domain_id: currentDomain,
            crawler_account_id: currentCrawlerAccount
        }
        dispatch(getAgentNoDetails(params))
        dispatch(getDomains())
        dispatch(getCrawlerAccounts())
        dispatch(getConfigNotifies())
        dispatch(getTelegramGroups())
        dispatch(handleMenuHidden(false))
        checkPermissionByScreen(PERMISSION_SCREEN_ENUM.agent).then(res => {
            setCheckPermission(res)
        })
        localStorage.removeItem('agent')
        localStorage.setItem('agent', '/account-warning/list')
    }, [])

    useEffect(() => {
        for (const agent of store.agents) {
            agent.checked = (selectedAgents.findIndex(item => item?.id === agent?.id && +item?.currentPage === +currentPage) > -1)
        }
    }, [selectedAgents])

    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.page) {
            params.page = filterParams.page
        }
        if (filterParams.domain_id) {
            params.domain_id = filterParams.domain_id
        }
        if (filterParams.crawler_account_id) {
            params.crawler_account_id = filterParams.crawler_account_id
        }
        if (filterParams.search) {
            params.search = filterParams.search
        }
        params.perPage = filterParams.perPage

        const urlSearchParams = new URLSearchParams(params)
        history.replace({pathname: location.pathname, search: urlSearchParams.toString()})
    }

    // ** Table data to render
    const dataToRender = () => {
        const filters = {}
        const isFiltered = Object.keys(filters).some(function (k) {
            return filters[k].length > 0
        })
        if (store.agents?.length > 0) {
            for (const agent of store.agents) {
                agent.checked = (selectedAgents.findIndex(item => item?.id === agent?.id && +item?.currentPage === +currentPage) > -1)
            }
            return store.agents
        } else if (store.agents?.length === 0 && isFiltered) {
            return []
        }
    }
    // ** Custom Pagination
    const CustomPagination = () => {
        const count = Number(Math.ceil(store.totalRecords / rowsPerPage))
        if (count > 1) {
            return (
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    pageCount={count || 1}
                    activeClassName='active'
                    forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                    onPageChange={handlePagination}
                    pageClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next'}
                    previousClassName={'page-item prev'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
                />
            )
        } else return <></>
    }

    const handlePagination = page => {
        const params = {page: page.selected + 1, perPage: rowsPerPage, domain_id: currentDomain, crawler_account_id: currentCrawlerAccount, search: searchTemp}
        dispatch(getAgentNoDetails(params))
        checkParams(params)
        setCurrentPage(page.selected + 1)
    }

    const handleSelectDomain = domain => {
        const crawlerAccountIndex = store?.crawlerAccounts?.findIndex(item => item?.id === currentCrawlerAccount && item?.domain_id === domain?.id)
        let params = {page: 1, perPage: rowsPerPage, domain_id: domain?.id, crawler_account_id: currentCrawlerAccount, search: searchTemp}
        if (crawlerAccountIndex === -1) {
            params = {page: 1, perPage: rowsPerPage, domain_id: domain?.id, search: searchTemp}
            setCurrentCrawlerAccount(0)
        }
        dispatch(getAgentNoDetails(params))
        dispatch(getCrawlerAccounts({domain_id: domain?.id}))
        checkParams(params)
        setCurrentPage(1)
        setCurrentDomain(domain?.id)
    }

    const handleSelectCrawlerAccount = crawlerAccount => {
        const params = {page: 1, perPage: rowsPerPage, crawler_account_id: crawlerAccount?.id, domain_id: currentDomain, search: searchTemp}
        dispatch(getAgentNoDetails(params))
        checkParams(params)
        setCurrentPage(1)
        setCurrentCrawlerAccount(crawlerAccount?.id)
    }

    const handleChangeSearchTemp = e => {
        const params = {page: 1, perPage: rowsPerPage, crawler_account_id: currentCrawlerAccount, domain_id: currentDomain, search: e?.target?.value}
        dispatch(getAgentNoDetails(params))
        checkParams(params)
        setCurrentPage(1)
        setSearchTemp(e?.target?.value)
    }

    const handleSelectedAgentsChange = state => {
        const selectedRows = state?.selectedRows
        if (store.agents?.length > 0) {
            for (const agent of store.agents) {
                const index = selectedRows?.findIndex(item => item?.id === agent?.id)
                agent.checked = (index > -1)
                const selectedAgentIndex = selectedAgents?.findIndex(item => item?.id === agent?.id && +item?.currentPage === +currentPage)
                if (agent.checked) {
                    if (selectedAgentIndex === -1) {
                        selectedAgents.push({...agent, currentPage: +currentPage})
                    }
                } else {
                    if (selectedAgentIndex > -1) {
                        selectedAgents.splice(selectedAgentIndex, 1)
                    }
                }
            }
        }
        setSelectedAgents([...selectedAgents])
    }

    const handleConfigSubmit = () => {
        if (selectedAgents?.length > 0) {
            setIsOpenModal(true)
        }
    }

    const handleRemoveItem = (item) => {
        // let countCheckedAgent = 0
        for (const agent of store?.agents) {
            if (agent?.id === item?.id) {
                // document.querySelector(`[aria-label='select-row-${item?.id}']`).checked = false
                agent.checked = false
            }
            // countCheckedAgent += (agent?.checked ? 1 : 0)
        }
        /*if (countCheckedAgent === 0) {
            document.querySelector(`[aria-label='select-all-rows']`).checked = false
        }*/
        const newSelectedAccounts = selectedAgents?.filter(agent => agent?.id !== item?.id)
        setSelectedAgents([...newSelectedAccounts])
    }

    const handleCloseModal = () => {
        setIsOpenModal(false)
    }

    const handleReloadAgents = () => {
        setSelectedAgents([])
        const params = {page: currentPage, perPage: rowsPerPage, crawler_account_id: currentCrawlerAccount, domain_id: currentDomain, search: searchTemp}
        dispatch(getAgentNoDetails(params))
    }

    const handleStopWarningSubmit = () => {
        if (selectedAgents?.length > 0) {
            const data = []
            for (const agent of selectedAgents) {
                agent.is_warning = false
                data.push(agent)
            }

            dispatch(updateStatusAgents(data))
        }
    }

    return (
        <Fragment>
            {
                store.loading && <Loading/>
            }
            <Card id={'userContainer'}>
                <AccountWarningHeader domains={store?.domains}
                                      crawlerAccounts={store?.crawlerAccounts}
                                      currentCrawlerAccount={currentCrawlerAccount}
                                      handleSelectCrawlerAccount={handleSelectCrawlerAccount}
                                      handleSelectDomain={handleSelectDomain}
                                      handleConfigSubmit={handleConfigSubmit}
                                      disabledConfigButton={selectedAgents?.length === 0}
                                      setSearchTemp={handleChangeSearchTemp}
                                      searchTemp={searchTemp}
                                      handleStopWarningSubmit={handleStopWarningSubmit}
                                      currentDomain={currentDomain}/>
                <div  className={'my-1 d-flex flex-wrap'}>
                    {
                        selectedAgents?.length > 0 &&
                        selectedAgents?.map((item, index) => {
                            return (
                                <div key={index} className={'selected-item'}>
                                    <span>{item?.account_id}</span>
                                    {/*<Button className={'btn-delete-item'} onClick={() => handleRemoveItem(item)}><X size={14}/></Button>*/}
                                </div>
                            )
                        })
                    }
                </div>
                <DataTable
                    noHeader
                    responsive
                    selectableRows
                    selectableRowsComponent={BootstrapCheckbox}
                    selectableRowSelected={row => row?.checked}
                    onSelectedRowsChange={handleSelectedAgentsChange}
                    persistTableHead
                    paginationServer
                    columns={columns(store?.domains, checkPermission)}
                    sortIcon={<ChevronDown/>}
                    className='react-dataTable px-0'
                    data={dataToRender()}
                    noDataComponent={<NoDataComponent message={'No accounts are being warned'}/>}
                />
                <div>
                    {
                        CustomPagination()
                    }
                </div>
            </Card>
            {
                isOpenModal &&
                <ModalConfig isOpen={isOpenModal}
                             selectedAgents={selectedAgents}
                             telegramGroups={store?.telegramGroups}
                             configNotifies={store?.configNotifies}
                             handleReloadAgents={handleReloadAgents}
                             toggle={handleCloseModal}/>
            }
        </Fragment>
    )
}

export default AccountWarning
