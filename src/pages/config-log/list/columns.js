// ** Third Party Components
import {Badge} from 'reactstrap'
import React from 'react'
import {convertDate} from "../../../utility/ConvertDate"

export const columns = ([
  {
    name: 'Id',
    width: '100px',
    sortable: true,
    selector: 'id',
    cell: row => row?.id
  },
  {
    name: 'Account',
    minWidth: '150px',
    cell: row => row?.user_name
  },
  {
    name: 'Domain',
    minWidth: '150px',
    center: 'true',
    cell: row => row?.domain
  },
  {
    name: 'Error Message',
    minWidth: '300px',
    cell: row => row?.error_log.substring(0, 100)
  },
  {
    name: 'Status',
    minWidth: '150px',
    center: 'true',
    cell: row => (
        <Badge className='text-capitalize' color={!row?.is_login ? 'light-danger' : 'light-success'} pill>
          {row.is_login ? 'Success' : 'Fail'}
        </Badge>
    )
  },
  {
    name: 'CreateDate',
    minWidth: '150px',
    selector: 'created_at',
    center: 'true',
    sortable: true,
    cell: row => convertDate(row?.created_date)
  }
])
