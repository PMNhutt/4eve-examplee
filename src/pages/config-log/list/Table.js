// ** React Imports
import React, {Fragment, useState, useEffect} from 'react'

// ** Columns
import {columns} from './columns'

// ** Store & Actions
import {useDispatch, useSelector} from 'react-redux'

// ** Third Party Components
import {ChevronDown} from 'react-feather'
import DataTable from 'react-data-table-component'
import {Card, Col, Input, Row} from 'reactstrap'
import {NoDataComponent} from "../../../components/NoDataComponent"
import {useHistory, useLocation} from 'react-router-dom'
import {Loading} from "../../../components/Loading"
import ReactPaginate from "react-paginate"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/logs.scss'
import {checkPermissionByScreen} from "../../users/profile/store/action"
import {PERMISSION_SCREEN_ENUM} from "../../../enum/permission-screens-constant"
import {getConfigLogs, getDomains} from "../store/action"
import Select from "react-select"


const ConfigLogHeader = ({searchTemp, setSearchTemp, domains, currentDomain, handleSelectDomain}) => {
    return (
        <Row className={'mb-1'}>
            <Col sm={12} lg={3}>
                <Select
                    isSearchable={false}
                    name='rowsPerPage'
                    options={domains}
                    noOptionsMessage={() => 'No companies yet'}
                    className='select-option section-select w-100 section-phone'
                    classNamePrefix='select'
                    placeholder={'Select company'}
                    onChange={handleSelectDomain}
                    getOptionValue={option => option?.domain}
                    getOptionLabel={option => option?.name}
                    value={domains?.find(domain => domain?.domain === +currentDomain)}
                />
            </Col>
            <Col sm={12} lg={3}>
                <Input
                    name='idAgents'
                    id='name'
                    placeholder='Enter account id'
                    className={'form-control'}
                    value={searchTemp}
                    onChange={setSearchTemp}
                />
            </Col>
        </Row>
    )
}

const ConfigNotifyList = () => {
    // ** Store Vars
    const dispatch = useDispatch()
    const store = useSelector(state => state.configLog)
    const history = useHistory()
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)
    const [checkPermission, setCheckPermission] = useState(null)
    const rowsPerPage = 10

    // ** States
    const [currentPage, setCurrentPage] = useState(searchParams?.get('page') ? searchParams?.get('page') : 1)
    const [userName, setUserName] = useState(searchParams?.get('user_name') ? searchParams?.get('user_name') : '')
    const [currentDomain, setCurrentDomain] = useState(searchParams?.get('domain') ? searchParams?.get('domain') : '')
    // ** Get data on mount
    useEffect(() => {
        const params = {
            domain: currentDomain,
            user_name: userName,
            page: currentPage,
            perPage: rowsPerPage,
            domain_id: currentDomain
        }
        dispatch(getConfigLogs(params))
        dispatch(getDomains())
        checkPermissionByScreen(PERMISSION_SCREEN_ENUM.telegram_group).then(res => {
            setCheckPermission(res)
        })
    }, [])

    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.page) {
            params.page = filterParams.page
        }
        if (filterParams.domain) {
            params.domain = filterParams.domain
        }
        if (filterParams.user_name) {
            params.user_name = filterParams.user_name
        }
        params.perPage = filterParams.perPage

        const urlSearchParams = new URLSearchParams(params)
        history.replace({pathname: location.pathname, search: urlSearchParams.toString()})
    }

    // ** Table data to render
    const dataToRender = () => {
        if (store.configLogs?.length > 0) {
            return store.configLogs
        } else {
            return []
        }
    }

    // ** Custom Pagination
    const CustomPagination = () => {
        const count = Number(Math.ceil(store.totalRecords / rowsPerPage))
        if (count > 1) {
            return (
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    pageCount={count || 1}
                    activeClassName='active'
                    forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                    onPageChange={handlePagination}
                    pageClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next'}
                    previousClassName={'page-item prev'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
                />
            )
        } else return <></>
    }

    const handlePagination = page => {
        const params = {page: page.selected + 1, perPage: rowsPerPage, domain: currentDomain, user_name: userName}
        dispatch(getConfigLogs(params))
        checkParams(params)
        setCurrentPage(page.selected + 1)
    }

    const handleChangeSearchTemp = e => {
        const params = {page: 1, perPage: rowsPerPage, domain: currentDomain, user_name: e?.target?.value}
        dispatch(getConfigLogs(params))
        checkParams(params)
        setCurrentPage(1)
        setUserName(e?.target?.value)
    }


    const handleSelectDomain = domain => {
        const params = {page: 1, perPage: rowsPerPage, domain: domain?.domain, user_name: userName}
        dispatch(getConfigLogs(params))
        checkParams(params)
        setCurrentPage(1)
        setCurrentDomain(domain?.id)
    }

    return (
        <Fragment>
            {
                store.loading && <Loading/>
            }
            <Card id={'userContainer'}>
                <ConfigLogHeader domains={store?.domains}
                                 handleSelectDomain={handleSelectDomain}
                                 setSearchTemp={handleChangeSearchTemp}
                                 searchTemp={userName}
                                 currentDomain={currentDomain}/>
                <DataTable
                    noHeader
                    responsive
                    persistTableHead
                    paginationServer
                    columns={columns}
                    sortIcon={<ChevronDown/>}
                    className='react-dataTable px-0'
                    data={dataToRender()}
                    noDataComponent={<NoDataComponent message={'Currently, no configs yet'}/>}
                />
                <div>
                    {
                        CustomPagination()
                    }
                </div>
            </Card>
        </Fragment>
    )
}

export default ConfigNotifyList
