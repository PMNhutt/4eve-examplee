import {
  GET_CONFIG_LOGS_BEGIN,
  GET_CONFIG_LOGS_FAIL,
  GET_CONFIG_LOGS_SUCCESS,
  GET_DOMAINS_BEGIN,
  GET_DOMAINS_SUCCESS,
  GET_DOMAINS_FAIL
} from "../action"

// ** Initial State
const initialState = {
  loading: false,
  totalRecords: 0,
  errorMessage: null,
  configLogs: [],
  params: {}
}

const configLog = (state = initialState, action) => {
  switch (action.type) {
    case GET_CONFIG_LOGS_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_CONFIG_LOGS_SUCCESS:
      return {
        ...state,
        configLogs: action.data,
        totalRecords: action.totalRecords,
        params: action.params,
        loading: false
      }
    case GET_CONFIG_LOGS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_DOMAINS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_DOMAINS_SUCCESS:
      return {
        ...state,
        domains: action.domains,
        loading: false
      }
    case GET_DOMAINS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    default:
      return { ...state }
  }
}
export default configLog
