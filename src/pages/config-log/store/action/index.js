import instances from "../../../../@core/plugin/axios"
export const GET_CONFIG_LOGS_BEGIN = 'GET_CONFIG_LOGS_BEGIN'
export const GET_CONFIG_LOGS_SUCCESS = 'GET_CONFIG_LOGS_SUCCESS'
export const GET_CONFIG_LOGS_FAIL = 'GET_CONFIG_LOGS_FAIL'
export const GET_DOMAINS_BEGIN = 'GET_DOMAINS_BEGIN'
export const GET_DOMAINS_SUCCESS = 'GET_DOMAINS_SUCCESS'
export const GET_DOMAINS_FAIL = 'GET_DOMAINS_FAIL'

export const getConfigLogs = (params) => {
    const config = {params: params}
    return async dispatch => {
        dispatch({
            type: GET_CONFIG_LOGS_BEGIN
        })
        await instances.get('/admin/crawler-account/log', config).then(response => {
            dispatch({
                type: GET_CONFIG_LOGS_SUCCESS,
                data: response.data?.data?.data,
                totalRecords: response.data?.data?.totalRecords,
                params
            })
        }).catch(error => {
            dispatch({
                type: GET_CONFIG_LOGS_FAIL,
                errorMessage: error
            })
        })
    }
}

export const getDomains = () => {
    return async dispatch => {
        dispatch({
            type: GET_DOMAINS_BEGIN
        })
        await instances.get('/admin/domain/all').then(response => {
            dispatch({
                type: GET_DOMAINS_SUCCESS,
                domains: [{id: '', name: 'Select all companies'}, ...response.data?.data?.domains]
            })
        }).catch(error => {
            dispatch({
                type: GET_DOMAINS_FAIL,
                errorMessage: error
            })
        })
    }
}
