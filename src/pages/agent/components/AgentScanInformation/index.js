import {Card, Col, FormGroup, Input, Label, Row} from "reactstrap"
import React from "react"

const AgentScanInformation = (props) => {
    const {agent, isEdit} = props
    return (
        <Card className={'p-2 agent-information'}>
            <Row>
                <Col lg={3} sm={12}>
                    <FormGroup>
                        <Label className={'label'} for='member'>Turn Orver {isEdit && <span className='text-danger'>*</span>}</Label>
                        <Input
                            name='member'
                            id='url'
                            placeholder=''
                            value={agent?.turn_over || ''}
                            disabled={true}
                        />
                    </FormGroup>
                </Col>
                <Col lg={3} sm={12}>
                    <FormGroup>
                        <Label className={'label'} for='member'>Win/Loss {isEdit && <span className='text-danger'>*</span>}</Label>
                        <Input
                            name='member'
                            id='url'
                            placeholder=''
                            value={agent?.win_loss || ''}
                            disabled={true}
                        />
                    </FormGroup>
                </Col>
                <Col lg={3} sm={12}>
                    <FormGroup>
                        <Label className={'label'} for='member'>Comm {isEdit && <span className='text-danger'>*</span>}</Label>
                        <Input
                            name='member'
                            id='url'
                            placeholder=''
                            value={agent?.comm || ''}
                            disabled={true}
                        />
                    </FormGroup>
                </Col>
                <Col lg={3} sm={12}>
                    <FormGroup>
                        <Label className={'label'} for='member'>Outstanding {isEdit && <span className='text-danger'>*</span>}</Label>
                        <Input
                            name='member'
                            id='url'
                            placeholder=''
                            value={agent?.outstanding || ''}
                            disabled={true}
                        />
                    </FormGroup>
                </Col>
            </Row>
        </Card>
    )
}

export default AgentScanInformation
