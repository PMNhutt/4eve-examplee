import {
    Button,
    Card,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    FormGroup,
    Label,
    Row,
    UncontrolledDropdown
} from "reactstrap"
import Select from "react-select"
import React, {useEffect, useState} from "react"
import {Archive, MoreVertical, Trash2, X} from "react-feather"
import {TIMERS} from "../../../../enum/time-constant"

const timerList = TIMERS

const AgentTelegramGroups = (props) => {
    const {agent, telegramGroups, configNotifies, isEdit, handleUpdateAgent} = props
    const [isShowTime, setIsShowTime] = useState([])
    const handleChangeTelegramGroup = (telegramGroup, index) => {
        if (agent?.agent_details[index]) {
            agent.agent_details[index].telegram_group_id = telegramGroup?.id
            handleUpdateAgent(agent)
        }
    }
    useEffect(() => {
        if (agent?.agent_details?.length > 0) {
            getIsShowTimer()
        }
    }, [agent])
    const handleChangeConfigNotify = (configNotify, index) => {
        if (agent?.agent_details[index]) {
            agent.agent_details[index].config_notify_id = configNotify?.id
            handleUpdateAgent(agent)
        }
    }

    const handleAddAgentDetail = () => {
        agent.agent_details.push({
            time_notify: []
        })
        isShowTime?.push(false)
        setIsShowTime([...isShowTime])
        handleUpdateAgent(agent)
    }

    const handleDeleteAgentDetail = (index) => {
        agent.agent_details.splice(index, 1)
        isShowTime?.splice(index, 1)
        setIsShowTime([...isShowTime])
        handleUpdateAgent(agent)
    }
    const getIsShowTimer = () => {
        const array = []
        agent?.agent_details?.forEach((item) => {
            if (item?.time_notify?.length > 0) {
                array?.push(true)
            } else {
                array?.push(false)
            }

        })
        setIsShowTime([...array])
    }
    const handleShowTime = (index) => {
        isShowTime[index] = true
        if (!agent.agent_details[index].time_notify) {
            agent.agent_details[index] = {...agent?.agent_details[index], time_notify: []}
            handleUpdateAgent(agent)
        }
        setIsShowTime([...isShowTime])
    }
    const handleHideTime = (index) => {
        isShowTime[index] = false
        setIsShowTime([...isShowTime])
        agent.agent_details[index].time_notify = []
        handleUpdateAgent(agent)
    }
    const handleAddTime = (index, value) => {
        const findData = agent?.agent_details[index]?.time_notify?.findIndex(item => item === value)
        if (findData >= 0) {
            agent.agent_details[index].time_notify.splice(findData, 1)
            handleUpdateAgent(agent)
        } else {
            agent.agent_details[index]?.time_notify?.push(
                value
            )
            handleUpdateAgent(agent)
        }
    }
    const handleCheckActive = (index, value) => {
        const findData = agent.agent_details[index]?.time_notify?.findIndex(item => item === value)
        return findData >= 0
    }
    return (
        <Card className={'p-2 agent-information'}>
            <div className={'d-flex justify-content-between'}>
                <h4 className={'label'}>List warning groups</h4>
                <p><b>Number of telegram groups: </b>{agent?.agent_details?.length}</p>
            </div>

            {
                agent?.agent_details?.length > 0 &&
                <div className={'agent-border mx-0 p-50'}>
                    {
                        agent?.agent_details?.map((agent_detail, index) => {
                            const config_notify = configNotifies?.find(item => item?.id === agent_detail?.config_notify_id)
                            return (
                                <>
                                    <Row key={index} className={index === 0 ? 'mx-0 align-items-end' : 'mx-0 pt-50 border-top align-items-end'}>
                                        <Col lg={3} sm={12}>
                                            <FormGroup>
                                                <Label className={'sub-label'}>Telegram group</Label>
                                                <Select className='react-select'
                                                        classNamePrefix='select'
                                                        options={telegramGroups}
                                                        placeholder={'Select telegram group'}
                                                        isDisabled={!isEdit}
                                                        getOptionValue={option => option?.id}
                                                        getOptionLabel={option => option?.telegram_group_name}
                                                        value={telegramGroups?.find(item => item?.id === agent_detail?.telegram_group_id)}
                                                        onChange={(item) => handleChangeTelegramGroup(item, index)}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col lg={3} sm={12}>
                                            <FormGroup>
                                                <Label className={'sub-label'}>Config</Label>
                                                <Select className='react-select'
                                                        classNamePrefix='select'
                                                        options={configNotifies}
                                                        placeholder={'Select config'}
                                                        isDisabled={!isEdit}
                                                        getOptionValue={option => option?.id}
                                                        getOptionLabel={option => option?.name}
                                                        value={configNotifies?.find(item => item?.id === agent_detail?.config_notify_id)}
                                                        onChange={(item) => handleChangeConfigNotify(item, index)}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col lg={isEdit ? 2 : 3} sm={12}>
                                            <FormGroup>
                                                <Label className={'sub-label'}>Outstanding</Label>
                                                <p className={'form-control form-disabled'}>{config_notify?.outstanding || 0}</p>
                                            </FormGroup>
                                        </Col>
                                        <Col lg={isEdit ? 2 : 3} sm={12}>
                                            <FormGroup>
                                                <Label className={'sub-label'}>Win/Loss</Label>
                                                <p className={'form-control form-disabled'}>{config_notify?.win_loss || 0}</p>
                                            </FormGroup>
                                        </Col>
                                        <Col lg={isEdit ? 1 : 0} sm={12} className={'pb-2 d-flex align-items-center'}>
                                            {
                                                isEdit &&
                                                <>
                                                    {
                                                        !isShowTime[index] &&
                                                        <UncontrolledDropdown>
                                                            <DropdownToggle tag='div' className='btn btn-sm p-0 pr-1'>
                                                                <Button color={'primary'} className={'p-25'}>
                                                                    <MoreVertical size={18} />
                                                                </Button>
                                                            </DropdownToggle>
                                                            <DropdownMenu right>
                                                                <DropdownItem
                                                                    className='w-100'
                                                                    onClick={() => handleShowTime(index)}
                                                                >
                                                                    <Archive size={14} className='mr-50' />
                                                                    <span className='align-middle'>{'Select a warning time frame'}</span>
                                                                </DropdownItem>
                                                            </DropdownMenu>
                                                        </UncontrolledDropdown>
                                                    }
                                                    <Button color={'primary'} className={'p-25'} onClick={() => handleDeleteAgentDetail(index)}><Trash2 size={18}/></Button>
                                                </>
                                            }
                                        </Col>
                                    </Row>
                                    {
                                        isShowTime[index] &&
                                        <div className={'container-time'}>
                                            <div className={'timer'}>
                                                <Label className={'timer-label'}>Select time frame</Label>
                                                {
                                                    isEdit && <X size={12} className={'ml-1 cursor-pointer'} onClick={() => handleHideTime(index)}/>
                                                }
                                            </div>
                                            <Row>
                                                {
                                                    timerList?.map((timer, time_index) => {
                                                        return (
                                                            <Col key={`${index}Item${time_index}`} lg={1} sm={2}  className={''}>
                                                                <div className={`container-time__item ${handleCheckActive(index, timer?.value) ? 'active' : ''}`} onClick={() => handleAddTime(index, timer?.value)}>
                                                                    {timer?.label}
                                                                </div>

                                                            </Col>
                                                        )
                                                    })
                                                }
                                            </Row>
                                        </div>
                                    }
                                </>
                            )
                        })
                    }
                </div>
            }
            {
                isEdit && <Button color={'primary'} className={'mt-2 w-fit-content'} onClick={handleAddAgentDetail}>Add warning group</Button>
            }
        </Card>
    )
}

export default AgentTelegramGroups
