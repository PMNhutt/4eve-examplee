// ** React Imports
import React, {useState} from 'react'

// ** Third Party Components
import ReactPaginate from 'react-paginate'
import {ChevronDown} from 'react-feather'
import DataTable from 'react-data-table-component'
import { Card } from 'reactstrap'
import {NoDataComponent} from "../../../../components/NoDataComponent"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'

const ChildAgentList = (props) => {
    const rowsPerPage = props?.perPage
    const [currentPage, setCurrentPage] = useState(1)

    // ** Function in get data on page change
    const handlePagination = page => {
        props?.handleChangePageChildAgents(page.selected + 1)
        setCurrentPage(page.selected + 1)
    }

    // ** Custom Pagination
    const AgentPagination = () => {
        const count = Number(Math.ceil(props?.data?.totalRecords / rowsPerPage))
        if (count > 1) {
            return (
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    pageCount={count || 1}
                    activeClassName='active'
                    forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                    onPageChange={handlePagination}
                    pageClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next'}
                    previousClassName={'page-item prev'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
                />
            )
        } else return <></>
    }
    // ** Table data to render
    const dataToRender = () => {
        const filters = {}
        const isFiltered = Object.keys(filters).some(function (k) {
            return filters[k].length > 0
        })
        if (props?.data && props?.data?.data?.length > 0) {
            return [props?.data?.data[0]]
        } else if (props?.data?.data?.length === 0 && isFiltered) {
            return []
        }
    }

    return (
        <Card id={'agentContainer'}>
            <DataTable
                noHeader
                responsive
                persistTableHead
                paginationServer
                columns={props?.columns}
                sortIcon={<ChevronDown/>}
                className='react-dataTable px-0 w-fit-content agent-dataTable'
                data={dataToRender()}
                noDataComponent={<NoDataComponent message={'Hiện tại chưa có dữ liệu'}/>}
            />
        </Card>
    )
}

export default ChildAgentList
