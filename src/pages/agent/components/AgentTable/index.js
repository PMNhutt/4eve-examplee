import {Card} from "reactstrap"
import React from "react"
import Table from "./Table"

const AgentChildAgents = (props) => {
    return (
        <Card className={'p-2 agent-information'}>
            <h4 className={'label'}>{props?.title}</h4>
            <Table perPage={props?.perPage} data={props?.data} columns={props?.columns} handleChangePageChildAgents={props?.handleChangePage}/>
        </Card>
    )
}

export default AgentChildAgents
