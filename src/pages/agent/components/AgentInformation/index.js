import {Card, Col, FormGroup, Input, Label, Row} from "reactstrap"
import React, {useState} from "react"
import Select from "react-select"
import {Check, X} from "react-feather"

const warnings = [
    {
        label: 'Warning',
        value: true
    },
    {
        label: 'Stop Warning',
        value: false
    }
]

const AgentInformation = (props) => {
    const {agent, crawlerAccounts, isEdit, handleUpdateAgent} = props

    const [error, setError] = useState({})

    const handleChangeLevel = (level) => {
        agent.agent_level_id = level?.id
        handleUpdateAgent(agent)
    }

    const handleChangeName = (e) => {
        agent.display_name = e.target.value
        error.display_name = (agent.display_name === '')
        setError({...error})
        handleUpdateAgent(agent)
    }

    const handleChangeWarning = (warning) => {
        agent.is_warning = warning?.value
        handleUpdateAgent(agent)
    }

    return (
        <Card className={'p-2 agent-information'}>
            <Row>
                <Col lg={3} sm={12}>
                    <FormGroup>
                        <Label className={'label'} for='member'>Tree Account Name {isEdit && <span className='text-danger'>*</span>}</Label>
                        <Input
                            name='member'
                            id='url'
                            placeholder='Enter agent name'
                            value={agent?.display_name || ''}
                            onChange={handleChangeName}
                            disabled={true}
                        />
                    </FormGroup>
                </Col>
                <Col lg={3} sm={12}>
                    <FormGroup>
                        <Label className={'label'} for='account_id'>Scan Account {isEdit && <span className='text-danger'>*</span>}</Label>
                        <Input
                            name='member'
                            id='url'
                            placeholder='Enter agent name'
                            value={agent?.crawler_account?.username || ''}
                            onChange={handleChangeName}
                            disabled={true}
                        />
                    </FormGroup>
                </Col>
                <Col lg={3} sm={12}>
                    <FormGroup>
                        <Label className={'label'} for='member'>Company Name</Label>
                        <Input
                            name='member'
                            id='url'
                            placeholder='Company Name'
                            value={agent?.crawler_account?.domain?.name || ''}
                            onChange={handleChangeName}
                            disabled={true}
                        />
                    </FormGroup>
                </Col>
                {
                    agent?.id &&
                    <Col lg={3} sm={12} >
                        <FormGroup>
                            <Label className={'label'} for={'status'}>Status</Label>
                            <Select
                                isClearable={false}
                                className='react-select'
                                classNamePrefix='select'
                                options={warnings}
                                defaultValue={warnings?.find(item => item?.value === agent?.is_warning)}
                                onChange={handleChangeWarning}
                            />
                        </FormGroup>
                    </Col>
                }
            </Row>
        </Card>
    )
}

export default AgentInformation
