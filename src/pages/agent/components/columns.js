import React from 'react'
import {convertDate} from "../../../utility/ConvertDate"

export const agentWinLossColumns = (levels => [
  {
    name: 'NameAccount',
    width: '200px',
    selector: 'id',
    sortable: true,
    cell: row => row?.id
  },
  {
    name: 'GrossCom',
    minWidth: '200px',
    cell: row => row?.player_commission
  },
  {
    name: 'Win/Loss',
    minWidth: '200px',
    cell: row => row?.player_win_loss
  },
  {
    name: 'Turnover',
    minWidth: '100px',
    cell: row => row?.turnover
  },
  {
    name: 'Outstanding',
    minWidth: '200px',
    cell: () => ''
  }
])

export const agentOutstandingColumns = (levels => [
  {
    name: 'Id',
    width: '100px',
    selector: 'id',
    sortable: true,
    cell: row => row?.id
  },
  {
    name: 'Members',
    minWidth: '200px',
    cell: row => row?.member_total || 0
  },
  {
    name: 'DataScanDate',
    minWidth: '200px',
    cell: row => convertDate(row?.date_scan)
  },
  {
    name: 'CreatedDate',
    width: '120px',
    cell: row => convertDate(row?.created_at)
  },
  {
    name: 'UpdatedDate',
    width: '120px',
    cell: row => convertDate(row?.updated_at)
  }
])
