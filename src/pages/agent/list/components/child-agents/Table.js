// ** React Imports
import React, {Fragment, useEffect, useState} from 'react'

// ** Columns
import {columns} from '../columns'

// ** Store & Actions
import {useSelector} from 'react-redux'

// ** Third Party Components
import ReactPaginate from 'react-paginate'
import {ChevronDown} from 'react-feather'
import DataTable from 'react-data-table-component'
import { Card } from 'reactstrap'
import {NoDataComponent} from "../../../../../components/NoDataComponent"
import {Loading} from "../../../../../components/Loading"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import './style/style.scss'
import ChildAgent from "./index"
import {getAgentsTotal, getChildAgentsByParentAgent} from "../../../store/action"

const ChildAgentList = (props) => {
    const rowsPerPage = props?.perPage
    const [currentPage, setCurrentPage] = useState(1)
    const [agentExpanded, setAgentExpanded] = useState(0)
    const agentStore = useSelector(state => state?.agents)
    const [dataChild, setDataChild] = useState(null)
    const [accountId, setAccountId] = useState(null)
    const [totalRecords, setTotalRecords] = useState(null)
    const [totalTelegram, setTotalTelegram] = useState([])

    // ** Function in get data on page change
    const handlePagination = page => {
        props?.handleChangePageChildAgents(page.selected + 1, props?.data?.account_id)
        setCurrentPage(page.selected + 1)
    }
    useEffect(() => {
        if (dataChild?.length > 0) {
            const total = []
            dataChild.forEach(item => {
                getDetail(item?.id, total)
            })
        }
    }, [dataChild?.length])
    const getDetail = (id, total) => {
        getAgentsTotal(id).then(r => {
            if (r && r?.data && r?.data?.data) {
                total.push({id: id, ...r?.data?.data})
                setTotalTelegram([...total])
            }
        })
    }

    // ** Custom Pagination
    const AgentPagination = () => {
        const count = Number(Math.ceil(props.totalRecords / rowsPerPage))
        if (count > 1) {
            return (
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    pageCount={count || 1}
                    activeClassName='active'
                    forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                    onPageChange={handlePagination}
                    pageClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next'}
                    previousClassName={'page-item prev'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
                />
            )
        } else return <></>
    }
    // ** Table data to render
    const dataToRender = () => {
        const filters = {}
        const isFiltered = Object.keys(filters).some(function (k) {
            return filters[k].length > 0
        })
        if (props?.dataChild?.length > 0) {
            return props?.dataChild
        } else if (props?.dataChild?.length === 0 && isFiltered) {
            return []
        }
    }
    const handleExpandToggled = (expand, row) => {
        if (expand) {
            const params = {page: 1, perPage: rowsPerPage, parent_account_id: row?.account_id}
            getChildAgentsByParentAgent(params).then(r => {
                setDataChild(r?.data?.data?.data)
                setTotalRecords(r?.data?.data?.totalRecords)
            })
            setAccountId(row?.account_id)
            setAgentExpanded(row?.id)
        } else {
            setAgentExpanded(0)
        }
    }
    return (
        <Fragment>
            {
                agentStore.loading &&
                <Loading/>
            }
            <h4 className={'px-2 font-weight-bolder mb-0 mt-2'} style={{fontSize: '16px'}}>List of agents under {props?.accountId} account</h4>
            <Card id={'agentContainer'}>
                <DataTable
                    noHeader
                    responsive
                    persistTableHead
                    paginationServer
                    columns={columns(true, props?.checkPermission, props?.storeTelegram, props?.totalTelegram)}
                    sortIcon={<ChevronDown/>}
                    className='react-dataTable px-0 agent-dataTable'
                    data={dataToRender()}
                    expandableRows
                    expandOnRowClicked={true}
                    expandableRowExpanded={row => row?.id === agentExpanded}
                    onRowExpandToggled={handleExpandToggled}
                    expandableRowsComponent={dataChild?.length > 0 ?  <ChildAgent totalTelegram={totalTelegram} storeTelegram={props?.storeTelegram} accountId={accountId} totalRecords={totalRecords} checkPermission={props?.checkPermission} dataChild={dataChild} perPage={props.perPage} handleChangePageChildAgents={props.handleChangePageChildAgents}/> : <></>}
                    noDataComponent={<NoDataComponent message={'Currently, no data yet'}/>}
                />
                <div>
                    {
                        AgentPagination()
                    }
                </div>
            </Card>
        </Fragment>
    )
}

export default ChildAgentList
