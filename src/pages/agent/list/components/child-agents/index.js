// ** User List Component
import Table from './Table'
import React from "react"

// ** Styles
import '@styles/react/apps/app-users.scss'

const ChildAgent = (props) => {
    return (
        <div id='userCustomer' className='app-user-list'>
            {
                props?.data?.telegram_groups?.length > 0 &&
                <>
                    <h4 className={'px-2 font-weight-bolder mb-0 mt-2'}>Nhóm telegram đã tham gia</h4>
                    <ol className={'mx-2 my-1'}>
                        {
                            props?.data?.telegram_groups?.map((telegram, index) => {
                                return (
                                    <li key={index}>{telegram?.telegram_group_name}</li>
                                )
                            })
                        }
                    </ol>
                </>
            }
            <Table storeTelegram={props?.storeTelegram} totalTelegram={props?.totalTelegram} accountId={props?.accountId} totalRecords={props?.totalRecords} checkPermission={props?.checkPermission} dataChild={props?.dataChild} handleChangePageChildAgents={props?.handleChangePageChildAgents} perPage={props?.perPage} data={props?.data}/>
        </div>
    )
}

export default ChildAgent
