import React from 'react'
import {convertDate} from "../../../../utility/ConvertDate"
import {Badge, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap"
import {Link} from "react-router-dom"
import {MoreVertical, StopCircle, PlayCircle, FileText} from "react-feather"
import { store } from '@store/storeConfig/store'
import {updateAgent, updateStatusAgent} from "../../store/action"

export const columns = ((isChild, checkPermission, storeTelegram, totalTelegram) => [
  {
    name: 'NameAccount',
    width: '200px',
    cell: row => {
      if (checkPermission) {
        return <Link to={`/agent/edit/${row.id}`} target="_blank" className={'text-link'}>{row?.account_id}</Link>
      } else return row?.account_id
    }
  },
  {
    name: 'Company',
    minWidth: '200px',
    maxWidth: isChild ? '200px' : '300px',
    sortable: true,
    cell: row => row?.crawler_account?.domain?.name
  },
  {
    name: 'AccountCrawler',
    minWidth: '200px',
    maxWidth: '200px',
    sortable: true,
    cell: row => row?.crawler_account?.username
  },
  {
    name: 'GroupTele',
    width: '100px',
    cell: row => totalTelegram?.length > 0 ? (
            <Badge className='text-capitalize' color={row.agent_details?.length === 0 ? 'light-danger' : 'light-success'} pill>
              {row.agent_details?.length === 0 ? 'Not added yet' : `${row.agent_details?.length}`}({totalTelegram?.find(item => item?.id === row.id)?.agentNoDetails} / {totalTelegram?.find(item => item?.id === row.id)?.agents})
            </Badge>
        ) : (
            <Badge className='text-capitalize' color={row.agent_details?.length === 0 ? 'light-danger' : 'light-success'} pill>
              {row.agent_details?.length === 0 ? 'Not added yet' : `There are ${row.agent_details?.length} groups`}
            </Badge>
        )
  },
  {
    name: 'Status',
    width: '250px',
    selector: 'status',
    sortable: true,
    center: true,
    cell: row => (
        <Badge className='text-capitalize' color={row?.is_warning ? 'light-success' : 'light-warning'} pill>
          {row.is_warning ? 'Warning' : 'Stop warning'}
        </Badge>
    )
  },
  {
    name: 'CreateDate',
    width: '150px',
    selector: 'created_at',
    sortable: true,
    cell: row => convertDate(row?.created_at)
  },
  {
    name: 'Action',
    cell: row => (
        <UncontrolledDropdown>
          <DropdownToggle tag='div' className='btn btn-sm'>
            <MoreVertical size={14} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu right>
            {
                checkPermission?.edit &&
                <>
                  <DropdownItem
                      tag={Link}
                      to={`/agent/edit/${row?.id}`}
                      target="_blank"
                      className='w-100'>
                    <FileText size={14} className='mr-50' />
                    <span className='align-middle'>Edit</span>
                  </DropdownItem>
                  <DropdownItem
                      onClick={() => {
                        store.dispatch(updateStatusAgent(row?.account_id, {is_warning: !row?.is_warning}, true))
                      }}
                      className='w-100'>
                    {
                      row?.is_warning ? <StopCircle size={14} className='mr-50' /> : <PlayCircle size={14} className='mr-50' />
                    }
                    <span className='align-middle'>{row?.is_warning ? 'Stop warning' : 'Warning'}</span>
                  </DropdownItem>
                </>
            }
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
])
