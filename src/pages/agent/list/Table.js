// ** React Imports
import React, {Fragment, useState, useEffect} from 'react'

// ** Columns
import {columns} from './components/columns'

// ** Store & Actions
import {useDispatch, useSelector} from 'react-redux'

// ** Third Party Components
import ReactPaginate from 'react-paginate'
import {ChevronDown} from 'react-feather'
import DataTable from 'react-data-table-component'
import {Card, Col, Row} from 'reactstrap'
import {NoDataComponent} from "../../../components/NoDataComponent"
import {useHistory, useLocation} from 'react-router-dom'
import {Loading} from "../../../components/Loading"
import {getAgents, getAgentsTotal, getChildAgentsByParentAgent} from "../store/action"
import ChildAgent from "./components/child-agents"
import {handleMenuHidden} from "../../../redux/actions/layout"
import {checkPermissionByScreen} from "../../users/profile/store/action"
import {PERMISSION_SCREEN_ENUM} from "../../../enum/permission-screens-constant"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/agent.scss'
import {getTelegramGroups} from "../../telegram-group/store/action"
import Select from "react-select"
import {getCrawlerAccounts, getDomains} from "../../account-warning/store/action"
const statusList = [
    {
        label: 'All',
        value: ''
    },
    {
        label: 'Warning',
        value: true
    }
]

const CustomHeader = ({handleChangeStatus, status, storeCrawler, handleChangeDomain, domain, handleFilterByCrawlerAccount, crawlerAccount}) => {
    return (
        <Row className={'mb-1'}>
            {/*<Col sm={12} lg={2}>
                <Select
                    isSearchable={false}
                    name='rowsPerPage'
                    options={statusList}
                    className='select-option section-select w-100 section-phone'
                    classNamePrefix='select'
                    defaultValue={statusList.find(item => item.value === status) || statusList[1]}
                    placeholder={'Select status'}
                    onChange={handleChangeStatus}
                />
            </Col>*/}
            <Col sm={12} lg={3}>
                <Select
                    isClearable={false}
                    className='react-select'
                    classNamePrefix='select'
                    name='domain'
                    options={storeCrawler?.domains}
                    getOptionValue={option => option.id}
                    getOptionLabel={option => option.name}
                    value={storeCrawler?.domains?.find(item => item.id === parseInt(domain)) || {id: '', name: 'Select all companies'}}
                    placeholder={'Select company'}
                    onChange={handleChangeDomain}
                />
            </Col>
            <Col sm={12} lg={3}>
                <Select
                    isClearable={false}
                    className='react-select'
                    classNamePrefix='select'
                    name='crawlerAccount'
                    options={storeCrawler?.crawlerAccounts}
                    getOptionValue={option => option.id}
                    getOptionLabel={option => option.username}
                    value={storeCrawler?.crawlerAccounts?.find(item => item.username === crawlerAccount) || {id: 0, username: 'Select all crawler accounts'}}
                    placeholder={'Select crawler account'}
                    onChange={handleFilterByCrawlerAccount}
                />
            </Col>
        </Row>
    )
}

const AgentList = () => {
    // ** Store Vars
    const dispatch = useDispatch()
    const store = useSelector(state => state.agents)
    const storeTelegram = useSelector(state => state.telegram)
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)
    const rowsPerPage = 10
    const history = useHistory()
    const [accountId, setAccountId] = useState(null)
    const [totalTelegram, setTotalTelegram] = useState([])
    const storeCrawler = useSelector(state => state.agentNoDetails)

    // ** States
    const [currentPage, setCurrentPage] = useState(searchParams?.get('page') || 1)
    const [agentExpanded, setAgentExpanded] = useState(0)
    const [checkPermission, setCheckPermission] = useState(null)
    const [dataChild, setDataChild] = useState(null)
    const [totalChild, setTotalChild] = useState(null)
    const [totalRecords, setTotalRecords] = useState(null)
    const [status, setStatus] = useState(searchParams?.get('status') || '')
    const [domain, setDomain] = useState(searchParams?.get('domain') ? searchParams?.get('domain') : '')
    const [crawlerAccount, setCrawlerAccount] = useState(searchParams?.get('account') ? searchParams?.get('account') : '')
    // ** Get data on mount
    useEffect(() => {
        const params = {
            page: currentPage,
            perPage: rowsPerPage,
            status: status,
            domain_id: domain,
            account: crawlerAccount
        }
        dispatch(getAgents(params))
        dispatch(handleMenuHidden(false))
        checkPermissionByScreen(PERMISSION_SCREEN_ENUM.agent).then(res => {
            setCheckPermission(res)
        })
        dispatch(getDomains())
        const paramsTelegram = {page: 1, perPage: 1}
        dispatch(getTelegramGroups(paramsTelegram))
        dispatch(getCrawlerAccounts())
        localStorage.removeItem('agent')
        localStorage.setItem('agent', `/agent/list?${status ? `status=${status}&` : ''}${domain ? `domain=${domain}&` : ''}${crawlerAccount ? `account=${crawlerAccount}&` : ''}page=${currentPage}&perPage=10`)
    }, [])
    useEffect(() => {
        if (store.agents?.length > 0) {
            const total = []
            store?.agents.forEach(item => {
                getDetail(item?.id, total)
            })
        }
    }, [store.agents])
    const getDetail = (id, total) => {
        getAgentsTotal(id).then(r => {
            if (r && r?.data && r?.data?.data) {
                total.push({id: id, ...r?.data?.data})
                setTotalTelegram([...total])
            }
        })
    }
    const getDetailChild = (id, total) => {
        getAgentsTotal(id).then(r => {
            if (r && r?.data && r?.data?.data) {
                total.push({id: id, ...r?.data?.data})
                setTotalChild([...total])
            }
        })
    }
    const handleChangeStatus = (value) => {
        setStatus(value?.value)
        const params = {
            page: currentPage,
            perPage: rowsPerPage,
            status: value?.value,
            domain_id: domain,
            account: crawlerAccount
        }
        dispatch(getAgents(params))
        checkParams(params)
    }
    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.page) {
            params.page = filterParams.page
        }
        if (filterParams.domain_id) {
            params.domain = filterParams.domain_id
        }
        if (filterParams.account) {
            params.account = filterParams.account
        }
        params.perPage = filterParams.perPage

        const urlSearchParams = new URLSearchParams(params)
        history.replace({pathname: location.pathname, search: urlSearchParams.toString()})
        localStorage.removeItem('agent')
        localStorage.setItem('agent', `/agent/list?${filterParams.domain_id ? `domain=${filterParams.domain_id}&` : ''}${crawlerAccount ? `account=${crawlerAccount}&` : ''}page=${filterParams.page}&perPage=10`)
    }
    const handleChangeDomain = value => {
        setDomain(value?.id)
        setCurrentPage(1)
        const crawlerAccountIndex = storeCrawler?.crawlerAccounts?.findIndex(item => item?.username === crawlerAccount && item?.domain_id === value?.id)
        let params = {domain_id: value?.id || undefined, page: 1, perPage: rowsPerPage, status: status !== undefined ? status : '', account: crawlerAccount}
        if (crawlerAccountIndex === -1) {
            params = {page: 1, perPage: rowsPerPage, domain_id: value?.id, status: status !== undefined ? status : ''}
            setCrawlerAccount('')
        }
        dispatch(getAgents(params))
        dispatch(getCrawlerAccounts({domain_id: value?.id}))
        checkParams(params)
    }

    // ** Function in get data on page change
    const handlePagination = page => {
        const params = {page: page.selected + 1, perPage: rowsPerPage, domain_id: domain, status: status, account: crawlerAccount}
        dispatch(getAgents(params))
        checkParams(params)
        setCurrentPage(page.selected + 1)
    }

    const handleFilterByCrawlerAccount = account => {
        let params = {page: 1, perPage: rowsPerPage, domain_id: domain, status: status, account: account?.username}
        if (account?.id !== 0) {
            setCrawlerAccount(account?.username)
        } else {
            params = {page: 1, perPage: rowsPerPage, domain_id: domain, status: status, account: ''}
            setCrawlerAccount('')
        }
        dispatch(getAgents(params))
        checkParams(params)
    }

    // ** Custom Pagination
    const AgentPagination = () => {
        const count = Number(Math.ceil(store.totalRecords / rowsPerPage))
        if (count > 1) {
            return (
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    pageCount={count || 1}
                    activeClassName='active'
                    forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                    onPageChange={handlePagination}
                    pageClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next'}
                    previousClassName={'page-item prev'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
                />
            )
        } else return <></>
    }
    // ** Table data to render
    const dataToRender = () => {
        const filters = {}
        const isFiltered = Object.keys(filters).some(function (k) {
            return filters[k].length > 0
        })
        if (store.agents?.length > 0) {
            return store.agents
        } else if (store.agents?.length === 0 && isFiltered) {
            return []
        }
    }

    const handleExpandToggled = (expand, row) => {
        if (expand) {
            const params = {page: 1, perPage: rowsPerPage, parent_account_id: row?.account_id, status: status}
            getChildAgentsByParentAgent(params).then(r => {
                setDataChild(r?.data?.data?.data)
                if (r?.data?.data?.data?.length > 0) {
                    const total = []
                    r?.data?.data?.data.forEach(item => {
                        getDetailChild(item?.id, total)
                    })
                }
                setTotalRecords(r?.data?.data?.totalRecords)
            })
            setAccountId(row?.account_id)
            setAgentExpanded(row?.id)
        } else {
            setAgentExpanded(0)
        }
    }
    const handleChangePageChildAgents = (page, parentAgentId) => {
        const params = {page: page, perPage: rowsPerPage, parent_account_id: parentAgentId}
        getChildAgentsByParentAgent(params).then(r => {
            setDataChild(r?.data?.data?.data)
            setTotalRecords(r?.data?.data?.totalRecords)
        })
    }

    const handleLinkToDetail = (row) => {
        window.open(`/agent/edit/${row?.id}`, '_blank')
    }

    return (
        <Fragment>
            {
                store.loading &&
                <Loading/>
            }
            <Card id={'agentContainer'}>
                <CustomHeader
                    handleChangeStatus={handleChangeStatus}
                    status={status}
                    storeCrawler={storeCrawler}
                    handleChangeDomain={handleChangeDomain}
                    domain={domain}
                    handleFilterByCrawlerAccount={handleFilterByCrawlerAccount}
                    crawlerAccount={crawlerAccount}
                />
                <DataTable
                    noHeader
                    responsive
                    persistTableHead
                    paginationServer
                    columns={columns(false, checkPermission, storeTelegram, totalTelegram)}
                    onRowClicked={handleLinkToDetail}
                    expandableRows
                    expandOnRowClicked={true}
                    expandableRowExpanded={row => row?.id === agentExpanded}
                    onRowExpandToggled={handleExpandToggled}
                    expandableRowsComponent={<ChildAgent storeTelegram={storeTelegram} totalTelegram={totalChild} accountId={accountId} totalRecords={totalRecords} dataChild={dataChild} perPage={rowsPerPage} checkPermission={checkPermission} handleChangePageChildAgents={handleChangePageChildAgents}/>}
                    sortIcon={<ChevronDown/>}
                    className='react-dataTable px-0 table-no-padding'
                    data={dataToRender()}
                    noDataComponent={<NoDataComponent message={'Currently, no data yet'}/>}
                />
                <div>
                    {
                        AgentPagination()
                    }
                </div>
            </Card>
        </Fragment>
    )
}

export default AgentList
