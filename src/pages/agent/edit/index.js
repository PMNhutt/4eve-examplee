// ** React Imports
import React, {useEffect, Fragment, useState} from 'react'
import {useHistory, useParams} from "react-router-dom"
import CommonHeaderFooter from "../../../components/CommonFooter/CommonHeaderFooter"
import {Loading} from "../../../components/Loading"
import AgentInformation from "../components/AgentInformation"
import { useDispatch, useSelector } from 'react-redux'
import AgentTelegramGroups from "../components/AgentTelegramGroups"
import {toast} from "react-toastify"
import ErrorNotificationToast from "../../../components/Toast/ToastFail"
import SuccessNotificationToast from "../../../components/Toast/ToastSuccess"
import {checkPermissionByScreen} from "../../users/profile/store/action"
import {PERMISSION_SCREEN_ENUM} from "../../../enum/permission-screens-constant"

// ** Store & Actions
import {
    getAgentById, getAgentOutstanding, getAgentWinLoss,
    getConfigNotifies, getTelegramGroups, updateAgent, updateAgentWait
} from "../store/action"

import '@styles/base/pages/app-ecommerce-details.scss'
import './style/style.scss'
import AgentScanInformation from "../components/AgentScanInformation"

const AgentEdit = () => {
    // ** Store Vars
    const dispatch = useDispatch()
    const store = useSelector(state => state.agents)
    const history = useHistory()

    // ** Vars
    const id = useParams().id
    const rowsPerPage = 5

    const [currentPageOutstanding, setCurrentPageOutstanding] = useState(1)
    const [currentPageWinLoss, setCurrentPageWinLoss] = useState(1)
    const [agent, setAgent] = useState({})
    const [checkPermission, setCheckPermission] = useState(null)

    useEffect(() => {
        checkPermissionByScreen(PERMISSION_SCREEN_ENUM.agent).then(res => {
            setCheckPermission(res)
            if (res?.view) {
                if (id) {
                    dispatch(getAgentById(id))
                    dispatch(getAgentWinLoss({page: currentPageWinLoss, perPage: rowsPerPage, agent_id: id}))
                    dispatch(getAgentOutstanding({page: currentPageOutstanding, perPage: rowsPerPage, agent_id: id}))
                    dispatch(getTelegramGroups())
                    dispatch(getConfigNotifies())
                }
            }
        })
    }, [])

    async function handleSubmitButton() {
        if (checkPermission?.edit) {
            const agentData = store?.agent
            const data = {}
            data.agent_details = []
            data.is_warning = agentData?.is_warning
            if (agentData?.agent_details?.length > 0) {
                for (const group of agentData?.agent_details) {
                    if (group?.telegram_group_id && group?.config_notify_id) {
                        data.agent_details.push(group)
                    }
                }
            }
            const response = await updateAgentWait(agentData?.id, data)
            if (response?.status && response.data) {
                toast.success(<SuccessNotificationToast/>)
            } else {
                toast.error(<ErrorNotificationToast message={store?.errorMessage?.data?.message}/>)
            }
        }
    }

    function handleChangePageWinLoss(page) {
        dispatch(getAgentWinLoss({page: page, perPage: rowsPerPage, agent_id: id}))
        setCurrentPageWinLoss(page)
    }

    function handleChangePageOutstanding(page) {
        dispatch(getAgentOutstanding({page: page, perPage: rowsPerPage, agent_id: id}))
        setCurrentPageOutstanding(page)
    }

    function handleUpdateAgent(agentUpdate) {
        store.agent = agentUpdate
        setAgent({...store.agent})
    }
    return (
        <Fragment>
            {
                store?.loading && <Loading />
            }
            <div className='app-ecommerce-details container-fluid' id={'notificationDetail'}>
                <CommonHeaderFooter breadcrumb={{list_link: localStorage.getItem('agent'), list_title: 'Agent', detail_title: store?.agent?.member}}
                                    textButtonRight= {'Update'}
                                    handleSubmitButton={handleSubmitButton}
                                    header={true}/>
                <AgentInformation agent={store?.agent} isEdit={true} handleUpdateAgent={handleUpdateAgent}/>
                <AgentTelegramGroups agent={store?.agent} telegramGroups={store?.telegramGroups} configNotifies={store?.configNotifies} isEdit={true} handleUpdateAgent={handleUpdateAgent}/>

                {/*<AgentTable data={store?.agentWinLoss} perPage={rowsPerPage} handleChangePage={handleChangePageWinLoss} title={'List win/loss'} columns={agentWinLossColumns('')}/>*/}
                {/*<AgentTable data={store?.agentOutstanding} perPage={rowsPerPage} handleChangePage={handleChangePageOutstanding} title={'List outstanding'}  columns={agentOutstandingColumns('')}/>*/}
                <AgentScanInformation agent={store?.agent} handleUpdateAgent={handleUpdateAgent}/>
                <CommonHeaderFooter handleSubmitButton={handleSubmitButton}
                                    backUrl={localStorage.getItem('agent')}
                                    textButtonRight= {'Update'}
                                    dispatch={dispatch}
                />
            </div>
        </Fragment>
    )
}

export default AgentEdit
