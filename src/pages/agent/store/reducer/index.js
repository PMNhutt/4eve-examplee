import {
  GET_DATA_SUCCESS,
  GET_DATA_FAILURE,
  GET_DATA_BEGIN,
  GET_CHILD_AGENTS_BEGIN,
  GET_CHILD_AGENTS_SUCCESS,
  GET_CHILD_AGENTS_FAILURE,
  UPDATE_AGENT_BEGIN,
  UPDATE_AGENT_SUCCESS,
  UPDATE_AGENT_FAIL,
  GET_AGENT_BY_ID_BEGIN,
  GET_AGENT_BY_ID_SUCCESS,
  GET_AGENT_BY_ID_FAIL,
  GET_AGENT_WIN_LOSS_BEGIN,
  GET_AGENT_WIN_LOSS_SUCCESS,
  GET_AGENT_WIN_LOSS_FAIL,
  GET_AGENT_OUTSTANDING_BEGIN,
  GET_AGENT_OUTSTANDING_SUCCESS,
  GET_AGENT_OUTSTANDING_FAIL,
  GET_CRAWLER_ACCOUNTS_BEGIN,
  GET_CRAWLER_ACCOUNTS_SUCCESS,
  GET_CRAWLER_ACCOUNTS_FAIL,
  GET_TELEGRAM_GROUPS_BEGIN,
  GET_TELEGRAM_GROUPS_SUCCESS,
  GET_TELEGRAM_GROUPS_FAIL,
  GET_CONFIG_NOTIFIES_FAIL,
  GET_CONFIG_NOTIFIES_SUCCESS,
  GET_CONFIG_NOTIFIES_BEGIN,
  UPDATE_AGENT_STATUS_BEGIN,
  UPDATE_AGENT_STATUS_SUCCESS,
  UPDATE_AGENT_STATUS_FAIL
} from "../action"

// ** Initial State
const initialState = {
  loading: false,
  agents: [],
  childAgents: [],
  totalRecordsChildAgents: [],
  totalRecords: 0,
  errorMessage: null,
  agent: null,
  telegramGroups: [],
  params: {},
  childParams: {},
  agentWinLoss: {},
  agentOutstanding: {},
  crawlerAccounts: [],
  configNotifies: []
}

const agents = (state = initialState, action) => {
  switch (action.type) {
    case GET_AGENT_WIN_LOSS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_AGENT_WIN_LOSS_SUCCESS:
      return {
        ...state,
        agentWinLoss: action.data,
        loading: false
      }
    case GET_AGENT_WIN_LOSS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_AGENT_OUTSTANDING_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_AGENT_OUTSTANDING_SUCCESS:
      return {
        ...state,
        agentOutstanding: action.data,
        loading: false
      }
    case GET_AGENT_OUTSTANDING_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_AGENT_BY_ID_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_AGENT_BY_ID_SUCCESS:
      return {
        ...state,
        agent: action.data,
        loading: false
      }
    case GET_AGENT_BY_ID_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_DATA_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_DATA_SUCCESS:
      return {
        ...state,
        agents: action.data,
        totalRecords: action?.totalRecords,
        params: action.params,
        loading: false
      }
    case GET_DATA_FAILURE:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_CHILD_AGENTS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_CHILD_AGENTS_SUCCESS:
      return {
        ...state,
        childAgents: action.data,
        totalRecordsChildAgents: action?.totalRecords,
        childParams: action.params,
        loading: false
      }
    case GET_CHILD_AGENTS_FAILURE:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case UPDATE_AGENT_BEGIN:
      return {
        ...state,
        loading: true
      }
    case UPDATE_AGENT_SUCCESS:
      return {
        ...state,
        agent: null,
        loading: false
      }
    case UPDATE_AGENT_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case UPDATE_AGENT_STATUS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case UPDATE_AGENT_STATUS_SUCCESS:
      return {
        ...state,
        agent: null,
        loading: false
      }
    case UPDATE_AGENT_STATUS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_CRAWLER_ACCOUNTS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_CRAWLER_ACCOUNTS_SUCCESS:
      return {
        ...state,
        crawlerAccounts: action.data,
        loading: false
      }
    case GET_CRAWLER_ACCOUNTS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_TELEGRAM_GROUPS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_TELEGRAM_GROUPS_SUCCESS:
      return {
        ...state,
        telegramGroups: action.data,
        loading: false
      }
    case GET_TELEGRAM_GROUPS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_CONFIG_NOTIFIES_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_CONFIG_NOTIFIES_SUCCESS:
      return {
        ...state,
        configNotifies: action.data,
        loading: false
      }
    case GET_CONFIG_NOTIFIES_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    default:
      return { ...state }
  }
}
export default agents
