import instances from "../../../../@core/plugin/axios"

export const GET_DATA_BEGIN = 'GET_DATA_BEGIN'
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS'
export const GET_DATA_FAILURE = 'GET_DATA_FAILURE'

export const GET_AGENT_BY_ID_BEGIN = 'GET_AGENT_BY_ID_BEGIN'
export const GET_AGENT_BY_ID_SUCCESS = 'GET_AGENT_BY_ID_SUCCESS'
export const GET_AGENT_BY_ID_FAIL = 'GET_AGENT_BY_ID_FAIL'

export const GET_CHILD_AGENTS_BEGIN = 'GET_CHILD_AGENTS_BEGIN'
export const GET_CHILD_AGENTS_SUCCESS = 'GET_CHILD_AGENTS_SUCCESS'
export const GET_CHILD_AGENTS_FAILURE = 'GET_CHILD_AGENTS_FAILURE'

export const UPDATE_AGENT_BEGIN = 'UPDATE_AGENT_BEGIN'
export const UPDATE_AGENT_SUCCESS = 'UPDATE_AGENT_SUCCESS'
export const UPDATE_AGENT_FAIL = 'UPDATE_AGENT_FAIL'

export const GET_AGENT_WIN_LOSS_BEGIN = 'GET_AGENT_WIN_LOSS_BEGIN'
export const GET_AGENT_WIN_LOSS_SUCCESS = 'GET_AGENT_WIN_LOSS_SUCCESS'
export const GET_AGENT_WIN_LOSS_FAIL  = 'GET_AGENT_WIN_LOSS_FAIL'

export const GET_AGENT_OUTSTANDING_BEGIN = 'GET_AGENT_OUTSTANDING_BEGIN'
export const GET_AGENT_OUTSTANDING_SUCCESS = 'GET_AGENT_OUTSTANDING_SUCCESS'
export const GET_AGENT_OUTSTANDING_FAIL  = 'GET_AGENT_OUTSTANDING_FAIL'

export const GET_CRAWLER_ACCOUNTS_BEGIN = 'GET_CRAWLER_ACCOUNTS_BEGIN'
export const GET_CRAWLER_ACCOUNTS_SUCCESS = 'GET_CRAWLER_ACCOUNTS_SUCCESS'
export const GET_CRAWLER_ACCOUNTS_FAIL  = 'GET_CRAWLER_ACCOUNTS_FAIL'

export const GET_CONFIG_NOTIFIES_BEGIN = 'GET_CONFIG_NOTIFIES_BEGIN'
export const GET_CONFIG_NOTIFIES_SUCCESS = 'GET_CONFIG_NOTIFIES_SUCCESS'
export const GET_CONFIG_NOTIFIES_FAIL  = 'GET_CONFIG_NOTIFIES_FAIL'

export const GET_TELEGRAM_GROUPS_BEGIN = 'GET_TELEGRAM_GROUPS_BEGIN'
export const GET_TELEGRAM_GROUPS_SUCCESS = 'GET_TELEGRAM_GROUPS_SUCCESS'
export const GET_TELEGRAM_GROUPS_FAIL  = 'GET_TELEGRAM_GROUPS_FAIL'

export const UPDATE_AGENT_STATUS_BEGIN = 'UPDATE_AGENT_STATUS_BEGIN'
export const UPDATE_AGENT_STATUS_SUCCESS = 'UPDATE_AGENT_STATUS_SUCCESS'
export const UPDATE_AGENT_STATUS_FAIL = 'UPDATE_AGENT_STATUS_FAIL'

export const getChildAgentsByParentAgent = async (params) => {
    const config = {params: params}
    return await instances.get(`/admin/agent/paging`, config)
}

export const getAgents = (params) => {
    const config = {params: params}

    return async dispatch => {
        dispatch({
            type: GET_DATA_BEGIN
        })
        await instances.get(`/admin/agent/paging`, config)
            .then(response => {
                dispatch({
                    type: GET_DATA_SUCCESS,
                    data: response?.data?.data?.data,
                    totalRecords: response?.data?.data?.totalRecords,
                    params
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_DATA_FAILURE,
                    errorMessage: error
                })
            })
    }
}
export const getAgentsTotal = async (id) => {
    return await instances.get(`/admin/agent/count/${id}`)
}

export const getAgentById = (id) => {
    return async dispatch => {
        dispatch({
            type: GET_AGENT_BY_ID_BEGIN
        })
        await instances.get(`/admin/agent/${id}`)
            .then(response => {
                dispatch({
                    type: GET_AGENT_BY_ID_SUCCESS,
                    data: response?.data?.data
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_AGENT_BY_ID_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const updateAgentWait = (id, data) => {
    return instances.put(`/admin/agent/update/${id}`, data)
}
export const updateAgent = (id, data, isUpdateStatus) => {
    return (dispatch, getState) => {
        dispatch({
            type: UPDATE_AGENT_BEGIN
        })
        instances.put(`/admin/agent/update/${id}`, data)
            .then(() => {
                dispatch({
                    type: UPDATE_AGENT_SUCCESS
                })
            })
            .then(() => {
                if (isUpdateStatus) {
                    dispatch(getAgents(getState().agents.params))
                }
            })
            .catch(error => {
                dispatch({
                    type: UPDATE_AGENT_FAIL,
                    errorMessage: error
                })
            })
    }
}
export const updateStatusAgent = (id, data, isUpdateStatus) => {
    return (dispatch, getState) => {
        dispatch({
            type: UPDATE_AGENT_STATUS_BEGIN
        })
        instances.put(`/admin/agent/update-status/${id}`, data)
            .then(() => {
                dispatch({
                    type: UPDATE_AGENT_STATUS_SUCCESS
                })
            })
            .then(() => {
                if (isUpdateStatus) {
                    dispatch(getAgents(getState().agents.params))
                }
            })
            .catch(error => {
                dispatch({
                    type: UPDATE_AGENT_STATUS_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const getAgentWinLoss = (params) => {
    const config = {params: params}

    return async dispatch => {
        dispatch({
            type: GET_AGENT_WIN_LOSS_BEGIN
        })
        await instances.get(`/admin/agent/win_loss/paging`, config)
            .then(response => {
                dispatch({
                    type: GET_AGENT_WIN_LOSS_SUCCESS,
                    data: response?.data?.data
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_AGENT_WIN_LOSS_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const getAgentOutstanding = (params) => {
    const config = {params: params}

    return async dispatch => {
        dispatch({
            type: GET_AGENT_OUTSTANDING_BEGIN
        })
        await instances.get(`/admin/agent/outstanding/paging`, config)
            .then(response => {
                dispatch({
                    type: GET_AGENT_OUTSTANDING_SUCCESS,
                    data: response?.data?.data
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_AGENT_OUTSTANDING_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const getCrawlerAccounts = () => {
    return async dispatch => {
        dispatch({
            type: GET_CRAWLER_ACCOUNTS_BEGIN
        })
        await instances.get(`/admin/crawler-account/all`)
            .then(response => {
                dispatch({
                    type: GET_CRAWLER_ACCOUNTS_SUCCESS,
                    data: response?.data?.data?.accounts
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_CRAWLER_ACCOUNTS_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const getConfigNotifies = () => {
    return async dispatch => {
        dispatch({
            type: GET_CONFIG_NOTIFIES_BEGIN
        })
        await instances.get(`/admin/config-notify/all`)
            .then(response => {
                dispatch({
                    type: GET_CONFIG_NOTIFIES_SUCCESS,
                    data: response?.data?.data?.configNotifies
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_CONFIG_NOTIFIES_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const getTelegramGroups = () => {
    return async dispatch => {
        dispatch({
            type: GET_TELEGRAM_GROUPS_BEGIN
        })
        await instances.get(`/admin/telegram-group/all`)
            .then(response => {
                dispatch({
                    type: GET_TELEGRAM_GROUPS_SUCCESS,
                    data: response?.data?.data
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_TELEGRAM_GROUPS_FAIL,
                    errorMessage: error
                })
            })
    }
}
