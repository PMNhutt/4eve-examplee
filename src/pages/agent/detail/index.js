// ** React Imports
import React, {useEffect, Fragment, useState} from 'react'
import {useHistory, useParams} from "react-router-dom"
import CommonHeaderFooter from "../../../components/CommonFooter/CommonHeaderFooter"
import {Loading} from "../../../components/Loading"
import AgentInformation from "../components/AgentInformation"
import { useDispatch, useSelector } from 'react-redux'
import {checkPermissionByScreen} from "../../users/profile/store/action"
import {PERMISSION_SCREEN_ENUM} from "../../../enum/permission-screens-constant"
import AgentTable from "../components/AgentTable"
import AgentTelegramGroups from "../components/AgentTelegramGroups"

import {agentWinLossColumns, agentOutstandingColumns} from '../components/columns'

// ** Store & Actions
import {getAgentById, getAgentOutstanding, getAgentWinLoss, getCrawlerAccounts, getConfigNotifies, getTelegramGroups} from "../store/action"

import '@styles/base/pages/app-ecommerce-details.scss'
import './style/style.scss'

const AgentDetail = () => {
    // ** Store Vars
    const dispatch = useDispatch()
    const store = useSelector(state => state.agents)
    const history = useHistory()

    // ** Vars
    const id = useParams().id
    const rowsPerPage = 5

    const [currentPageOutstanding, setCurrentPageOutstanding] = useState(1)
    const [currentPageWinLoss, setCurrentPageWinLoss] = useState(1)
    const [checkPermission, setCheckPermission] = useState(null)

    useEffect(() => {
        checkPermissionByScreen(PERMISSION_SCREEN_ENUM.agent).then(res => {
            setCheckPermission(res)
            if (res?.view) {
                if (id) {
                    dispatch(getAgentById(id))
                    dispatch(getAgentWinLoss({page: currentPageWinLoss, perPage: rowsPerPage, agent_id: id}))
                    dispatch(getAgentOutstanding({page: currentPageOutstanding, perPage: rowsPerPage, agent_id: id}))
                    dispatch(getCrawlerAccounts())
                    dispatch(getTelegramGroups())
                    dispatch(getConfigNotifies())
                }
            }
        })
    }, [])

    function handleSubmitButton() {
        history.push(`/agent/edit/${id}`)
    }

    function handleChangePageWinLoss(page) {
        dispatch(getAgentWinLoss({page: page, perPage: rowsPerPage, agent_id: id}))
        setCurrentPageWinLoss(page)
    }

    function handleChangePageOutstanding(page) {
        dispatch(getAgentOutstanding({page: page, perPage: rowsPerPage, agent_id: id}))
        setCurrentPageOutstanding(page)
    }

    return (
        <Fragment>
            {
                store?.loading && <Loading />
            }
            <div className='app-ecommerce-details container-fluid' id={'notificationDetail'}>
                <CommonHeaderFooter breadcrumb={{list_link: localStorage.getItem('agent'), list_title: 'Đại lý', detail_title: store?.agent?.member}}
                                    textButtonRight= {'Chỉnh sửa'}
                                    hideButtonSave={!checkPermission?.edit}
                                    handleSubmitButton={handleSubmitButton}
                                    header={true}/>
                <AgentInformation agent={store?.agent} crawlerAccounts={store?.crawlerAccounts}/>
                <AgentTelegramGroups agent={store?.agent} configNotifies={store?.configNotifies} telegramGroups={store?.telegramGroups}/>
                <AgentTable data={store?.agentWinLoss} perPage={rowsPerPage} handleChangePage={handleChangePageWinLoss} title={'Danh sách win/loss'} columns={agentWinLossColumns('')}/>
                {
                    store?.agent?.crawler_account?.domain?.name !== 'LVS789' &&
                    <AgentTable data={store?.agentOutstanding} perPage={rowsPerPage} handleChangePage={handleChangePageOutstanding} title={'Danh sách outstanding'} columns={agentOutstandingColumns('')}/>
                }
                <CommonHeaderFooter handleSubmitButton={handleSubmitButton}
                                    backUrl={localStorage.getItem('agent')}
                                    textButtonRight= {'Chỉnh sửa'}
                                    hideButtonSave={!checkPermission?.edit}
                                    dispatch={dispatch}
                />
            </div>
        </Fragment>
    )
}

export default AgentDetail
