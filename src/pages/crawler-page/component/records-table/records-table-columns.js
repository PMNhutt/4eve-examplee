// ** React Imports
import {Link} from 'react-router-dom'

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge, Button} from 'reactstrap'
import {MoreVertical, Archive, Settings} from 'react-feather'
import React from 'react'

export const recordsTableColumns = [
    {
        name: 'Tên domain',
        minWidth: '300px',
        sortable: true,
        cell: row => row?.domain?.domain
    },
    {
        name: 'Tên tài khoản',
        minWidth: '300px',
        sortable: true,
        cell: row => row?.crawlerAccount?.userName
    },
    {
        name: 'Ngày chạy',
        minWidth: '150px',
        sortable: true,
        cell: row => row?.crawlerDate
    },
    {
        name: 'Kết quả',
        minWidth: '150px',
        center: 'true',
        sortable: true,
        cell: row => row?.status ? "Thành công" : "Thất bại"
    },
    {
        minWidth: row => `${row.packageName === 'Agbong88DotCom' ? '50px' : '200px'}`,
        center: 'true',
        sortable: true,
        cell: row => <UncontrolledDropdown>
            <DropdownToggle tag='div' className='btn btn-sm'>
                <MoreVertical size={14} className='cursor-pointer' />
            </DropdownToggle>
            <DropdownMenu right>
                {
                    row?.domain?.packageName === 'Agbong88DotCom' &&
                    <DropdownItem
                        className='w-100'
                        tag={Link}
                        to={`/agent-win-loss?pageId=${row.id}`}>
                        <Archive size={14} className='mr-50' />
                        <span className='align-middle'>Win
                    Loss</span>
                    </DropdownItem>
                }
                {
                    row?.domain?.packageName === 'Agbong88DotCom' &&
                    <DropdownItem
                        className='w-100'
                        tag={Link} to={`/agent-win-loss-detail?pageId=${row.id}`}>
                        <Archive size={14} className='mr-50' />
                        <span className='align-middle'>Win Loss
                    Detail</span>
                    </DropdownItem>
                }
                {
                    row?.domain?.packageName === 'AgentDotChokdeesudsudDotcom' &&
                    <DropdownItem
                        className='w-100'
                        tag={Link} to={`/agent-win-loss-detail?pageId=${row.id}`}>
                        <Archive size={14} className='mr-50' />
                        <span className='align-middle'>Top 10 player
                    win/lose</span>
                    </DropdownItem>

                }
            </DropdownMenu>
        </UncontrolledDropdown>
    }
]
