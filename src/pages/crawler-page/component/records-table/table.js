import {ChevronDown} from "react-feather"
import {NoDataComponent} from "../../../../components/NoDataComponent"
import DataTable from "react-data-table-component"
import ReactPaginate from "react-paginate"
import React, {useEffect, useState} from "react"
import {fetchActionCrawler} from "../../store/action"
import {useDispatch, useSelector} from "react-redux"

const RecordsTable = ({data, columns}) => {
    const rowsPerPage = 10
    const [currentPage, setCurrentPage] = useState(1)
    const store = useSelector(state => state.crawlerPage)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchActionCrawler({
            size: rowsPerPage,
            page: currentPage,
            domain: data.domain
        }))
    }, [dispatch])
    const subTableDataRender = (id) => {
        const renderData = []
        if (store.crawler_records?.length > 0) {
            for (const record of store.crawler_records) {
                if (record.domain.id === id) {
                    renderData.push(record)
                }
            }
            return {renderData, total: renderData.length}
        } else if (store.crawler_records?.length === 0) {
            return []
        }
    }
    const dataToRender = subTableDataRender(data.id)
    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.domain) {
            params.domain = filterParams.domain
        }
        params.size = filterParams.size
        params.page = filterParams.page
    }
    const handlePagination = page => {
        checkParams({page: page.selected + 1, size: rowsPerPage})
        setCurrentPage(page.selected + 1)
    }
    const CustomPagination = (total, rowsPerPage) => {
        const count = Number(Math.ceil(total / rowsPerPage))
        return (
            <ReactPaginate
                previousLabel={''}
                nextLabel={''}
                pageCount={count || 1}
                activeClassName='active'
                forcePage={currentPage !== 0 ? currentPage - 1 : 0}
                onPageChange={page => handlePagination(page)}
                pageClassName={'page-item'}
                nextLinkClassName={'page-link'}
                nextClassName={'page-item next'}
                previousClassName={'page-item prev'}
                previousLinkClassName={'page-link'}
                pageLinkClassName={'page-link'}
                containerClassName={'pagination react-paginate justify-content-end my-2 pr-1'}
            />
        )
    }
    const customeStyle = {
        rows : {
            style: {
                backgroundColor: '#DFD8D6'
            }
        }
    }
    return (
        <DataTable
            noHeader
            noTableHead
            responsive
            persistTableHead
            paginationServer
            columns={columns}
            sortIcon={<ChevronDown/>}
            className='react-dataTable px-0'
            data={dataToRender?.renderData}
            noDataComponent={<NoDataComponent message={'Hiện tại chưa có dữ liệu'}/>}
            paginationComponent={CustomPagination(dataToRender?.total, rowsPerPage)}
            customStyles={customeStyle}
        />
    )
}
export default RecordsTable