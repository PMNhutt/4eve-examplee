// ** Initial State
import {GET_CRAWLER_BEGIN, GET_CRAWLERS_FAIL, GET_CRAWLER_RECORDS_FAIL, GET_CRAWLER_RECORDS_SUCCESS, GET_CRAWLERS_SUCCESS} from "../action"

const initialState = {
  loading: false,
  crawlers: [],
  crawler_records: [],
  errorMessage: null
}

const crawlerPage = (state = initialState, action) => {
  switch (action.type) {
    case GET_CRAWLER_BEGIN:
      return {
        ...state,
        loading: true
      }
    case GET_CRAWLER_RECORDS_SUCCESS:
      return {
        ...state,
        crawler_records: action.data.content,
        totalPages: action?.data.totalPages,
        totalRecords: action?.data.totalElements,
        loading: false,
        params: action?.params
      }
    case GET_CRAWLERS_SUCCESS:
      return {
        ...state,
        crawlers: action.data,
        loading: false
      }
    case GET_CRAWLER_RECORDS_FAIL:
      return {
        ...state,
        loading: false,
        error: true,
        params: action?.params
      }
      case GET_CRAWLERS_FAIL:
      return {
        ...state,
        loading: false,
        error: true
      }
    default:
      return { ...state }
  }
}
export default crawlerPage
