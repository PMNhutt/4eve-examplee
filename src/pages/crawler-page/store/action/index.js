import instances from "../../../../@core/plugin/axios"
export const GET_CRAWLER_BEGIN = 'GET_CRAWLER_BEGIN'
export const GET_CRAWLERS_SUCCESS = 'GET_CRAWLER_SUCCESS'
export const GET_CRAWLERS_FAIL = 'GET_CRAWLERS_FAIL'
export const  GET_CRAWLER_RECORDS_SUCCESS = 'GET_CRAWLER_RECORDS_SUCCESS'
export const  GET_CRAWLER_RECORDS_FAIL = 'GET_CRAWLER_RECORDS_FAIL'
export const fetchActionCrawler = (params)  => {
    const config = {params: params}
    return async dispatch => {
        dispatch({
            type: GET_CRAWLER_BEGIN
        })
        await instances.get('/crawler/list', config)
            .then(response => {
                dispatch({
                    type: GET_CRAWLER_RECORDS_SUCCESS,
                    data: response.data,
                    params: params
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_CRAWLERS_FAIL,
                    payload: { error },
                    params: params
                })
                return error
            })
    }
}

export const fetchCrawlerPage = () => {
    return async dispatch => {
        dispatch({
            type: GET_CRAWLER_BEGIN
        })
        await instances.get('/domain/all')
            .then(response => {
                dispatch({
                    type: GET_CRAWLERS_SUCCESS,
                    data: response.data
                })
            })
            .catch(error => {
                dispatch({
                    type: GET_CRAWLERS_FAIL,
                    payload: { error }
                })
                return error
            })
    }
}