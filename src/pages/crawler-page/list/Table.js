// ** React Imports
import React, {Fragment, useState, useEffect} from 'react'

// ** Columns
import {columns} from './columns'

// ** Store & Actions
import {useDispatch, useSelector} from 'react-redux'

// ** Third Party Components
import ReactPaginate from 'react-paginate'
import {ChevronDown, Search} from 'react-feather'
import DataTable from 'react-data-table-component'
import {
    Card,
    Input,
    Row,
    Col,
    Button,
    InputGroup,
    InputGroupAddon,
    InputGroupText
} from 'reactstrap'
import {NoDataComponent} from "../../../components/NoDataComponent"

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import '../styles/user.scss'
import {useHistory, useLocation} from 'react-router-dom'
import {Loading} from "../../../components/Loading"
import {fetchActionCrawler, fetchCrawlerPage} from "../store/action"
import RecordsTable from "../component/records-table/table"
import {recordsTableColumns} from "../component/records-table/records-table-columns"

// ** Table Header
const CustomHeader = ({handleFilter, searchTerm, handleRefreshData, removeValue}) => {
    return (
        <div className='invoice-list-table-header w-100 mt-2 mb-75'>
            <Row className={'w-100 justify-content-between mx-0'}>
                <Col lg={5} sm={12} className='d-flex align-items-center p-0'>
                    <InputGroup className='ml-sm-0 input-group-merge product-search'>
                        <Input placeholder='Tìm kiếm theo tên domain' value={searchTerm}
                               onChange={e => handleFilter(e.target.value)}/>
                        <InputGroupAddon addonType='append'>
                            <InputGroupText>
                                <Search size={14}/>
                            </InputGroupText>
                        </InputGroupAddon>
                    </InputGroup>
                    <Button.Ripple color='primary' className={'ml-2 w-50'} onClick={() => {
                        removeValue()
                        handleRefreshData()
                    }}>Làm mới</Button.Ripple>
                </Col>
            </Row>
        </div>
    )
}

const CrawlerPageList = () => {
    // ** Store Vars
    const dispatch = useDispatch()
    const store = useSelector(state => state.crawlerPage)
    const history = useHistory()
    const location = useLocation()
    const searchParams = new URLSearchParams(location.search)

    // ** States
    const [searchTerm, setSearchTerm] = useState(searchParams?.get('search') ? searchParams?.get('search') : '')
    const [sidebarOpen, setSidebarOpen] = useState(false)

    // ** Function to toggle sidebar
    const toggleSidebar = () => setSidebarOpen(!sidebarOpen)

    // ** Get data on mount
    useEffect(() => {
        dispatch(fetchCrawlerPage())
    }, [dispatch])
    const handleRefreshData = () => {
        dispatch(fetchCrawlerPage())
    }
    const checkParams = (filterParams) => {
        const params = {}
        if (filterParams.domain) {
            params.domain = filterParams.domain
        }
        const urlSearchParams = new URLSearchParams(params)
        history.replace({pathname: location.pathname, search: urlSearchParams.toString()})
    }

    // ** Function in get data on search query change
    const handleFilter = val => {
        checkParams({domain: val})
        setSearchTerm(val)
    }

    // ** Table data to render
    const dataToRender = () => {
        if (store.crawlers?.length > 0) {
            return store.crawlers.filter(item => item.domain.includes(searchTerm))
        } else if (store.crawlers?.length === 0) {
            return []
        }
    }

    const removeValue = () => {
        setSearchTerm('')
        history.replace({pathname: location.pathname, search: ''})
    }
    return (
        <Fragment>
            {
                store.loading &&
                <Loading/>
            }
            <Card id={'userContainer'}>
                <div className={'user'}>
                    <CustomHeader
                        toggleSidebar={toggleSidebar}
                        handleFilter={handleFilter}
                        searchTerm={searchTerm}
                        removeValue={removeValue}
                        handleRefreshData={handleRefreshData}
                    />
                </div>
                <DataTable
                    noHeader
                    responsive
                    persistTableHead
                    paginationServer
                    expandableRows
                    expandOnRowClicked
                    expandableRowsComponent={<RecordsTable columns={recordsTableColumns}/>}
                    columns={columns}
                    sortIcon={<ChevronDown/>}
                    className='react-dataTable px-0'
                    data={dataToRender()}
                    noDataComponent={<NoDataComponent message={'Hiện tại chưa có dữ liệu'}/>}
                />
                <div>
                </div>
            </Card>

        </Fragment>
    )
}

export default CrawlerPageList
