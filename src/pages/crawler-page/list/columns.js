// ** React Imports
import {Link} from 'react-router-dom'

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge, Button} from 'reactstrap'
import {MoreVertical, Archive, Settings} from 'react-feather'
import React from 'react'
import {convertDateTime} from "../../../utility/ConvertDate"

export const columns = [
    {
        name: 'Tên domain',
        minWidth: '400px',
        sortable: true,
        cell: row => row?.domain
    },
    {
        name: 'Ngày tạo',
        minWidth: '150px',
        sortable: true,
        cell: row => convertDateTime(row?.createdDate)
    },
    {
        name: 'Sửa đổi lần cuối',
        minWidth: '150px',
        sortable: true,
        cell: row => convertDateTime(row?.createdDate)
    },
    {
        name: 'Trạng thái',
        minWidth: '150px',
        sortable: true,
        cell: row => row?.active ? "Đang hoạt động" : "Ngưng hoạt động"
    }
]
