import instances from "../../../../@core/plugin/axios"

export const GET_TELEGRAM_TOKENS_BEGIN = 'GET_TELEGRAM_TOKENS_BEGIN'
export const GET_TELEGRAM_TOKENS_SUCCESS = 'GET_TELEGRAM_TOKENS_SUCCESS'
export const GET_TELEGRAM_TOKENS_FAIL = 'GET_TELEGRAM_TOKENS_FAIL'

export const GET_TELEGRAM_GROUP_BY_ID_BEGIN = 'GET_TELEGRAM_GROUP_BY_ID_BEGIN'
export const GET_TELEGRAM_GROUP_BY_ID_SUCCESS = 'GET_TELEGRAM_GROUP_BY_ID_SUCCESS'
export const GET_TELEGRAM_GROUP_BY_ID_FAIL = 'GET_TELEGRAM_GROUP_BY_ID_FAIL'

export const UPDATE_TELEGRAM_TOKEN_BEGIN = 'UPDATE_TELEGRAM_TOKEN_BEGIN'
export const UPDATE_TELEGRAM_TOKEN_SUCCESS = 'UPDATE_TELEGRAM_TOKEN_SUCCESS'
export const UPDATE_TELEGRAM_TOKEN_FAIL = 'UPDATE_TELEGRAM_TOKEN_FAIL'

export const SYNC_TELEGRAM_GROUP_BY_TOKEN_BEGIN = 'SYNC_TELEGRAM_GROUP_BY_TOKEN_BEGIN'
export const SYNC_TELEGRAM_GROUP_BY_TOKEN_SUCCESS = 'SYNC_TELEGRAM_GROUP_BY_TOKEN_SUCCESS'
export const SYNC_TELEGRAM_GROUP_BY_TOKEN_FAIL = 'SYNC_TELEGRAM_GROUP_BY_TOKEN_FAIL'

export const getTelegramTokens = (params) => {
    const config = {params: params}
    return async dispatch => {
        dispatch({
            type: GET_TELEGRAM_TOKENS_BEGIN
        })
        await instances.get('/admin/telegram', config).then(response => {
            dispatch({
                type: GET_TELEGRAM_TOKENS_SUCCESS,
                telegramTokens: response.data?.data?.data,
                totalRecords: response.data?.data?.totalRecords,
                params
            })
        }).catch(error => {
            dispatch({
                type: GET_TELEGRAM_TOKENS_FAIL,
                errorMessage: error
            })
        })
    }
}

export const createTelegramToken = async (data) => {
    return await instances.post('/admin/telegram', data)
}

export const updateTelegramToken = (id, data) => {
    return async (dispatch, getState) => {
        dispatch({
            type: UPDATE_TELEGRAM_TOKEN_BEGIN
        })
        await instances.put(`/admin/telegram/${id}`, data)
            .then(() => {
                dispatch({
                    type: UPDATE_TELEGRAM_TOKEN_SUCCESS
                })
            })
            .then(() => {
                dispatch(getTelegramTokens(getState().telegramToken.params))
            })
            .catch(error => {
                dispatch({
                    type: UPDATE_TELEGRAM_TOKEN_FAIL,
                    errorMessage: error
                })
            })
    }
}

export const syncTelegramGroupByToken = (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: SYNC_TELEGRAM_GROUP_BY_TOKEN_BEGIN
        })
        await instances.put(`/admin/telegram/sync/${id}`)
            .then(() => {
                dispatch({
                    type: SYNC_TELEGRAM_GROUP_BY_TOKEN_SUCCESS
                })
            })
            .then(() => {
                dispatch(getTelegramTokens(getState().telegramToken.params))
            })
            .catch(error => {
                dispatch({
                    type: SYNC_TELEGRAM_GROUP_BY_TOKEN_FAIL,
                    errorMessage: error
                })
            })
    }
}
