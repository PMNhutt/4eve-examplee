import {
  UPDATE_TELEGRAM_TOKEN_BEGIN,
  UPDATE_TELEGRAM_TOKEN_FAIL,
  UPDATE_TELEGRAM_TOKEN_SUCCESS,
  GET_TELEGRAM_GROUP_BY_ID_BEGIN,
  GET_TELEGRAM_GROUP_BY_ID_FAIL,
  GET_TELEGRAM_GROUP_BY_ID_SUCCESS,
  GET_TELEGRAM_TOKENS_BEGIN,
  GET_TELEGRAM_TOKENS_FAIL,
  GET_TELEGRAM_TOKENS_SUCCESS
} from "../action"

// ** Initial State
const initialState = {
  loading: false,
  telegramTokens: [],
  totalRecords: 0,
  totalRecordCompanies: 0,
  telegramGroup: null,
  errorMessage: null,
  companies: [],
  params: {}
}

const telegramToken = (state = initialState, action) => {
  switch (action.type) {
    case GET_TELEGRAM_TOKENS_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_TELEGRAM_TOKENS_SUCCESS:
      return {
        ...state,
        telegramTokens: action.telegramTokens,
        totalRecords: action.totalRecords,
        params: action.params,
        loading: false
      }
    case GET_TELEGRAM_TOKENS_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case GET_TELEGRAM_GROUP_BY_ID_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case GET_TELEGRAM_GROUP_BY_ID_SUCCESS:
      return {
        ...state,
        telegramGroup: action.telegramGroup,
        loading: false
      }
    case GET_TELEGRAM_GROUP_BY_ID_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    case UPDATE_TELEGRAM_TOKEN_BEGIN:
      return {
        ...state,
        loading:  true
      }
    case UPDATE_TELEGRAM_TOKEN_SUCCESS:
      return {
        ...state,
        telegramGroup: null,
        loading: false
      }
    case UPDATE_TELEGRAM_TOKEN_FAIL:
      return {
        errorMessage: action.errorMessage,
        loading: false
      }
    default:
      return { ...state }
  }
}
export default telegramToken
