// ** Store & Actions
import {updateTelegramToken, syncTelegramGroupByToken} from '../store/action'
import { store } from '@store/storeConfig/store'

// ** Third Party Components
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge} from 'reactstrap'
import {MoreVertical, PlayCircle, RefreshCw, StopCircle} from 'react-feather'
import React from 'react'
import {convertDate} from "../../../utility/ConvertDate"

export const columns = [
  {
    name: 'Id',
    width: '100px',
    sortable: true,
    selector: 'id',
    cell: row => row?.id
  },
  {
    name: 'Name',
    minWidth: '150px',
    cell: row => row?.name
  },
  {
    name: 'Token',
    minWidth: '150px',
    cell: row => row?.token_id
  },
  {
    name: 'Status',
    minWidth: '150px',
    center: 'true',
    cell: row => (
        <Badge className='text-capitalize' color={row?.status ? 'light-success' : 'light-warning'} pill>
          {row.status ? 'Active' : 'Inactive'}
        </Badge>
    )
  },
  {
    name: 'CreatedDate',
    minWidth: '150px',
    selector: 'created_at',
    center: 'true',
    sortable: true,
    cell: row => convertDate(row?.created_at)
  },
  {
    name: 'Actions',
    width: '100px',
    cell: row => (
        <UncontrolledDropdown>
          <DropdownToggle tag='div' className='btn btn-sm'>
            <MoreVertical size={14} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem className='w-100'
                          onClick={() => {
                            row.status = !row?.status
                            store.dispatch(updateTelegramToken(row?.id, row))
                          }}>
              {
                row?.status ? <StopCircle size={14} className='mr-50' /> : <PlayCircle size={14} className='mr-50' />
              }
              <span className='align-middle'>{row?.status ? 'Inactive' : 'Active'}</span>
            </DropdownItem>
            <DropdownItem className='w-100'
                          onClick={() => {
                            store.dispatch(syncTelegramGroupByToken(row?.id))
                          }}
            >
              <RefreshCw size={14} className='mr-50' />
              <span className='align-middle'>Synchronize telegram groups</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
]
