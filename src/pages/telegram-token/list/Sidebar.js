// ** React Import
import React from 'react'

// ** Custom Components
import Sidebar from '@components/sidebar'
import "flatpickr/dist/themes/material_blue.css"
// ** Utils
import {isObjEmpty} from '@utils'

// ** Third Party Components
import classnames from 'classnames'
import {useForm} from 'react-hook-form'
import {Button, FormGroup, Form, Input, Label} from 'reactstrap'

// ** Store & Actions
import {createTelegramToken, getTelegramTokens} from '../store/action'
import {toast} from "react-toastify"
import SuccessNotificationToast from "../../../components/Toast/ToastSuccess"
import ErrorNotificationToast from "../../../components/Toast/ToastFail"
import { store } from '@store/storeConfig/store'

const SidebarTelegramToken = ({open, toggleSidebar}) => {
    // ** Vars
    const {register, errors, handleSubmit} = useForm()

    // ** Function to handle form submit
    const onSubmit = async (values) => {
        if (isObjEmpty(errors)) {
            const data = {}
            data.token_id = values.token_id
            data.name = values.name
            createTelegramToken(data).then(res => {
                if (res?.data?.status) {
                    store.dispatch(getTelegramTokens(store.getState().telegramToken.params))
                    toggleSidebar()
                    toast.success(<SuccessNotificationToast/>)
                }
            }).catch(error => {
                toast.error(<ErrorNotificationToast message={error.response?.data?.message}/>)
            })
        }
    }

    return (
        <Sidebar
            size='lg'
            open={open}
            title='Create new token'
            headerClassName='mb-1'
            contentClassName='pt-0 user__side-bar'
            toggleSidebar={toggleSidebar}
        >
            <Form onSubmit={handleSubmit(onSubmit)} autoComplete={'off'}>
                <FormGroup>
                    <Label className={'label'} for='name'>Name <span className='text-danger'>*</span></Label>
                    <Input
                        name='name'
                        id='name'
                        placeholder='Enter name'
                        innerRef={register({required: true})}
                        className={classnames({'is-invalid': errors['name']})}
                    />
                </FormGroup>
                <FormGroup>
                    <Label className={'label'} for='token_id'>Token <span className='text-danger'>*</span></Label>
                    <Input
                        name='token_id'
                        id='token_id'
                        placeholder='Enter token'
                        innerRef={register({required: true})}
                        className={classnames({'is-invalid': errors['token_id']})}
                    />
                </FormGroup>
                <div className={'row mt-2'}>
                    <Button type='submit' className={'ml-1 mr-1'} color='primary'>Save</Button>
                    <Button type='reset' color='secondary' outline onClick={toggleSidebar}>Cancel</Button>
                </div>
            </Form>
        </Sidebar>
    )
}

export default SidebarTelegramToken
